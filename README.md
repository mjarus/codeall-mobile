# Codeall mobile

## Instalacja

1. Konfiguracja klucza SSH z dostępem do repozytorium `bitbucket.org/mjarus/react-native-smartbox.git`
1. npm install

Rozwiązadanie błędu z brakiem dostępu do repozytorium (windows): https://stackoverflow.com/a/63173335/5939565  
Testowanie dostępu: `ssh -T git@bitbucket.org`
```
authenticated via a deploy key.
                    
You can use git or hg to connect to Bitbucket. Shell access is disabled.

You need to ask about adding your ssh key to the following repository:
mjarus/react-native-smartbox
```
                    
## Rozwiązywanie problemów

W przypadku problemów z uruchomieniem apki
1. `npx react-native start --reset-cache`
1. Usunięcie `package-lock.json`, `node-modules`, `android\app\build` i ponowne zainstalowanie zależności i uruchomienie projektu

## Environment switching

Do zmiany środowisk używamy react-native-dotenv (https://github.com/goatandsheep/react-native-dotenv).

W plikach .env.production oraz .env.development. znajdują się zmienne środowiskowe. Aby lokalnie uruchomić aplikacje w wersji produkcyjnej, należy użyć komendy
'react-native run-android --variant=release'. Wersję debugową uruchamiamy tak samo, bez opcji --variant.

Aby wygenerowac apk produkcyjne lub debugowe, należy użyć komendy './gradlew assembleRelease' lub './gradlew assembleDebug'
wewnątrz folderu /android.

Aktualnie zmienne środowiskowe są używane do wybierania odpowiedniego adresu API.

## Połączenie ze SmartBoxem

Dane w formacie JSON przesyłane ze SmartBoxa oznaczone są na początku bajtem 0x02, a na końcu 0x04.
