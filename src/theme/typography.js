import colors from './colors.js';

const sizes = {
  xxxs: 9,
  xxs: 11,
  xxss: 12,
  xs: 13,
  xss: 14,
  s: 15,
  ssm: 16,
  sm: 17,
  smm: 20,
  m: 22,
  xmm: 24,
  xm: 27,
  x: 30,
  xl: 32,
};

const fonts = {
  primary: 'Lato-Regular',
  secondary: 'Quicksand-Bold',
  secondaryRegular: 'Quicksand-Regular',
};

const inputLabel = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xss,
  color: colors.textSecondary,
  marginBottom: 15,
};

const smallTurquiseButton = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xxs,
};
const paymentText = {
  fontFamily: fonts.primary,
  fontSize: sizes.xxs,
  color: colors.paymentText,
};
const paymentTitle = {
  fontFamily: fonts.primary,
  fontWeight: 'bold',
  fontSize: fonts.ssm,
  color: colors.paymentText,
};
const turquoiseHeader = {
  fontFamily: fonts.secondary,
  fontSize: sizes.m,
  color: colors.turquoiseHeader,
};

const sensorsListName = {
  fontFamily: fonts.secondary,
  color: colors.sensorTitle,
  fontSize: sizes.xxss,
};

const sensorsListText = {
  fontFamily: fonts.primary,
  color: colors.sensorTitle,
  fontSize: sizes.xxxs,
};

const turquoiseLink = {
  fontFamily: fonts.primary,
  fontSize: sizes.xxxs,
  lineHeight: 14,
  marginTop: 3,
  marginBottom: 3,
  textDecorationLine: 'underline',
  color: colors.hyperlink,
};
const sensorTitle = {
  marginBottom: 8,
  fontFamily: fonts.secondary,
  fontSize: sizes.ssm,
  lineHeight: 20,
  color: colors.sensorTitle,
};
const sensorPrice = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xxss,
  lineHeight: 14,
  color: colors.sensorTitle,
};
const middleHeader = {
  fontSize: sizes.m,
  fontFamily: fonts.headers,
  letterSpacing: 0.2,
  lineHeight: 30,
  color: colors.header,
};

const text = {
  fontSize: sizes.s,
  fontFamily: fonts.primary,
  letterSpacing: 0.35,
  lineHeight: 20,
  color: colors.text,
  textAlign: 'center',
};

const popupText = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xs,
  color: colors.popupText,
  textAlign: 'center',
  letterSpacing: 0.1,
  lineHeight: 15,
  marginTop: 5,
  marginBottom: 10,
};

const highlitedHeader = {
  fontSize: sizes.xs,
  fontFamily: fonts.headers,
  textTransform: 'uppercase',
  color: colors.highlitedHeader,
};

const buttonsText = {
  textAlign: 'center',
  width: '80%',
  fontSize: sizes.sm,
  color: colors.buttonDefaultText,
  fontFamily: fonts.secondary,
};

const topicHeader = {
  fontFamily: fonts.primary,
  fontSize: sizes.xs,
  letterSpacing: 0.3,
  color: colors.topicHeader,
  textAlign: 'center',
};

const topicName = {
  fontFamily: fonts.primary,
  fontWeight: 'bold',
  fontSize: sizes.xss,
  lineHeight: 17,
  letterSpacing: 0.12,
  color: colors.topicText,
};

const lessonsLength = {
  alignItems: 'center',
  justifyContent: 'center',
  width: 86,
  height: 26,
  backgroundColor: 'black',
  opacity: 0.25,
  borderRadius: 15,
  position: 'absolute',
  bottom: 0,
  left: 7,
  marginLeft: 6.5,
  marginBottom: 10,
  zIndex: 0,
};

const lessonsLengthText = {
  textAlign: 'center',
  fontFamily: fonts.secondary,
  fontSize: sizes.s,
  color: colors.topicName,
  opacity: 1,
};

const percent = {
  fontSize: sizes.xs,
  fontFamily: fonts.primary,
  marginLeft: 5,
};

const startHeader = {
  fontSize: sizes.x,
  color: colors.welcomeHeader,
  lineHeight: 36,
  textAlign: 'left',
};

const welcomeHeader = {
  ...startHeader,
  fontFamily: fonts.secondaryRegular,
};

const userProfileHeader = {
  ...startHeader,
  fontFamily: fonts.primary,
};

const welcomeHeaderName = {
  ...startHeader,
  fontFamily: fonts.secondary,
};

const lessonsHeader = {
  color: colors.fauxCetacean,
  fontSize: sizes.xmm,
  fontFamily: fonts.secondary,
  textAlign: 'center',
};

const profileName = {
  fontFamily: fonts.secondary,
  fontSize: sizes.m,
  lineHeight: 30,
  letterSpacing: 0.2,
  color: colors.textSecondary,
  marginTop: 14,
  marginBottom: 33,
};

const profileTitle = {
  fontFamily: fonts.secondary,
  marginLeft: 10,
  fontSize: sizes.s,
  lineHeight: 19,
  color: colors.profileLinks,
  textDecorationLine: 'underline',
};

const userSettingsInputTitle = {
  fontFamily: fonts.primary,
  textAlign: 'left',
  marginTop: 18,
  fontSize: sizes.xs,
  lineHeight: 20,
  letterSpacing: 0.3,
  color: colors.profileSettings,
};

const userSettingsTitle = {
  fontFamily: fonts.secondary,
  fontSize: sizes.m,
  textAlign: 'left',
  lineHeight: 30,
  letterSpacing: 0.2,
  width: '100%',
  marginTop: 41,
};

const userInviteDescriptionCode = {
  fontFamily: fonts.primary,
  fontSize: sizes.sm,
  letterSpacing: 0.3,
  color: colors.userInviteDescription,
};

const userInviteCode = {
  fontFamily: fonts.secondary,
  fontSize: sizes.sm,
  letterSpacing: 4,
  color: colors.userInviteGeneralText,
};
const userInviteHeader = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xm,
  letterSpacing: 0.3,
  textAlign: 'center',
  color: colors.userInviteGeneralText,
};

const userInviteShare = {
  fontFamily: fonts.secondary,
  fontSize: sizes.s,
  letterSpacing: 0.3,
  textAlign: 'center',
  color: colors.userInviteGeneralText,
};

const userInviteStatisticText = {
  fontFamily: fonts.primary,
  fontSize: sizes.xs,
  letterSpacing: 0.3,
  textAlign: 'center',
  color: colors.userInviteGeneralText,
};

const userInviteDescriptionText = {
  fontFamily: fonts.primary,
  fontSize: sizes.s,
  textAlign: 'center',
  width: '80%',
  marginTop: 40,
  color: colors.userInviteDescription,
};

const userInviteDescriptionTextLine2 = {
  ...userInviteDescriptionText,
  marginTop: 10,
};
const profileLogoutText = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xs,
  lineHeight: 16,
  letterSpacing: 0.3,
  color: colors.profileLogoutText,
  marginLeft: 10,
  marginBottom: 71,
};
const invitationText = {
  fontFamily: fonts.secondary,
  fontSize: sizes.s,
  lineHeight: 18,
  letterSpacing: 0.11,
  color: colors.invitationText,
};
const sensorName = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xs,
  lineHeight: 16,
  letterSpacing: 0.2,
  color: colors.sensorName,
  marginLeft: 5,
  marginRight: 5,
  marginBottom: 5,
  marginTop: 5,
};

const sensorDataText = {
  fontSize: sizes.xxs,
  lineHeight: 16,
  letterSpacing: 0.3,
  textAlign: 'center',
  bottom: 5,
};
const sensorTitleValue = {
  ...sensorDataText,
  fontFamily: fonts.primary,
  color: colors.textPrimary,
  fontWeight: 'bold',
  textAlign: 'left',
};

const sensorTextAvailable = {
  ...sensorDataText,
  fontFamily: fonts.primary,
  color: colors.sensorTextAvailable,
  marginLeft: 5,
};

const sensorTouchableText = {
  ...sensorDataText,
  fontFamily: fonts.secondary,
  color: colors.sensorTouchableText,
  textDecorationLine: 'underline',
  textDecorationStyle: 'solid',
  marginLeft: 5,
};
const sensorSmartboxConnected = {
  fontFamily: fonts.secondary,
  fontSize: sizes.s,
  lineHeight: 18,
  letterSpacing: 0.2,
  color: colors.sensorSmartboxConnected,
  marginBottom: 5,
  marginTop: 10,
};
const sensorTextInformation = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xs,
  width: '100%',
  textAlign: 'center',
  lineHeight: 15,
  letterSpacing: 0.2,
  color: colors.sensorTextInformation,
  paddingBottom: 5,
};

const smartBoxNotConnected = {
  fontFamily: fonts.primary,
  fontSize: sizes.xss,
  lineHeight: 18,
  letterSpacing: 0.1,
  color: colors.smartBoxNotConnected,
  textAlign: 'center',
};

const smartboxNotConnectedError = {
  color: colors.smartBoxNotConnectedError,
  fontSize: sizes.s,
  fontFamily: fonts.primary,
  fontWeight: 'bold',
  textAlign: 'center',
  width: '100%',
  marginBottom: 10,
};

const smartBoxNotConnectedShop = {
  fontFamily: fonts.primary,
  fontSize: sizes.xs,
  lineHeight: 16,
  letterSpacing: 0.1,
  textAlign: 'center',
  color: colors.smartBoxNotConnectedShop,
  margin: 5,
};
const sensorTextLabel = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xss,
  lineHeight: 14,
  letterSpacing: 0.1,
  textAlign: 'center',
  color: colors.sensorTextLabel,
  marginTop: 10,
  marginBottom: 10,
};
const headerWhite = {
  fontFamily: fonts.secondary,
  fontSize: sizes.m,
  lineHeight: 27.5,
  letterSpacing: 0.2,
  textAlign: 'center',
  color: colors.textSecondary,
};

const sensorDetailsName = {
  fontFamily: fonts.secondary,
  color: colors.header,
  lineHeight: 20,
  letterSpacing: 0.25,
  fontSize: sizes.ssm,
  alignSelf: 'flex-start',
};
const sensorborderText = {
  fontFamily: fonts.primary,
  fontSize: sizes.xss,
  borderWidth: 1,
  borderColor: colors.inputBorder,
  color: colors.sensorBorderText,
  width: 243,
  flexDirection: 'row',
  padding: 8,
  lineHeight: 16,
  borderRadius: 4,
};
const inputText = {
  fontFamily: fonts.primary,
  fontSize: sizes.xss,
  color: colors.sensorBorderText,
  lineHeight: 16,
};
const sensorAvailableHeader = {
  fontFamily: fonts.secondary,
  fontSize: sizes.sm,
  lineHeight: 14,
  letterSpacing: 0.2,
  color: colors.sensorAvailableHeader,
};

const sensorTextList = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xss,
  lineHeight: 14,
  letterSpacing: 0.11,
  color: colors.sensorTextList,
  width: '100%',
  textAlign: 'center',
  marginTop: 12,
  marginBottom: 10,
};
const sensorConnectInfo = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xs,
  width: '85%',
  lineHeight: 18,
  letterSpacing: 0.2,
  color: colors.sensorConnectInfo,
  textAlign: 'center',
  padding: 10,
  marginRight: 20,
  marginLeft: 5,
};
const sensorOptionsDots = {
  fontFamily: fonts.primary,
  fontSize: sizes.sm,
  lineHeight: 18,
  letterSpacing: 0.2,
  color: colors.sensorOptionsDots,
  textAlign: 'center',
};
const navTitleText = {
  fontFamily: 'Lato',
  fontStyle: 'normal',
  fontWeight: '600',
  fontSize: sizes.xss,
  letterSpacing: 0.05 * sizes.xss,
  lineHeight: 17,
  width: '60%',
  color: colors.navTabLessons,
  textAlign: 'center',
};
const navSensorTabLabel = (focused) => ({
  fontFamily: fonts.secondary,
  fontSize: sizes.sm,
  lineHeight: 18,
  textTransform: 'none',
  color: focused ? colors.activeTint : colors.textSecondary,
});

const registerLoginHeader = {
  fontFamily: fonts.secondary,
  fontSize: sizes.m,
  letterSpacing: 0.2,
  textAlign: 'left',
  lineHeight: 30,
  color: colors.registerLoginHeader,
  marginBottom: 5,
  width: 242,
};
const subTitle = {
  fontFamily: fonts.primary,
  fontSize: sizes.xs,
  letterSpacing: 0.3,
  lineHeight: 20,
  width: 242,
};
const registerLoginTitle = {
  ...subTitle,
  textAlign: 'left',
  marginBottom: 18,
  color: colors.registerLoginText,
};

const registerLoginSubTitle = {
  ...subTitle,
  textAlign: 'left',
  marginBottom: 4,
  color: colors.registerLoginSubTitle,
};

const registerLoginAlternativesText = {
  ...subTitle,
  marginLeft: 12,
  marginRight: 11,
  color: colors.registerLoginText,
};

const registerLoginText = {
  ...subTitle,
  textAlign: 'left',
  color: colors.registerLoginText,
};

const lessonsNumber = {
  fontFamily: fonts.secondary,
  fontSize: sizes.xl,
};

const registerLoginTurquoiseText = {
  ...subTitle,
  textAlign: 'center',
  color: colors.registerLoginTurquoiseText,
};

const typography = {
  middleHeader,
  text,
  highlitedHeader,
  buttonsText,
  topicHeader,
  topicName,
  percent,
  welcomeHeader,
  welcomeHeaderName,
  popupText,
  userProfileHeader,
  profileName,
  profileTitle,
  userSettingsInputTitle,
  userSettingsTitle,
  profileLogoutText,
  invitationText,
  sensorName,
  sensorDataText,
  sensorTitleValue,
  sensorTextAvailable,
  sensorTouchableText,
  sensorSmartboxConnected,
  sensorTextInformation,
  smartBoxNotConnected,
  smartBoxNotConnectedShop,
  headerWhite,
  sensorTextLabel,
  sensorborderText,
  sensorAvailableHeader,
  sensorTextList,
  sensorConnectInfo,
  navTitleText,
  navSensorTabLabel,
  registerLoginHeader,
  subTitle,
  registerLoginTitle,
  registerLoginSubTitle,
  registerLoginAlternativesText,
  registerLoginText,
  registerLoginTurquoiseText,
  userInviteDescriptionText,
  userInviteCode,
  userInviteHeader,
  userInviteShare,
  userInviteStatisticText,
  userInviteDescriptionCode,
  userInviteDescriptionTextLine2,
  sensorOptionsDots,
  lessonsLength,
  lessonsLengthText,
  sensorsListName,
  fonts,
  sizes,
  lessonsNumber,
  inputLabel,
  smartboxNotConnectedError,
  sensorTitle,
  sensorPrice,
  turquoiseLink,
  sensorDetailsName,
  inputText,
  sensorsListText,
  smallTurquiseButton,
  turquoiseHeader,
  paymentText,
  paymentTitle,
  lessonsHeader,
};

export default typography;
