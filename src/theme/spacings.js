const margins = {
  xxs: {
    marginTop: 8,
  },
  xs: {
    marginTop: 15,
  },
  s: {
    marginTop: 20,
  },
  sl: {
    marginTop: 30,
  },
  l: {
    marginTop: 40,
  },
  xl: {
    marginTop: 50,
  },
};

export default {
  margins,
};
