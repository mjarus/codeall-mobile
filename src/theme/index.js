import {DefaultTheme} from '@react-navigation/native';
import colors from './colors';
import images from './images';
import typography from './typography';
import spacings from './spacings';

const centeredElement = {
  alignItems: 'center',
  justifyContent: 'center',
};

const base = {
  ...centeredElement,
  backgroundColor: colors.background,
  height: '100%',
  width: '100%',
};

export const theme = {
  ...DefaultTheme,
  base,
  images,
  typography,
  spacings,
  centeredElement,
  colors,
};
