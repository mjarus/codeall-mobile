const images = {
  logotype: {
    m: {
      width: 169,
      height: 56,
      resizeMode: 'contain',
    },
    x: {
      width: 400,
      height: 114,
      resizeMode: 'contain',
    },
  },
};

export default images;
