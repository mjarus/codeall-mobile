import React, {useState} from 'react';
import {ScrollView, Image, StyleSheet, View} from 'react-native';
import {connect} from 'react-redux';
import DismissKeyboard from '../../../components/DismissKeyboard';
import { CircleButton } from '../../../components/Buttons';
import {bindActionCreators} from 'redux';
import {userDeviceLogin} from '../../../store/auth/actions';
import RegisterInput from './RegisterInput';
import {useTheme} from '@react-navigation/native';
import typo from '../../../theme/typography';
import TranslatedText from '../../../components/TranslatedText';
import {useTranslation} from 'react-i18next';

const logotype = require('../../../assets/img/codeall_logotype_m.png');
const owl = require('../../../assets/img/register_owl.png');

// TODO @Nowaczyk please refactor...
const correct = 'correct';
const none = 'none';

const RegisterDevice = props => {
  let [nick, setNick] = useState('');
  let [referralCode, setReferralCode] = useState('');
  let [isButtonVisible, setButtonVisibility] = useState(false);
  let [nickInputStatus, validateNickValue] = useState(none);
  let [referralInputStatus] = useState(none);
  const {t} = useTranslation();
  const {images, typography, spacings} = useTheme();

  const handleSubmitPress = () => {
    props.userDeviceLogin({
      nick,
      referralCode,
    });
  };

  const handleNickInput = inputNick => {
    if (inputNick.length >= 1) {
      setButtonVisibility(true);
      validateNickValue(correct);
      setNick(inputNick);
    } else {
      setButtonVisibility(false);
      validateNickValue(none);
    }
  };

  return (
    <DismissKeyboard>
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.sectionContainer}>
          <Image source={logotype} style={images.logotype.m} />
          <TranslatedText
            id="register.setNick"
            style={{
              ...typography.middleHeader,
              ...spacings.margins.xs,
              ...styles.nickHeader,
            }}
          />
          <RegisterInput
            onChangeText={handleNickInput}
            status={nickInputStatus}
            style={spacings.margins.xxs}
            placeholder="Nick"
          />
          <Image source={owl} style={[styles.owl, spacings.margins.xxs]} />
        </View>
        <View style={styles.sectionContainer}>
          <TranslatedText
            id="register.optional"
            style={{
              ...typography.highlitedHeader,
              ...spacings.margins.xxs,
              ...styles.optional,
            }}
          />
          <TranslatedText
            id="register.setRefferal"
            style={{
              ...typography.middleHeader,
              ...spacings.margins.xxs,
              ...styles.reedemCode,
            }}
          />
          <View style={spacings.margins.xxs}>
            <TranslatedText
              id="register.refferalDescription.line1"
              style={typography.text}
            />
            <TranslatedText
              id="register.refferalDescription.line2"
              style={typography.text}
            />
            <TranslatedText
              id="register.refferalDescription.line3"
              style={typography.text}
            />
            <TranslatedText
              id="register.refferalDescription.line4"
              style={typography.text}
            />
          </View>
          <RegisterInput
            onChangeText={setReferralCode}
            status={referralInputStatus}
            codeInput
            style={spacings.margins.sl}
            placeholder={t('register.placeholder.referral')}
          />
          {isButtonVisible && <CircleButton onPress={handleSubmitPress} />}
        </View>
      </ScrollView>
    </DismissKeyboard>
  );
};

const styles = StyleSheet.create({
  owl: {
    width: 145,
    height: 121,
    marginRight: 40,
  },
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  sectionContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  nickHeader: {
    fontFamily: typo.fonts.secondary,
    fontSize: 20,
    alignSelf: 'flex-start',
  },
  reedemCode: {
    fontFamily: typo.fonts.secondary,
    fontSize: 20,
  },
  optional: {
    fontFamily: typo.fonts.secondary,
    marginTop: 25,
  },
});

const mapStateToProps = state => ({
  loginPending: state.auth.isPending,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({userDeviceLogin}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(RegisterDevice);
