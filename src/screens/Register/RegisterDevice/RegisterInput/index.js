import React from 'react';
import {TextInput} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import TranslatedText from '../../../../components/TranslatedText';
import {styles} from './styles';

const RegisterInput = ({
  onChangeText,
  status,
  codeInput,
  style,
  placeholder,
}) => {
  return (
    <>
      {codeInput && status === 'incorrect' && (
        <TranslatedText id="register.code.error" style={styles.error} />
      )}
      <TextInput
        onChangeText={onChangeText}
        autoCapitalize={'none'}
        style={[styles.input(status), style]}
        placeholder={placeholder}
        placeholderTextColor={COLORS.topicProgressBackground}
      />
    </>
  );
};

export default RegisterInput;
