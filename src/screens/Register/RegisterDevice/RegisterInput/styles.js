import COLORS from "CodeallMobile/src/theme/colors";
import {StyleSheet} from 'react-native';

const input = (status) =>({
  width: 242,
  height: 41,
  fontFamily: 'Quicksand-Bold',
  fontSize: 17,
  letterSpacing: 0.15,
  color: COLORS.inputRegisterText,
  marginTop: 7,
  marginBottom: 7,
  paddingLeft: 20,
  borderRadius: 4,
  borderWidth: 1,
  borderColor: status === "incorrect" ? COLORS.error : COLORS.messageInputBorder,
});

const error = {
  width: 242,
  textAlign: 'right',
  fontSize: 12,
  color: COLORS.error,
  fontFamily: 'Lato-Regular',
  marginTop: 10,
};

export const styles = StyleSheet.create({
  input,
  error,
});
