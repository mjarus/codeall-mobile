import React, {Component} from 'react';
import {Text, FlatList, View, Image} from 'react-native';
import {connect} from 'react-redux';
import {getUsersRanking} from '../../store/ranking/actions';
import {styles} from './styles';
import Item from './components/Item';
import TranslatedText from '../../components/TranslatedText';

const UserIcon = require('CodeallMobile/src/assets/img/user.png');

class Ranking extends Component {
  componentDidMount() {
    this.props.getUsersRanking();
  }

  Item = item => {
    return <Item item={item} />;
  };

  render() {
    return (
      <View style={styles.body}>
        <Text style={styles.header}>
          <TranslatedText id="leaderboard.ranking" />
        </Text>
        <View style={styles.separator} />
        <FlatList
          style={{marginHorizontal: '13.3%'}}
          data={this.props.ranking}
          showsVerticalScrollIndicator={false}
          keyExtractor={(item, index) => `${item.id || index}-ranking`}
          renderItem={({item}) => this.Item(item)}
          ListHeaderComponent={() => (
            <View style={styles.list}>
              <Image style={styles.picture} source={UserIcon} />
              <Text style={styles.name}>{this.props.nick}</Text>
              <View style={styles.rankingTitles}>
                <Text style={styles.rankingTitle} />
                <Text style={styles.turquiseTitle}>
                  <TranslatedText id="leaderboard.ranking" />
                </Text>
              </View>
              <View style={styles.separator} />
            </View>
          )}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  ranking: state.ranking.ranking,
  nick: state.profile.nick,
});

const mapDispatchToProps = dispatch => ({
  getUsersRanking: () => dispatch(getUsersRanking()),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Ranking);
