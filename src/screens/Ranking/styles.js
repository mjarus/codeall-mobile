import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

export const body = {
  flex: 1,
};

export const header = {
  fontFamily: 'Quicksand-Bold',
  marginTop: '8.4%',
  marginHorizontal: '13.3%',
  fontSize: 22,
  textAlign: 'left',
  lineHeight: 30,
  letterSpacing: 0.2,
  width: '100%',
};

export const list = {
  marginTop: 40,
  alignItems: 'center',
  justifyContent: 'center',
};

export const separator = {
  marginHorizontal: '13.3%',
  width: '73.4%',
  height: 1,
  backgroundColor: COLORS.bone,
  marginTop: 11,
};

export const picture = {
  width: 142,
  height: 142,
  borderRadius: 100,
  borderColor: 'transparent',
  borderWidth: 1,
};

export const name = {
  fontFamily: 'Quicksand-Bold',
  fontSize: 22,
  lineHeight: 30,
  letterSpacing: 0.2,
  color: COLORS.blueWhale,
  marginTop: 14,
  marginBottom: 12,
};

export const plainPic = {
  width: 142,
  height: 142,
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  borderWidth: 1,
  borderRadius: 100,
  borderColor: 'transparent',
  backgroundColor: COLORS.orange,
  marginTop: 48,
  marginBottom: 16,
};

export const plus = {
  color: COLORS.background,
  fontSize: 60,
};

export const rankingTitles = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
  flexDirection: 'row',
  marginTop: 15,
};

export const rankingTitle = {
  fontFamily: 'Quicksand-Bold',
  textAlign: 'left',
  color: COLORS.header,
  width: '50%',
  fontWeight: 'bold',
  fontSize: 15,
  lineHeight: 19,
};

export const turquiseTitle = {
  fontFamily: 'Quicksand-Bold',
  color: COLORS.turquoiseHeader,
  width: '50%',
  textAlign: 'right',
  fontWeight: 'bold',
  fontSize: 15,
  lineHeight: 19,
};

export const styles = StyleSheet.create({
  header,
  body,
  separator,
  picture,
  name,
  plainPic,
  plus,
  rankingTitles,
  rankingTitle,
  turquiseTitle,
  list,
});
