import React, {Component} from 'react';
import {Text, View} from 'react-native';
import {styles} from './styles';

class Item extends Component {
  render() {
    return (
      <View style={styles.body}>
        <Text style={styles.rankingPlace}>
          {this.props.item.rankingPosition}
        </Text>
        <Text style={styles.nick}>{this.props.item.nick}</Text>
        <Text style={styles.points}>{this.props.item.intentPoints}</Text>
        <Text style={styles.verification}>
          {this.props.item.newIntentsCount}
        </Text>
      </View>
    );
  }
}

export default Item;
