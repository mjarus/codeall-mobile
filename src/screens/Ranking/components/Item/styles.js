import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

export const body = {
  width: '100%',
  display: 'flex',
  justifyContent: 'center',
  marginTop: 20,
  flexDirection: 'row',
  marginBottom: 10,
};

export const rankingPlace = {
  width: '15%',
  fontFamily: 'Lato-Regular',
  fontSize: 13,
  lineHeight: 20,
  letterSpacing: 0.3,
  color: COLORS.black,
  textAlign: 'center',
};

export const nick = {
  fontFamily: 'Lato-Regular',
  width: '45%',
  fontSize: 13,
  lineHeight: 20,
  letterSpacing: 0.3,
  color: COLORS.header,
  textAlign: 'center',
};

export const points = {
  width: '25%',
  fontSize: 13,
  lineHeight: 20,
  letterSpacing: 0.3,
  color: COLORS.turquoisHeader,
  fontWeight: 'bold',
  textAlign: 'center',
};

export const verification = {
  width: '15%',
  fontSize: 13,
  lineHeight: 20,
  letterSpacing: 0.3,
  color: COLORS.header,
  textAlign: 'center',
};

export const styles = StyleSheet.create({
  body,
  rankingPlace,
  nick,
  points,
  verification,
});
