import React, {useEffect} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';

import {getProfile} from '../../store/profile/actions';
import {userDeviceLogin} from '../../store/auth/actions';
import {getLessons} from '../../store/lessons/actions';
import SplashLogo from '../../components/SplashLogo';

const SplashScreen = (props) => {
  useEffect(() => {
    props
      .userDeviceLogin({})
      .then(() => {
        props.getProfile({userId: props.userId});
        props.getLessons();
      })
      .catch(() => {});
  }, [props.userId]);

  return <SplashLogo error={props.authError} />;
};

const mapStateToProps = (state) => ({
  userId: state.auth.userId,
  authError: state.auth.error,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({getProfile, userDeviceLogin, getLessons}, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(SplashScreen);
