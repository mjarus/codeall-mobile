import React, {useState, useEffect} from 'react';
import {ScrollView, View} from 'react-native';
import {connect} from 'react-redux';
import {FLASH_MESSAGE_TYPES} from '../../../components/FlashMessage/messageTypes';
import {UNIT_TYPES} from './assets/types';
import {
  FIRST_MARGIN_ID,
  BOTTOM_MARGIN_ID_INCREMENT_VALUE,
  ID_OBJECT_STEP,
} from './assets';
import {setFlashMessage} from '../../../store/editor/actions';
import getNamesObject from './functions/getNamesObject';
import getFocusedGroupType from './functions/getFocusedGroupType';
import getProgramCodeWithIds from './functions/getProgramCodeWithIds';
import addObjectToCommaContainer from './functions/addObjectToCommaContainer';
import getProgramCodeWithNewObject from './functions/getProgramCodeWithNewObject';
import getProgramCodeWithChangedValue from './functions/getProgramCodeWithChangedValue';
import getProgramCodeWithDeletedObject from './functions/getProgramCodeWithDeletedObject';
import CodeBlock from './components/main/CodeBlock';
import BottomKeys from './components/keyboard/BottomKeys';
import TouchableMargin from './components/main/TouchableMargin';
import code from './assets/code';
import {styles} from './styles';
import ProjectSaveModal from './components/keyboard/ProjectSaveModal';
import RefreshModal from './components/keyboard/RefreshModal';
import MiniEditorModal from './components/keyboard/MiniEditorModal';
import {output} from '../../../mocks/output';
import {useTranslation} from 'react-i18next';

// on iOS use directionalLockEnabled instead of double ScrollView

const initialNamesObject = getNamesObject(code);
const [initialProgramCode, initialLatestId] = getProgramCodeWithIds(code);

const Editor = (props) => {
  const [focusedId, setFocusedId] = useState(null);
  const [focusedType, setFocusedType] = useState(null);
  const [doubleFocusedId, setDoubleFocusedId] = useState(null);
  const [focusedGroupType, setFocusedGroupType] = useState(null);
  const [nativeKeyboardVisible, setNativeKeyboardVisible] = useState(false);
  const [keyboardButtonsVisible, setKeyboardButtonsVisible] = useState(true);
  const [latestId, setLastId] = useState(initialLatestId);
  const [programCode, setProgramCode] = useState(initialProgramCode);
  const [namesObject, setNamesObject] = useState(initialNamesObject);
  const [isProjectSaveModal, setIsProjectSaveModal] = useState(false);
  const [isRefreshModal, setIsRefreshModal] = useState(false);
  const [isMiniEditorModal, setIsMiniEditorModal] = useState(false);
  const {t} = useTranslation();

  useEffect(() => {
    setFocusedId(FIRST_MARGIN_ID);
    setFocusedType(UNIT_TYPES.TOUCHABLE_MARGIN);
    setDoubleFocusedId(null);
    setFocusedGroupType(getFocusedGroupType(UNIT_TYPES.TOUCHABLE_MARGIN));
    setKeyboardButtonsVisible(true);
  }, []);

  const sendCode = () => {};

  const onPressKeyboardButton = () => {
    setKeyboardButtonsVisible(!keyboardButtonsVisible);
  };

  const onPressRefreshButton = () => {
    if (programCode.length === 0) {
      return;
    }

    setIsRefreshModal(true);
  };

  const handleCodeRefresh = () => {
    setProgramCode(initialProgramCode);
    setNamesObject(initialNamesObject);
    setIsRefreshModal(false);
  };

  const onPressPlayButton = () => {
    setIsMiniEditorModal(true);
  };

  const onPressUndoButton = () => {};

  const onPressRedoButton = () => {};

  const onPressSaveButton = () => {
    //TODO add send code functionality

    sendCode();
    setIsProjectSaveModal(true);
  };

  const onPressDeleteButton = () => {
    if (focusedType === null) {
      props.setFlashMessage({
        text: t('editor.error1'),
        type: FLASH_MESSAGE_TYPES.EDITOR_INFO,
      });
      return;
    }
    // if (focusedType === UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER) {
    //   props.setFlashMessage({
    //     text: 'Nie można usunąć tego obiektu',
    //     type: FLASH_MESSAGE_TYPES.EDITOR_ERROR,
    //   });
    //   return;
    // }
    if (focusedType === UNIT_TYPES.TOUCHABLE_MARGIN) {
      setFocusedId(null);
      setFocusedType(null);
      setDoubleFocusedId(null);
      setFocusedGroupType(null);
      return;
    }
    const newProgramCode = getProgramCodeWithDeletedObject(
      programCode,
      focusedId,
    );
    setFocusedId(null);
    setFocusedType(null);
    setDoubleFocusedId(null);
    setFocusedGroupType(null);
    setProgramCode(newProgramCode);
    setNamesObject(getNamesObject(newProgramCode));
  };

  const onPressCustomButton = (pressedGroupType, objectCode) => {
    // if (focusedType === null) {
    //   props.setFlashMessage({
    //     text: 'Wybierz miejsce w którym chcesz wstawić obiekt',
    //     type: FLASH_MESSAGE_TYPES.EDITOR_INFO,
    //   });
    //   return;
    // }
    const newProgramCode = getProgramCodeWithNewObject(
      programCode,
      latestId,
      focusedId,
      focusedGroupType,
      pressedGroupType,
      objectCode,
    );
    if (newProgramCode) {
      setFocusedId(null);
      setFocusedType(null);
      setDoubleFocusedId(null);
      setFocusedGroupType(null);
      setLastId(latestId + ID_OBJECT_STEP);
      setProgramCode(newProgramCode);
    }
  };

  const args = {
    focusedId,
    doubleFocusedId,
    itemPress: (data) => {
      setFocusedId(data.id);
      setFocusedType(data.type);
      setDoubleFocusedId(null);
      setFocusedGroupType(getFocusedGroupType(data.type));
      setKeyboardButtonsVisible(true);
    },
    itemDoublePress: (data) => {
      setFocusedId(null);
      setFocusedType(null);
      setDoubleFocusedId(data.id);
      setFocusedGroupType(null);
      setNativeKeyboardVisible(true);
    },
    valueChange: (data) => {
      const newProgramCode = getProgramCodeWithChangedValue(
        programCode,
        data.id,
        data.value,
      );
      setFocusedId(null);
      setFocusedType(null);
      setDoubleFocusedId(null);
      setFocusedGroupType(null);
      setNativeKeyboardVisible(false);
      setProgramCode(newProgramCode);
      setNamesObject(getNamesObject(newProgramCode));
    },
    additionButtonPress: (data) => {
      const newProgramCode = addObjectToCommaContainer(
        programCode,
        latestId,
        data.id,
        data.objectItemField,
      );
      setFocusedId(null);
      setFocusedType(null);
      setDoubleFocusedId(null);
      setFocusedGroupType(null);
      setLastId(latestId + ID_OBJECT_STEP);
      setProgramCode(newProgramCode);
    },
  };

  //console.log(programCode);

  return (
    <View style={styles.mainContainer}>
      <ScrollView
        horizontal
        contentContainerStyle={styles.horizontalScrollContainer}>
        <ScrollView contentContainerStyle={styles.verticalScrollContainer}>
          <TouchableMargin
            id={FIRST_MARGIN_ID}
            args={args}
            setNativeKeyboardVisible={setNativeKeyboardVisible}
          />
          {Array.isArray(programCode) &&
            programCode.flatMap((element, idx) => [
              <CodeBlock key={element.id} item={element} args={args} />,
              <TouchableMargin
                wide={idx + 1 === programCode.length}
                narrow={Array.isArray(element.block)}
                key={element.id + BOTTOM_MARGIN_ID_INCREMENT_VALUE}
                id={element.id + BOTTOM_MARGIN_ID_INCREMENT_VALUE}
                args={args}
              />,
            ])}
        </ScrollView>
      </ScrollView>
      {!nativeKeyboardVisible && (
        <BottomKeys
          latestId={latestId}
          namesObject={namesObject}
          focusedGroupType={focusedGroupType}
          keyboardButtonsVisible={keyboardButtonsVisible}
          onPressPlayButton={onPressPlayButton}
          onPressUndoButton={onPressUndoButton}
          onPressRedoButton={onPressRedoButton}
          onPressDeleteButton={onPressDeleteButton}
          onPressCustomButton={onPressCustomButton}
          onPressKeyboardButton={onPressKeyboardButton}
          onPressSaveButton={onPressSaveButton}
          onPressRefreshButton={onPressRefreshButton}
        />
      )}
      <ProjectSaveModal
        isModalVisible={isProjectSaveModal}
        setIsModalVisible={setIsProjectSaveModal}
      />
      <RefreshModal
        isModalVisible={isRefreshModal}
        setIsModalVisible={setIsRefreshModal}
        handleCodeRefresh={handleCodeRefresh}
      />
      <MiniEditorModal
        isModalVisible={isMiniEditorModal}
        setIsModalVisible={setIsMiniEditorModal}
        output={output}
        isError
      />
    </View>
  );
};

const mapDispatchToProps = (dispatch) => ({
  setFlashMessage: (data) => dispatch(setFlashMessage(data)),
});

export default connect(null, mapDispatchToProps)(Editor);
