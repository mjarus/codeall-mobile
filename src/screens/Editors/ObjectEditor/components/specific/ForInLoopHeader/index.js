import React from 'react';
import {View, Text} from 'react-native';
import LineContainer from '../../specific/LineContainer';
import {styles} from '../styles';

const ForInLoopHeader = ({item, args}) => {
  if (Array.isArray(item.params) && item.params[1]) {
    return (
      <View style={styles.rowContainer}>
        <View style={styles.forInLoopHeaderPrimaryContainer}>
          <Text style={styles.forInLoopHeaderText}>{'for'}</Text>
        </View>
        <LineContainer item={item.params[0]} args={args} />
        <View style={styles.forInLoopHeaderSecondaryContainer}>
          <Text style={styles.forInLoopHeaderText}>{'in'}</Text>
        </View>
        <LineContainer item={item.params[1]} args={args} />
        <Text style={styles.forInLoopHeaderText}>{':'}</Text>
      </View>
    );
  }

  return null;
};

export default ForInLoopHeader;
