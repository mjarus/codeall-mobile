import React from 'react';
import {Text} from 'react-native';
import TouchableContainer from '../../main/TouchableContainer';
import {styles} from '../styles';

const AdditionSign = ({item, args}) => (
  <TouchableContainer item={item} args={args}>
    <Text style={styles.additionSignText}>{'+'}</Text>
  </TouchableContainer>
);

export default AdditionSign;
