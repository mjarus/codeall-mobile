import React from 'react';
import {View} from 'react-native';
import SingleLineCodeBlock from '../../main/SingleLineCodeBlock';
import TouchableContainer from '../../main/TouchableContainer';
import {styles} from '../styles';

const LineContainer = ({item, args}) => {
  if (Array.isArray(item.params) && item.params.length) {
    return (
      <View style={styles.rowContainer}>
        {item.params.map(element => (
          <SingleLineCodeBlock key={element.id} item={element} args={args} />
        ))}
      </View>
    );
  }

  return (
    <TouchableContainer item={item} args={args} style={styles.emptyField} />
  );
};

export default LineContainer;
