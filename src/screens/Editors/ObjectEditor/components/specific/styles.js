import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const BASIC_HEIGHT = 32;

const basicContainer = {
  height: BASIC_HEIGHT,
  flexDirection: 'row',
};

const basicText = {
  height: BASIC_HEIGHT,
  fontSize: 17,
  lineHeight: 23,
  fontFamily: 'monospace',
  textAlign: 'center',
  textAlignVertical: 'center',
};

///////////////////////////////////////

const rowContainer = {
  flexDirection: 'row',
  alignItems: 'center',
};

const commaText = {
  ...basicText,
  paddingRight: 4,
  color: '#aaccff',
};

const additionButtonContainer = {
  width: 16,
  height: 21,
  borderRadius: 4,
  backgroundColor: '#ffffff30',
};

const additionButtonText = {
  ...basicText,
  height: 20,
  fontSize: 20,
  lineHeight: 20,
  paddingTop: 2.5,
  color: COLORS.editor.background,
};

const emptyField = {
  height: BASIC_HEIGHT,
  width: 40,
  backgroundColor: '#ffffff1a',
};

///////////////////////////////////////

const forInLoopHeaderPrimaryContainer = {
  ...basicContainer,
  borderRadius: 8,
  marginRight: 6,
  paddingHorizontal: 3,
  backgroundColor: '#fc7a57',
};

const forInLoopHeaderSecondaryContainer = {
  ...basicContainer,
  borderRadius: 8,
  marginHorizontal: 6,
  paddingHorizontal: 3,
  backgroundColor: '#fc7a57',
};

const forInLoopHeaderText = {
  ...basicText,
  color: 'white',
};

const functionDefinitionPrefixText = {
  ...basicText,
  marginRight: 6,
  color: '#d1baff',
};

const functionDefinitionBracketsText = {
  ...basicText,
  color: '#6fdad1',
};

///////////////////////////////////////

const equationSignText = {
  ...basicText,
  paddingHorizontal: 6,
  color: 'lightblue',
};

const additionSignText = {
  ...basicText,
  paddingHorizontal: 6,
  color: 'lightgreen',
};

const functionCallNameText = {
  ...basicText,
  color: '#6fdad1',
};

const functionCallBracketsText = {
  ...basicText,
  color: '#6fdad1',
};

const objectFieldBracketsText = {
  ...basicText,
  color: '#6495ed',
};

const objectFieldColonText = {
  ...basicText,
  paddingRight: 6,
  color: COLORS.background,
};

const variableExtensionText = {
  ...basicText,
  color: '#aaccff',
};

const listItemExtensionText = {
  ...basicText,
  color: COLORS.background,
};

///////////////////////////////////////

const focusedInputFieldContainer = {
  ...basicContainer,
  minWidth: 30,
  borderRadius: 8,
  borderWidth: 1,
  borderColor: COLORS.background,
  justifyContent: 'center',
  backgroundColor: '#ffffff45',
};

const blurredInputFieldContainer = {
  ...basicContainer,
};

const focusedInputFieldText = color => ({
  ...basicText,
  paddingVertical: 0,
  paddingHorizontal: 5,
  textAlign: 'center',
  color,
});

const blurredInputFieldText = color => ({
  ...basicText,
  textAlign: 'center',
  color,
});

export const styles = StyleSheet.create({
  rowContainer,
  commaText,
  additionButtonContainer,
  additionButtonText,

  forInLoopHeaderPrimaryContainer,
  forInLoopHeaderSecondaryContainer,
  forInLoopHeaderText,
  functionDefinitionPrefixText,
  functionDefinitionBracketsText,
  emptyField,

  equationSignText,
  additionSignText,
  functionCallNameText,
  functionCallBracketsText,
  objectFieldBracketsText,
  objectFieldColonText,
  variableExtensionText,
  listItemExtensionText,

  focusedInputFieldContainer,
  blurredInputFieldContainer,
  focusedInputFieldText,
  blurredInputFieldText,
});
