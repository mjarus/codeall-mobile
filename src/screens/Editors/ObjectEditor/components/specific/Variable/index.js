import React, {useEffect, useState} from 'react';
import {UNIT_TYPES} from '../../../assets/types';
import InputField from '../InputField';
import TouchableContainer from '../../main/TouchableContainer';
import SingleLineExtensionList from '../../main/SingleLineExtensionList';
import {styles} from '../styles';

const Variable = ({item, args}) => (
  <TouchableContainer
    style={styles.blurredInputFieldContainer}
    item={item}
    args={args}>
    <InputField
      item={item}
      args={args}
      type={UNIT_TYPES.VARIABLE}
      initialFocus={true}
    />

    <SingleLineExtensionList item={item} args={args} />
  </TouchableContainer>
);
export default Variable;
