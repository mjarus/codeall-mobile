import React from 'react';
import {Text} from 'react-native';
import TouchableContainer from '../../main/TouchableContainer';
import {styles} from '../styles';

const VariableExtension = ({item, args}) => (
  <TouchableContainer item={item} args={args}>
    <Text style={styles.variableExtensionText}>{`.${item.value}`}</Text>
  </TouchableContainer>
);

export default VariableExtension;
