import React from 'react';
import {Text, View} from 'react-native';
import TouchableContainer from '../../main/TouchableContainer';
import LineContainer from '../LineContainer';
import {styles} from '../styles';

const ListItemExtension = ({item, args}) => {
  if (Array.isArray(item.params) && item.params[0]) {
    return (
      <TouchableContainer item={item} args={args}>
        <Text style={styles.listItemExtensionText}>{'['}</Text>
        <LineContainer item={item.params[0]} args={args} />
        <Text style={styles.listItemExtensionText}>{']'}</Text>
      </TouchableContainer>
    );
  }
  return null;
};

export default ListItemExtension;
