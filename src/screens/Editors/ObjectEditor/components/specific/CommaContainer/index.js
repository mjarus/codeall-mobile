import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {SUPPORT_ID_INCREMENT_VALUE} from '../../../assets';
import LineContainer from '../../specific/LineContainer';
import {styles} from '../styles';

const CommaContainer = ({item, args}) => {
  const handleOnPress = () => {
    args.additionButtonPress({id: item.id});
  };

  if (Array.isArray(item.params)) {
    return [
      item.params.flatMap(element => [
        <LineContainer key={element.id} item={element} args={args} />,
        <Text
          key={element.id + SUPPORT_ID_INCREMENT_VALUE}
          style={styles.commaText}>
          {','}
        </Text>,
      ]),
      <TouchableOpacity
        key={item.id}
        style={styles.additionButtonContainer}
        onPress={handleOnPress}>
        <Text style={styles.additionButtonText}>{'+'}</Text>
      </TouchableOpacity>,
    ];
  }

  return null;
};

export default CommaContainer;
