import React from 'react';
import {Text} from 'react-native';
import {UNIT_TYPES} from '../../../assets/types';
import InputField from '../InputField';
import LineContainer from '../LineContainer';
import TouchableContainer from '../../main/TouchableContainer';
import {styles} from '../styles';

const ObjectItemField = ({item, args}) => {
  if (Array.isArray(item.params) && item.params.length) {
    return (
      <TouchableContainer item={item} args={args}>
        <InputField
          item={item}
          args={args}
          type={UNIT_TYPES.OBJECT_ITEM_FIELD}
        />
        <Text style={styles.objectFieldColonText}>{':'}</Text>
        <LineContainer item={item.params[0]} args={args} />
      </TouchableContainer>
    );
  }

  return null;
};

export default ObjectItemField;
