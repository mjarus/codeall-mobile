import React from 'react';
import {Text} from 'react-native';
import TouchableContainer from '../../main/TouchableContainer';
import {styles} from '../styles';

const EquationSign = ({item, args}) => (
  <TouchableContainer item={item} args={args}>
    <Text style={styles.equationSignText}>{'='}</Text>
  </TouchableContainer>
);

export default EquationSign;
