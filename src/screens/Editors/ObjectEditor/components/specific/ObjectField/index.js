import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import {SUPPORT_ID_INCREMENT_VALUE} from '../../../assets';
import ObjectItemField from '../ObjectItemField';
import TouchableContainer from '../../main/TouchableContainer';
import SingleLineExtensionList from '../../main/SingleLineExtensionList';
import {styles} from '../styles';

const ObjectField = ({item, args}) => {
  const handleOnPress = () => {
    args.additionButtonPress({id: item.id, objectItemField: true});
  };

  if (Array.isArray(item.params)) {
    return (
      <TouchableContainer item={item} args={args}>
        <Text style={styles.objectFieldBracketsText}>{'{'}</Text>
        {item.params.flatMap(element => [
          <ObjectItemField key={element.id} item={element} args={args} />,
          <Text
            key={element.id + SUPPORT_ID_INCREMENT_VALUE}
            style={styles.commaText}>
            {','}
          </Text>,
        ])}
        <TouchableOpacity
          style={styles.additionButtonContainer}
          onPress={handleOnPress}>
          <Text style={styles.additionButtonText}>{'+'}</Text>
        </TouchableOpacity>
        <Text style={styles.objectFieldBracketsText}>{'}'}</Text>
        <SingleLineExtensionList item={item} args={args} />
      </TouchableContainer>
    );
  }

  return null;
};

export default ObjectField;
