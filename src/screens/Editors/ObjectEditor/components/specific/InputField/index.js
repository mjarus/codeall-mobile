import React, {useEffect, useState} from 'react';
import {Text, View, TextInput, TouchableOpacity} from 'react-native';
import {INPUT_FIELD_PARAMS} from '../../../assets/inputs';
import {styles} from '../styles';

const FocusedInputField = ({item, args, type}) => {
  const [value, setValue] = useState(item.value);

  const handleOnChangeText = text => {
    setValue(text.replace(INPUT_FIELD_PARAMS[type].regularExpression, ''));
  };

  const handleOnBlur = () => {
    args.valueChange({id: item.id, value});
  };

  return (
    <View style={styles.focusedInputFieldContainer}>
      <TextInput
        style={styles.focusedInputFieldText(
          INPUT_FIELD_PARAMS[type].focusedInputFieldTextColor,
        )}
        keyboardType={INPUT_FIELD_PARAMS[type].keyboardType}
        autoFocus={true}
        spellCheck={false}
        autoCorrect={false}
        autoCapitalize="none"
        autoCompleteType="off"
        textContentType="none"
        importantForAutofill="no"
        underlineColorAndroid="transparent"
        value={value}
        onBlur={handleOnBlur}
        onChangeText={handleOnChangeText}
      />
    </View>
  );
};

const InputField = ({item, args, type, initialFocus}) => {
  const handleOnPress = () => {
    if (item.id === args.focusedId) {
      args.itemDoublePress({id: item.id, type: item.type});
    } else {
      args.itemPress({id: item.id, type: item.type});
    }
  };

  useEffect(() => {
    if (initialFocus) {
      args.itemDoublePress({id: item.id, type: item.type});
    }
  }, []);

  if (item.id === args.doubleFocusedId) {
    return <FocusedInputField item={item} args={args} type={type} />;
  }

  return (
    <TouchableOpacity
      style={styles.blurredInputFieldContainer}
      activeOpacity={1}
      onPress={handleOnPress}>
      <Text
        style={styles.blurredInputFieldText(
          INPUT_FIELD_PARAMS[type].blurredInputFieldTextColor,
        )}>
        {item.value || '___'}
      </Text>
    </TouchableOpacity>
  );
};

export default InputField;
