import React from 'react';
import {Text} from 'react-native';
import CommaContainer from '../CommaContainer';
import TouchableContainer from '../../main/TouchableContainer';
import SingleLineExtensionList from '../../main/SingleLineExtensionList';
import {styles} from '../styles';

const FunctionCall = ({item, args}) => (
  <TouchableContainer item={item} args={args}>
    <Text style={styles.functionCallNameText}>{item.value}</Text>
    <Text style={styles.functionCallBracketsText}>{'('}</Text>
    <CommaContainer item={item} args={args} />
    <Text style={styles.functionCallBracketsText}>{')'}</Text>
    <SingleLineExtensionList item={item} args={args} />
  </TouchableContainer>
);

export default FunctionCall;
