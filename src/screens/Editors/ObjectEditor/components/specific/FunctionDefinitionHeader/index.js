import React from 'react';
import {View, Text} from 'react-native';
import {UNIT_TYPES} from '../../../assets/types';
import CommaContainer from '../CommaContainer';
import InputField from '../InputField';
import {styles} from '../styles';

const FunctionDefinitionHeader = ({item, args}) => (
  <View style={styles.rowContainer}>
    <Text style={styles.functionDefinitionPrefixText}>{'def'}</Text>
    <InputField item={item} args={args} type={UNIT_TYPES.FUNCTION_DEFINITION} />
    <Text style={styles.functionDefinitionBracketsText}>{'('}</Text>
    <CommaContainer item={item} args={args} />
    <Text style={styles.functionDefinitionBracketsText}>{'):'}</Text>
  </View>
);

export default FunctionDefinitionHeader;
