import React from 'react';
import MultiLineCodeBlock from '../MultiLineCodeBlock';
import LineContainer from '../../specific/LineContainer';

const CodeBlock = ({item, args}) => {
  if (Array.isArray(item.block)) {
    return <MultiLineCodeBlock item={item} args={args} />;
  }
  return <LineContainer item={item} args={args} />;
};

export default CodeBlock;
