import {StyleSheet} from 'react-native';

const focusedMargin = {
  height: 30,
  width: 40,
  marginVertical: 16,
  backgroundColor: '#b4ecf390',
};

const focusedNarrowMargin = {
  height: 30,
  width: 40,
  marginVertical: 12,
  backgroundColor: '#b4ecf390',
};

const wideMargin = {
  height: 62,
  width: '100%',
  backgroundColor: '#ffffff07',
};

const narrowMargin = {
  height: 8,
  width: '100%',
  backgroundColor: '#ffffff07',
};

const customMargin = {
  height: 16,
  width: '100%',
  backgroundColor: '#ffffff07',
};

export const styles = StyleSheet.create({
  focusedNarrowMargin,
  focusedMargin,
  wideMargin,
  narrowMargin,
  customMargin,
});
