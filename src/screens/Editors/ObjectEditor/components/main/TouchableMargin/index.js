import React from 'react';
import {TouchableOpacity} from 'react-native';
import {UNIT_TYPES} from '../../../assets/types';
import {styles} from './styles';

const TouchableMargin = ({wide, narrow, id, args}) => {
  const handleOnPress = () => {
    args.itemPress({id, type: UNIT_TYPES.TOUCHABLE_MARGIN});
  };

  return (
    <TouchableOpacity
      style={
        args.focusedId === id && narrow
          ? styles.focusedNarrowMargin
          : args.focusedId === id
          ? styles.focusedMargin
          : wide
          ? styles.wideMargin
          : narrow
          ? styles.narrowMargin
          : styles.customMargin
      }
      onPress={handleOnPress}
    />
  );
};

export default TouchableMargin;
