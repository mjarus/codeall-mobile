import {Dimensions, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../../../../../theme/typography';

export const styles = StyleSheet.create({
  container: {
    width: Dimensions.get('window').width - 30,
    position: 'absolute',
    right: 0,
    bottom: 50,
    backgroundColor: COLORS.editor.backgroundSecondary,
    borderRadius: 15,
    overflow: 'hidden',
  },

  contentButton: {
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 36,
    height: 36,
  },

  content: {
    width: '95%',
    alignSelf: 'center',
    backgroundColor: COLORS.background,
    color: COLORS.editor.backgroundSecondary,
    paddingHorizontal: 20,
    paddingVertical: 5,
  },

  contentText: {
    color: COLORS.editor.backgroundSecondary,
    fontFamily: typography.fonts.primary,
    fontSize: 16,
  },
});
