import React from 'react';
import {View} from 'react-native';
import HTML from 'react-native-render-html';
import {styles} from './styles';

const TaskContentModal = ({currentStep}) => {
  const {chatBubbles} = currentStep;

  const content = chatBubbles[chatBubbles.length - 1].contents[0];

  return (
    <View style={styles.container}>
      <View style={styles.content}>
        <HTML html={content} baseFontStyle={styles.contentText} />
      </View>
    </View>
  );
};

export default TaskContentModal;
