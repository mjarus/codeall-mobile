import React from 'react';
import {TouchableOpacity, View} from 'react-native';
import {styles} from './styles';

const TouchableContainer = ({item, args, style, children}) => {
  const handleOnPress = () => {
    args.itemPress({id: item.id, type: item.type});
  };

  return (
    <TouchableOpacity
      style={style || styles.rowContainer}
      activeOpacity={1}
      onPress={handleOnPress}>
      {children}
      {item.id === args.focusedId && (
        <View style={styles.fadeFocusedContainer} pointerEvents={'none'} />
      )}
    </TouchableOpacity>
  );
};

export default TouchableContainer;
