import {StyleSheet} from 'react-native';

const rowContainer = {
  flexDirection: 'row',
  alignItems: 'center',
};

const fadeFocusedContainer = {
  width: '100%',
  height: '100%',
  position: 'absolute',
  backgroundColor: '#b4ecf390',
};

export const styles = StyleSheet.create({
  rowContainer,
  fadeFocusedContainer,
});
