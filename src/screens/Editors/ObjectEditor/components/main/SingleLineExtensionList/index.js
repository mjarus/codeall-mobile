import React from 'react';
import {UNIT_TYPES} from '../../../assets/types';
import VariableExtension from '../../specific/VariableExtension';
import ListItemExtension from '../../specific/ListItemExtension';

const SingleLineExtensionElement = ({item, args}) => {
  switch (item.type) {
    case UNIT_TYPES.VARIABLE_EXTENSION:
      return <VariableExtension item={item} args={args} />;

    case UNIT_TYPES.LIST_ITEM_EXTENSION:
      return <ListItemExtension item={item} args={args} />;

    default:
      return null;
  }
};

const SingleLineExtensionList = ({item, args}) => {
  if (Array.isArray(item.extension)) {
    return item.extension.map(element => (
      <SingleLineExtensionElement key={element.id} item={element} args={args} />
    ));
  }

  return null;
};

export default SingleLineExtensionList;
