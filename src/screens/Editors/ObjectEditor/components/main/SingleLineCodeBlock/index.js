import React from 'react';
import {UNIT_TYPES} from '../../../assets/types';
import AdditionSign from '../../specific/AdditionSign';
import EquationSign from '../../specific/EquationSign';
import FunctionCall from '../../specific/FunctionCall';
import ObjectField from '../../specific/ObjectField';
import Variable from '../../specific/Variable';

const SingleLineCodeBlock = ({item, args}) => {
  switch (item.type) {
    case UNIT_TYPES.OBJECT_FIELD:
      return <ObjectField item={item} args={args} />;

    case UNIT_TYPES.FUNCTION_CALL:
      return <FunctionCall item={item} args={args} />;

    case UNIT_TYPES.EQUATION_SIGN:
      return <EquationSign item={item} args={args} />;

    case UNIT_TYPES.ADDITION_SIGN:
      return <AdditionSign item={item} args={args} />;

    case UNIT_TYPES.VARIABLE:
      return <Variable item={item} args={args} />;

    default:
      return null;
  }
};

export default SingleLineCodeBlock;
