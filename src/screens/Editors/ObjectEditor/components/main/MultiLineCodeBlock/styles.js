import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const mainContainer = {
  width: '100%',
};

const blockContainer = {
  paddingLeft: 40,
  borderLeftWidth: 0.5,
  borderColor: COLORS.dimGray,
};

export const styles = StyleSheet.create({
  mainContainer,
  blockContainer,
});
