import React from 'react';
import {View} from 'react-native';
import {UNIT_TYPES} from '../../../assets/types';
import {
  BOTTOM_MARGIN_ID_INCREMENT_VALUE,
  FIRST_BLOCK_MARGIN_ID_INCREMENT_VALUE,
} from '../../../assets';
import TouchableContainer from '../TouchableContainer';
import TouchableMargin from '../TouchableMargin';
import ForInLoopHeader from '../../specific/ForInLoopHeader';
import FunctionDefinitionHeader from '../../specific/FunctionDefinitionHeader';
import CodeBlock from '../CodeBlock';
import {styles} from './styles';

const MultiLineCodeBlockHeader = ({item, args}) => {
  switch (item.type) {
    case UNIT_TYPES.FOR_IN_LOOP:
      return <ForInLoopHeader item={item} args={args} />;

    case UNIT_TYPES.FUNCTION_DEFINITION:
      return <FunctionDefinitionHeader item={item} args={args} />;

    default:
      return null;
  }
};

const MultiLineCodeBlock = ({item, args}) => (
  <TouchableContainer style={styles.mainContainer} item={item} args={args}>
    <MultiLineCodeBlockHeader item={item} args={args} />
    <View style={styles.blockContainer}>
      <TouchableMargin
        narrow={!item.block.length}
        id={item.id + FIRST_BLOCK_MARGIN_ID_INCREMENT_VALUE}
        args={args}
      />
      {item.block.flatMap((element, idx) => [
        <CodeBlock key={element.id} item={element} args={args} />,
        <TouchableMargin
          narrow={idx + 1 === item.block.length || Array.isArray(element.block)}
          key={element.id + BOTTOM_MARGIN_ID_INCREMENT_VALUE}
          id={element.id + BOTTOM_MARGIN_ID_INCREMENT_VALUE}
          args={args}
        />,
      ])}
    </View>
  </TouchableContainer>
);

export default MultiLineCodeBlock;
