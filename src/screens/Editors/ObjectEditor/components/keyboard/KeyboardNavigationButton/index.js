import React from 'react';
import {TouchableOpacity, View, Text, Image} from 'react-native';
import {styles} from './styles';

const KeyboardNavigationButton = ({
  title,
  color,
  onPress,
  icon,
  style,
  iconStyle,
  titleMargin = 20,
}) => {
  return (
    <TouchableOpacity
      style={{...styles.button(color), ...style}}
      onPress={() => onPress(title)}>
      {icon ? (
        <Image source={icon} style={{...styles.icon, ...iconStyle}} />
      ) : (
        <View style={styles.icon}>
          <View style={styles.iconElement(color)} />
          <View style={styles.iconElement(color)} />
          <View style={styles.iconElement(color)} />
        </View>
      )}
      {title && (
        <Text style={{...styles.title, marginLeft: titleMargin}}>{title}</Text>
      )}
    </TouchableOpacity>
  );
};

export default KeyboardNavigationButton;
