import {StyleSheet} from 'react-native';
import typography from '../../../../../../theme/typography';

export const styles = StyleSheet.create({
  button: (color) => ({
    flexDirection: 'row',
    height: 40,
    borderColor: color,
    borderWidth: 2,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    minWidth: 40,
    paddingHorizontal: 12,
    marginHorizontal: 3,
    marginBottom: 10,
  }),

  icon: {
    position: 'absolute',
    left: 8,
    flexDirection: 'column',
    width: 14,
    height: 14,
    justifyContent: 'space-between',
  },

  iconElement: (color) => ({
    width: 14,
    height: 2,
    backgroundColor: color,
  }),

  title: {
    fontFamily: 'monospace',
    fontSize: typography.sizes.sm,
    marginLeft: 20,
    lineHeight: 40,
    fontWeight: 'bold',
  },
});
