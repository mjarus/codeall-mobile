import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../../../../../theme/typography';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'flex-end',
  },

  container: {
    width: '100%',
    height: 170,
    backgroundColor: COLORS.background,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    alignItems: 'center',
    paddingTop: 20,
  },

  separator: {
    width: 50,
    height: 3,
    backgroundColor: COLORS.frenchGray,
  },

  title: {
    fontFamily: typography.fonts.secondary,
    fontSize: typography.sizes.s,
    marginTop: 10,
    width: '60%',
    textAlign: 'center',
  },

  orangeButtonText: {
    fontSize: typography.sizes.sm,
  },

  whiteButton: {
    backgroundColor: COLORS.background,
    borderColor: COLORS.header,
    borderWidth: 1,
  },

  whiteButtonText: {
    color: '#5E5E5E',
  },

  buttonsContainer: {
    marginTop: 30,
    flexDirection: 'row',
    width: '70%',
    justifyContent: 'space-between',
  },

  button: {
    width: 100,
    height: 35,
  },
});
