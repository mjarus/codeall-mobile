import React from 'react';
import {View} from 'react-native';
import Modal from '../Modal';
import {styles} from './styles';
import TranslatedText from '../../../../../../components/TranslatedText';
import { RoundedButton } from '../../../../../../components/Buttons';

const RefreshModal = ({
  isModalVisible,
  setIsModalVisible,
  handleCodeRefresh,
}) => {
  return (
    <Modal
      isModalVisible={isModalVisible}
      setIsModalVisible={setIsModalVisible}
      style={styles.modal}>
      <View style={styles.container}>
        <View style={styles.separator} />
        <TranslatedText style={styles.title} id="editor.refresh.title" />
        <View style={styles.buttonsContainer}>
          <RoundedButton
            headerTextId="editor.projectSave.yes"
            onPress={handleCodeRefresh}
            size="medium"
          />
          <RoundedButton
            headerTextId="editor.projectSave.no"
            onPress={() => {
              setIsModalVisible(false);
            }}
            size="medium"
          />
        </View>
      </View>
    </Modal>
  );
};

export default RefreshModal;
