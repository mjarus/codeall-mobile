import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const hoveringButtons = {
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  height: 45,
  alignSelf: 'flex-end',
  marginBottom: 9,
  marginRight: 4,
};

const chatButton = {
  alignItems: 'center',
  width: 36,
  height: 36,
  borderRadius: 50,
  marginRight: 15,
};

const chatIcon = {
  width: '100%',
  height: '100%',
};

const playButton = {
  width: 49,
  height: 45,
  justifyContent: 'center',
  alignItems: 'center',
};

const playIcon = {
  width: '100%',
  height: '100%',
};

const styles = StyleSheet.create({
  hoveringButtons,
  chatButton,
  chatIcon,
  playButton,
  playIcon,
});

export default styles;
