import React, {useState} from 'react';
import {Image, TouchableOpacity, View} from 'react-native';

import styles from './styles';
import TaskContentModal from 'CodeallMobile/src/screens/Editors/ObjectEditor/components/main/TaskContentModal';
import {useSelector} from 'react-redux';

const hintOff = require('CodeallMobile/src/assets/img/hintOff.png');
const hintOn = require('CodeallMobile/src/assets/img/hintOn.png');
const playIcon = require('CodeallMobile/src/assets/img/playBottomKeys.png');

const CurrentTaskDescription = (props) => {
  const [isTaskContentModal, setIsTaskContentModal] = useState(false);

  const currentStep = useSelector((state) => state.chat.currentStep);

  const onPressTaskContentButton = () => {
    setIsTaskContentModal((prev) => !prev);
  };

  return (
    <>
      {isTaskContentModal && (
        <TaskContentModal
          onPressTaskContentButton={onPressTaskContentButton}
          currentStep={currentStep}
        />
      )}
      {currentStep && (
        <TouchableOpacity
          style={styles.chatButton}
          onPress={onPressTaskContentButton}>
          <Image
            source={isTaskContentModal ? hintOn : hintOff}
            style={styles.chatIcon}
          />
        </TouchableOpacity>
      )}
    </>
  );
};

const HoveringButtons = (props) => {
  return (
    <View style={styles.hoveringButtons}>
      <CurrentTaskDescription />
      <TouchableOpacity
        style={styles.playButton}
        onPress={props.onPressPlayButton}>
        <Image source={playIcon} style={styles.playIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default HoveringButtons;
