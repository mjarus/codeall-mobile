import React from 'react';
import RNModal from 'react-native-modal';
import {styles} from './styles';

const Modal = ({children, isModalVisible, setIsModalVisible, style}) => {
  const handleModalClose = () => {
    setIsModalVisible(false);
  };

  return (
    <RNModal
      isVisible={isModalVisible}
      onSwipeComplete={handleModalClose}
      onBackdropPress={handleModalClose}
      useNativeDriverForBackdrop
      swipeDirection="down"
      style={{...styles.modal, ...style}}>
      {children}
    </RNModal>
  );
};

export default Modal;
