import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../../../../../theme/typography';

export const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'flex-end',
    margin: 0,
  },

  container: {
    width: '100%',
    minHeight: 240,
    backgroundColor: COLORS.background,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    alignItems: 'center',
    paddingVertical: 20,
  },

  separator: {
    width: 50,
    height: 3,
    backgroundColor: COLORS.frenchGray,
  },

  title: {
    fontFamily: typography.fonts.secondary,
    marginTop: 10,
  },

  infoContainer: {
    width: '90%',
    marginTop: 20,
  },

  label: {
    color: COLORS.switchSecondary,
    fontFamily: 'monospace',
    fontWeight: 'bold',
  },

  info: {
    flexDirection: 'row',
  },

  infoText: {
    fontFamily: 'monospace',
    color: COLORS.switchSecondary,
    flexShrink: 1,
  },

  exampleContainer: {
    marginTop: 20,
  },
});
