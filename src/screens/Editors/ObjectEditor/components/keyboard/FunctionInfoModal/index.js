import React from 'react';
import {View, Text} from 'react-native';
import Modal from '../Modal';
import {keyboardButtonFunctions} from '../../../../../../mocks/keyboardButtons';
import {styles} from './styles';
import TranslatedText from '../../../../../../components/TranslatedText';

const FunctionInfoModal = ({isModalVisible, setIsModalVisible, type, id}) => {
  const data = keyboardButtonFunctions[type].filter(
    functionData => id === functionData.id,
  )[0];

  return (
    <Modal
      isModalVisible={isModalVisible}
      setIsModalVisible={setIsModalVisible}>
      <View style={styles.container}>
        {data && (
          <>
            <View style={styles.separator} />
            <Text style={styles.title}>{data.title}</Text>
            <View style={styles.infoContainer}>
              <View style={styles.info}>
                {data.info.parameters ? (
                  <TranslatedText
                    id="editor.functionInfo.parameters"
                    style={styles.label}
                  />
                ) : (
                  <TranslatedText
                    id="editor.functionInfo.noParams"
                    style={styles.label}
                  />
                )}
                <Text style={styles.infoText}>{data.info.parameters}</Text>
              </View>
              <View style={styles.info}>
                <TranslatedText
                  style={styles.label}
                  id="editor.functionInfo.returns"
                />
                <Text style={styles.infoText}>{data.info.returns}</Text>
              </View>
              <View style={styles.exampleContainer}>
                <TranslatedText
                  style={styles.infoText}
                  id="editor.functionInfo.example"
                />
                {data.info.example.map((line, index) => (
                  <Text style={styles.infoText} key={index}>
                    {'>'}
                    {'>'}
                    {'>'} {line}
                  </Text>
                ))}
              </View>
            </View>
          </>
        )}
      </View>
    </Modal>
  );
};

export default FunctionInfoModal;
