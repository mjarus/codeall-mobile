import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
  },

  keyboardIconContainer: {
    width: 40,
  },

  keyboardIcon: {
    width: 22,
    height: 19,
    left: 7,
  },

  divider: {
    width: '100%',
    alignSelf: 'center',
    height: 5,
    backgroundColor: '#fff',
    marginBottom: 10,
  },

  listButtonIcon: {
    width: 22,
    height: 22,
  },
});
