import React, {useState} from 'react';
import {FlatList, ScrollView, View} from 'react-native';
import {
  keyboardButtonFunctions,
  keyboardButtonsData,
} from '../../../../../../mocks/keyboardButtons';
import {keyboardButtonColors} from '../Buttons';
import FunctionInfoModal from '../FunctionInfoModal';
import KeyboardBreadcrumbIcon from '../KeyboardBreadcrumbsIcon';
import KeyboardNavigationButton from '../KeyboardNavigationButton';
import {styles} from './styles';
import Button from '../Button';

const keyboardNavigationIcon = require('CodeallMobile/src/assets/img/keyboardNavigationIcon.png');

const KeyboardNavigationBreadcrumbs = ({
  type,
  handleGoBackToKeyboard,
  onPressCustomButton,
  latestId,
}) => {
  const [sensorClicked, setSensorClicked] = useState(null);
  const [isFunctionInfoModal, setIsFunctionInfoModal] = useState(false);
  const [functionInfoId, setFunctionInfoId] = useState(null);
  const [sensorId, setSensorId] = useState(0);

  const handleButtonPress = (sensorTitle, sensorId) => {
    setSensorClicked(sensorTitle);
    setSensorId(sensorId);
  };

  const handleTypeButtonPress = () => {
    setSensorClicked(null);
  };

  const handleInfoIconPress = (id) => {
    setFunctionInfoId(id);
    setIsFunctionInfoModal(true);
  };

  const getKeyboardButtonFunctions = (type, sensorId) => {
    return keyboardButtonFunctions[type].filter(
      (button) => button.sensorId === sensorId,
    );
  };

  const renderFunctionList = () => {
    const _renderItem = ({item}) => (
      <Button
        key={item.id}
        value={item.value}
        title={item.title}
        color={item.color}
        groupType={item.groupType}
        latestId={latestId}
        onPressCustomButton={onPressCustomButton}
        getObject={item.getObject}
        textColor={item.textColor}
        iconRight={item.iconRight}
        arrowLeft={item.arrowLeft}
        onPressInfoIcon={() => handleInfoIconPress(item.id)}
        info={item.info}
      />
    );

    return (
      <FlatList
        data={getKeyboardButtonFunctions(type, sensorId)}
        keyExtractor={(item) => item.id.toString()}
        renderItem={_renderItem}
      />
    );
  };

  const renderSensorList = () => {
    const _renderItem = ({item}) =>
      type === 'phone' ? (
        <Button
          key={item.id}
          value={item.value}
          title={item.title}
          color={item.color}
          groupType={item.groupType}
          latestId={latestId}
          onPressCustomButton={onPressCustomButton}
          getObject={item.getObject}
          textColor={item.textColor}
          iconRight={item.iconRight}
          arrowLeft={item.arrowLeft}
          onPressInfoIcon={() => handleInfoIconPress(item.id)}
          info={item.info}
        />
      ) : (
        <View style={{width: 'auto', flexDirection: 'row'}}>
          <KeyboardNavigationButton
            icon={item.icon}
            title={item.title}
            color={keyboardButtonColors[type]}
            iconStyle={styles.listButtonIcon}
            titleMargin={25}
            onPress={(title) => handleButtonPress(title, item.id)}
          />
        </View>
      );

    return (
      <FlatList
        data={
          type === 'phone'
            ? keyboardButtonFunctions[type]
            : keyboardButtonsData[type]
        }
        keyExtractor={(item) => item.id.toString()}
        renderItem={_renderItem}
      />
    );
  };

  return (
    <>
      <ScrollView>
        <ScrollView
          contentContainerStyle={styles.container}
          showsHorizontalScrollIndicator={false}
          horizontal>
          <KeyboardNavigationButton
            icon={keyboardNavigationIcon}
            style={styles.keyboardIconContainer}
            iconStyle={styles.keyboardIcon}
            onPress={handleGoBackToKeyboard}
          />
          <KeyboardBreadcrumbIcon />
          <KeyboardNavigationButton
            title={type}
            color={keyboardButtonColors[type]}
            onPress={handleTypeButtonPress}
          />
          {sensorClicked && (
            <>
              <KeyboardBreadcrumbIcon />
              <KeyboardNavigationButton
                title={sensorClicked}
                color={keyboardButtonColors[type]}
                onPress={() => {}}
              />
            </>
          )}
        </ScrollView>
        <View style={styles.divider} />
        <ScrollView showsHorizontalScrollIndicator={false} horizontal>
          {sensorClicked ? renderFunctionList() : renderSensorList()}
        </ScrollView>
      </ScrollView>
      <FunctionInfoModal
        type={type}
        id={functionInfoId}
        isModalVisible={isFunctionInfoModal}
        setIsModalVisible={setIsFunctionInfoModal}
      />
    </>
  );
};

export default KeyboardNavigationBreadcrumbs;
