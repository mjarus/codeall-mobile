import {StyleSheet} from 'react-native';

export const styles = StyleSheet.create({
  container: {
    width: 30,
    height: 36,
    alignItems: 'center',
    justifyContent: 'center',
  },

  icon: {
    fontSize: 30,
    fontWeight: 'bold',
  },
});
