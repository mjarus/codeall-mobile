import React from 'react';
import {View, Text} from 'react-native';
import {styles} from './styles';

const KeyboardBreadcrumbIcon = () => {
  return (
    <View style={styles.container}>
      <Text style={styles.icon}>{'>'}</Text>
    </View>
  );
};

export default KeyboardBreadcrumbIcon;
