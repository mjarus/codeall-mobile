import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const keyboardLine = {
  height: 57,
  borderColor: COLORS.editorKeyboard,
  borderTopWidth: 2,
  borderBottomWidth: 1,
  paddingHorizontal: 6,
  width: '100%',
  flexDirection: 'row',
  backgroundColor: COLORS.background,
  justifyContent: 'space-between',
};

const rightLineContainer = {
  flexDirection: 'row',
  width: '78%',
  left: 4,
};

const keyboardLineBox = {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: COLORS.background,
  borderWidth: 1,
  borderRadius: 4,
  borderColor: COLORS.editorKeyboard,
  paddingVertical: 9,
  paddingHorizontal: 6,
  marginVertical: 6,
  marginHorizontal: 5,
};

const keyboardLineImage = {
  tintColor: COLORS.editorKeyboardSecondary,
};

const saveIcon = {
  ...keyboardLineImage,
  width: 18,
  height: 18,
};

const refreshIcon = {
  ...keyboardLineImage,
  width: 28,
  height: 24,
};

const undoIcon = {
  ...keyboardLineImage,
  width: 19.6,
  height: 19.6,
};

const redoIcon = {
  ...undoIcon,
};

const deleteIcon = {
  ...keyboardLineImage,
  width: 24,
  height: 16.5,
};

const keyboardButton = {
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: COLORS.background,
  marginLeft: 10,
  marginTop: 17,
  marginBottom: 10,
};

const keyboardIcon = (buttonsVisible) => ({
  ...keyboardLineImage,
  width: 28,
  height: 23,
  transform: [{rotate: buttonsVisible ? '0deg' : '180deg'}],
});

export const styles = StyleSheet.create({
  keyboardLine,
  keyboardButton,
  rightLineContainer,
  keyboardLineBox,
  saveIcon,
  refreshIcon,
  undoIcon,
  redoIcon,
  keyboardIcon,
  deleteIcon,
});
