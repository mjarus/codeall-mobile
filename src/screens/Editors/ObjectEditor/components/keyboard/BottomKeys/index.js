import React from 'react';
import {View, Image, TouchableOpacity, Text} from 'react-native';
import Buttons from '../Buttons';

import {styles} from './styles';
import HoveringButtons from 'CodeallMobile/src/screens/Editors/ObjectEditor/components/keyboard/HoveringButtons';

const hideKeyboard = require('CodeallMobile/src/assets/img/keyboardHide.png');
const refresh = require('CodeallMobile/src/assets/img/refreshBottomKeys.png');
const deleteIcon = require('CodeallMobile/src/assets/img/deleteBottomKeys.png');
const redo = require('CodeallMobile/src/assets/img/redoBottomKeys.png');
const undo = require('CodeallMobile/src/assets/img/undoBottomKeys.png');
const save = require('CodeallMobile/src/assets/img/saveBottomKeys.png');

const BottomKeys = (props) => (
  <View>
    <HoveringButtons onPressPlayButton={props.onPressPlayButton} />
    <View style={styles.keyboardLine}>
      <TouchableOpacity
        style={styles.keyboardButton}
        onPress={props.onPressKeyboardButton}>
        <Image
          source={hideKeyboard}
          style={styles.keyboardIcon(props.keyboardButtonsVisible)}
        />
      </TouchableOpacity>
      <View style={styles.rightLineContainer}>
        <TouchableOpacity
          style={styles.keyboardLineBox}
          onPress={props.onPressSaveButton}>
          <Image source={save} style={styles.saveIcon} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.keyboardLineBox}
          onPress={props.onPressRefreshButton}>
          <Image source={refresh} style={styles.refreshIcon} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.keyboardLineBox}
          onPress={props.onPressUndoButton}>
          <Image source={undo} style={styles.undoIcon} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.keyboardLineBox}
          onPress={props.onPressRedoButton}>
          <Image source={redo} style={styles.redoIcon} />
        </TouchableOpacity>
        <TouchableOpacity
          style={styles.keyboardLineBox}
          onPress={props.onPressDeleteButton}>
          <Image source={deleteIcon} style={styles.deleteIcon} />
        </TouchableOpacity>
      </View>
    </View>
    {props.keyboardButtonsVisible && (
      <Buttons
        latestId={props.latestId}
        namesObject={props.namesObject}
        focusedGroupType={props.focusedGroupType}
        onPressCustomButton={props.onPressCustomButton}
      />
    )}
  </View>
);

export default BottomKeys;
