import {StyleSheet, Dimensions} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const SCREEN_HEIGHT = Dimensions.get('window').height;

const scrollContainer = {
  height: SCREEN_HEIGHT * 0.3,
  width: '100%',
  backgroundColor: COLORS.background,
};

const scrollContentContainer = {
  flexGrow: 1,
  padding: 12,
  flexWrap: 'wrap',
  flexDirection: 'row',
};

const noButtonsMessageContainer = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  padding: 30,
};

const noButtonsMessageText = {
  textAlign: 'center',
  fontSize: 15,
  fontFamily: 'Lato-Regular',
  color: COLORS.switchSecondary,
};

export const styles = StyleSheet.create({
  scrollContainer,
  scrollContentContainer,
  noButtonsMessageContainer,
  noButtonsMessageText,
});
