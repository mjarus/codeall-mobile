import React, {useState} from 'react';
import {ScrollView, View, Text} from 'react-native';
import {CUSTOM_BUTTONS, VARIABLE_BUTTONS} from '../../../assets/buttons';
import Button from '../Button';
import KeyboardNavigationBreadcrumbs from '../KeyboardNavigationBreadcrumbs';
import KeyboardNavigationButton from '../KeyboardNavigationButton';
import {styles} from './styles';
import ScrollWithCustomIndicator from 'CodeallMobile/src/components/ScrollWithCustomIndicator';
import { useTranslation } from 'react-i18next';

export const keyboardButtonColors = {
  sensors: '#6CAE75',
  phone: '#4DABF8',
};

const Buttons = (props) => {
  const [navigationRoute, setNavigationRoute] = useState(null);
  const {t} = useTranslation();

  const customButtonsList = CUSTOM_BUTTONS.flatMap((element, index) =>
    element.allowedFocusedGroupTypes.includes(props.focusedGroupType)
      ? [
          <Button
            key={index}
            value={''}
            title={element.title}
            color={element.color}
            groupType={element.groupType}
            latestId={props.latestId}
            onPressCustomButton={props.onPressCustomButton}
            getObject={element.getObject}
            textColor={element.textColor}
            iconRight={element.iconRight}
            arrowLeft={element.arrowLeft}
          />,
        ]
      : [],
  );

  const variableButtonsList = VARIABLE_BUTTONS.flatMap((element, index) =>
    props.namesObject && Array.isArray(props.namesObject[element.type])
      ? props.namesObject[element.type].flatMap((value) =>
          element.allowedFocusedGroupTypes.includes(props.focusedGroupType)
            ? [
                <Button
                  key={index + value}
                  value={value}
                  title={element.prefix + value + element.postfix}
                  color={element.color}
                  textColor={element.textColor}
                  groupType={element.groupType}
                  latestId={props.latestId}
                  onPressCustomButton={props.onPressCustomButton}
                  getObject={element.getObject}
                />,
              ]
            : [],
        )
      : [],
  );

  const handleGoBackToKeyboard = () => {
    setNavigationRoute(null);
  };

  return (
    <ScrollWithCustomIndicator
      style={styles.scrollContainer}
      contentContainerStyle={styles.scrollContentContainer}>
      {navigationRoute ? (
        <>
          <KeyboardNavigationBreadcrumbs
            type={navigationRoute}
            handleGoBackToKeyboard={handleGoBackToKeyboard}
            onPressCustomButton={props.onPressCustomButton}
            latestId={props.latestId}
          />
        </>
      ) : (
        <>
          {!customButtonsList.length && !variableButtonsList.length && (
            <View style={styles.noButtonsMessageContainer}>
              <Text style={styles.noButtonsMessageText}>
                {props.focusedGroupType
                  ? t('editor.error3')
                  : t('editor.error2')}
              </Text>
            </View>
          )}
          {customButtonsList.length > 0 && (
            <View style={{width: '100%', flexDirection: 'row'}}>
              <KeyboardNavigationButton
                title="sensors"
                color={keyboardButtonColors.sensors}
                onPress={setNavigationRoute}
              />
              <KeyboardNavigationButton
                title="phone"
                color={keyboardButtonColors.phone}
                onPress={setNavigationRoute}
              />
            </View>
          )}
          {customButtonsList}
          {variableButtonsList}
        </>
      )}
    </ScrollWithCustomIndicator>
  );
};

export default Buttons;
