import React from 'react';
import {Text, TouchableOpacity, Image, View} from 'react-native';
import {styles} from './styles';

const Button = props => {
  const onPressCustomButton = () => {
    const objectCode = props.getObject(props.latestId, props.value);
    props.onPressCustomButton(props.groupType, objectCode);
  };

  const renderTitle = () => {
    return props.title.split('').map((char, index) => {
      if (char === '_') {
        return (
          <Text style={styles.underline} key={index}>
            _
          </Text>
        );
      }

      return char;
    });
  };

  return (
    <View style={styles.container}>
      {props.info && (
        <TouchableOpacity onPress={props.onPressInfoIcon}>
          <View style={styles.icon(props.color)}>
            <Text style={styles.iconText}>i</Text>
          </View>
        </TouchableOpacity>
      )}
      <TouchableOpacity
        style={styles.buttonContainer(
          props.color,
          !!props.iconRight,
          !!props.arrowLeft,
        )}
        onPress={onPressCustomButton}>
        {props.arrowLeft && (
          <View style={styles.arrowContainer}>
            <Text style={styles.arrowLeft}>◀︎</Text>
          </View>
        )}
        <Text style={styles.buttonText(props.textColor)}>{renderTitle()}</Text>
        {props.iconRight && (
          <Image source={props.iconRight} style={styles.iconRight} />
        )}
      </TouchableOpacity>
    </View>
  );
};

export default Button;
