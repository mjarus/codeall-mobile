import {StyleSheet} from 'react-native';
import typography from '../../../../../../theme/typography';
import COLORS from 'CodeallMobile/src/theme/colors';

export const buttonContainer = (color, iconRight, arrowLeft) => ({
  flexDirection: iconRight || arrowLeft ? 'row' : 'column',
  borderWidth: 2,
  borderColor: color,
  borderRadius: 10,
  minWidth: 40,
  height: 40,
  paddingHorizontal: 12,
  marginHorizontal: 3,
});

export const buttonText = (textColor = COLORS.editorKeyboardButtonText) => ({
  fontFamily: 'monospace',
  fontWeight: 'bold',
  color: textColor || '#fff',
  fontSize: 17,
  lineHeight: 40,
});

export const iconRight = {
  width: 18,
  height: 11,
  marginLeft: 5,
  marginTop: 5,
  tintColor: COLORS.editorKeyboardButtonText,
};

export const underline = {
  color: '#59C9B4',
};

export const arrowContainer = {
  marginRight: 3,
  marginBottom: 2,
};

export const arrowLeft = {
  fontSize: 17,
  color: 'white',
};

const icon = (color) => ({
  width: 22,
  height: 22,
  borderRadius: 50,
  backgroundColor: color,
  alignItems: 'center',
  justifyContent: 'center',
});

const iconText = {
  color: '#fff',
  fontFamily: 'monospace',
  fontSize: typography.sizes.sm,
  fontWeight: 'bold',
};

const container = {
  flexDirection: 'row',
  alignItems: 'center',
  marginBottom: 10,
};

export const styles = StyleSheet.create({
  buttonContainer,
  buttonText,
  iconRight,
  underline,
  arrowLeft,
  arrowContainer,
  icon,
  iconText,
  container,
});
