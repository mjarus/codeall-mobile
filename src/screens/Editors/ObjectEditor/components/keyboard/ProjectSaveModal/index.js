import React, {useState} from 'react';
import {View, TextInput} from 'react-native';
import {useTranslation} from 'react-i18next';
import Modal from '../Modal';
import {styles} from './styles';
import TranslatedText from '../../../../../../components/TranslatedText';
import { RoundedButton } from '../../../../../../components/Buttons';

const ProjectSaveModal = ({isModalVisible, setIsModalVisible}) => {
  const [projectExists, setProjectExists] = useState(false);
  const [projectName, setProjectName] = useState('');
  const {t} = useTranslation();

  const handleProjectSave = () => {
    if (projectName.length < 1) {
      return;
    }

    // handle project save request

    // just for demo to go through all elements
    setProjectExists(true);
  };

  const handleProjectOverwrite = () => {
    // handle project overwrite request

    setProjectName('');
    setProjectExists(false);
    setIsModalVisible(false);
  };

  const handleProjectOverwriteReject = () => {
    setProjectName('');
    setProjectExists(false);
  };

  return (
    <Modal
      isModalVisible={isModalVisible}
      setIsModalVisible={setIsModalVisible}>
      <View style={styles.container}>
        <View style={styles.separator} />
        {projectExists ? (
          <>
            <TranslatedText
              style={styles.title}
              id="editor.projectSave.exists"
            />
            <View style={styles.buttonsContainer}>
              <RoundedButton
                headerTextId="editor.projectSave.yes"
                onPress={handleProjectOverwrite}
              />
              <RoundedButton
                headerTextId="editor.projectSave.no"
                onPress={handleProjectOverwriteReject}
              />
            </View>
          </>
        ) : (
          <>
            <TranslatedText
              id="editor.projectSave.title"
              style={styles.title}
            />
            <TextInput
              placeholder={t('editor.projectSave.placeholder')}
              style={styles.input}
              onChangeText={setProjectName}
            />
            <RoundedButton
              headerTextId="editor.projectSave.save"
              onPress={handleProjectSave}
            />
          </>
        )}
      </View>
    </Modal>
  );
};

export default ProjectSaveModal;
