import React from 'react';
import {View, Text, TouchableOpacity, Image} from 'react-native';
import TranslatedText from '../../../../../../components/TranslatedText';
import Modal from '../Modal';
import {styles} from './styles';
import {RectangularButton} from 'CodeallMobile/src/components/Buttons'

const refreshIcon = require('CodeallMobile/src/assets/img/refreshWhite.png');

const MiniEditorModal = ({
  isModalVisible,
  setIsModalVisible,
  isError,
  output,
}) => {
  return (
    <Modal
      isModalVisible={isModalVisible}
      setIsModalVisible={setIsModalVisible}>
      <View style={styles.container}>
        <View style={styles.separator} />
        {isError ? (
          <TranslatedText id="editor.playCode.error" style={styles.title} />
        ) : (
          <TranslatedText id="editor.playCode.success" style={styles.title} />
        )}
        <View style={styles.output}>
          {output.map((codeLine, index) => (
            <Text style={styles.codeLine} key={index}>
              {'>'}
              {'>'}
              {'>'} {codeLine}
            </Text>
          ))}
          <Text style={styles.codeLine}>
            {'>'}
            {'>'}
            {'>'}
          </Text>
        </View>
        <View style={styles.buttonsContainer}>
          {isError && (
            <TouchableOpacity style={styles.redButton}>
              <Image source={refreshIcon} style={styles.redButtonIcon} />
            </TouchableOpacity>
          )}
        <RectangularButton
          headerTextId='editor.playCode.next'
          onPress={() => {
            () => setIsModalVisible(false)
          }}
        />  
        </View>
      </View>
    </Modal>
  );
};

export default MiniEditorModal;
