import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../../../../../theme/typography';

export const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.background,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    alignItems: 'center',
    paddingTop: 20,
  },

  separator: {
    width: 50,
    height: 3,
    backgroundColor: COLORS.frenchGray,
  },

  title: {
    marginVertical: 20,
    fontFamily: 'monospace',
    fontSize: typography.sizes.sm,
    fontWeight: 'bold',
  },

  output: {
    width: '90%',
    borderColor: COLORS.switchSecondary,
    borderWidth: 1,
    padding: 15,
    minHeight: 150,
  },

  codeLine: {
    fontFamily: 'monospace',
  },

  redButton: {
    position: 'absolute',
    left: -70,
    width: 36,
    height: 36,
    backgroundColor: COLORS.coral,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 30,
  },

  redButtonIcon: {
    width: 26,
    height: 24,
  },

  orangeButton: {
    width: 110,
    height: 36,
  },

  buttonsContainer: {
    flexDirection: 'row',
    marginVertical: 15,
  },

  orangeButtonText: {
    fontFamily: typography.fonts.secondary,
    fontSize: 19,
  },
});
