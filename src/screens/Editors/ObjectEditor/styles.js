import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const mainContainer = {
  flex: 1,
  backgroundColor: COLORS.editor.background,
};

const horizontalScrollContainer = {
  flexGrow: 1,
};

const verticalScrollContainer = {
  flexGrow: 1,
  padding: 10,
};

const hintsButton = {
  position: 'absolute',
  bottom: 15,
  left: 15,
  width: 36,
  height: 36,
};

export const styles = StyleSheet.create({
  mainContainer,
  horizontalScrollContainer,
  verticalScrollContainer,
  hintsButton,
});
