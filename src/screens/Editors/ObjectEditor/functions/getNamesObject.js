import {VARIABLE_BUTTONS} from '../assets/buttons';

const VARIABLE_BUTTON_TYPES = VARIABLE_BUTTONS.map(item => item.type);

function addNamesToObject(programCode, objectPointer) {
  programCode.forEach(element => {
    if (Array.isArray(element.params)) {
      addNamesToObject(element.params, objectPointer);
    }
    if (Array.isArray(element.block)) {
      addNamesToObject(element.block, objectPointer);
    }
    if (Array.isArray(element.extension)) {
      addNamesToObject(element.extension, objectPointer);
    }
    if (element.value && VARIABLE_BUTTON_TYPES.includes(element.type)) {
      objectPointer[element.type].push(element.value);
    }
  });
}

function getNamesObject(programCode) {
  const objectPointer = {};
  const objectWithUniqueItems = {};

  VARIABLE_BUTTON_TYPES.forEach(type => {
    objectPointer[type] = [];
  });

  addNamesToObject(programCode, objectPointer);

  VARIABLE_BUTTON_TYPES.forEach(type => {
    objectWithUniqueItems[type] = [...new Set(objectPointer[type])];
  });

  return objectWithUniqueItems;
}

export default getNamesObject;
