import {ID_STEP, getLineContainerObject} from '../assets';

function replaceObjectMap(programCode, focusedId, objectCode) {
  return programCode.map(element => {
    if (element.id === focusedId) {
      return objectCode;
    }

    return {
      ...element,
      ...(Array.isArray(element.params)
        ? {params: replaceObjectMap(element.params, focusedId, objectCode)}
        : {}),
      ...(Array.isArray(element.block)
        ? {block: replaceObjectMap(element.block, focusedId, objectCode)}
        : {}),
      ...(Array.isArray(element.extension)
        ? {
            extension: replaceObjectMap(
              element.extension,
              focusedId,
              objectCode,
            ),
          }
        : {}),
    };
  });
}

function replaceObject(
  programCode,
  focusedId,
  objectCode,
  latestId,
  insertInLineContainer,
) {
  const newObjectCode = insertInLineContainer
    ? getLineContainerObject(latestId + ID_STEP, [objectCode])
    : objectCode;

  return replaceObjectMap(programCode, focusedId, newObjectCode);
}

export default replaceObject;
