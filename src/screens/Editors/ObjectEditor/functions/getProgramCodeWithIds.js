import {ID_STEP} from '../assets';

function getProgramCodeWithIdsMap(programCode, objectPointer) {
  return programCode.map(element => {
    objectPointer.id = objectPointer.id + ID_STEP;

    return {
      ...element,
      id: objectPointer.id,
      ...(Array.isArray(element.params)
        ? {params: getProgramCodeWithIdsMap(element.params, objectPointer)}
        : {}),
      ...(Array.isArray(element.block)
        ? {block: getProgramCodeWithIdsMap(element.block, objectPointer)}
        : {}),
      ...(Array.isArray(element.extension)
        ? {
            extension: getProgramCodeWithIdsMap(
              element.extension,
              objectPointer,
            ),
          }
        : {}),
    };
  });
}

function getProgramCodeWithIds(programCode) {
  const objectPointer = {id: 0};
  const programCodeWithIds = getProgramCodeWithIdsMap(
    programCode,
    objectPointer,
  );

  return [programCodeWithIds, objectPointer.id];
}

export default getProgramCodeWithIds;
