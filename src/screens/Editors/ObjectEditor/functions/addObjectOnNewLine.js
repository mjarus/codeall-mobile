import {
  ID_STEP,
  FIRST_MARGIN_ID,
  BOTTOM_MARGIN_ID_INCREMENT_VALUE,
  FIRST_BLOCK_MARGIN_ID_INCREMENT_VALUE,
  getLineContainerObject,
} from '../assets';

function addObjectOnNewLineMap(
  programCode,
  baseId,
  objectCode,
  insertElementUnderBlock,
) {
  return programCode.flatMap(element => {
    if (element.id === baseId) {
      return insertElementUnderBlock
        ? [element, objectCode]
        : Array.isArray(element.block)
        ? [{...element, block: [objectCode, ...element.block]}]
        : [element];
    }

    return [
      {
        ...element,
        ...(Array.isArray(element.params)
          ? {
              params: addObjectOnNewLineMap(
                element.params,
                baseId,
                objectCode,
                insertElementUnderBlock,
              ),
            }
          : {}),
        ...(Array.isArray(element.block)
          ? {
              block: addObjectOnNewLineMap(
                element.block,
                baseId,
                objectCode,
                insertElementUnderBlock,
              ),
            }
          : {}),
        ...(Array.isArray(element.extension)
          ? {
              extension: addObjectOnNewLineMap(
                element.extension,
                baseId,
                objectCode,
                insertElementUnderBlock,
              ),
            }
          : {}),
      },
    ];
  });
}

function addObjectOnNewLine(
  programCode,
  focusedId,
  objectCode,
  latestId,
  insertInLineContainer,
) {
  const restId = focusedId % ID_STEP;
  const baseId = focusedId - restId;
  const newObjectCode = insertInLineContainer
    ? getLineContainerObject(latestId + ID_STEP, [objectCode])
    : objectCode;

  if (focusedId === FIRST_MARGIN_ID) {
    return [newObjectCode, ...programCode];
  }

  if (restId === FIRST_BLOCK_MARGIN_ID_INCREMENT_VALUE) {
    return addObjectOnNewLineMap(programCode, baseId, newObjectCode, false);
  }

  if (restId === BOTTOM_MARGIN_ID_INCREMENT_VALUE) {
    return addObjectOnNewLineMap(programCode, baseId, newObjectCode, true);
  }

  return programCode;
}

export default addObjectOnNewLine;
