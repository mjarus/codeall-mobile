function addObjectToVariableExtensionMap(
  programCode,
  focusedId,
  objectCode,
  currentlyExtensionListMap = false,
) {
  return programCode.flatMap(element => {
    if (element.id === focusedId) {
      return currentlyExtensionListMap
        ? [element, objectCode]
        : [{...element, extension: [...(element.extension || []), objectCode]}];
    }

    return [
      {
        ...element,
        ...(Array.isArray(element.params)
          ? {
              params: addObjectToVariableExtensionMap(
                element.params,
                focusedId,
                objectCode,
              ),
            }
          : {}),
        ...(Array.isArray(element.block)
          ? {
              block: addObjectToVariableExtensionMap(
                element.block,
                focusedId,
                objectCode,
              ),
            }
          : {}),
        ...(Array.isArray(element.extension)
          ? {
              extension: addObjectToVariableExtensionMap(
                element.extension,
                focusedId,
                objectCode,
                true,
              ),
            }
          : {}),
      },
    ];
  });
}

function addObjectToVariableExtension(programCode, focusedId, objectCode) {
  return addObjectToVariableExtensionMap(programCode, focusedId, objectCode);
}

export default addObjectToVariableExtension;
