import {UNIT_GROUPS} from '../assets';

function getFocusedGroupType(type) {
  return Object.keys(UNIT_GROUPS).find(key => UNIT_GROUPS[key].includes(type));
}

export default getFocusedGroupType;
