import {
  ID_STEP,
  getLineContainerObject,
  getObjectItemFieldObject,
} from '../assets';

function addObjectToCommaContainerMap(programCode, id, objectCode) {
  return programCode.map(element => {
    if (element.id === id) {
      return {...element, params: [...element.params, objectCode]};
    }

    return {
      ...element,
      ...(Array.isArray(element.params)
        ? {
            params: addObjectToCommaContainerMap(
              element.params,
              id,
              objectCode,
            ),
          }
        : {}),
      ...(Array.isArray(element.block)
        ? {
            block: addObjectToCommaContainerMap(element.block, id, objectCode),
          }
        : {}),
      ...(Array.isArray(element.extension)
        ? {
            extension: addObjectToCommaContainerMap(
              element.extension,
              id,
              objectCode,
            ),
          }
        : {}),
    };
  });
}

function addObjectToCommaContainer(programCode, latestId, id, objectItemField) {
  const objectCode = objectItemField
    ? getObjectItemFieldObject(latestId + ID_STEP)
    : getLineContainerObject(latestId + ID_STEP);

  return addObjectToCommaContainerMap(programCode, id, objectCode);
}

export default addObjectToCommaContainer;
