import {UNIT_GROUP_TYPES} from '../assets/types';
import replaceObject from './replaceObject';
import addObjectToLine from './addObjectToLine';
import addObjectOnNewLine from './addObjectOnNewLine';
import addObjectToVariableExtension from './addObjectToVariableExtension';

function getProgramCodeWithNewObject(
  programCode,
  latestId,
  focusedId,
  focusedGroupType,
  pressedGroupType,
  objectCode,
) {
  switch (true) {
    case focusedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_SIGN:
      // todo - empty line allowed
      // return addObjectToLine(
      //   programCode,
      //   focusedId,
      //   objectCode,
      //   latestId,
      //   false,
      // );
      return addObjectToLine(programCode, focusedId, objectCode, latestId);

    case focusedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT:
      return replaceObject(programCode, focusedId, objectCode, latestId, false);

    case focusedGroupType === UNIT_GROUP_TYPES.MARGIN_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.BLOCK_OBJECT:
      return addObjectOnNewLine(
        programCode,
        focusedId,
        objectCode,
        latestId,
        false,
      );

    case focusedGroupType === UNIT_GROUP_TYPES.MARGIN_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT:
      return addObjectOnNewLine(
        programCode,
        focusedId,
        objectCode,
        latestId,
        true,
      );

    // todo - empty line allowed
    // case focusedGroupType === UNIT_GROUP_TYPES.LINE_CONTAINER_OBJECT &&
    //   pressedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT:
    //   return addObjectToLine(
    //     programCode,
    //     focusedId,
    //     objectCode,
    //     latestId,
    //     true,
    //   );

    case focusedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_SIGN &&
      pressedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_SIGN:
      return replaceObject(programCode, focusedId, objectCode, latestId, false);

    case focusedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.VARIABLE_EXTENSION_OBJECT:
      return addObjectToVariableExtension(programCode, focusedId, objectCode);

    case focusedGroupType === UNIT_GROUP_TYPES.BLOCK_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.BLOCK_OBJECT:
      return replaceObject(programCode, focusedId, objectCode, latestId, false);

    case focusedGroupType === UNIT_GROUP_TYPES.BLOCK_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.ARITHMETIC_OBJECT:
      return replaceObject(programCode, focusedId, objectCode, latestId, true);

    case focusedGroupType === UNIT_GROUP_TYPES.VARIABLE_EXTENSION_OBJECT &&
      pressedGroupType === UNIT_GROUP_TYPES.VARIABLE_EXTENSION_OBJECT:
      return addObjectToVariableExtension(programCode, focusedId, objectCode);

    default:
      return null;
  }
}

export default getProgramCodeWithNewObject;
