function getProgramCodeWithChangedValueMap(programCode, id, value) {
  return programCode.map(element => {
    if (element.id === id) {
      return {...element, value};
    }

    return {
      ...element,
      ...(Array.isArray(element.params)
        ? {params: getProgramCodeWithChangedValueMap(element.params, id, value)}
        : {}),
      ...(Array.isArray(element.block)
        ? {block: getProgramCodeWithChangedValueMap(element.block, id, value)}
        : {}),
      ...(Array.isArray(element.extension)
        ? {
            extension: getProgramCodeWithChangedValueMap(
              element.extension,
              id,
              value,
            ),
          }
        : {}),
    };
  });
}

function getProgramCodeWithChangedValue(programCode, id, value) {
  return getProgramCodeWithChangedValueMap(programCode, id, value);
}

export default getProgramCodeWithChangedValue;
