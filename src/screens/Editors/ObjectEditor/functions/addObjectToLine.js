import {ID_STEP, getVariableObject} from '../assets';

function addObjectToLineMap(
  programCode,
  focusedId,
  objectCode,
  additionalEmptyVariable,
) {
  return programCode.flatMap(element => {
    if (element.id === focusedId) {
      // todo - empty line allowed
      // return additionalEmptyVariable
      //   ? [element, objectCode, additionalEmptyVariable]
      //   : [{...element, params: [objectCode]}];
      return [element, objectCode, additionalEmptyVariable];
    }

    return [
      {
        ...element,
        ...(Array.isArray(element.params)
          ? {
              params: addObjectToLineMap(
                element.params,
                focusedId,
                objectCode,
                additionalEmptyVariable,
              ),
            }
          : {}),
        ...(Array.isArray(element.block)
          ? {
              block: addObjectToLineMap(
                element.block,
                focusedId,
                objectCode,
                additionalEmptyVariable,
              ),
            }
          : {}),
        ...(Array.isArray(element.extension)
          ? {
              extension: addObjectToLineMap(
                element.extension,
                focusedId,
                objectCode,
                additionalEmptyVariable,
              ),
            }
          : {}),
      },
    ];
  });
}

function addObjectToLine(programCode, focusedId, objectCode, latestId) {
  // todo - empty line allowed
  // const additionalEmptyVariable = emptyLineContainer
  //   ? null
  //   : getVariableObject(latestId + ID_STEP);
  const additionalEmptyVariable = getVariableObject(latestId + ID_STEP);

  return addObjectToLineMap(
    programCode,
    focusedId,
    objectCode,
    additionalEmptyVariable,
  );
}

export default addObjectToLine;
