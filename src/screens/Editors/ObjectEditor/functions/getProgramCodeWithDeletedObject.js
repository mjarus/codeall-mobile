import {UNIT_TYPES} from '../assets/types';
import {getVariableObject} from '../assets';

const getProgramCodeElement = (element, id) => ({
  ...element,
  ...(Array.isArray(element.params)
    ? {params: getProgramCodeWithDeletedObjectMap(element.params, id)}
    : {}),
  ...(Array.isArray(element.block)
    ? {block: getProgramCodeWithDeletedObjectMap(element.block, id)}
    : {}),
  ...(Array.isArray(element.extension)
    ? {extension: getProgramCodeWithDeletedObjectMap(element.extension, id)}
    : {}),
});

function lineContainerParamsMap(programCode, id) {
  let removeNextElement = false;
  const newProgramCode = programCode.flatMap(element => {
    if (removeNextElement) {
      removeNextElement = false;
      return [];
    }

    if (element.id === id) {
      removeNextElement = true;
      return [];
    }

    return [getProgramCodeElement(element, id)];
  });

  return removeNextElement && newProgramCode.length
    ? newProgramCode.slice(0, -1)
    : newProgramCode;
}

function getProgramCodeWithDeletedObjectMap(programCode, id) {
  return programCode.flatMap(element => {
    if (element.id === id) {
      return [];
    }

    if (
      element.type === UNIT_TYPES.REMOVABLE_LINE_CONTAINER &&
      Array.isArray(element.params)
    ) {
      const params = lineContainerParamsMap(element.params, id);
      return params.length ? [{...element, params}] : [];
    }

    if (
      element.type === UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER &&
      Array.isArray(element.params)
    ) {
      // todo - empty line allowed
      // return [
      //   {
      //     ...element,
      //     params: lineContainerParamsMap(element.params, id),
      //   },
      // ];
      const params = lineContainerParamsMap(element.params, id);
      const newParams = params.length
        ? params
        : element.params[0]
        ? [getVariableObject(element.params[0].id)]
        : [];
      return [{...element, params: newParams}];
    }

    return [getProgramCodeElement(element, id)];
  });
}

function getProgramCodeWithDeletedObject(programCode, id) {
  return getProgramCodeWithDeletedObjectMap(programCode, id);
}

export default getProgramCodeWithDeletedObject;
