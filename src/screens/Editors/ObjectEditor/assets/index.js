import {UNIT_TYPES, UNIT_GROUP_TYPES} from './types';

export const ID_STEP = 100;
export const FIRST_MARGIN_ID = 0;
export const ID_OBJECT_STEP = 400;
export const SUPPORT_ID_INCREMENT_VALUE = 20;
export const BOTTOM_MARGIN_ID_INCREMENT_VALUE = 10;
export const FIRST_BLOCK_MARGIN_ID_INCREMENT_VALUE = 11;

export const getVariableObject = (id, value = '') => ({
  id,
  type: UNIT_TYPES.VARIABLE,
  value,
});

export const getLineContainerObject = (id, params) => ({
  id,
  type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
  params: params || [getVariableObject(id + ID_STEP)],
});

export const getObjectItemFieldObject = (id, value = '') => ({
  id,
  type: UNIT_TYPES.OBJECT_ITEM_FIELD,
  value,
  params: [
    {
      id: id + ID_STEP,
      type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
      params: [getVariableObject(id + 2 * ID_STEP)],
    },
  ],
});

export const UNIT_GROUPS = {
  [UNIT_GROUP_TYPES.OTHER_OBJECT]: [UNIT_TYPES.OBJECT_ITEM_FIELD],
  [UNIT_GROUP_TYPES.BLOCK_OBJECT]: [
    UNIT_TYPES.FOR_IN_LOOP,
    UNIT_TYPES.FUNCTION_DEFINITION,
  ],
  [UNIT_GROUP_TYPES.MARGIN_OBJECT]: [UNIT_TYPES.TOUCHABLE_MARGIN],
  [UNIT_GROUP_TYPES.ARITHMETIC_SIGN]: [
    UNIT_TYPES.ADDITION_SIGN,
    UNIT_TYPES.EQUATION_SIGN,
  ],
  [UNIT_GROUP_TYPES.ARITHMETIC_OBJECT]: [
    UNIT_TYPES.BRACKETS,
    UNIT_TYPES.VARIABLE,
    UNIT_TYPES.LIST_FIELD,
    UNIT_TYPES.OBJECT_FIELD,
    UNIT_TYPES.STRING_FIELD,
    UNIT_TYPES.NUMBER_FIELD,
    UNIT_TYPES.FUNCTION_CALL,
  ],
  [UNIT_GROUP_TYPES.LINE_CONTAINER_OBJECT]: [
    UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
    UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
  ],
  [UNIT_GROUP_TYPES.VARIABLE_EXTENSION_OBJECT]: [
    UNIT_TYPES.VARIABLE_EXTENSION,
    UNIT_TYPES.LIST_ITEM_EXTENSION,
    UNIT_TYPES.LIST_RANGE_EXTENSION,
  ],
};

// const KEYBOARD_DID_SHOW = 'keyboardDidShow';
// const KEYBOARD_DID_HIDE = 'keyboardDidHide';
// useEffect(() => {
//   Keyboard.addListener(KEYBOARD_DID_SHOW, _keyboardDidShow);
//   Keyboard.addListener(KEYBOARD_DID_HIDE, _keyboardDidHide);
//   return () => {
//     Keyboard.removeAllListeners(KEYBOARD_DID_SHOW);
//     Keyboard.removeAllListeners(KEYBOARD_DID_HIDE);
//   };
// }, []);
// const _keyboardDidShow = event => {
//   console.log('KEYBOARD_DID_SHOW');
//   // if (event && event.endCoordinates && event.endCoordinates.height) {
//   //   setKeyboardHeight(event.endCoordinates.height);
//   // }
// };
//
// const _keyboardDidHide = () => {
//   console.log('KEYBOARD_DID_HIDE');
// };
