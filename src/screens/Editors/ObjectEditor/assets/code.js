import {UNIT_TYPES} from './types';

// const testCode1 = [
//   {
//     id: 1,
//     type: 'fun_def',
//     value: 'fun1',
//     params: [{id: 2, type: 'var', value: 'x'}], // parametry mapowane w comma_container
//     block: [
//       {
//         id: 3,
//         type: 'return',
//         params: [{id: 4, type: 'var', value: 'x'}], // parametry mapowane w line_container
//       },
//     ],
//   },
//   {
//     id: 112,
//     type: 'line',
//     params: [
//       // parametry mapowane w line_container
//       {id: 5, type: 'var', value: 'arr1'},
//       {id: 6, type: '='},
//       {
//         id: 7,
//         type: 'list',
//         params: [
//           // parametry mapowane w comma_container
//           {id: 8, type: 'num', value: 1},
//           {
//             id: 9,
//             type: 'obj',
//             params: [
//               // parametry mapowane w comma_container
//               {
//                 id: 10,
//                 type: 'obj_field',
//                 value: 'y',
//                 params: [{id: 113, type: 'str', value: 'cat'}], // parametry mapowane w line_container
//               },
//             ],
//           },
//         ],
//       },
//     ],
//   },
//   {
//     id: 114,
//     type: 'line',
//     // parametry mapowane w line_container
//     params: [
//       {id: 12, type: 'var', value: 'z'},
//       {id: 13, type: '='},
//       {
//         id: 14,
//         type: 'round_brackets',
//         // parametry mapowane w line_container
//         params: [
//           {id: 15, type: 'str', value: 'my'},
//           {id: 16, type: '+'},
//           {
//             id: 17,
//             type: 'var',
//             value: 'arr1',
//             params: [
//               // parametry mapowane w line_container
//               {
//                 id: 18,
//                 type: 'list_item_ext',
//                 params: [{id: 19, type: 'num', value: 1}], // parametry mapowane w line_container
//               },
//               {id: 20, type: 'var_ext', value: 'y'},
//               {
//                 id: 1893,
//                 type: 'list_range _ext',
//                 params: [
//                   // dwa parametry specyficzne
//                   {
//                     id: 1903,
//                     type: 'line',
//                     params: [{id: 194234, type: 'num', value: 1}], // parametry mapowane w line_container
//                   },
//                   {
//                     id: 1903,
//                     type: 'line',
//                     params: [{id: 134539, type: 'num', value: 2}], // parametry mapowane w line_container
//                   },
//                 ],
//               },
//             ],
//           },
//         ],
//       },
//       {id: 21, type: '/'},
//       {id: 22, type: 'num', value: 2},
//     ],
//   },
//   {
//     id: 23,
//     type: 'for_in',
//     params: [
//       // dwa parametry specyficzne
//       {id: 116, type: 'line', params: [{id: 24, type: 'var', value: 'i'}]}, // parametry mapowane w line_container
//       {id: 117, type: 'line', params: [{id: 25, type: 'var', value: 'arr1'}]}, // parametry mapowane w line_container
//     ],
//     block: [
//       {
//         id: 177,
//         type: 'fun_def',
//         value: 'my_fun2',
//         params: [
//           // parametry mapowane w comma_container
//           {id: 277, type: 'var', value: 'y'},
//           {id: 27721, type: 'var', value: 'x'},
//         ],
//         block: [
//           {
//             id: 111833,
//             type: 'line',
//             params: [
//               // parametry mapowane w line_container
//               {id: 3737, type: 'var', value: 'idx'},
//               {id: 13399, type: '='},
//               {id: 4737, type: 'num', value: 666},
//             ],
//           },
//           {
//             id: 3773,
//             type: 'return',
//             params: [
//               // parametry mapowane w line_container
//               {id: 4773, type: 'var', value: 'idx'},
//               {id: 16937, type: '+'},
//               {id: 4737, type: 'num', value: 9},
//             ],
//           },
//         ],
//       },
//       {
//         id: 118,
//         type: 'line',
//         params: [
//           // parametry mapowane w line_container
//           {id: 26, type: 'var', value: 'c'},
//           {id: 27, type: '='},
//           {id: 28, type: 'var', value: 'i'},
//           {id: 29, type: '+'},
//           {
//             id: 30,
//             type: 'fun',
//             value: 'fun1',
//             params: [{id: 31, type: 'var', value: 'i'}], // parametry mapowane w comma_container
//           },
//         ],
//       },
//     ],
//   },
// ];

// const testCode = [
//   {
//     type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//     params: [
//       {type: UNIT_TYPES.VARIABLE, value: 'variable1'},
//       {type: UNIT_TYPES.EQUATION_SIGN},
//       {type: UNIT_TYPES.VARIABLE, value: 'a'},
//       {type: UNIT_TYPES.ADDITION_SIGN},
//       {type: UNIT_TYPES.VARIABLE, value: 'b'},
//       {type: UNIT_TYPES.ADDITION_SIGN},
//       {type: UNIT_TYPES.VARIABLE, value: 'c'},
//       {type: UNIT_TYPES.ADDITION_SIGN},
//       {type: UNIT_TYPES.VARIABLE, value: 'd'},
//       {type: UNIT_TYPES.ADDITION_SIGN},
//       {type: UNIT_TYPES.VARIABLE, value: 'e'},
//       {type: UNIT_TYPES.ADDITION_SIGN},
//       {type: UNIT_TYPES.VARIABLE, value: 'f'},
//     ],
//   },
//   {
//     type: UNIT_TYPES.FUNCTION_DEFINITION,
//     value: 'function1',
//     params: [
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [{type: UNIT_TYPES.VARIABLE, value: 'local_object'}],
//       },
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [{type: UNIT_TYPES.VARIABLE, value: 'x'}],
//       },
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [{type: UNIT_TYPES.VARIABLE, value: 'y'}],
//       },
//     ],
//     block: [
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [
//           {type: UNIT_TYPES.VARIABLE, value: 'var2'},
//           {type: UNIT_TYPES.EQUATION_SIGN},
//           {
//             type: UNIT_TYPES.VARIABLE,
//             value: 'local_object',
//             extension: [
//               {type: UNIT_TYPES.VARIABLE_EXTENSION, value: 'field1'},
//               {
//                 type: UNIT_TYPES.LIST_ITEM_EXTENSION,
//                 params: [
//                   {
//                     type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//                     params: [{type: UNIT_TYPES.VARIABLE, value: 'x'}],
//                   },
//                 ],
//               },
//             ],
//           },
//         ],
//       },
//     ],
//   },
//   {
//     type: UNIT_TYPES.FOR_IN_LOOP,
//     params: [
//       {
//         type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//         params: [
//           {type: UNIT_TYPES.VARIABLE, value: 'item1'},
//           {type: UNIT_TYPES.ADDITION_SIGN},
//           {type: UNIT_TYPES.VARIABLE, value: 'y'},
//         ],
//       },
//       {
//         type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//         params: [{type: UNIT_TYPES.VARIABLE, value: 'local_list1'}],
//       },
//     ],
//     block: [
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [
//           {type: UNIT_TYPES.VARIABLE, value: 'local_num'},
//           {type: UNIT_TYPES.EQUATION_SIGN},
//           {
//             type: UNIT_TYPES.FUNCTION_CALL,
//             value: 'function1',
//             params: [
//               {
//                 type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//                 params: [{type: UNIT_TYPES.VARIABLE, value: 'a'}],
//               },
//               {
//                 type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//                 params: [
//                   {type: UNIT_TYPES.VARIABLE, value: 'b'},
//                   {type: UNIT_TYPES.ADDITION_SIGN},
//                   {type: UNIT_TYPES.VARIABLE, value: 'c'},
//                 ],
//               },
//             ],
//           },
//         ],
//       },
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [
//           {type: UNIT_TYPES.VARIABLE, value: 'el'},
//           {type: UNIT_TYPES.EQUATION_SIGN},
//           {type: UNIT_TYPES.VARIABLE, value: 'item1'},
//         ],
//       },
//       {
//         type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//         params: [
//           {type: UNIT_TYPES.VARIABLE, value: 'var3'},
//           {type: UNIT_TYPES.EQUATION_SIGN},
//           {type: UNIT_TYPES.FUNCTION_CALL, value: 'function1', params: []},
//           {type: UNIT_TYPES.ADDITION_SIGN},
//           {type: UNIT_TYPES.VARIABLE, value: 'el'},
//         ],
//       },
//     ],
//   },
//   {
//     type: UNIT_TYPES.FOR_IN_LOOP,
//     params: [
//       {
//         type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//         params: [{type: UNIT_TYPES.VARIABLE, value: ''}],
//       },
//       {
//         type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//         params: [{type: UNIT_TYPES.VARIABLE, value: ''}],
//       },
//     ],
//     block: [],
//   },
//   {
//     type: UNIT_TYPES.REMOVABLE_LINE_CONTAINER,
//     params: [
//       {type: UNIT_TYPES.VARIABLE, value: 'obj1'},
//       {type: UNIT_TYPES.EQUATION_SIGN},
//       {
//         type: UNIT_TYPES.OBJECT_FIELD,
//         params: [
//           {
//             type: UNIT_TYPES.OBJECT_ITEM_FIELD,
//             value: 'f1',
//             params: [
//               {
//                 type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//                 params: [{type: UNIT_TYPES.VARIABLE, value: 'x'}],
//               },
//             ],
//           },
//           {
//             type: UNIT_TYPES.OBJECT_ITEM_FIELD,
//             value: 'f2',
//             params: [
//               {
//                 type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//                 params: [
//                   {type: UNIT_TYPES.VARIABLE, value: 's'},
//                   {type: UNIT_TYPES.ADDITION_SIGN},
//                   {type: UNIT_TYPES.VARIABLE, value: 'h'},
//                 ],
//               },
//             ],
//           },
//           {
//             type: UNIT_TYPES.OBJECT_ITEM_FIELD,
//             value: '',
//             params: [
//               {
//                 type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
//                 params: [{type: UNIT_TYPES.VARIABLE, value: ''}],
//               },
//             ],
//           },
//         ],
//       },
//     ],
//   },
// ];

const testCode = [];

export default testCode;
