import {UNIT_TYPES} from './types';

export const INPUT_FIELD_PARAMS = {
  [UNIT_TYPES.VARIABLE]: {
    keyboardType: 'default',
    regularExpression: /[^\w]/g,
    focusedInputFieldTextColor: 'white',
    blurredInputFieldTextColor: 'white',
  },
  [UNIT_TYPES.OBJECT_ITEM_FIELD]: {
    keyboardType: 'default',
    regularExpression: /[^\w]/g,
    focusedInputFieldTextColor: '#6495ed',
    blurredInputFieldTextColor: '#6495ed',
  },
  [UNIT_TYPES.FUNCTION_DEFINITION]: {
    keyboardType: 'default',
    regularExpression: /[^\w]/g,
    focusedInputFieldTextColor: '#6fdad1',
    blurredInputFieldTextColor: '#6fdad1',
  },
};
