import {UNIT_GROUP_TYPES, UNIT_TYPES} from './types';
import {ID_STEP} from './index';
import COLORS from 'CodeallMobile/src/theme/colors';

const ORANGE = '#F0AE49';
const BLACK = '#000000';
const GREEN = '#61D851';
const PINK = '#BA95E1';
const BLUE = '#4AA9E6';
const WHITE = '#FFFFFF';
const TEXT = COLORS.editorKeyboardButtonText;

const keyboardIcon = require('CodeallMobile/src/assets/img/keyboardButtons.png');

export const CUSTOM_BUTTONS = [
  {
    title: 'for_in_',
    color: ORANGE,
    textColor: TEXT,
    // type: UNIT_TYPES.FOR_IN_LOOP,
    groupType: UNIT_GROUP_TYPES.BLOCK_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.BLOCK_OBJECT,
      UNIT_GROUP_TYPES.MARGIN_OBJECT,
    ],
    getObject: (latestId) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.FOR_IN_LOOP,
      params: [
        {
          id: latestId + 3 * ID_STEP,
          type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
          params: [],
        },
        {
          id: latestId + 4 * ID_STEP,
          type: UNIT_TYPES.NON_REMOVABLE_LINE_CONTAINER,
          params: [],
        },
      ],
      block: [],
    }),
  },
  {
    title: 'def_()',
    color: GREEN,
    textColor: TEXT,
    // type: UNIT_TYPES.FUNCTION_DEFINITION,
    groupType: UNIT_GROUP_TYPES.BLOCK_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.BLOCK_OBJECT,
      UNIT_GROUP_TYPES.MARGIN_OBJECT,
    ],
    getObject: (latestId) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.FUNCTION_DEFINITION,
      value: '',
      params: [],
      block: [],
    }),
  },
  {
    title: 'var',
    color: BLUE,
    textColor: TEXT,
    iconRight: keyboardIcon,
    // type: UNIT_TYPES.VARIABLE,
    groupType: UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.BLOCK_OBJECT,
      UNIT_GROUP_TYPES.MARGIN_OBJECT,
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
      UNIT_GROUP_TYPES.LINE_CONTAINER_OBJECT,
    ],
    getObject: (latestId) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.VARIABLE,
      value: '',
    }),
  },
  {
    title: '{_}',
    color: BLUE,
    textColor: TEXT,
    // type: UNIT_TYPES.OBJECT_FIELD,
    groupType: UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.BLOCK_OBJECT,
      UNIT_GROUP_TYPES.MARGIN_OBJECT,
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
      UNIT_GROUP_TYPES.LINE_CONTAINER_OBJECT,
    ],
    getObject: (latestId) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.OBJECT_FIELD,
      params: [],
    }),
  },
  {
    title: '+_',
    color: PINK,
    arrowLeft: true,
    textColor: TEXT,
    // type: UNIT_TYPES.ADDITION_SIGN,
    groupType: UNIT_GROUP_TYPES.ARITHMETIC_SIGN,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.ARITHMETIC_SIGN,
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
    ],
    getObject: (latestId) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.ADDITION_SIGN,
    }),
  },
  {
    title: '=_',
    color: PINK,
    arrowLeft: true,
    textColor: TEXT,
    // type: UNIT_TYPES.EQUATION_SIGN,
    groupType: UNIT_GROUP_TYPES.ARITHMETIC_SIGN,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.ARITHMETIC_SIGN,
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
    ],
    getObject: (latestId) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.EQUATION_SIGN,
    }),
  },
];

export const VARIABLE_BUTTONS = [
  {
    prefix: '',
    postfix: '(...)',
    color: '#6fdad1',
    textColor: TEXT,
    type: UNIT_TYPES.FUNCTION_DEFINITION,
    groupType: UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.BLOCK_OBJECT,
      UNIT_GROUP_TYPES.MARGIN_OBJECT,
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
      UNIT_GROUP_TYPES.LINE_CONTAINER_OBJECT,
    ],
    getObject: (latestId, value) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.FUNCTION_CALL,
      value,
      params: [],
    }),
  },
  {
    prefix: '.',
    postfix: '',
    color: '#6495ed',
    textColor: TEXT,
    type: UNIT_TYPES.OBJECT_ITEM_FIELD,
    groupType: UNIT_GROUP_TYPES.VARIABLE_EXTENSION_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
      UNIT_GROUP_TYPES.VARIABLE_EXTENSION_OBJECT,
    ],
    getObject: (latestId, value) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.VARIABLE_EXTENSION,
      value,
    }),
  },
  {
    prefix: '',
    postfix: '',
    color: '#aaaaff',
    textColor: TEXT,
    type: UNIT_TYPES.VARIABLE,
    groupType: UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
    allowedFocusedGroupTypes: [
      UNIT_GROUP_TYPES.BLOCK_OBJECT,
      UNIT_GROUP_TYPES.MARGIN_OBJECT,
      UNIT_GROUP_TYPES.ARITHMETIC_OBJECT,
      UNIT_GROUP_TYPES.LINE_CONTAINER_OBJECT,
    ],
    getObject: (latestId, value) => ({
      id: latestId + 2 * ID_STEP,
      type: UNIT_TYPES.VARIABLE,
      value,
    }),
  },
];
