export const UNIT_TYPES = {
  // other objects
  OBJECT_ITEM_FIELD: 'OBJECT_ITEM_FIELD',
  // block objects
  FOR_IN_LOOP: 'FOR_IN_LOOP',
  FUNCTION_DEFINITION: 'FUNCTION_DEFINITION',
  // margin objects
  TOUCHABLE_MARGIN: 'TOUCHABLE_MARGIN',
  // arithmetic signs
  ADDITION_SIGN: 'ADDITION_SIGN',
  EQUATION_SIGN: 'EQUATION_SIGN',
  // arithmetic objects
  BRACKETS: 'BRACKETS',
  VARIABLE: 'VARIABLE',
  LIST_FIELD: 'LIST_FIELD',
  TUPLE_FIELD: 'TUPLE_FIELD',
  OBJECT_FIELD: 'OBJECT_FIELD',
  STRING_FIELD: 'STRING_FIELD',
  NUMBER_FIELD: 'NUMBER_FIELD',
  FUNCTION_CALL: 'FUNCTION_CALL',
  // line container objects
  REMOVABLE_LINE_CONTAINER: 'REMOVABLE_LINE_CONTAINER',
  NON_REMOVABLE_LINE_CONTAINER: 'NON_REMOVABLE_LINE_CONTAINER',
  // variable extensions
  VARIABLE_EXTENSION: 'VARIABLE_EXTENSION',
  LIST_ITEM_EXTENSION: 'LIST_ITEM_EXTENSION',
  LIST_RANGE_EXTENSION: 'LIST_RANGE_EXTENSION',
};

export const UNIT_GROUP_TYPES = {
  OTHER_OBJECT: 'OTHER_OBJECT',
  BLOCK_OBJECT: 'BLOCK_OBJECT',
  MARGIN_OBJECT: 'MARGIN_OBJECT',
  ARITHMETIC_SIGN: 'ARITHMETIC_SIGN',
  ARITHMETIC_OBJECT: 'ARITHMETIC_OBJECT',
  LINE_CONTAINER_OBJECT: 'LINE_CONTAINER_OBJECT',
  VARIABLE_EXTENSION_OBJECT: 'VARIABLE_EXTENSION_OBJECT',
};

// gdy dodajemy w pustej linijce zmienna (jest na to niepotrzebna funkcja)

// number
// true
// brackets
// double list
// TUPLE_FIELD: 'TUPLE_FIELD'                           (1, 2).pow()         (1, 2)[2]
// VARIABLE: 'VARIABLE',            var1.var2           var1.pow()           var1[3]
// LIST_FIELD: 'LIST_FIELD',                            [x ,y].pow()         [x, y][2]
// OBJECT_FIELD: 'OBJECT_FIELD',    {var2: 1}.var2      {var2: 1}.own()      {var2: 1}["var2"]
// STRING_FIELD: 'STRING_FIELD',                        "xyz".split()        "xyz"[2]
// FUNCTION_CALL: 'FUNCTION_CALL',  fun1().var          fun1().fun2()        fun1()[3]
