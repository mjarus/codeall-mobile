import {EDITOR_STYLES, KEY_WORDS} from './assets';

const COMMENTS = '"""|\'\'\'|#.*';
const STRINGS =
  '"(?:|(?:\\\\"|[^"])*[^\\\\"])"|\'(?:|(?:\\\\\'|[^\'])*[^\\\\\'])\'';
const WORDS = '[_a-zA-Z_][_a-zA-Z0-9]*';
const NUMBERS = '[.0-9]*[0-9]+';
const OPERATORS = '[\\.]+|[`~!@$%^&=:;<,>/?|\\*\\(\\)\\-\\+\\{\\}\\[\\]\\\\]+';
const DOTS = '[•]+';
const COMMENTS_REGEXP = new RegExp(`^(?:${COMMENTS})$`);
const STRINGS_REGEXP = new RegExp(`^(?:${STRINGS})$`);
const WORDS_REGEXP = new RegExp(`^${WORDS}$`);
const NUMBERS_REGEXP = new RegExp(`^${NUMBERS}$`);
const OPERATORS_REGEXP = new RegExp(`^(?:${OPERATORS})$`);
const DOTS_REGEXP = new RegExp(`^${DOTS}$`);
const MAIN_REGEXP = new RegExp(
  `(${COMMENTS}|${STRINGS}|${WORDS}|${NUMBERS}|${OPERATORS}|${DOTS})`,
);

export const splitToElements = value => {
  return value.split(MAIN_REGEXP).filter(Boolean);
};

export const createCommentTokensArray = array => {
  var outputArray = [];
  for (var i = 0; i < array.length; i++) {
    if (/'''|"""/.test(array[i])) {
      const itemsArray = splitToElements(array[i]);
      for (var j = 0; j < itemsArray.length; j++) {
        if (/^\s*(?:''')$/.test(itemsArray[j])) {
          outputArray.push({line: i, item: j, type: 0});
        } else if (/^\s*(?:""")$/.test(itemsArray[j])) {
          outputArray.push({line: i, item: j, type: 1});
        }
      }
    }
  }
  return outputArray;
};

export const createBlockSectionsArray = array => {
  var commentMarker = [false, false];
  var outputArray = [];
  for (var k = 0; k < array.length; k++) {
    switch (true) {
      case commentMarker[0] && array[k].type === 0:
        commentMarker[0] = false;
        outputArray[outputArray.length - 1] = {
          ...outputArray[outputArray.length - 1],
          endLine: array[k].line,
          endItem: array[k].item,
        };
        break;
      case commentMarker[1] && array[k].type === 1:
        commentMarker[1] = false;
        outputArray[outputArray.length - 1] = {
          ...outputArray[outputArray.length - 1],
          endLine: array[k].line,
          endItem: array[k].item,
        };
        break;
      case !commentMarker[1] && array[k].type === 0:
        commentMarker[0] = true;
        outputArray.push({
          startLine: array[k].line,
          startItem: array[k].item,
          endLine: null,
          endItem: null,
        });
        break;
      case !commentMarker[0] && array[k].type === 1:
        commentMarker[1] = true;
        outputArray.push({
          startLine: array[k].line,
          startItem: array[k].item,
          endLine: null,
          endItem: null,
        });
        break;
    }
  }
  return outputArray;
};

export const getForceUpdateIndex = (array, arrayLast) => {
  var value = null;
  for (var i = 0; i < array.length || i < arrayLast.length; i++) {
    if (!array[i]) {
      value = arrayLast[i].startLine;
      break;
    } else if (!arrayLast[i]) {
      value = array[i].startLine;
      break;
    } else if (JSON.stringify(array[i]) !== JSON.stringify(arrayLast[i])) {
      if (
        array[i].startLine !== arrayLast[i].startLine ||
        array[i].startItem !== arrayLast[i].startItem
      ) {
        value = Math.min(array[i].startLine, arrayLast[i].startLine);
        break;
      } else {
        if (arrayLast[i].endLine === null) {
          value = array[i].endLine;
          break;
        } else if (array[i].endLine === null) {
          value = arrayLast[i].endLine;
          break;
        } else {
          value = Math.min(array[i].endLine, arrayLast[i].endLine);
          break;
        }
      }
    }
  }
  return value;
};

export const getCommentMarker = (array, lineIndex, itemIndex) => {
  var value = false;
  for (var k = 0; k < array.length; k++) {
    if (
      lineIndex >= array[k].startLine &&
      (lineIndex <= array[k].endLine || array[k].endLine === null) &&
      ((lineIndex > array[k].startLine &&
        (lineIndex < array[k].endLine || array[k].endLine === null)) ||
        (array[k].startLine !== array[k].endLine &&
          ((lineIndex === array[k].startLine &&
            itemIndex >= array[k].startItem) ||
            (lineIndex === array[k].endLine &&
              itemIndex <= array[k].endItem))) ||
        (array[k].startLine === array[k].endLine &&
          lineIndex === array[k].startLine &&
          itemIndex >= array[k].startItem &&
          itemIndex <= array[k].endItem))
    ) {
      value = true;
      break;
    }
  }
  return value;
};

const getKeywordStyle = (value, nextValue) => {
  for (var i = 0; i < KEY_WORDS.length; i++) {
    if (KEY_WORDS[i].objects.includes(value)) {
      return KEY_WORDS[i].style;
    }
  }
  if (nextValue && nextValue[0] === '(') {
    return EDITOR_STYLES.functions;
  }
  return EDITOR_STYLES.variables;
};

export const getElementStyle = (value, nextValue) => {
  switch (true) {
    case DOTS_REGEXP.test(value):
      return EDITOR_STYLES.dots;
    case OPERATORS_REGEXP.test(value):
      return EDITOR_STYLES.operators;
    case NUMBERS_REGEXP.test(value):
      return EDITOR_STYLES.numbers;
    case STRINGS_REGEXP.test(value):
      return EDITOR_STYLES.strings;
    case COMMENTS_REGEXP.test(value):
      return EDITOR_STYLES.comments;
    case WORDS_REGEXP.test(value):
      return getKeywordStyle(value, nextValue);
    default:
      return EDITOR_STYLES.errors;
  }
};
