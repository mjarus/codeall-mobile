import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const mainContainer = {
  flex: 1,
  backgroundColor: COLORS.editor.background,
};

const horizontalScrollContainer = {
  flexGrow: 1,
};

const verticalScrollContainer = {
  flexGrow: 1,
  flexDirection: 'row',
};

const lineNumbersContainer = {
  position: 'absolute',
  top: 0,
  left: 0,
  width: 50,
  zIndex: 1,
  paddingTop: 10,
  borderRightWidth: 1,
  height: '100%',
  alignItems: 'center',
  borderColor: COLORS.background,
  textAlignVertical: 'top',
  justifyContent: 'flex-start',
};

const lineNmbers = {
  fontSize: 17,
  lineHeight: 37,
  color: COLORS.editor.lineNumbers,
  textAlign: 'center',
  fontFamily: 'monospace',
};

const textInput = {
  flexGrow: 1,
  padding: 10,
  paddingLeft: 60,
  fontSize: 17,
  lineHeight: 37,
  textAlign: 'left',
  fontFamily: 'monospace',
  textAlignVertical: 'top',
};

const debugContainer = {
  position: 'absolute',
  right: 0,
  top: 40,
  backgroundColor: '#000000AA',
};

const debugText1 = {
  color: 'red',
  fontSize: 10,
  fontFamily: 'monospace',
};

const debugText2 = {
  color: 'orange',
  fontSize: 10,
  fontFamily: 'monospace',
};

const keyboardChangeButton = {
  position: 'absolute',
  right: 0,
  top: 3,
  width: 55,
  height: 35,
  backgroundColor: '#f4d03f',
  alignItems: 'center',
  justifyContent: 'center',
  borderRadius: 3,
};

export const styles = StyleSheet.create({
  mainContainer,
  lineNumbersContainer,
  lineNmbers,
  textInput,
  debugContainer,
  debugText1,
  debugText2,
  keyboardChangeButton,
  horizontalScrollContainer,
  verticalScrollContainer,
});
