import React, {useContext, useEffect, useState, useRef} from 'react';
import {ScrollView, Text, View, TextInput} from 'react-native';
import {connect} from 'react-redux';
import {postChatBubbles, getStep} from '../../../store/chat/actions';
import {
  addSharedEditorChatMessage,
  sendSharedEditorChatMessageDone,
} from '../../../store/editor/actions';
import {
  verifySourceCode,
  setFlashMessage,
  saveSourceCodeForStep,
} from '../../../store/editor/actions';
import {
  splitToElements,
  createCommentTokensArray,
  createBlockSectionsArray,
  getForceUpdateIndex,
  getCommentMarker,
  getElementStyle,
} from './functions';
import BottomKeys from './components/BottomKeys';
import socketIO from 'socket.io-client';
import {EDITOR_STYLES} from './assets';
import {styles} from './styles';
import {SmartBoxContext} from '../../../utils/SmartBoxProvider';

const usePrevious = value => {
  const ref = useRef();
  useEffect(() => {
    ref.current = value;
  });
  return ref.current;
};

const Editor = props => {
  const messProp = text => ({top: 100, text});
  const MEMORY = 50;
  const DEFAULT_CODE = '';

  let jsxArray = [];
  let strArrayLast = [];
  let commentActiveTokensLast = [];

  const lesson = props.route?.params?.lesson;

  const SmartBox = useContext(SmartBoxContext);

  const [codeTab, setCodeTab] = useState([DEFAULT_CODE]);
  const [codeTabIndex, setCodeTabIndex] = useState(0);
  const [socket, setSocket] = useState(null); // todo maybe change to useRef? needs test
  const [sharedEditorUserId, setSharedEditorUserId] = useState(null);
  const [
    sharedEditorConnectedTeacherId,
    setSharedEditorConnectedTeacherId,
  ] = useState(null);
  const [isTextInputEnabled, setIsTextInputEnabled] = useState(true);

  const prevStep = usePrevious(props.currentStep);

  useEffect(() => {
    if (!socket) {
      const newSocket = socketIO('ws://dev.expans.io:3000', {
        transports: ['websocket'],
        jsonp: false,
      });
      setSocket(newSocket);
      newSocket.connect();
      newSocket.on('connect', () => {
        // console.log('connected to socket server');
      });

      newSocket.on('message', message => {
        switch (message.operation) {
          case 'login':
            sharedEditorHandleLogin(message);
            break;
          case 'offer':
            sharedEditorHandleOffer(message);
            break;
          case 'answer':
            sharedEditorHandleAnswer(message);
            break;
          case 'code':
            sharedEditorHandleCode(message);
            break;
          case 'lock':
            sharedEditorHandleLock(message);
            break;
          case 'leave':
            sharedEditorHandleLeave(true);
            break;
          case 'chatMessage':
            sharedEditorHandleChatMessage(message);
            break;
          default:
            break;
        }
      });
    }

    const currentBubble = props.currentLesson[0];
    if (currentBubble) {
      props.getStep({stepId: currentBubble.stepId}).then(response => {
        if (response.payload && response.payload.step) {
          if (
            response.payload.step.sourceCode &&
            response.payload.step.sourceCode.sourceCode
          ) {
            updateCodeTab(response.payload.step.sourceCode.sourceCode);
          } else if (response.payload.step.baseSourceCode.length > 0) {
            updateCodeTab(response.payload.step.baseSourceCode);
          }
        }
      });
    }
  }, []);

  useEffect(() => {
    if (socket && !sharedEditorUserId) {
      sharedEditorSend({
        operation: 'login',
        userId: props.userId,
        authToken: props.userToken,
      });
    }

    if (props.sharedEditorSendChatMessagePending) {
      const message =
        props.sharedEditorChatMessages[
          props.sharedEditorChatMessages.length - 1
        ].content;

      sharedEditorSendChatMessage(message);
      props.sendSharedEditorChatMessageDone();
    }

    if (
      props.currentStep &&
      (!prevStep || props.currentStep.id !== prevStep.id) &&
      props.currentStep.baseSourceCode.length > 0
    ) {
      updateCodeTab(props.currentStep.baseSourceCode);
    }
  }, [props.currentStep]);

  const connectTeacher = () => {
    sharedEditorSend({
      operation: 'offer',
    });
    props.setFlashMessage(messProp('Wysłano prośbę o połączenie.'));
  };

  const disconnectTeacher = () => {
    sharedEditorSend({
      operation: 'leave',
    });
    sharedEditorHandleLeave(false);
  };

  const enableEditor = () => {
    setIsTextInputEnabled(true);
  };

  const disableEditor = () => {
    setIsTextInputEnabled(false);
  };

  const sharedEditorSend = message => {
    if (sharedEditorConnectedTeacherId) {
      message.recipient = sharedEditorConnectedTeacherId;
    }
    if (sharedEditorUserId) {
      message.sender = sharedEditorUserId;
    }

    socket.emit('message', message);
  };

  const sharedEditorSendChatMessage = message => {
    sharedEditorSend({
      operation: 'chatMessage',
      message: message,
    });
  };

  const sharedEditorHandleChatMessage = data => {
    props.addSharedEditorChatMessage({
      message: data.message,
      isUserMessage: false,
    });
  };

  const sharedEditorHandleOffer = data => {
    if (!data.success) {
      props.setFlashMessage(
        messProp('Brak dostępnych nauczycieli. Spróbuj później.'),
      );
    }
  };

  const sharedEditorHandleLogin = data => {
    if (data.success) {
      setSharedEditorUserId(data.user.id);
    } else {
      console.warn(data.error.userMessage);
    }
  };

  const sharedEditorHandleAnswer = data => {
    if (data.success) {
      props.setFlashMessage(messProp('Połączono z nauczycielem.'));
      setSharedEditorConnectedTeacherId(data.sender);
      disableEditor();
      sharedEditorSendCode();
    } else {
      if (data.message) {
        console.warn(data.message);
      }
    }
  };

  const sharedEditorHandleCode = data => {
    updateCodeTab(data.code);
  };

  const sharedEditorHandleLock = data => {
    if (data.currentOwner === sharedEditorUserId) {
      props.setFlashMessage(
        messProp('Nauczyciel przekazał Ci kontrolę. Możesz pisać.'),
      );
      enableEditor();
    } else {
      props.setFlashMessage(
        messProp('Nauczyciel wprowadza zmiany w kodzie. Zaczekaj.'),
      );
      disableEditor();
    }
  };

  const sharedEditorHandleLeave = remote => {
    setSharedEditorConnectedTeacherId(null);
    enableEditor();
    if (remote) {
      props.setFlashMessage(messProp('Nauczyciel zakończył połączenie.'));
    } else {
      props.setFlashMessage(messProp('Połączenie zostało zakończone.'));
    }
  };

  const sharedEditorSendCode = () => {
    sharedEditorSend({
      operation: 'code',
      code: getCodeFromEditor(),
    });
  };

  const getCodeFromEditor = () => {
    const sourceCode = codeTab[codeTabIndex];
    return sourceCode.replace(/•/g, ' ');
  };

  const updateCodeTab = code => {
    const max = codeTabIndex + 1 >= MEMORY;
    const newCodeTabIndex = max ? codeTabIndex : codeTabIndex + 1;
    const start = max ? 1 : 0;
    const newCodeTab = [...codeTab.slice(start, codeTabIndex + 1), code];
    setCodeTab(newCodeTab);
    setCodeTabIndex(newCodeTabIndex);
  };

  const onUndo = () => {
    const index = codeTabIndex;
    if (index > 0) {
      setCodeTabIndex(index - 1);
    }
  };

  const onRedo = () => {
    const index = codeTabIndex;
    const length = codeTab.length;
    if (length > index + 1) {
      setCodeTabIndex(index + 1);
    }
  };

  const sendCode = () => {
    if (!lesson) {
      const sourceCode = getCodeFromEditor();
      SmartBox.sendFile(sourceCode);
      return;
    }

    const {currentLesson, isVerfificationPending, chatBubbelsPending} = props;
    const currentBubble = currentLesson[0];
    const unlocked = currentBubble && currentBubble.status === 'unlocked';
    if (lesson && currentBubble && unlocked && !isVerfificationPending) {
      const sourceCode = getCodeFromEditor();
      const stepId = (currentBubble && currentBubble.stepId) || 0;

      props
        .saveSourceCodeForStep({stepId, sourceCode})
        .then(saveSourceCodeResponse => {
          const saveData = saveSourceCodeResponse.payload.data;
          const saveError = saveSourceCodeResponse.payload.error;
          if (saveData) {
            props.postChatBubbles({chatBubbleId: currentBubble.id});

            // todo refactor - ni mo czasu, teraz to już się zrobił niezły bajzel hihi
            const sourceCodeWithHeader =
              props.currentStep.sourceCodeHeader.length > 0
                ? props.currentStep.sourceCodeHeader + '\n' + sourceCode
                : sourceCode;

            SmartBox.sendFile(sourceCodeWithHeader);

            props
              .verifySourceCode({stepId, sourceCode})
              .then(verifyResponse => {
                // console.warn(verifyResponse.payload.data.error);
                const correct = verifyResponse.payload.data.correct;
                const dataError = verifyResponse.payload.data.error;
                const generalError = verifyResponse.payload.error;
                if (correct === true && !chatBubbelsPending) {
                  props.setFlashMessage(
                    messProp('Doskonale, kod jest poprawny!'),
                  );
                } else if (correct === false) {
                  const line = dataError.line.toString();
                  props.setFlashMessage(
                    messProp(`Kod niepoprawny, sprawdź linię nr ${line}`),
                  );
                } else if (dataError) {
                  const message = dataError.userMessage
                    ? dataError.userMessage
                    : dataError.message || '';
                  const codeNo = dataError.code ? `(${dataError.code})` : '';
                  props.setFlashMessage(messProp(`${message}${codeNo}`));
                } else if (generalError) {
                  const message = generalError.userMessage
                    ? generalError.userMessage
                    : generalError.message || '';
                  const codeNo = generalError.code
                    ? `(${generalError.code})`
                    : '';
                  props.setFlashMessage(messProp(`${message}${codeNo}`));
                } else {
                  props.setFlashMessage(messProp('Nieznany błąd'));
                }
              });
          } else {
            const message = saveError.userMessage
              ? saveError.userMessage
              : saveError.message || '';
            const codeNo = saveError.code ? `(${saveError.code})` : '';
            props.setFlashMessage(messProp(`${message}${codeNo}`));
          }
        });
    } else {
      const pendingError = 'Weryfikacja trwa';
      const lessonError =
        'Aby zweryfikować kod, musisz przejść do aktywnej lekcji';
      const bubbleError = 'Lekcja zablokowana';
      const lockedError = 'To zadanie zostało już wykonane';
      const massage = !lesson
        ? lessonError
        : !currentBubble
        ? bubbleError
        : !unlocked
        ? lockedError
        : pendingError;
      props.setFlashMessage(messProp(massage));
    }
  };

  const keyboardOnPress = value => {
    switch (value) {
      case 'clear':
        updateCodeTab('');
        break;
      case 'play':
        sendCode();
        break;
      case 'undo':
        onUndo();
        break;
      case 'redo':
        onRedo();
        break;
      case 'sharedEditorConnectTeacher':
        connectTeacher();
        break;
      case 'sharedEditorDisconnectTeacher':
        disconnectTeacher();
        break;
    }
  };

  const renderLinesNumbers = () => {
    const length = codeTab[codeTabIndex].split('\n').length;
    let numbers = '1';
    for (let i = 2; i <= length; i++) {
      numbers = numbers + '\n' + i;
    }
    return (
      <View style={styles.lineNumbersContainer}>
        <Text numberOfLines={length} style={styles.lineNmbers}>
          {numbers}
        </Text>
      </View>
    );
  };

  const renderCode = () => {
    const code = codeTab[codeTabIndex];
    const dottedCode = code.replace(/[ ]/g, '•');
    const strArray = dottedCode.split('\n');
    const commentTokens = createCommentTokensArray(strArray);
    const commentActiveTokens = createBlockSectionsArray(commentTokens);
    const forceUpdateIndex = getForceUpdateIndex(
      commentActiveTokens,
      commentActiveTokensLast,
    );
    jsxArray = jsxArray.slice(0, strArray.length);
    for (let i = 0; i < strArray.length; i++) {
      if (
        strArray[i] !== strArrayLast[i] ||
        (strArrayLast.length < strArray.length && i > strArray.length - 3) ||
        (strArrayLast.length > strArray.length && i > strArray.length - 2) ||
        (forceUpdateIndex !== null && i >= forceUpdateIndex)
      ) {
        jsxArray[i] = [];
        const itemsArray = splitToElements(strArray[i]);
        for (let j = 0; j < itemsArray.length; j++) {
          const style = getCommentMarker(commentActiveTokens, i, j)
            ? EDITOR_STYLES.comments
            : getElementStyle(itemsArray[j], itemsArray[j + 1]);
          jsxArray[i].push(
            <Text key={`line${i}-obj${j}`} style={style}>
              {itemsArray[j]}
              {i + 1 < strArray.length && itemsArray.length === j + 1 && '\n'}
            </Text>,
          );
        }
        if (i + 1 < strArray.length && itemsArray.length === 0) {
          jsxArray[i].push(<Text key={`line${i}-0`}>{'\n'}</Text>);
        }
      }
    }
    strArrayLast = strArray;
    commentActiveTokensLast = commentActiveTokens;
    return jsxArray;
  };

  const onChangeText = code => {
    updateCodeTab(code);
    if (sharedEditorConnectedTeacherId) {
      sharedEditorSendCode();
    }
  };

  return (
    <View style={styles.mainContainer}>
      <ScrollView
        horizontal
        contentContainerStyle={styles.horizontalScrollContainer}>
        <ScrollView contentContainerStyle={styles.verticalScrollContainer}>
          {renderLinesNumbers()}
          <TextInput
            editable={isTextInputEnabled}
            multiline
            autoCorrect={false}
            spellCheck={false}
            autoCapitalize="none"
            autoCompleteType="off"
            importantForAutofill="no"
            textContentType="none"
            underlineColorAndroid="transparent"
            style={styles.textInput}
            onChangeText={onChangeText}>
            {renderCode()}
          </TextInput>
        </ScrollView>
      </ScrollView>
      <BottomKeys
        keyboardOnPress={keyboardOnPress}
        isTeacherConnected={sharedEditorConnectedTeacherId !== null}
      />
    </View>
  );
};

const mapStateToProps = state => ({
  currentLesson: state.chat.currentLesson,
  currentStep: state.chat.currentStep,
  chatBubbelsPending: state.chat.chatBubbelsPending,
  isVerfificationPending: state.editor.isVerfificationPending,
  sharedEditorChatMessages: state.editor.sharedEditorChatMessages,
  sharedEditorSendChatMessagePending:
    state.editor.sharedEditorSendChatMessagePending,
  userId: state.auth.userId,
  userToken: state.auth.token,
});

const mapDispatchToProps = dispatch => ({
  verifySourceCode: data => dispatch(verifySourceCode(data)),
  setFlashMessage: data => dispatch(setFlashMessage(data)),
  postChatBubbles: data => dispatch(postChatBubbles(data)),
  saveSourceCodeForStep: data => dispatch(saveSourceCodeForStep(data)),
  getStep: data => dispatch(getStep(data)),
  addSharedEditorChatMessage: data =>
    dispatch(addSharedEditorChatMessage(data)),
  sendSharedEditorChatMessageDone: data =>
    dispatch(sendSharedEditorChatMessageDone(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Editor);
