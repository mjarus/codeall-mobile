import React from 'react';
import {View, Image, TouchableOpacity, Keyboard, Text} from 'react-native';
import {styles} from './styles';

const HideKeyboard = require('CodeallMobile/src/assets/img/hideKeyboard.png');
const PlayIcon = require('CodeallMobile/src/assets/img/playButton.png');
const Redo = require('CodeallMobile/src/assets/img/redo.png');
const Undo = require('CodeallMobile/src/assets/img/undo.png');

const BottomKeys = props => {
  return (
    <View style={styles.mainBox}>
      <TouchableOpacity
        style={styles.lineBox}
        onPress={() => Keyboard.dismiss()}>
        <Image source={HideKeyboard} style={styles.keyboardIcon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.lineBox}
        onPress={() => props.keyboardOnPress('clear')}>
        <Text style={styles.clearKey}>CLR</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.lineBox}
        onPress={() => props.keyboardOnPress('play')}>
        <Image source={PlayIcon} style={styles.customIcon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.lineBox}
        onPress={() =>
          props.keyboardOnPress(
            props.isTeacherConnected
              ? 'sharedEditorDisconnectTeacher'
              : 'sharedEditorConnectTeacher',
          )
        }>
        <Text>{props.isTeacherConnected ? 'Rozłącz' : 'Połącz'}</Text>
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.lineBox}
        onPress={() => props.keyboardOnPress('undo')}>
        <Image source={Undo} style={styles.customIcon} />
      </TouchableOpacity>
      <TouchableOpacity
        style={styles.lineBox}
        onPress={() => props.keyboardOnPress('redo')}>
        <Image source={Redo} style={styles.customIcon} />
      </TouchableOpacity>
    </View>
  );
};

export default BottomKeys;
