import {StyleSheet, Dimensions} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
const WINDOW_WIDTH = Dimensions.get('window').width;

const mainBox = {
  width: '100%',
  flexDirection: 'row',
  height: WINDOW_WIDTH / 7,
  backgroundColor: COLORS.ice,
  borderTopWidth: 1,
  borderBottomWidth: 1,
  borderColor: COLORS.ligthGrey,
};

const lineBox = {
  flex: 1,
  alignItems: 'center',
  justifyContent: 'center',
  borderRightWidth: 0.5,
  borderLeftWidth: 0.5,
  borderColor: COLORS.ligthGrey,
};

const customIcon = {
  width: 35,
  height: 35,
};

const keyboardIcon = {
  height: 30,
  width: 30,
};

const clearKey = {
  color: '#AA7777',
  fontSize: 18,
  fontWeight: 'bold',
};

export const styles = StyleSheet.create({
  mainBox,
  lineBox,
  customIcon,
  keyboardIcon,
  clearKey,
});
