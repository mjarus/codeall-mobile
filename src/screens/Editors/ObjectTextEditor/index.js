import React, {Component} from 'react';
import {ScrollView, View, TouchableOpacity, Text} from 'react-native';
import {connect} from 'react-redux';
import {
  verifySourceCode,
  setFlashMessage,
  saveSourceCodeForStep,
} from '../../../store/editor/actions';
import {postChatBubbles} from '../../../store/chat/actions';
import {
  getNewObj,
  isEmptyInput,
  getCodeString,
  changeObjValue,
  addObjFunction,
  deleteObjFunction,
  replaceObjFunction,
} from './functions';
import TouchableMargin from './components/TouchableMargin';
import Block from './components/Block';
import BottomKeys from './components/BottomKeys';
import {styles} from './styles';

const MEMORY = 50;
const messProp = text => ({top: 100, text});

class Editor extends Component {
  constructor(props) {
    super(props);
    this.refsArray = [];
    this.defaultCode = [getNewObj({id: 1})];
    this.params = this.props.route.params;
    this.lesson = this.params && this.params.lesson;
    this.state = {
      codeTab: [this.defaultCode],
      codeTabIndex: 0,
      globalID: 1,
      scale: 1,
      focusID: null,
      fousType: null,
      lastEditedID: null,
      modalViewID: null, // TODO removed after double-focus input implementation
      systemKeyboard: true,
    };
  }

  sendCode = () => {
    const {
      currentLesson,
      isVerfificationPending,
      chatBubbelsPending,
    } = this.props;
    const currentBubble = currentLesson[0];
    const codeRequired =
      currentBubble && currentBubble.isCodeRequiredToContinue === true;
    const unlocked = currentBubble && currentBubble.status === 'unlocked';
    if (
      this.lesson &&
      currentBubble &&
      unlocked &&
      codeRequired &&
      !isVerfificationPending
    ) {
      const code = this.state.codeTab[this.state.codeTabIndex];
      const sourceCode = code.map(item => getCodeString(item) + '\n').join('');
      const stepId = (currentBubble && currentBubble.stepId) || 0;
      this.props.verifySourceCode({stepId, sourceCode}).then(verifyResponse => {
        const correct = verifyResponse.payload.data.correct;
        const dataError = verifyResponse.payload.data.error;
        const generalError = verifyResponse.payload.error;
        if (correct === true && !chatBubbelsPending) {
          this.props
            .saveSourceCodeForStep({stepId, sourceCode})
            .then(saveResponse => {
              const saveData = saveResponse.payload.data;
              const saveError = saveResponse.payload.error;
              if (saveData) {
                this.props.postChatBubbles({chatBubbleId: currentBubble.id});
                this.props.setFlashMessage(
                  messProp('Doskonale, kod jest poprawny!'),
                );
              } else {
                const message = saveError.userMessage
                  ? saveError.userMessage
                  : saveError.message || '';
                const codeNo = saveError.code ? `(${saveError.code})` : '';
                this.props.setFlashMessage(messProp(`${message}${codeNo}`));
              }
            });
        } else if (correct === false) {
          const name = dataError && dataError.name ? dataError.name : 'Błąd';
          const message =
            dataError && dataError.message ? `: ${dataError.message}` : '';
          this.props.setFlashMessage(messProp(`${name}${message}`));
        } else if (dataError) {
          const message = dataError.userMessage
            ? dataError.userMessage
            : dataError.message || '';
          const codeNo = dataError.code ? `(${dataError.code})` : '';
          this.props.setFlashMessage(messProp(`${message}${codeNo}`));
        } else if (generalError) {
          const message = generalError.userMessage
            ? generalError.userMessage
            : generalError.message || '';
          const codeNo = generalError.code ? `(${generalError.code})` : '';
          this.props.setFlashMessage(messProp(`${message}${codeNo}`));
        } else {
          this.props.setFlashMessage(messProp('Nieznany błąd'));
        }
      });
    } else {
      const requiredError = 'W tym zadaniu nie można zweryfikować kodu';
      const pendingError = 'Weryfikacja trwa';
      const lessonError =
        'Aby zweryfikować kod, musisz przejść do aktywnej lekcji';
      const bubbleError = 'Lekcja zablokowana';
      const lockedError = 'To zadanie zostało już wykonane';
      const massage = !this.lesson
        ? lessonError
        : !currentBubble
        ? bubbleError
        : !codeRequired
        ? requiredError
        : !unlocked
        ? lockedError
        : pendingError;
      this.props.setFlashMessage(messProp(massage));
    }
  };

  systemKeyboardChange = systemKeyboard => {
    this.setState({systemKeyboard});
  };

  keyboardOnPress = (type, value) => {
    if (type === 1) {
      this.replaceObjMethod(value);
    } else if (type === 2) {
      switch (value) {
        case 'delete':
          this.deleteObjMethod(this.state.focusID);
          break;
        case 'play':
          this.sendCode();
          break;
        case 'undo':
          this.onUndo();
          break;
        case 'redo':
          this.onRedo();
          break;
      }
    }
  };

  focusOrBlurObject = (id, focus) => {
    if (this[`ref-${id}`] && this[`ref-${id}`].current) {
      if (focus) {
        this[`ref-${id}`].current.focus();
      } else {
        this[`ref-${id}`].current.blur();
      }
    }
  };

  onUndo = () => {
    const index = this.state.codeTabIndex;
    if (index > 0) {
      this.setState({codeTabIndex: index - 1, lastEditedID: null});
    }
  };

  onRedo = () => {
    const index = this.state.codeTabIndex;
    const length = this.state.codeTab.length;
    if (length > index + 1) {
      this.setState({codeTabIndex: index + 1, lastEditedID: null});
    }
  };

  updateCodeTab = ({
    code,
    globalID = this.state.globalID,
    lastEditedID = null,
    modalViewID = null,
  }) => {
    if (
      typeof lastEditedID === 'number' &&
      lastEditedID === this.state.lastEditedID
    ) {
      const codeTab = [
        ...this.state.codeTab.slice(0, this.state.codeTabIndex),
        code,
      ];
      this.setState({codeTab});
    } else {
      const max = this.state.codeTabIndex + 1 >= MEMORY;
      const codeTabIndex = max
        ? this.state.codeTabIndex
        : this.state.codeTabIndex + 1;
      const start = max ? 1 : 0;
      const codeTab = [
        ...this.state.codeTab.slice(start, this.state.codeTabIndex + 1),
        code,
      ];
      this.setState({
        codeTab,
        codeTabIndex,
        globalID,
        lastEditedID,
        modalViewID,
      });
    }
  };

  marginPress = ({id = null, isContent = null}) => {
    const codeTab = this.state.codeTab[this.state.codeTabIndex];
    const globalID = this.state.globalID + 1;
    if (typeof id === 'number') {
      const [newAlert, code] = addObjFunction(codeTab, globalID, id, isContent);
      if (newAlert) {
        this.props.setFlashMessage(messProp(newAlert));
      } else {
        this.updateCodeTab({code, globalID});
      }
    } else if (isEmptyInput(codeTab[0])) {
      this.props.setFlashMessage(messProp('Dodano już puste pole poniżej'));
    } else {
      const code = [getNewObj({id: globalID}), ...codeTab];
      this.updateCodeTab({code, globalID});
    }
  };

  replaceObjMethod = type => {
    if (typeof this.state.focusID === 'number') {
      const [newAlert, code] = replaceObjFunction(
        this.state.codeTab[this.state.codeTabIndex],
        this.state.focusID,
        this.state.globalID,
        type,
      );
      if (newAlert) {
        this.props.setFlashMessage(messProp(newAlert));
      } else {
        const globalID = this.state.globalID + 5; // TODO this is example increment
        this.updateCodeTab({code, globalID});
      }
    }
  };

  valueChange = (id, value) => {
    const code = this.state.codeTab[this.state.codeTabIndex].map(item =>
      changeObjValue(item, id, value),
    );
    this.updateCodeTab({code, lastEditedID: id});
  };

  focusChange = (focusID, fousType, focus) => {
    if (focus) {
      this.setState({focusID, fousType});
    } else if (this.state.focusID === focusID) {
      this.setState({focusID: null, fousType: null});
    }
  };

  deleteObjMethod = id => {
    if (typeof id === 'number') {
      const [newAlert, code] = deleteObjFunction(
        this.state.codeTab[this.state.codeTabIndex],
        id,
      );
      if (newAlert) {
        this.props.setFlashMessage(messProp(newAlert));
      } else {
        this.updateCodeTab({code});
      }
    }
  };

  itemLongPress = id => {
    // TODO removed if useless after double-focus input implementation
    this.setState({modalViewID: id});
  };

  itemPress = id => {}; // TODO removed if useless after double-focus input implementation

  renderModal = () => {
    // TODO removed after double-focus input implementation
    if (typeof this.state.modalViewID === 'number') {
      const startButton = {
        width: 180,
        height: 35,
        backgroundColor: '#fff',
        marginBottom: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderTopLeftRadius: 4,
        borderTopRightRadius: 4,
      };
      const endButton = {
        width: 180,
        height: 35,
        backgroundColor: '#fff',
        marginBottom: 1,
        alignItems: 'center',
        justifyContent: 'center',
        borderBottomLeftRadius: 4,
        borderBottomRightRadius: 4,
      };
      const container = {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        zIndex: 1,
        backgroundColor: '#00000066',
        alignItems: 'center',
        justifyContent: 'center',
      };
      const cancelText = {fontWeight: 'bold', color: 'turquoise'};
      return (
        <TouchableOpacity
          activeOpacity={1}
          onPress={() => this.setState({modalViewID: null})}
          style={container}>
          <TouchableOpacity
            style={startButton}
            onPress={() => this.deleteObjMethod(this.state.modalViewID)}>
            <Text>Usuń obiekt</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={endButton}
            onPress={() => this.setState({modalViewID: null})}>
            <Text style={cancelText}>Anuluj</Text>
          </TouchableOpacity>
        </TouchableOpacity>
      );
    }
    return null;
  };

  updateRefsArray = globalID => {
    const refsArrayLength = this.refsArray.length;
    if (globalID >= refsArrayLength) {
      for (var i = refsArrayLength; i <= globalID; i++) {
        this.refsArray.push((this[`ref-${i}`] = React.createRef()));
      }
    }
  };

  render() {
    this.updateRefsArray(this.state.globalID);
    const code = this.state.codeTab[this.state.codeTabIndex];
    const codeLength = code.length;
    const args = {
      refsArray: this.refsArray,
      scale: this.state.scale,
      focusID: this.state.focusID,
      systemKeyboard: this.state.systemKeyboard,
      itemPress: this.itemPress,
      marginPress: this.marginPress,
      focusChange: this.focusChange,
      valueChange: this.valueChange,
      itemLongPress: this.itemLongPress,
    };
    return (
      <View style={styles.mainContainer}>
        {codeLength > 0 ? (
          <ScrollView
            horizontal
            contentContainerStyle={styles.horizontalScrollContainer}>
            <ScrollView contentContainerStyle={styles.verticalScrollContainer}>
              <TouchableOpacity
                style={styles.firstMargin(args.scale)}
                onPress={this.marginPress}
              />
              {code.flatMap((element, index) => [
                <Block
                  key={`${element.id}-block`}
                  item={element}
                  args={args}
                />,
                <TouchableMargin
                  key={`${element.id}-margin`}
                  item={element}
                  args={args}
                  content={false}
                  last={codeLength === index + 1}
                />,
              ])}
            </ScrollView>
          </ScrollView>
        ) : (
          <TouchableOpacity
            style={styles.flexMargin}
            onPress={this.marginPress}
          />
        )}
        {this.renderModal()}
        <BottomKeys
          refsArray={this.refsArray}
          focusID={this.state.focusID}
          systemKeyboard={this.state.systemKeyboard}
          keyboardOnPress={this.keyboardOnPress}
          focusOrBlurObject={this.focusOrBlurObject}
          systemKeyboardChange={this.systemKeyboardChange}
        />
      </View>
    );
  }
}

const mapStateToProps = state => ({
  currentLesson: state.chat.currentLesson,
  chatBubbelsPending: state.chat.chatBubbelsPending,
  isVerfificationPending: state.editor.isVerfificationPending,
});

const mapDispatchToProps = dispatch => ({
  verifySourceCode: data => dispatch(verifySourceCode(data)),
  setFlashMessage: data => dispatch(setFlashMessage(data)),
  postChatBubbles: data => dispatch(postChatBubbles(data)),
  saveSourceCodeForStep: data => dispatch(saveSourceCodeForStep(data)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Editor);
