import React from 'react';
import {Text} from 'react-native';

export const STRINGS =
  '\\s*"(?:|(?:\\\\"|[^"])*[^\\\\"])"|\\s*\'(?:|(?:\\\\\'|[^\'])*[^\\\\\'])\'';
export const POINTS = '\\s*[.]+';
export const NUMBERS = '\\s*[.0-9]*[0-9]+';
export const ALLOWED_CHARS = '\\s*[\\[\\]\\(\\)\\{\\}\\:\\,\\=]+';
export const VARIABLES = '\\s*[_a-zA-Z_][_a-zA-Z0-9]*';
export const WHITESPACES = '\\s*';
export const STRINGS_REGEXP = new RegExp(`^(?:${STRINGS})$`);
export const POINTS_REGEXP = new RegExp(`^${POINTS}$`);
export const NUMBERS_REGEXP = new RegExp(`^${NUMBERS}$`);
export const ALLOWED_CHARS_REGEXP = new RegExp(`^${ALLOWED_CHARS}$`);
export const VARIABLES_REGEXP = new RegExp(`^${VARIABLES}$`);
export const WHITESPACES_REGEXP = new RegExp(`${WHITESPACES}`);
export const EMPTY_REGEXP = new RegExp(`^${WHITESPACES}$`);
export const MAIN_REGEXP = new RegExp(
  `(${STRINGS}|${POINTS}|${NUMBERS}|${ALLOWED_CHARS}|${VARIABLES}|${WHITESPACES})`,
);

export const EDITOR_STYLES = {
  strings: {color: '#559966'},
  points: {color: 'turquoise'},
  numbers: {color: '#ff8800'},
  variables: {color: 'white'},
  allowedChars: {color: 'white'},
  functions: {color: 'orange'},
  keywords1: {color: '#1b74bf'},
  keywords2: {color: '#4682B4'},
  keywords3: {color: '#aa88ff'},
  keywords4: {color: 'khaki'},
  errorKeywords: {color: '#FF0000'},
  empty: {color: 'white'},
  errors: {color: '#FF0000'},
};

export const KEY_WORDS = [
  {
    objects: ['and', 'or', 'not', 'is', 'in'],
    style: EDITOR_STYLES.keywords1,
  },
  {
    objects: [
      'apply',
      'basestring',
      'buffer',
      'cmp',
      'coerce',
      'execfile',
      'file',
      'intern',
      'long',
      'raw_input',
      'reduce',
      'reload',
      'unichr',
      'unicode',
      'xrange',
      'False',
      'True',
      'None',
    ],
    style: EDITOR_STYLES.keywords2,
  },
  {
    objects: [
      'as',
      'assert',
      'break',
      'continue',
      'def',
      'del',
      'except',
      'finally',
      'from',
      'global',
      'lambda',
      'pass',
      'raise',
      'try',
      'with',
      'yield',
    ],
    style: EDITOR_STYLES.keywords3,
  },
  {
    objects: [
      'abs',
      'all',
      'any',
      'append',
      'bin',
      'bool',
      'bytearray',
      'callable',
      'chr',
      'classmethod',
      'compile',
      'complex',
      'delattr',
      'dict',
      'dir',
      'divmod',
      'enumerate',
      'eval',
      'filter',
      'float',
      'format',
      'frozenset',
      'getattr',
      'globals',
      'hasattr',
      'hash',
      'help',
      'hex',
      'id',
      'input',
      'int',
      'isinstance',
      'issubclass',
      'iter',
      'len',
      'list',
      'locals',
      'map',
      'max',
      'memoryview',
      'min',
      'next',
      'object',
      'oct',
      'open',
      'ord',
      'pop',
      'pow',
      'print',
      'property',
      'push',
      'range',
      'read',
      'remove',
      'repr',
      'reversed',
      'round',
      'self',
      'set',
      'setattr',
      'slice',
      'sorted',
      'staticmethod',
      'str',
      'sum',
      'super',
      'tuple',
      'type',
      'vars',
      'zip',
      'NotImplemented',
      'Ellipsis',
    ],
    style: EDITOR_STYLES.keywords4,
  },
  {
    objects: [
      'class',
      'elif',
      'else',
      'for',
      'if',
      'import',
      'return',
      'while',
    ],
    style: EDITOR_STYLES.errorKeywords,
  },
];

export const getVariablesStyle = (value, nextValue) => {
  const item = value.replace(WHITESPACES_REGEXP, '');
  for (var i = 0; i < KEY_WORDS.length; i++) {
    if (KEY_WORDS[i].objects.includes(item)) {
      return KEY_WORDS[i].style;
    }
  }
  if (nextValue && nextValue[0] === '(') {
    return EDITOR_STYLES.functions;
  }
  return EDITOR_STYLES.variables;
};

export const getStyle = (value, nextValue) => {
  switch (true) {
    case STRINGS_REGEXP.test(value):
      return EDITOR_STYLES.strings;
    case POINTS_REGEXP.test(value):
      return EDITOR_STYLES.points;
    case NUMBERS_REGEXP.test(value):
      return EDITOR_STYLES.numbers;
    case ALLOWED_CHARS_REGEXP.test(value):
      return EDITOR_STYLES.allowedChars;
    case VARIABLES_REGEXP.test(value):
      return getVariablesStyle(value, nextValue);
    case EMPTY_REGEXP.test(value):
      return EDITOR_STYLES.empty;
    default:
      return EDITOR_STYLES.errors;
  }
};

export const renderContent = value => {
  const strArray = value.split(MAIN_REGEXP).filter(Boolean);
  const jsxArray = strArray.map((item, index) => (
    <Text key={`${index}-content`} style={getStyle(item, strArray[index + 1])}>
      {item}
    </Text>
  ));
  return jsxArray;
};
