import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

export const input = (focused, empty, scale) => ({
  color: COLORS.background,
  height: 32 * scale,
  minWidth: 29 * scale,
  fontSize: 17 * scale,
  lineHeight: 24 * scale,
  borderRadius: 8 * scale,
  borderWidth: 0.5 * scale,
  paddingVertical: 0 * scale,
  paddingHorizontal: 4 * scale,
  textAlign: 'center',
  fontFamily: 'monospace',
  textAlignVertical: 'center',
  borderColor: focused ? '#ffffff' : 'transparent',
  backgroundColor: focused ? '#ffffff45' : empty ? '#ffffff20' : '#ffffff05',
});

export const styles = StyleSheet.create({
  input,
});
