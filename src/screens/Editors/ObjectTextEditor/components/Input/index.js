import React, {Component} from 'react';
import {TextInput} from 'react-native';
import {renderContent, EMPTY_REGEXP} from './functions';
import {styles} from './styles';

class Input extends Component {
  componentWillUnmount = () => {
    const {item, args} = this.props;
    const focused = args.focusID === item.id;
    if (focused) {
      args.focusChange(item.id, item.type, false);
    }
  };

  render() {
    const {item, args} = this.props;
    const focused = args.focusID === item.id;
    const empty = EMPTY_REGEXP.test(item.value);
    return (
      <TextInput
        spellCheck={false}
        autoCorrect={false}
        autoCapitalize="none"
        autoCompleteType="off"
        textContentType="none"
        importantForAutofill="no"
        underlineColorAndroid="transparent"
        ref={args.refsArray[item.id]}
        showSoftInputOnFocus={args.systemKeyboard}
        style={styles.input(focused, empty, args.scale)}
        onChangeText={value => args.valueChange(item.id, value)}
        onFocus={() => args.focusChange(item.id, item.type, true)}
        onBlur={() => args.focusChange(item.id, item.type, false)}>
        {renderContent(item.value)}
      </TextInput>
    );
  }
}

export default Input;
