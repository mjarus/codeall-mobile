import React from 'react';
import {TouchableOpacity} from 'react-native';
import {styles} from './styles';

const TouchableMargin = props => (
  <TouchableOpacity
    activeOpacity={1}
    style={
      props.last
        ? styles.lastContainer
        : props.isContent
        ? styles.contentContainer(props.args.scale)
        : styles.defaultContainer(props.args.scale)
    }
    onPress={() =>
      props.args.marginPress({id: props.item.id, isContent: props.isContent})
    }
  />
);

export default TouchableMargin;
