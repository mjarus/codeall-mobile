import {StyleSheet} from 'react-native';

const defaultContainer = scale => ({
  height: 12 * scale,
  width: '100%',
  backgroundColor: '#ffffff05',
});

const contentContainer = scale => ({
  height: 12 * scale,
  width: '100%',
  paddingLeft: 25 * scale,
  borderLeftWidth: 0.5 * scale,
  borderColor: 'grey',
  backgroundColor: '#ffffff05',
});

const lastContainer = {
  height: 100,
  width: '100%',
  backgroundColor: '#ffffff05',
};

export const styles = StyleSheet.create({
  defaultContainer,
  contentContainer,
  lastContainer,
});
