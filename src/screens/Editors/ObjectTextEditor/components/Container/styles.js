import {StyleSheet} from 'react-native';

const container = {
  flexDirection: 'row',
};

export const styles = StyleSheet.create({
  container,
});
