import React from 'react';
import {TouchableOpacity} from 'react-native';
import {styles} from './styles';

const Container = props => (
  <TouchableOpacity
    activeOpacity={1}
    style={styles.container}
    onPress={() => props.args.itemPress(props.item.id)}
    onLongPress={() => props.args.itemLongPress(props.item.id)}>
    {props.children}
  </TouchableOpacity>
);

export default Container;
