import React from 'react';
import {Text, TouchableOpacity, Image} from 'react-native';

const Button = props => {
  return (
    <TouchableOpacity
      style={props.containerStyle}
      onPress={() => props.onPress(props.type, props.value)}>
      {typeof props.content === 'string' ? (
        <Text style={props.contentStyle}>{props.content}</Text>
      ) : (
        <Image style={props.contentStyle} source={props.content} />
      )}
    </TouchableOpacity>
  );
};

export default Button;
