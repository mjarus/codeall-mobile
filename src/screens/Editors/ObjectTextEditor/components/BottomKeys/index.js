import React, {Component} from 'react';
import {View, Image, TouchableOpacity, Keyboard, Text} from 'react-native';
import ButtonsKeyboard from './ButtonsKeyboard';
import {styles} from './styles';

const HideKeyboard = require('CodeallMobile/src/assets/img/hideKeyboard.png');
const KeyboardIcon = require('CodeallMobile/src/assets/img/keyboard.png');
const IntIcon = require('CodeallMobile/src/assets/img/intIcon.png');
const PlayIcon = require('CodeallMobile/src/assets/img/playButton.png');
const Redo = require('CodeallMobile/src/assets/img/redo.png');
const Undo = require('CodeallMobile/src/assets/img/undo.png');

class BottomKeys extends Component {
  constructor(props) {
    super(props);
    this.lastFocusId = null;
    this.hideSystemKeyboard = false;
    this.hideButtonsKeyboard = false;
    this.state = {
      height: null,
    };
  }

  componentDidMount = () => {
    Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  };

  componentWillUnmount = () => {
    Keyboard.removeAllListeners('keyboardDidShow');
    Keyboard.removeAllListeners('keyboardDidHide');
  };

  keyboardDidShow = event => {
    if (event && event.endCoordinates && event.endCoordinates.height) {
      this.setState({height: event.endCoordinates.height});
    }
  };

  keyboardDidHide = event => {
    const {focusID, focusOrBlurObject, systemKeyboardChange} = this.props;
    if (this.hideSystemKeyboard && focusID === null) {
      this.hideSystemKeyboard = false;
      systemKeyboardChange(false);
      focusOrBlurObject(this.lastFocusId, true);
    }
  };

  componentDidUpdate = prevProps => {
    const {focusID, focusOrBlurObject} = this.props;
    if (prevProps.focusID !== null && focusID === null) {
      if (this.hideButtonsKeyboard) {
        this.hideButtonsKeyboard = false;
        focusOrBlurObject(this.lastFocusId, true);
      }
    }
  };

  systemKeyboardChange = () => {
    const {
      focusID,
      systemKeyboard,
      focusOrBlurObject,
      systemKeyboardChange,
    } = this.props;
    if (focusID) {
      if (systemKeyboard) {
        this.hideSystemKeyboard = true;
      } else {
        this.hideButtonsKeyboard = true;
        systemKeyboardChange(true);
      }
      this.lastFocusId = focusID;
      focusOrBlurObject(this.lastFocusId, false);
    } else {
      this.hideKeyboards();
    }
  };

  hideKeyboards = () => {
    const {systemKeyboardChange} = this.props;
    systemKeyboardChange(true);
    Keyboard.dismiss();
  };

  render() {
    const {systemKeyboard, keyboardOnPress} = this.props;
    const qwertyButtonSource = systemKeyboard ? IntIcon : KeyboardIcon;
    const qwertyButtonStyle = systemKeyboard
      ? styles.customIcon
      : styles.keyboardIcon;
    return (
      <View style={styles.mainBox}>
        <View style={styles.keyboardLine}>
          <TouchableOpacity style={styles.lineBox} onPress={this.hideKeyboards}>
            <Image source={HideKeyboard} style={styles.keyboardIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lineBox}
            onPress={this.systemKeyboardChange}>
            <Image source={qwertyButtonSource} style={qwertyButtonStyle} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lineBox}
            onPress={() => keyboardOnPress(2, 'delete')}>
            <Text style={styles.delKey}>DEL</Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lineBox}
            onPress={() => keyboardOnPress(2, 'play')}>
            <Image source={PlayIcon} style={styles.customIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lineBox}
            onPress={() => keyboardOnPress(2, 'undo')}>
            <Image source={Undo} style={styles.customIcon} />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.lineBox}
            onPress={() => keyboardOnPress(2, 'redo')}>
            <Image source={Redo} style={styles.customIcon} />
          </TouchableOpacity>
        </View>
        {!systemKeyboard && (
          <View style={styles.keyboardBox(this.state.height)}>
            <ButtonsKeyboard keyboardOnPress={keyboardOnPress} />
          </View>
        )}
      </View>
    );
  }
}

export default BottomKeys;
