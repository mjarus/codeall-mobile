import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

export const scrollBox = {
  flexGrow: 1,
};

export const keyboardBox = {
  flex: 1,
  width: '100%',
  backgroundColor: COLORS.ice,
  flexDirection: 'row',
  flexWrap: 'wrap',
  paddingVertical: 12,
  paddingHorizontal: 6,
};

export const customContent = color => ({
  color,
  fontSize: 22,
  lineHeight: 40,
});

export const customContainer = color => ({
  alignItems: 'center',
  justifySelf: 'center',
  borderWidth: 2,
  borderColor: color,
  borderRadius: 10,
  paddingLeft: 10,
  paddingRight: 10,
  marginLeft: '1%',
  marginRight: '1%',
  marginBottom: 12,
});

export const styles = StyleSheet.create({
  scrollBox,
  keyboardBox,
  customContent,
  customContainer,
});
