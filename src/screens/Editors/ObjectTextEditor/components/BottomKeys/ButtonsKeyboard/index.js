import React, {Component} from 'react';
import {View, ScrollView} from 'react-native';
import Button from '../Button';
import {styles} from './styles';

class ButtonsKeyboard extends Component {
  typing = (type, value) => this.props.keyboardOnPress(type, value);

  render() {
    return (
      <ScrollView
        contentContainerStyle={styles.scrollBox}
        keyboardShouldPersistTaps="always">
        <View style={styles.keyboardBox}>
          <Button
            type={1}
            value="for"
            content="for"
            contentStyle={styles.customContent('#FF7F50')}
            containerStyle={styles.customContainer('#FF7F50')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="if"
            content="if"
            contentStyle={styles.customContent('#FF7F50')}
            containerStyle={styles.customContainer('#FF7F50')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="elif"
            content="elif"
            contentStyle={styles.customContent('#FF7F50')}
            containerStyle={styles.customContainer('#FF7F50')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="else"
            content="else"
            contentStyle={styles.customContent('#FF7F50')}
            containerStyle={styles.customContainer('#FF7F50')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="while"
            content="while"
            contentStyle={styles.customContent('#FF7F50')}
            containerStyle={styles.customContainer('#FF7F50')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="class"
            content="class"
            contentStyle={styles.customContent('#FF7F50')}
            containerStyle={styles.customContainer('#FF7F50')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value=">"
            content=">"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="="
            content="="
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="+="
            content="+="
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="-="
            content="-="
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="+"
            content="+"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="-"
            content="-"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="*"
            content="*"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="/"
            content="/"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="**"
            content="**"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="()"
            content="( )"
            contentStyle={styles.customContent('#32CD32')}
            containerStyle={styles.customContainer('#32CD32')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="return"
            content="return"
            contentStyle={styles.customContent('#9944FF')}
            containerStyle={styles.customContainer('#9944FF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="import"
            content="import"
            contentStyle={styles.customContent('#9944FF')}
            containerStyle={styles.customContainer('#9944FF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="fromImport"
            content="from import"
            contentStyle={styles.customContent('#9944FF')}
            containerStyle={styles.customContainer('#9944FF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="<"
            content="<"
            contentStyle={styles.customContent('#77BBFF')}
            containerStyle={styles.customContainer('#77BBFF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value=">"
            content=">"
            contentStyle={styles.customContent('#77BBFF')}
            containerStyle={styles.customContainer('#77BBFF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="=<"
            content="=<"
            contentStyle={styles.customContent('#77BBFF')}
            containerStyle={styles.customContainer('#77BBFF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value=">="
            content=">="
            contentStyle={styles.customContent('#77BBFF')}
            containerStyle={styles.customContainer('#77BBFF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="=="
            content="=="
            contentStyle={styles.customContent('#77BBFF')}
            containerStyle={styles.customContainer('#77BBFF')}
            onPress={(type, value) => this.typing(type, value)}
          />
          <Button
            type={1}
            value="!="
            content="!="
            contentStyle={styles.customContent('#77BBFF')}
            containerStyle={styles.customContainer('#77BBFF')}
            onPress={(type, value) => this.typing(type, value)}
          />
        </View>
      </ScrollView>
    );
  }
}

export default ButtonsKeyboard;
