import {StyleSheet} from 'react-native';

const mainContainer = {
  width: '100%',
};

const contentContainer = scale => ({
  paddingLeft: 25 * scale,
  borderLeftWidth: 0.5 * scale,
  borderColor: 'grey',
});

const functionContainer1 = scale => ({
  backgroundColor: '#fc7a57',
  borderRadius: 8 * scale,
  paddingHorizontal: 4 * scale,
  marginRight: 8 * scale,
});

const functionContainer2 = scale => ({
  backgroundColor: '#B473AC',
  borderRadius: 8 * scale,
  paddingHorizontal: 4 * scale,
  marginHorizontal: 8 * scale,
});

const functionContainer3 = scale => ({
  backgroundColor: '#B473AC',
  borderRadius: 8 * scale,
  paddingHorizontal: 4 * scale,
  marginRight: 8 * scale,
});

const functionContainer4 = scale => ({
  backgroundColor: '#559966',
  borderRadius: 8 * scale,
  paddingHorizontal: 4 * scale,
  marginRight: 8 * scale,
});

const functionContainer5 = scale => ({
  marginHorizontal: 8 * scale,
});

const text1 = scale => ({
  fontSize: 17 * scale,
  lineHeight: 24 * scale,
  paddingVertical: 4 * scale,
  fontFamily: 'monospace',
  color: 'white',
});

const text2 = scale => ({
  fontSize: 17 * scale,
  lineHeight: 24 * scale,
  paddingVertical: 4 * scale,
  fontFamily: 'monospace',
  color: 'grey',
});

const text3 = scale => ({
  fontSize: 17 * scale,
  lineHeight: 24 * scale,
  paddingVertical: 4 * scale,
  fontFamily: 'monospace',
  color: '#4682B4',
});

const text4 = scale => ({
  fontSize: 17 * scale,
  lineHeight: 24 * scale,
  paddingVertical: 4 * scale,
  fontFamily: 'monospace',
  color: '#aa88ff',
  marginRight: 8 * scale,
});

const text5 = scale => ({
  fontSize: 17 * scale,
  lineHeight: 24 * scale,
  paddingVertical: 4 * scale,
  fontFamily: 'monospace',
  color: '#aa88ff',
  marginHorizontal: 8 * scale,
});

export const styles = StyleSheet.create({
  mainContainer,
  contentContainer,
  functionContainer1,
  functionContainer2,
  functionContainer3,
  functionContainer4,
  functionContainer5,
  text1,
  text2,
  text3,
  text4,
  text5,
});
