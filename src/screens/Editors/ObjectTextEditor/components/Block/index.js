import React, {Component} from 'react';
import {View, Text} from 'react-native';
import TouchableMargin from '../TouchableMargin';
import Container from '../Container';
import Input from '../Input';
import {styles} from './styles';

class Block extends Component {
  input = (item, args) => (
    <Container item={item} args={args}>
      <Input item={item} args={args} />
    </Container>
  );

  equation = (item, args) => (
    <Container item={item} args={args}>
      <Block item={item.param[0]} args={args} />
      <View style={styles.functionContainer5(args.scale)}>
        <Text style={styles.text2(args.scale)}>{item.type}</Text>
      </View>
      <Block item={item.param[1]} args={args} />
    </Container>
  );

  inequality = (item, args) => (
    <Container item={item} args={args}>
      <Block item={item.param[0]} args={args} />
      <View style={styles.functionContainer5(args.scale)}>
        <Text style={styles.text3(args.scale)}>{item.type}</Text>
      </View>
      <Block item={item.param[1]} args={args} />
    </Container>
  );

  if = (item, args) => (
    <Container item={item} args={args}>
      <View style={styles.functionContainer1(args.scale)}>
        <Text style={styles.text1(args.scale)}>{item.type}</Text>
      </View>
      <Block item={item.param[0]} args={args} />
      <Text style={styles.text2(args.scale)}>{':'}</Text>
    </Container>
  );

  for = (item, args) => (
    <Container item={item} args={args}>
      <View style={styles.functionContainer3(args.scale)}>
        <Text style={styles.text1(args.scale)}>{item.type}</Text>
      </View>
      <Block item={item.param[0]} args={args} />
      <View style={styles.functionContainer2(args.scale)}>
        <Text style={styles.text1(args.scale)}>{'in'}</Text>
      </View>
      <Block item={item.param[1]} args={args} />
      <Text style={styles.text2(args.scale)}>{':'}</Text>
    </Container>
  );

  while = (item, args) => (
    <Container item={item} args={args}>
      <View style={styles.functionContainer3(args.scale)}>
        <Text style={styles.text1(args.scale)}>{item.type}</Text>
      </View>
      <Block item={item.param[0]} args={args} />
      <Text style={styles.text2(args.scale)}>{':'}</Text>
    </Container>
  );

  class = (item, args) => (
    <Container item={item} args={args}>
      <View style={styles.functionContainer4(args.scale)}>
        <Text style={styles.text1(args.scale)}>{item.type}</Text>
      </View>
      <Block item={item.param[0]} args={args} />
      <Text style={styles.text2(args.scale)}>{':'}</Text>
    </Container>
  );

  else = (item, args) => (
    <Container item={item} args={args}>
      <View style={styles.functionContainer1(args.scale)}>
        <Text style={styles.text1(args.scale)}>{item.type}</Text>
      </View>
      <Text style={styles.text2(args.scale)}>{':'}</Text>
    </Container>
  );

  brackets = (item, args) => (
    <Container item={item} args={args}>
      <Text style={styles.text2(args.scale)}>{'('}</Text>
      <Block item={item.param[0]} args={args} />
      <Text style={styles.text2(args.scale)}>{')'}</Text>
    </Container>
  );

  prefix = (item, args) => (
    <Container item={item} args={args}>
      <Text style={styles.text4(args.scale)}>{item.type}</Text>
      <Block item={item.param[0]} args={args} />
    </Container>
  );

  fromImport = (item, args) => (
    <Container item={item} args={args}>
      <Text style={styles.text4(args.scale)}>{'from'}</Text>
      <Block item={item.param[0]} args={args} />
      <Text style={styles.text5(args.scale)}>{'import'}</Text>
      <Block item={item.param[1]} args={args} />
    </Container>
  );

  headerComponentList = (item, args) => {
    switch (item.type) {
      case 'input':
        return this.input(item, args);
      case '=':
      case '+=':
      case '-=':
      case '+':
      case '-':
      case '/':
      case '*':
      case '**':
        return this.equation(item, args);
      case '<':
      case '>':
      case '<=':
      case '>=':
      case '==':
      case '!=':
        return this.inequality(item, args);
      case '()':
        return this.brackets(item, args);
      case 'return':
      case 'import':
        return this.prefix(item, args);
      case 'fromImport':
        return this.fromImport(item, args);
      case 'for':
        return this.for(item, args);
      case 'if':
      case 'elif':
        return this.if(item, args);
      case 'while':
        return this.while(item, args);
      case 'class':
        return this.class(item, args);
      case 'else':
        return this.else(item, args);
      default:
        return null;
    }
  };

  render() {
    const {item, args} = this.props;
    if (item && item.content) {
      return (
        <View style={styles.mainContainer}>
          {this.headerComponentList(item, args)}
          <TouchableMargin item={item} args={args} isContent={true} />
          <View style={styles.contentContainer(args.scale)}>
            {item.content
              .filter(Boolean)
              .flatMap(element => [
                <Block
                  key={`${element.id}-block`}
                  item={element}
                  args={args}
                />,
                <TouchableMargin
                  key={`${element.id}-margin`}
                  item={element}
                  args={args}
                  isContent={false}
                />,
              ])}
          </View>
        </View>
      );
    }
    if (item) {
      return <View>{this.headerComponentList(item, args)}</View>;
    }
    return null;
  }
}

export default Block;
