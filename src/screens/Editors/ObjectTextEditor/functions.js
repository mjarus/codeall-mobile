export function getNewObj({
  id,
  value = '',
  type = 'input',
  param = null,
  content = null,
  deleteAbility = true,
}) {
  return {id, value, type, param, content, deleteAbility};
}

export function isEmptyInput(field) {
  return !!(field && field.type === 'input' && field.value === '');
}

export function getObjForType({globalID, type, value, deleteAbility}) {
  switch (type) {
    case '=':
    case '+=':
    case '-=':
    case '+':
    case '-':
    case '/':
    case '*':
    case '**':
      return getNewObj({
        id: globalID + 1,
        type,
        deleteAbility,
        param: [
          getNewObj({id: globalID + 2, deleteAbility: false, value}),
          getNewObj({id: globalID + 3, deleteAbility: false}),
        ],
      });
    case '<':
    case '>':
    case '<=':
    case '>=':
    case '==':
    case '!=':
      return getNewObj({
        id: globalID + 1,
        type,
        deleteAbility,
        param: [
          getNewObj({id: globalID + 2, deleteAbility: false, value}),
          getNewObj({id: globalID + 3, deleteAbility: false}),
        ],
      });
    case '()':
    case 'return':
    case 'import':
      return getNewObj({
        id: globalID + 1,
        type,
        param: [getNewObj({id: globalID + 2, deleteAbility: false})],
      });
    case 'fromImport':
      return getNewObj({
        id: globalID + 1,
        type,
        param: [
          getNewObj({id: globalID + 2, deleteAbility: false}),
          getNewObj({id: globalID + 3, deleteAbility: false, value}),
        ],
      });
    case 'for':
      if (deleteAbility) {
        return getNewObj({
          id: globalID + 1,
          type,
          param: [
            getNewObj({id: globalID + 2, deleteAbility: false}),
            getNewObj({id: globalID + 3, deleteAbility: false, value}),
          ],
          content: [getNewObj({id: globalID + 4})],
        });
      } else {
        return null;
      }
    case 'if':
    case 'elif':
    case 'while':
    case 'class':
      if (deleteAbility) {
        return getNewObj({
          id: globalID + 1,
          type,
          param: [getNewObj({id: globalID + 2, deleteAbility: false, value})],
          content: [getNewObj({id: globalID + 3})],
        });
      } else {
        return null;
      }
    case 'else':
      if (deleteAbility) {
        return getNewObj({
          id: globalID + 1,
          type,
          content: [getNewObj({id: globalID + 2})],
        });
      } else {
        return null;
      }
    default:
      return null;
  }
}

export function changeObjValue(block, id, value) {
  switch (true) {
    case !!(block.param && block.content):
      return {
        ...block,
        param: block.param.map(item => changeObjValue(item, id, value)),
        content: block.content.map(item => changeObjValue(item, id, value)),
      };
    case !!block.param:
      return {
        ...block,
        param: block.param.map(item => changeObjValue(item, id, value)),
      };
    case !!block.content:
      return {
        ...block,
        content: block.content.map(item => changeObjValue(item, id, value)),
      };
    case block.type === 'input' && block.id === id:
      return {...block, value};
    default:
      return block;
  }
}

export function getCodeString(obj, nestLevel = 0) {
  if (obj) {
    const spaces = ' '.repeat(4 * nestLevel);
    switch (obj.type) {
      case 'input':
        return `${spaces}${obj.value}`;
      case '=':
      case '+=':
      case '-=':
      case '+':
      case '-':
      case '/':
      case '*':
      case '**':
      case '<':
      case '>':
      case '<=':
      case '>=':
      case '==':
      case '!=':
        return `${spaces}${getCodeString(obj.param[0])} ${
          obj.type
        } ${getCodeString(obj.param[1])}`;
      case '()':
        return `${spaces}(${getCodeString(obj.param[0])})`;
      case 'not':
      case 'def':
      case 'return':
      case 'import':
        return `${spaces}${obj.type} ${getCodeString(obj.param[0])}`;
      case 'fromImport':
        return `${spaces}from ${getCodeString(
          obj.param[0],
        )} import ${getCodeString(obj.param[1])}`;
      case 'for':
        return `${spaces}${obj.type} ${getCodeString(
          obj.param[0],
        )} in ${getCodeString(obj.param[1])}:${obj.content
          .map(item => '\n' + getCodeString(item, nestLevel + 1))
          .join('')}`;
      case 'if':
      case 'while':
      case 'class':
        return `${spaces}${obj.type} ${getCodeString(
          obj.param[0],
        )}:${obj.content
          .map(item => '\n' + getCodeString(item, nestLevel + 1))
          .join('')}`;
      case 'else':
        return `${spaces}${obj.type}:${obj.content
          .map(item => '\n' + getCodeString(item, nestLevel + 1))
          .join('')}`;
    }
  }
  return '';
}

export function deleteObjFunction(code, id) {
  var mainAlert = null;
  const newCode = code.flatMap(item => {
    if (item.id === id) {
      const newObject = !item.deleteAbility
        ? [getNewObj({id, deleteAbility: false})]
        : [];
      mainAlert =
        !item.deleteAbility && item.type === 'input' && item.value === ''
          ? 'Nie można usunąć pola obiektu, musisz usunąć cały obiekt'
          : mainAlert;
      return newObject;
    }
    if (item.param && item.content) {
      const [paramAlert, param] = deleteObjFunction(item.param, id);
      const [contentAlert, content] = deleteObjFunction(item.content, id);
      mainAlert = paramAlert || contentAlert || mainAlert;
      return [{...item, param, content}];
    }
    if (item.param) {
      const [paramAlert, param] = deleteObjFunction(item.param, id);
      mainAlert = paramAlert || mainAlert;
      return [{...item, param}];
    }
    if (item.content) {
      const [contentAlert, content] = deleteObjFunction(item.content, id);
      mainAlert = contentAlert || mainAlert;
      return [{...item, content}];
    }
    return [item];
  });
  return [mainAlert, newCode];
}

export function addObjFunction(code, globalID, id, isContent) {
  var mainAlert = null;
  const newCode = code.flatMap((item, index) => {
    if (item.id === id) {
      if (isContent && isEmptyInput(item.content[0])) {
        mainAlert = 'Dodano już puste pole poniżej';
        return [item];
      }
      if (isContent) {
        return [
          {...item, content: [getNewObj({id: globalID}), ...item.content]},
        ];
      }
      if (isEmptyInput(item)) {
        mainAlert = 'Dodano już puste pole powyżej';
        return [item];
      }
      if (isEmptyInput(code[index + 1])) {
        mainAlert = 'Dodano już puste pole poniżej';
        return [item];
      }
      return [item, getNewObj({id: globalID})];
    }
    if (item.content) {
      const [contentAlert, content] = addObjFunction(
        item.content,
        globalID,
        id,
        isContent,
      );
      mainAlert = contentAlert || mainAlert;
      return [{...item, content}];
    }
    return [item];
  });
  return [mainAlert, newCode];
}

export function replaceObjFunction(code, focusID, globalID, type) {
  var mainAlert = null;
  const newCode = code.map(item => {
    if (item.id === focusID) {
      const newObj = getObjForType({
        globalID,
        type,
        value: item.value,
        deleteAbility: item.deleteAbility,
      });
      if (newObj) {
        return newObj;
      } else {
        mainAlert = 'Nie można dodać obiektu';
        return item;
      }
    }
    if (item.param && item.content) {
      const [paramAlert, param] = replaceObjFunction(
        item.param,
        focusID,
        globalID,
        type,
      );
      const [contentAlert, content] = replaceObjFunction(
        item.content,
        focusID,
        globalID,
        type,
      );
      mainAlert = paramAlert || contentAlert || mainAlert;
      return {...item, param, content};
    }
    if (item.param) {
      const [paramAlert, param] = replaceObjFunction(
        item.param,
        focusID,
        globalID,
        type,
      );
      mainAlert = paramAlert || mainAlert;
      return {...item, param};
    }
    if (item.content) {
      const [contentAlert, content] = replaceObjFunction(
        item.content,
        focusID,
        globalID,
        type,
      );
      mainAlert = contentAlert || mainAlert;
      return {...item, content};
    }
    return item;
  });
  return [mainAlert, newCode];
}
