import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const mainContainer = {
  flex: 1,
  backgroundColor: COLORS.editor.background,
};

const horizontalScrollContainer = {
  flexGrow: 1,
};

const verticalScrollContainer = {
  flexGrow: 1,
  padding: 10,
};

const fadeConteiner = {
  position: 'absolute',
  top: 20,
  left: 20,
  right: 20,
  bottom: 20,
  zIndex: 1000,
  alignItems: 'center',
};

const alertContainer = {
  backgroundColor: '#ffffffaa',
  paddingHorizontal: 20,
  paddingVertical: 14,
  borderRadius: 40,
};

const alertText = {
  textAlign: 'center',
};

const firstMargin = size => ({
  height: 12 * size,
  width: '100%',
  backgroundColor: '#ffffff05',
});

const flexMargin = {
  flex: 1,
  backgroundColor: '#ffffff05',
};

export const styles = StyleSheet.create({
  mainContainer,
  horizontalScrollContainer,
  verticalScrollContainer,
  fadeConteiner,
  alertContainer,
  alertText,
  firstMargin,
  flexMargin,
});
