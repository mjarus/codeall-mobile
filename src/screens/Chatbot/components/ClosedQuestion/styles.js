import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

export const textContainer = background => ({
  paddingHorizontal: 12,
  paddingVertical: 9,
  borderRadius: 8,
  marginBottom: 6,
  marginLeft: 6,
  padding: 0,
  maxWidth: 200,
  elevation: 10,
  minWidth: 50,
  height: 35,
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor:
    background !== true && background !== false
      ? COLORS.bubbleBackground
      : background === true
      ? COLORS.bubbleGreen
      : COLORS.bubbleOrange,
});

export const text = {
  fontSize: 14,
  color: COLORS.background,
  textAlign: 'center',
  fontFamily: 'Lato-Regular',
  lineHeight: 22,
};

export const bar = {
  marginTop: 16,
  flexWrap: 'wrap',
  flexDirection: 'row',
  justifyContent: 'flex-end',
  alignItems: "center",
};

export const styles = StyleSheet.create({
  textContainer,
  text,
  bar,
});
