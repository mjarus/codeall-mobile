import React, {Component} from 'react';
import {View, TouchableOpacity} from 'react-native';
import {connect} from 'react-redux';
import HTML from 'react-native-render-html';
import {postChatBubbles} from '../../../../store/chat/actions';
import {styles} from './styles';

class ClosedQuestion extends Component {
  onPressAnswers = item => {
    this.props.postChatBubbles({
      chatBubbleId: this.props.id,
      answerId: item.id,
    });
  };

  renderItem = (item, index) => (
    <TouchableOpacity
      key={`${item.id || index}-closedQ`}
      disabled={typeof this.props.id !== 'number'}
      onPress={() => this.onPressAnswers(item)}
      style={styles.textContainer(item.isCorrect)}>
      {item.content && typeof item.content === 'string' ? (
        <HTML html={item.content} baseFontStyle={styles.text} />
      ) : null}
    </TouchableOpacity>
  );

  render() {
    const items = this.props.items;
    if (items) {
      return (
        <View style={styles.bar}>
          {items.map((item, index) => this.renderItem(item, index))}
        </View>
      );
    }
    return null;
  }
}

const mapDispatchToProps = dispatch => ({
  postChatBubbles: data => dispatch(postChatBubbles(data)),
});

export default connect(
  null,
  mapDispatchToProps,
)(ClosedQuestion);
