import React from 'react';
import {View} from 'react-native';
import Message from '../Message';

const renderBubble = (item, index, props) => {
  const {id, answers, questionType} = props.item;
  const closedQuestionItems = answers && answers.all;
  const contentsLength = props.item.contents.length;
  const param = {
    lesson: true,
    content: item,
    currentLesson: item,
    key: `${item.id || index}-bubble`,
  };
  if (index + 1 === contentsLength && questionType === 'closed') {
    return (
      <Message
        {...param}
        closedQuestionID={id}
        closedQuestionItems={closedQuestionItems}
      />
    );
  } else {
    return (
      <Message
        {...param}
        chatBubbleId={id}
        socialMediaAuthRequired={props.item.socialMediaAuthRequired}
      />
    );
  }
};

const BubblesContent = props => {
  const contents = props.item.contents;
  if (contents) {
    return (
      <View>
        {contents.map((item, index) => renderBubble(item, index, props))}
      </View>
    );
  }
  return null;
};

export default BubblesContent;
