import React, {Component} from 'react';
import {TouchableOpacity, TextInput, View, Image} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import {styles} from './styles';

const SEND_BUTTON = require('CodeallMobile/src/assets/img/send.png');
const EMPTY_REGEXP = new RegExp('^\\s*$');

class MessageInput extends Component {
  render() {
    const {
      inputRef,
      inputFocused,
      messageValue,
      onBlur,
      onFocus,
      onChangeText,
      placeholder,
      onPressSendButton,
    } = this.props;
    const disabled = EMPTY_REGEXP.test(messageValue);
    return (
      <View style={styles.mainContainer}>
        <View style={styles.inputContainer}>
          <TextInput
            ref={inputRef}
            style={styles.inputText(inputFocused)}
            multiline
            placeholderTextColor={COLORS.inputText}
            value={messageValue}
            onBlur={onBlur}
            onFocus={onFocus}
            onChangeText={onChangeText}
            placeholder={placeholder}
          />
        </View>
        <TouchableOpacity
          disabled={disabled}
          style={styles.sendButton(disabled)}
          onPress={onPressSendButton}>
          <Image style={styles.sendImage} source={SEND_BUTTON} />
        </TouchableOpacity>
      </View>
    );
  }
}

export default MessageInput;
