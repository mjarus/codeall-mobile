import {StyleSheet, Platform} from 'react-native';
import {ifIphoneX} from 'react-native-iphone-x-helper';
import COLORS from 'CodeallMobile/src/theme/colors';

export const mainContainer = {
  minHeight: 30,
  width: '100%',
  elevation: 10,
  flexDirection: 'row',
  backgroundColor: COLORS.background,
  ...ifIphoneX({paddingBottom: 20}, {paddingBottom: 0}),
};

export const inputContainer = {
  flex: 1,
  margin: 10,
  justifyContent: 'center',
  alignItems: 'flex-start',
  borderRadius: 4,
  borderWidth: 1,
  borderColor: COLORS.messageInputBorder,
  paddingHorizontal: 15,
  paddingTop: Platform.OS === 'ios' ? 5 : 0,
};

export const inputText = inputFocused => ({
  fontFamily: 'Lato-Regular',
  borderRadius: 4,
  color: COLORS.inputText,
  ...Platform.select({
    ios: {
      height: inputFocused ? 'auto' : 35,
      marginRight: 20,
      width: '80%',
      paddingHorizontal: 20,
      maxHeight: 140,
      fontSize: 16,
      lineHeight: 17,
    },
    android: {
      width: '100%',
      height: inputFocused ? 'auto' : 40,
      minHeight: 40,
      maxHeight: 125.5,
      paddingVertical: 3,
      fontSize: 16,
      lineHeight: 17,
      textAlignVertical: 'center',
      textAlign: 'left',
    },
  }),
});

export const sendButton = disabled => ({
  backgroundColor: disabled ? COLORS.disabledSendButton : COLORS.messageInputBorder,
  height: 40,
  width: 40,
  marginVertical: 10,
  marginRight: 10,
  borderRadius: 20,
  alignItems: 'center',
  justifyContent: 'center',
});

export const sendImage = {
  width: 24,
  height: 24,
};

export const styles = StyleSheet.create({
  mainContainer,
  inputContainer,
  inputText,
  sendButton,
  sendImage,
});
