import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

export const mainContainer = {
  marginHorizontal: 16,
  marginVertical: 8,
  flex: 1,
};

export const fieldContainer = chatbot => ({
  paddingRight: chatbot ? 35 : 0,
  paddingLeft: chatbot ? 0 : 35,
  flex: 1,
  justifyContent: chatbot ? 'flex-start' : 'flex-end',
  alignItems: 'flex-end',
  flexDirection: 'row',
});

export const textContainer = (chatbot, textField) => ({
  borderRadius: 8,
  flex: textField ? 1 : 0,
  minHeight: 40,
  borderColor: '#235B93',
  borderWidth: 1,
  padding: 10,
  justifyContent: 'center',
  backgroundColor: chatbot ? COLORS.editorKeyboardButtonText : COLORS.malibu,
});

export const textItem = {
  fontFamily: 'Lato',
  fontWeight: 'normal',
  fontStyle: 'normal',
  marginHorizontal: 5,
  fontSize: 14,
  lineHeight: 17,
  letterSpacing: -0.2,
  color: '#F9F9F9',
};

export const textReply = {
  marginHorizontal: 5,
  marginTop: 7,
  fontSize: 13,
  lineHeight: 13,
  fontFamily: 'Lato-Regular',
  fontStyle: 'italic',
  color: COLORS.switchSecondary,
};

export const intentContainer = {
  marginTop: 8,
  flexDirection: 'row',
};

export const inputContainer = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'flex-start',
  backgroundColor: COLORS.background,
  borderTopLeftRadius: 13,
  borderBottomLeftRadius: 13,
  paddingHorizontal: 13,
};

export const inputText = {
  width: '100%',
  height: 'auto',
  minHeight: 35,
  maxHeight: 70,
  paddingVertical: 3,
  fontSize: 15,
  lineHeight: 16,
  fontFamily: 'Lato-Regular',
  color: COLORS.switchSecondary,
  textAlignVertical: 'center',
  textAlign: 'left',
};

export const sendButton = disabled => ({
  borderTopRightRadius: 13,
  borderBottomRightRadius: 13,
  width: 32,
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: disabled ? COLORS.ashen : COLORS.mediumSlateBlue,
});

export const sendImage = {
  width: 12,
  height: 12,
};

export const dislikeButton = {
  backgroundColor: COLORS.background,
  elevation: 10,
  width: 30,
  height: 30,
  borderRadius: 15,
  marginLeft: 5,
};

export const dislikeImage = {
  width: 30,
  height: 30,
};

export const styles = StyleSheet.create({
  mainContainer,
  fieldContainer,
  textContainer,
  textItem,
  textReply,
  intentContainer,
  inputContainer,
  inputText,
  sendButton,
  sendImage,
  dislikeButton,
  dislikeImage,
});
