import React, {Component} from 'react';
import {View, Text, Image, TouchableOpacity, TextInput} from 'react-native';
import HTML from 'react-native-render-html';
import COLORS from 'CodeallMobile/src/theme/colors';
import ClosedQuestion from '../ClosedQuestion';
import {styles} from './styles';
import SocialMediaAuthButton from '../../../../components/SocialMediaAuthButton';
import {postChatBubbles} from '../../../../store/chat/actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {withTranslation} from 'react-i18next';

const ICON_PLUS = require('CodeallMobile/src/assets/img/plus.png');
const ICON_DISLIKE = require('CodeallMobile/src/assets/img/dislike.png');

class Message extends Component {
  constructor(props) {
    super(props);
    this.state = {
      intentValue: '',
      textUnlocked: true,
    };
  }

  onChangeText = intentValue => this.setState({intentValue});

  onPressSendIntent = () => {
    if (!this.props.intentsPending) {
      this.props.onPressSendIntent({
        answer: this.state.intentValue,
        context: '',
        id: this.props.item.id,
        newValue: this.state.intentValue,
      });
      this.inputRef.blur();
      this.setState({intentValue: ''});
    }
  };

  renderDislikeButton = () => (
    <TouchableOpacity
      onPress={this.props.onPressAddIntentField}
      style={styles.dislikeButton}>
      <Image source={ICON_DISLIKE} style={styles.dislikeImage} />
    </TouchableOpacity>
  );

  renderTextInput = () => (
    <View style={styles.intentContainer}>
      <View style={styles.inputContainer}>
        <TextInput
          multiline
          style={styles.inputText}
          onFocus={this.props.onFocus}
          value={this.state.intentValue}
          editable={this.state.textUnlocked}
          onEndEditing={() => this.setState({textUnlocked: false})}
          onChangeText={this.onChangeText}
          ref={ref => (this.inputRef = ref)}
          placeholderTextColor={COLORS.ashen}
          placeholder={this.props.t('chatbot.message.suggestion')}
        />
      </View>
      <TouchableOpacity
        style={styles.sendButton(!/[^\s]/.test(this.state.intentValue))}
        disabled={!/[^\s]/.test(this.state.intentValue)}
        onPress={this.onPressSendIntent}>
        <Image source={ICON_PLUS} style={styles.sendImage} />
      </TouchableOpacity>
    </View>
  );

  renderDefaultMessage = () => {
    const {content, type, fieldState, additional} = this.props.item;
    const state0 = fieldState && fieldState === 'state-0';
    const state1 = fieldState && fieldState === 'state-1';
    const state2 = fieldState && fieldState === 'state-2';
    const chatbot = type !== 1;
    const closedQuestionItems =
      additional &&
      additional.chatBubble &&
      additional.chatBubble.answers &&
      additional.chatBubble.answers.all;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.fieldContainer(chatbot)}>
          <View style={styles.textContainer(chatbot, state1)}>
            {content && typeof content === 'string' ? (
              <HTML html={content} baseFontStyle={styles.textItem} />
            ) : null}
            {chatbot && state2 && (
              <Text style={styles.textReply}>
                {this.props.t('chatbot.message.thanks')}
              </Text>
            )}
            {chatbot && state1 && this.renderTextInput()}
          </View>
          {chatbot && state0 && this.renderDislikeButton()}
        </View>
        <ClosedQuestion items={closedQuestionItems} />
      </View>
    );
  };

  handleSocialMediaAuthSuccess = () => {
    this.props.postChatBubbles({
      chatBubbleId: this.props.chatBubbleId,
    });
  };

  handleSocialMediaAuthError = error => {
    // not implemented - show error to user?
    console.log('SocialMediaAuthError: ', error);
  };

  handleSocialMediaAuthCancelled = () => {
    // not implemented - show error to user?
    console.log('SocialMediaAuth cancelled');
  };

  renderLessonMessage = () => {
    const {
      content,
      closedQuestionID,
      closedQuestionItems,
      socialMediaAuthRequired,
    } = this.props;
    const textField = false;
    const chatbot = true;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.fieldContainer(chatbot)}>
          <View style={styles.textContainer(chatbot, textField)}>
            {content && typeof content === 'string' ? (
              <HTML html={content} baseFontStyle={styles.textItem} />
            ) : null}
            {socialMediaAuthRequired && (
              <SocialMediaAuthButton
                type={socialMediaAuthRequired}
                onAuthSuccess={this.handleSocialMediaAuthSuccess}
                onAuthError={this.handleSocialMediaAuthError}
                onAuthCancelled={this.handleSocialMediaAuthCancelled}
              />
            )}
          </View>
        </View>

        <ClosedQuestion items={closedQuestionItems} id={closedQuestionID} />
      </View>
    );
  };

  renderContentMessage = () => {
    const {type, children} = this.props;
    const textField = false;
    const chatbot = type !== 1;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.fieldContainer(chatbot)}>
          <View style={styles.textContainer(chatbot, textField)}>
            {children}
          </View>
        </View>
      </View>
    );
  };

  render() {
    if (this.props.children) {
      return this.renderContentMessage();
    }
    if (this.props.lesson) {
      return this.renderLessonMessage();
    }
    return this.renderDefaultMessage();
  }
}

const mapDispatchToProps = dispatch =>
  bindActionCreators({postChatBubbles}, dispatch);

export default withTranslation()(
  connect(
    null,
    mapDispatchToProps,
  )(Message),
);
