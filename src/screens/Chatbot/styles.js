import {StyleSheet, Dimensions} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
const SCREEN_HEIGHT = Dimensions.get('window').height;

const mainContainer = {
  flex: 1,
};

const headerContainer = {
  height: 50,
  width: '100%',
  alignItems: 'center',
  justifyContent: 'center',
  backgroundColor: COLORS.mediumSlateBlue,
  elevation: 10,
  paddingHorizontal: 50,
};

const buttonBack = {
  position: 'absolute',
  top: 0,
  left: 0,
  height: '100%',
  width: 50,
  alignItems: 'center',
  justifyContent: 'center',
};

const iconExit = {
  height: 16,
  width: 16,
};

const headerTitle = {
  textAlign: 'center',
  fontSize: 15,
  lineHeight: 16,
  fontFamily: 'Lato-Regular',
  color: COLORS.background,
};

const flatList = {
  flex: 1,
  backgroundColor: COLORS.editorKeyboardButtonText,
};

const bottomSeparator = {
  height: 8,
};

const spinnerContainer = {
  height: 30,
  justifyContent: 'center',
};

const emptyComponentContainer = {
  height: SCREEN_HEIGHT / 2 - 50,
  alignItems: 'center',
  justifyContent: 'flex-end',
};

const emptyComponentText = {
  transform: [{scaleY: -1}],
  fontSize: 15,
  fontFamily: 'Lato-Regular',
  color: COLORS.sensorTextAvailable,
};

const typing = {
  width: 30,
  height: 10,
};

export const styles = StyleSheet.create({
  mainContainer,
  headerContainer,
  buttonBack,
  iconExit,
  headerTitle,
  flatList,
  bottomSeparator,
  spinnerContainer,
  emptyComponentContainer,
  emptyComponentText,
  typing,
});
