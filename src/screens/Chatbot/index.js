import React, {Component} from 'react';
import {connect} from 'react-redux';
import {Text, View, FlatList, Image, ActivityIndicator} from 'react-native';
import {
  getConversationHistory,
  postConversation,
  postIntents,
  addMessageToConversation,
  addIntentField,
} from '../../store/chat/actions';
import {postChatBubbles, getStep} from '../../store/chat/actions';
import {styles} from './styles';
import MessageInput from './components/MessageInput';
import Message from './components/Message';
import BubblesContent from './components/BubblesContent';
import COLORS from 'CodeallMobile/src/theme/colors';
import {bindActionCreators} from 'redux';
import DismissKeyboard from '../../components/DismissKeyboard';
import {withTranslation} from 'react-i18next';

const POINTS = require('CodeallMobile/src/assets/img/typing.gif');
const MESSAGES_PER_PAGE = 20;
const MS_PER_LETTER = 20;

function getTimeout(array) {
  if (array && Array.isArray(array)) {
    const length = array.filter(item => typeof item === 'string').join('')
      .length;
    return length * MS_PER_LETTER;
  }
  return 50 * MS_PER_LETTER;
}

class Chatbot extends Component {
  constructor(props) {
    super(props);
    this.timer = null;
    this.params = this.props.route.params;
    this.lesson = this.params && this.params.lesson;
    this.state = {
      messageValue: '',
      inputFocused: false,
    };
  }

  componentDidMount = () => {
    if (!this.lesson && this.props.conversation.length < MESSAGES_PER_PAGE) {
      this.getConversationHistory();
    }
  };

  componentDidUpdate = prevProps => {
    const currentBubble = this.props.currentLesson[0];
    const prevBubble = prevProps.currentLesson[0];

    if (currentBubble) {
      if (!prevBubble || currentBubble.id !== prevBubble.id) {
        this.props.getStep({stepId: currentBubble.stepId});
      }
    }
  };

  postBubbleIfRequired = () => {
    const {currentLesson, chatBubbelsPending} = this.props;
    const currentBubble = currentLesson[0];
    if (
      currentBubble &&
      currentBubble.status === 'unlocked' &&
      !chatBubbelsPending &&
      !this.timer
    ) {
      const noQuestion = currentBubble.questionType === 'noQuestion';
      const codeNotRequired = currentBubble.isCodeRequiredToContinue === false;
      const socialMediaAuthRequired = currentBubble.socialMediaAuthRequired;
      if (noQuestion && codeNotRequired && !socialMediaAuthRequired) {
        this.timer = setTimeout(() => {
          this.props.postChatBubbles({chatBubbleId: currentBubble.id});
          this.timer = null;
        }, getTimeout(currentBubble.contents));
      }
    }
  };

  componentWillUnmount = () => clearTimeout(this.timer);

  onPressAddIntentField = id => this.props.addIntentField({id});

  onChangeText = messageValue => this.setState({messageValue});

  onFocusMessageInput = () => this.setState({inputFocused: true});

  onBlurMessageInput = () => this.setState({inputFocused: false});

  getConversationHistory = () => {
    if (!this.props.endOfChat) {
      const page = Math.floor(
        this.props.conversation.length / MESSAGES_PER_PAGE,
      );
      this.props.getConversationHistory({page, MESSAGES_PER_PAGE});
    }
  };

  onEndReached = () => {
    if (!this.lesson) {
      this.getConversationHistory();
    }
  };

  scrollToIndex = index => {
    if (index && this.flatListRef) {
      this.flatListRef.scrollToIndex({animated: false, index});
    }
  };

  onPressSendIntent = ({answer, context, id, newValue}) => {
    const {conversation} = this.props;
    var idMarker = false;
    for (var i = 0; i < conversation.length; i++) {
      if (id === conversation[i].id) {
        idMarker = true;
      }
      if (idMarker && conversation[i].type === 1) {
        const question = conversation[i].content;
        this.props.postIntents({question, answer, context, id, newValue});
        break;
      }
    }
  };

  contentForChatbotResponse = response => {
    const data = response.payload.data;
    const error = response.payload.error;
    if (data) {
      return {
        content: data.content,
        fieldState: 'state-0',
      };
    }
    if (error) {
      switch (error.code) {
        case '2001':
        case '2002':
        case '2004':
        case '2005':
        case '2006':
          return {
            content: this.props.t('chatbot.noAnswear'),
            fieldState: 'state-1',
          };
        case '2003':
          return {
            content: this.props.t('chatbot.naughtWords'),
            fieldState: 'state-0',
          };
        default:
          return {
            content:
              this.props.t('chatbot.somethingsWrong') +
              (error.code ? '"Error ' + error.code + ' - ' : '"') +
              ((error.userMessage &&
                error.userMessage !== '' &&
                error.userMessage) ||
                (error.message && error.message !== '' && error.message) ||
                this.props.t('general.unknownError')) +
              '" ;/',
          };
      }
    }
    return {
      content: this.props.t('general.unknownError'),
    };
  };

  onPressSendButton = () => {
    if (!this.lesson) {
      this.props.addMessageToConversation({
        type: 1,
        content: this.state.messageValue,
      });
      this.props
        .postConversation({
          question: this.state.messageValue,
          learningSetName: 'default',
        })
        .then(response => {
          this.props.addMessageToConversation({
            type: 2,
            ...this.contentForChatbotResponse(response),
          });
        });
    }
    this.inputRef.blur();
    this.setState({messageValue: ''});
  };

  ListHeaderComponent = () => (
    <>
      {!!this.timer && (
        <Message type={2}>
          <Image source={POINTS} style={styles.typing} />
        </Message>
      )}
      <View style={styles.bottomSeparator} />
    </>
  );

  ListFooterComponent = () => (
    <View style={styles.spinnerContainer}>
      {this.props.historyPending && <ActivityIndicator color={COLORS.malibu} />}
    </View>
  );

  ListEmptyComponent = () => {
    if (!this.props.historyPending) {
      return (
        <View style={styles.emptyComponentContainer}>
          <Text style={styles.emptyComponentText}>
            {this.lesson
              ? 'Lekcja zablokowana'
              : this.props.t('chatbot.noMessages')}
          </Text>
        </View>
      );
    }
    return null;
  };

  renderItem = (item, index) => {
    if (this.lesson) {
      return <BubblesContent item={item} index={index} />;
    }
    return (
      <Message
        item={item}
        lesson={false}
        onFocus={() => this.scrollToIndex(index)}
        intentsPending={this.props.intentsPending}
        onPressSendIntent={this.onPressSendIntent}
        onPressAddIntentField={() => this.onPressAddIntentField(item.id)}
      />
    );
  };

  render() {
    if (this.lesson) {
      this.postBubbleIfRequired();
    }
    const data = this.lesson
      ? this.props.currentLesson
      : this.props.conversation;
    return (
      <View style={styles.mainContainer}>
        <FlatList
          inverted
          data={data}
          style={styles.flatList}
          onEndReachedThreshold={0.01}
          onEndReached={this.onEndReached}
          ref={r => (this.flatListRef = r)}
          keyboardShouldPersistTaps={'handled'}
          ListEmptyComponent={this.ListEmptyComponent}
          ListHeaderComponent={this.ListHeaderComponent}
          ListFooterComponent={this.ListFooterComponent}
          renderItem={({item, index}) => this.renderItem(item, index)}
          keyExtractor={(item, index) => `${item.id || index}-messages`}
        />
        <DismissKeyboard>
          <MessageInput
            inputRef={r => (this.inputRef = r)}
            inputFocused={this.state.inputFocused}
            commentText={this.state.messageValue}
            onBlur={this.onBlurMessageInput}
            onFocus={this.onFocusMessageInput}
            onChangeText={messageValue => this.onChangeText(messageValue)}
            messageValue={this.state.messageValue}
            onPressSendButton={this.onPressSendButton}
            placeholder={this.props.t('chatbot.inputPlaceholder')}
          />
        </DismissKeyboard>
      </View>
    );
  }
}

const mapStateToProps = state => ({
  historyPending: state.chat.historyPending,
  intentsPending: state.chat.intentsPending,
  conversation: state.chat.conversation,
  nextBubble: state.chat.nextBubble,
  currentLesson: state.chat.currentLesson,
  endOfChat: state.chat.endOfChat,
  chatBubbelsPending: state.chat.chatBubbelsPending,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      getConversationHistory,
      postConversation,
      postIntents,
      addMessageToConversation,
      addIntentField,
      postChatBubbles,
      getStep,
    },
    dispatch,
  );

export default withTranslation()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(Chatbot),
);
