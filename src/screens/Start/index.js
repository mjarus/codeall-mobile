import React from 'react';
import {ScrollView, Image, StyleSheet} from 'react-native';
import { CircleButton } from '../../components/Buttons';
import {useTheme} from '@react-navigation/native';
import typo from '../../theme/typography';
import TranslatedText from '../../components/TranslatedText';

const logotype = require('../../assets/img/logo_codeall.png');
const owl = require('../../assets/img/start_owl.png');

const Start = props => {
  const {base, images, typography, spacings} = useTheme();
  return (
    <ScrollView contentContainerStyle={base}>
      <Image source={logotype} style={images.logotype.m} />
      <Image source={owl} style={[styles.owl, spacings.margins.xl]} />
      <TranslatedText
        id="start.hi"
        style={{
          ...typography.middleHeader,
          ...spacings.margins.l,
          ...styles.hi,
        }}
      />
      <TranslatedText
        id="start.header.line1"
        style={[typography.text, spacings.margins.xxs]}
      />
      <TranslatedText id="start.header.line2" style={typography.text} />
      <CircleButton
        onPress={() => props.navigation.navigate('RegisterDevice')}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  owl: {
    width: 274,
    height: 233,
  },

  hi: {
    fontSize: typo.sizes.m,
    fontFamily: typo.fonts.secondary,
  },
});

export default Start;
