import React, {useState} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Image,
} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../../../theme/typography';
import TranslatedText from '../../../../components/TranslatedText';
import {useTranslation} from 'react-i18next';
const hiddenIcon = require('CodeallMobile/src/assets/img/hiddenInputIcon.png');
const shownIcon = require('CodeallMobile/src/assets/img/visibleInputIcon.png');

const InputWithLabel = ({
  labelId,
  onChange,
  defaultValue,
  secureTextEntry,
  placeholder,
  editable,
}) => {
  const {t} = useTranslation();

  const [isContentHidden, setContentVisibility] = useState(secureTextEntry);

  const handleContentVisibility = () => {
    setContentVisibility((prev) => !prev);
  };

  const renderIcon = () => {
    return isContentHidden ? (
      <TouchableOpacity
        onPress={handleContentVisibility}
        style={styles.iconContainer}>
        <Image source={shownIcon} style={styles.visibleIcon} />
      </TouchableOpacity>
    ) : (
      <TouchableOpacity
        onPress={handleContentVisibility}
        style={styles.iconContainer}>
        <Image source={hiddenIcon} style={styles.hiddenIcon} />
      </TouchableOpacity>
    );
  };

  return (
    <View style={styles.container}>
      <TranslatedText style={typography.inputLabel} id={labelId} />
      <View style={styles.inputBox}>
        <TextInput
          style={styles.input}
          onChangeText={onChange}
          defaultValue={defaultValue}
          placeholder={t(placeholder)}
          placeholderTextColor={COLORS.inputContent}
          secureTextEntry={isContentHidden}
          editable={editable}
        />
        {secureTextEntry && renderIcon()}
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 256,
    marginVertical: 5,
  },

  input: {
    width: 243,
    fontSize: typography.sizes.s,
    padding: 0,
    fontFamily: typography.primary,
    color: COLORS.inputContent,
    backgroundColor: COLORS.background,
    borderRadius: 4,
    height: 32,
    paddingLeft: 12.15,
  },
  iconContainer: {
    position: 'absolute',
    right: 25,
    bottom: 8,
  },
  hiddenIcon: {
    width: 20,
    height: 16,
  },
  visibleIcon: {
    width: 20,
    height: 13,
    marginBottom: 2,
  },
});

export default InputWithLabel;
