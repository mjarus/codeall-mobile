import React, {useState, useEffect} from 'react';
import {TouchableOpacity, Image, StyleSheet, View, Text} from 'react-native';
import {useTranslation} from 'react-i18next';
import COLORS from 'CodeallMobile/src/theme/colors';
import DismissKeyboard from '../../../components/DismissKeyboard';
import { RoundedButton } from '../../../components/Buttons';
import typography from '../../../theme/typography';
import {currentPin} from '../../../mocks/pin';
import InputWithLabel from './components/InputWithLabel';

const exitIcon = require('CodeallMobile/src/assets/img/blackExit.png');

const UserLock = ({navigation}) => {
  const {t} = useTranslation();

  const [pins, setPins] = useState({
    newPin: '',
    confirmedPin: '',
  });
  const [error, setError] = useState({isError: false, message: ''});

  useEffect(() => {
    if (error.isError) {
      setError({isError: false, message: ''});
    }
  }, [pins]);

  const handleSave = () => {
    const {newPin, confirmedPin} = pins;

    if (newPin.length < 4) {
      return setError({
        isError: true,
        message: t('userLock.errors.short'),
      });
    }

    if (newPin !== confirmedPin) {
      return setError({isError: true, message: t('userLock.errors.notMatch')});
    }

    navigation.goBack();
  };

  return (
    <DismissKeyboard>
      <View style={styles.container}>
        <View style={styles.header}>
          <TouchableOpacity
            onPress={() => navigation.goBack()}
            style={styles.back}>
            <Image source={exitIcon} style={styles.exitIcon} />
          </TouchableOpacity>
          <Text style={styles.title}>{t('userLock.header')}</Text>
        </View>
        <View style={styles.inputContainer}>
          {currentPin && (
            <InputWithLabel
              labelId="userLock.labels.currentPin"
              editable={false}
              defaultValue={currentPin}
              placeholder="userLock.labels.currentPin"
              secureTextEntry
              //error={error} handling errors in inputs to do
            />
          )}
          <InputWithLabel
            labelId="userLock.labels.newPin"
            onChange={(value) => setPins((prev) => ({...prev, newPin: value}))}
            defaultValue={pins.new}
            placeholder="userLock.labels.newPin"
            secureTextEntry
            //error={error} handling errors in inputs to do
          />
          <InputWithLabel
            labelId="userLock.labels.confirmPin"
            onChange={(value) =>
              setPins((prev) => ({...prev, confirmedPin: value}))
            }
            defaultValue={pins.confirm}
            placeholder="userLock.labels.confirmPin"
            secureTextEntry
            //error={error} handling errors in inputs to do
          />
          {error.isError && (
            <Text style={styles.errorMessage}>{error.message}</Text>
          )}
        </View>
        <RoundedButton
          headerTextId="userSettings.saveChanges"
          size="medium"
          onPress={handleSave}
        />       
      </View>
    </DismissKeyboard>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: COLORS.secondaryBackground,
  },
  back: {
    position: 'absolute',
    top: 17,
    left: 10,
  },

  inputContainer: {
    alignItems: 'center',
    marginTop: 60,
    marginBottom: 50,
  },
  errorMessage: {
    maxWidth: 238,
    textAlign: 'center',
    color: COLORS.textSecondary,
    fontFamily: typography.fonts.secondary,
    fontSize: typography.sizes.s,
    marginTop: 20,
  },
  header: {
    backgroundColor: COLORS.background,
    height: 55,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  exitIcon: {
    width: 27.27,
    height: 26.21,
  },
  title: {
    fontSize: 22,
    fontFamily: typography.fonts.secondary,
    color: COLORS.mainText,
  },
});

export default UserLock;
