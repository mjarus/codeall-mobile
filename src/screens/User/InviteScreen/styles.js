import {Dimensions, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../../theme/typography';

const bigSpace = {
  marginTop: 40,
};

const smallSpace = {
  marginTop: 20,
};

const giftImage = {
  width: 142,
  height: 142,
  ...bigSpace,
};

const header = {
  width: "65%", 
};

const shareButton = {
  ...bigSpace,
};

const copyText = {
  ...smallSpace,
};

const scrollBox = {
  alignItems: 'center',
  flex: 1,
};

const closeButton = {
  left: '45%',
  top: '2%',
  justifyContent: 'center',
};

const statisticsBox = {
  minWidth: '80%',
  ...bigSpace,
  marginBottom: 10,
};

const notification = {
  position: 'absolute',
  bottom: 30,
  left: Dimensions.get('window').width / 2 - 60,
  color: '#ccc',
  backgroundColor: COLORS.switchSecondary,
  borderRadius: 30,
  width: 120,
  height: 45,
  justifyContent: 'center',
  alignItems: 'center',
  elevation: 1,
};

const notificationText = {
  fontFamily: typography.fonts.primary,
  fontSize: typography.sizes.sm,
  color: COLORS.background,
};

export const styles = StyleSheet.create({
  giftImage,
  header,
  scrollBox,
  closeButton,
  statisticsBox,
  shareButton,
  copyText,
  notification,
  notificationText,
});
