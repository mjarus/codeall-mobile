import React, {useState, useEffect} from 'react';
import {ScrollView, Image, View, TouchableOpacity} from 'react-native';
import {useTheme} from '@react-navigation/native';
import {connect} from 'react-redux';
import StatisticsBox from './components/StatisticsBox';
import ShareButton from './components/ShareButton';
import {styles} from './styles';
import CopyCode from './components/CopyCode';
import TranslatedText from '../../../components/TranslatedText';
import Header from '../../../components/Header';

const giftImageSource = require('../../../assets/img/gift.png');
const closeIcon = require('../../../assets/img/turquiseCloseIcon.png');

function InviteScreen(props) {
  const {typography} = useTheme();
  const [isNotification, setIsNotification] = useState(false);

  useEffect(() => {
    let timeoutId;
    if (isNotification) {
      timeoutId = setTimeout(() => {
        setIsNotification(false);
      }, 3000);
    }

    return () => {
      clearTimeout(timeoutId);
    };
  }, [isNotification]);

  return (
    <ScrollView contentContainerStyle={styles.scrollBox}>
      <Header 
        title={"inviteScreen.header"} 
        style={styles.header} 
        rightIcon={closeIcon} 
        onRightPress={props.navigation.goBack}
      />
      <View style={styles.giftImage}>
        <Image source={giftImageSource} />
      </View>
      <TranslatedText
        id="inviteScreen.description.line1"
        style={typography.userInviteDescriptionText}
      />
      <TranslatedText
        id="inviteScreen.description.line2"
        style={typography.userInviteDescriptionTextLine2}
      />
      <ShareButton
        style={styles.shareButton}
        referralCode={props.user.referralCode}
      />
      <CopyCode
        style={styles.copyText}
        code={props.user.referralCode}
        setIsNotification={setIsNotification}
      />
      <StatisticsBox style={styles.statisticsBox} user={props.user} />
      {isNotification && (
        <TouchableOpacity
          style={styles.notification}
          onPress={() => setIsNotification(false)}>
          <TranslatedText
            id="inviteScreen.copy.notification"
            style={styles.notificationText}
          />
        </TouchableOpacity>
      )}
    </ScrollView>
  );
}

const mapStateToProps = state => ({
  user: state.profile.user,
});

export default connect(mapStateToProps)(InviteScreen);
