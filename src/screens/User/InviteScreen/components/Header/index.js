import {View} from 'react-native';
import React from 'react';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../../components/TranslatedText';

function Header({style}) {
  const {typography} = useTheme();

  return (
    <View style={style}>
      <TranslatedText
        style={typography.userInviteHeader}
        id="inviteScreen.header.top"
      />
      <TranslatedText
        style={typography.userInviteHeader}
        id="inviteScreen.header.bottom"
      />
    </View>
  );
}

export default Header;
