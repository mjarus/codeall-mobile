import Statistic from '../Statistic';
import React from 'react';
import {View} from 'react-native';

function StatisticsBox({style, user}) {
  return (
    <View style={style}>
      <Statistic
        descriptionId="inviteScreen.statistics.accountsCreated"
        count={user.referralUsedBy.length}
        icon="user-plus"
      />
      <Statistic
        descriptionId="inviteScreen.statistics.freeSensors"
        count={0}
        icon="wifi"
      />
      <Statistic
        descriptionId="inviteScreen.statistics.usedFreeSensors"
        count={0}
        icon="check"
      />
    </View>
  );
}

export default StatisticsBox;
