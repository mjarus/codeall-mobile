import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const button = {
  backgroundColor: COLORS.lightPurple,
  borderRadius: 6,
  paddingVertical: 7,
  paddingHorizontal: 14,
  width: '75%',
  display: 'flex',
  alignItems: 'center',
  flexDirection: 'row',
  justifyContent: 'space-between',
};

export const styles = StyleSheet.create({
  button,
});
