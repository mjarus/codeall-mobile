import {Icon} from 'react-native-elements';
import {TouchableOpacity, Share} from 'react-native';
import React from 'react';
import {styles} from './styles';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../../components/TranslatedText';

function ShareButton({style, referralCode}) {
  const {typography} = useTheme();

  async function handlePress() {
    const appURL = 'https://codeall.fun/referral/referralCode';
    const message = `Zacznij naukę programowania z CodeAll i zaprogramuj inteligentne otoczenie. Wykorzystaj kod ${referralCode}, żeby otrzymać punkty na darmowe czujniki. Oto link: ${appURL}`;
    await Share.share({message});
  }
  return (
    <TouchableOpacity
      style={{...styles.button, ...style}}
      onPress={handlePress}>
      <TranslatedText
        id="inviteScreen.shareButton"
        style={typography.userInviteShare}
      />
      <Icon name="share" type="material" />
    </TouchableOpacity>
  );
}

export default ShareButton;
