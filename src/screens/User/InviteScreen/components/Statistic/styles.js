import {StyleSheet} from 'react-native';

const mainContainer = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  marginVertical: 5,
};
const textContainer = {
  display: 'flex',
  flexDirection: 'row',
};
const counter = {
  fontFamily: 'Quicksand-Bold',
};
const iconContainer = {
  width: 30,
  alignItems: 'flex-start',
  justifyContent: 'center',
};

export const styles = StyleSheet.create({
  mainContainer,
  textContainer,
  counter,
  iconContainer,
});
