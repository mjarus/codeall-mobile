import React from 'react';
import {View, Text} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import {Icon} from 'react-native-elements';
import {styles} from './styles';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../../components/TranslatedText';

function Statistic({descriptionId, count, icon}) {
  const {typography} = useTheme();

  return (
    <View style={styles.mainContainer}>
      <View style={styles.textContainer}>
        <View style={styles.iconContainer}>
          <Icon
            name={icon}
            type="font-awesome-5"
            size={15}
            color={COLORS.aqua}
          />
        </View>
        <TranslatedText
          id={descriptionId}
          style={typography.userInviteStatisticText}
        />
        <Text style={typography.userInviteStatisticText}>: </Text>
      </View>
      <Text style={styles.counter}>{count}</Text>
    </View>
  );
}

export default Statistic;
