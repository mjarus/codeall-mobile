import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const mainContainer = {
  display: 'flex',
  flexDirection: 'row',
  justifyContent: 'space-between',
  alignItems: 'center',
  width: '70%',
};

const icon = {
  color: COLORS.switchSecondary,
};

export const styles = StyleSheet.create({
  mainContainer,
  icon,
});
