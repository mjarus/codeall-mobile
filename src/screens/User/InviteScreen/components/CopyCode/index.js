import React from 'react';
import {Text, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import Clipboard from '@react-native-community/clipboard';
import {Icon} from 'react-native-elements';
import {styles} from './styles';
import TranslatedText from '../../../../../components/TranslatedText';

function CopyCode({style, code, setIsNotification}) {
  const {typography} = useTheme();

  const handlePress = () => {
    Clipboard.setString(code);
    setIsNotification(true);
  };
  return (
    <View style={{...styles.mainContainer, ...style}}>
      <TranslatedText
        id="inviteScreen.yourCode"
        style={typography.userInviteDescriptionCode}
      />
      <Text style={typography.userInviteCode}>{code}</Text>
      <Icon
        name="content-copy"
        type="material"
        onPress={handlePress}
        iconStyle={styles.icon}
      />
    </View>
  );
}

export default CopyCode;
