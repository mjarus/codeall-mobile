import React, {Component} from 'react';
import {
  Image,
  View,
  TouchableOpacity,
  StyleSheet,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import {launchImageLibrary} from 'react-native-image-picker';
import {putProfileAvatar, putProfileInfo} from '../../../store/profile/actions';
import {getProfile} from '../../../store/profile/actions';
import {theme} from '../../../theme';
import InputWithLabel from 'CodeallMobile/src/components/InputWithLabel';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';
import { RoundedButton } from '../../../components/Buttons';
import TranslatedText from '../../../components/TranslatedText';
import Header from '../../../components/Header';

const exitIcon = require('CodeallMobile/src/assets/img/exit.png');

class UserSettings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      login: '',
      name: '',
      surname: '',
      password: '',
      rePassword: '',
      isSave: false,
      uriLocal: null,
    };
  }
  onFocus(i) {
    this.setState(prevState => {
      const newFocus = [...prevState.focus];
      newFocus[i] = !newFocus[i];
      return {focus: newFocus};
    });
  }

  toggleSecurePasswordEntry(i) {
    this.setState(prevState => {
      const newSecure = [...prevState.secureTextEntry];
      newSecure[i] = !newSecure[i];
      return {secureTextEntry: newSecure};
    });
  }

  saveChanges = () => {
    this.props
      .putProfileInfo({
        userId: this.props.userId,
        name: this.state.name,
        surname: this.state.surname,
        nick: this.state.login,
      })
      .then(this.props.putProfileAvatar(this.state.uriLocal))
      .then(this.props.getProfile({userId: this.props.userId}))
      .then(this.setState({isSave: true}))
      .then(this.props.navigation.goBack());
  };

  handlePhotoChange = () => {
    launchImageLibrary(
      {
        maxHeight: 141,
        maxWidth: 141,
        selectionLimit: 0,
        mediaType: 'photo',
        includeBase64: false,
        quality: 1,
      },
      response => {
        if (response.didCancel) {
          return;
        }

        const {uri} = response.assets[0];
        this.setState({uriLocal: uri});
      },
    );
  };

  render() {
    const {avatar} = this.props.route.params;
    const avatarUri = this.state.uriLocal
      ? this.state.uriLocal
      : this.props.avatarUri;

    const imageSource = avatarUri
      ? {source: {uri: avatarUri}}
      : {source: avatar};

    return (
      <ScrollView contentContainerStyle={styles.container}>
        <Header 
          title="userSettings.yourAccount" 
          leftIcon={exitIcon} 
          style={styles.header}
          onLeftPress={this.props.navigation.goBack}
        />
        <View style={styles.pictureBox}>
          <View style={[styles.avatarCircle, styles.shadowBox]}/>
          <View style={styles.avatarCircle}>
          < Image {...imageSource} style={styles.picture} />
          </View>
        </View>
        <TouchableOpacity onPress={this.handlePhotoChange}>
          <TranslatedText
            id="userSettings.changePhoto"
            style={typography.profileTitle}
          />
        </TouchableOpacity>
        <View style={styles.inputsContainer}>
          <InputWithLabel
            labelId="userSettings.name"
            onChange={value => this.setState({name: value})}
            defaultValue={this.props.name}
            placeholder="userSettings.name"
          />
          <InputWithLabel
            labelId="userSettings.surname"
            onChange={value => this.setState({surname: value})}
            defaultValue={this.props.surname}
            placeholder="userSettings.surname"
          />
          <InputWithLabel
            labelId="userSettings.language"
            onChange={value => this.setState({language: value})}
            defaultValue={this.props.surname}
            isSave={this.state.isSave}
            isSelect

          />
        </View>
        <View style={styles.buttonsContainer}>
          <RoundedButton
            headerTextId="userSettings.saveChanges"
            size="medium"
            onPress={this.saveChanges}
          />
          <TouchableOpacity
            style={styles.lockContainer}
            onPress={() => this.props.navigation.navigate('UserLock')}>
            <TranslatedText id="userSettings.setLock" style={typography.profileTitle} />
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  name: state.profile.name,
  surname: state.profile.surname,
  avatarUri: state.profile.avatarUri,
  email: state.profile.email,
  userId: state.auth.userId,
});

const mapDispatchToProps = dispatch => ({
  logout: () => dispatch({type: 'CLEAR_STATE'}),
  putProfileInfo: data => dispatch(putProfileInfo(data)),
  getProfile: data => dispatch(getProfile(data)),
  putProfileAvatar: avatarUri => dispatch(putProfileAvatar(avatarUri)),
});

export const styles = StyleSheet.create({
  exit: {
    position: 'absolute',
    top: 10,
    left: 10,
  },
  exitIcon: {
    width: 22,
    height: 21,
  },
  container: {
    alignItems: 'center',
    backgroundColor: COLORS.secondaryBackground,
  },

  pictureBox: {
    width: '100%',
    ...theme.centeredElement,
  },
  picture: {
    width: 142,
    height: 142,
    marginTop: 48,
    borderRadius: 100,
    borderColor: 'transparent',
    borderWidth: 1,
    marginBottom: 16,
  },
  avatarCircle: {
    width: 141,
    height: 141,
    borderRadius: 100,
    backgroundColor: COLORS.background,
    alignItems: "center",
    justifyContent: "flex-end",
  },

  inputsContainer: {
    marginTop: 30,
  },

  logoutIcon: {
    height: 20,
    width: 17,
  },
  lockContainer: {
    alignSelf: 'center',
    marginTop: 25,
  },

  buttonsContainer: {
    marginTop: 37,
    marginBottom: 50,
  },
  pictureBox: {
    position: "relative",
    marginTop: 43,
    marginBottom: 31,
  },

  shadowBox: {
    position: "absolute",
    top: 4,
    backgroundColor: COLORS.buttonShadow,
  },

  picture: {
    width: 100,
    height: 120,
    alignSelf: 'center',
    overflow: 'hidden',
    marginBottom: 6,
  },
  header: {
    color: COLORS.textSecondary,
    fontFamily: typography.fonts.secondary,
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(UserSettings);
