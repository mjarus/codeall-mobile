import React from 'react';
import {
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  StyleSheet,
  View,
} from 'react-native';
import {useSelector} from 'react-redux';
import {useTheme} from '@react-navigation/native';
import COLORS from 'CodeallMobile/src/theme/colors';
import { RoundedButton } from '../../../components/Buttons';
import TranslatedText from '../../../components/TranslatedText';
import Header from '../../../components/Header';

const avatar = require('CodeallMobile/src/assets/img/avatarContentImage.png');
const iconAchievement = require('../../../assets/img/iconAchievement.png');
const iconSettings = require('../../../assets/img/iconSettings.png');

const UserProfile = ({navigation}) => {
  const {nick, avatarUri} = useSelector((state) => state.profile);

  const imageSource = avatarUri ? {source: {uri: avatarUri}} : {source: avatar};

  const handleInviteButtonPress = () => {
    navigation.navigate('InviteScreen');
  };

  const {typography} = useTheme();

  return (
    <ScrollView contentContainerStyle={styles.scrollBox}>
      <Header title="userProfile.account" style={styles.header}/>
      <View style={styles.pictureBox}>
        <View style={[styles.avatarCircle, styles.shadowBox]} />
        <View style={styles.avatarCircle}>
          <Image {...imageSource} style={styles.picture} />
        </View>
      </View>
      <Text style={typography.profileName}>{nick}</Text>
      <TouchableOpacity style={styles.box}>
        <Image style={styles.iconAchievement} source={iconAchievement} />
        <TranslatedText
          id="userProfile.achievements"
          style={typography.profileTitle}
        />
      </TouchableOpacity>
      <TouchableOpacity
        onPress={() => {
          navigation.navigate('UserSettings', {avatar});
        }}
        style={[styles.box, {marginBottom: 55}]}>
        <Image
          style={{...styles.iconSettings, ...styles.settingsIcon}}
          source={iconSettings}
        />
        <TranslatedText
          id="userProfile.settings"
          style={typography.profileTitle}
        />
      </TouchableOpacity>
      <RoundedButton
        headerTextId="userProfile.inviteButton"
        size="big"
        onPress={handleInviteButtonPress}
      />
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  mainBox: {
    alignItems: 'center',
    width: '100%',
  },

  pictureBox: {
    position: 'relative',
    marginTop: 43,
    marginBottom: 31,
  },

  shadowBox: {
    position: 'absolute',
    top: 4,
    backgroundColor: COLORS.buttonShadow,
  },

  picture: {
    width: 100,
    height: 120,
    alignSelf: 'center',
    overflow: 'hidden',
    marginBottom: 6,
  },

  box: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginBottom: 29,
    width: 170,
    height: 48,
  },

  iconAchievement: {
    width: 40,
    height: 38,
  },

  iconSettings: {
    width: 48,
    height: 48,
  },

  scrollBox: {
    alignItems: 'center',
    paddingBottom: 40,
    flex: 1,
    backgroundColor: COLORS.secondaryBackground,
  },

  avatarCircle: {
    width: 141,
    height: 141,
    borderRadius: 100,
    backgroundColor: COLORS.background,
    alignItems: 'center',
    justifyContent: 'flex-end',
  },

  settingsIcon: {
    marginLeft: -4,
  },
  header: {
    color: COLORS.textSecondary,
  },
});

export default UserProfile;
