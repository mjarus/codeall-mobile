import React from 'react';
import {Text, View, TouchableOpacity, StyleSheet, Image} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import InformationHex from '../InformationHex';
import typography from '../../../../theme/typography';

const ellipse = require('CodeallMobile/src/assets/img/lessonsEllipse.png');
const doneLessonIcon = require('CodeallMobile/src/assets/img/doneLesson.png');
const lockedLessonIcon = require('CodeallMobile/src/assets/img/lockedLesson.png');
const inProgressLessonIcon = require('CodeallMobile/src/assets/img/inProgressLesson.png');

const LessonBox = ({
  lesson,
  sensors,
  index,
  setCurrentLesson,
  setIsModalVisible,
}) => {
  const handleLessonPress = () => {
    setCurrentLesson(lesson);
    setIsModalVisible(true);
  };
  const displayLessonStatusIcon = () => {
    switch (lesson.status) {
      case 'done':
        return doneLessonIcon;
      case 'inProgress':
        return inProgressLessonIcon;
      case 'locked':
        return lockedLessonIcon;
      default:
        return lockedLessonIcon;
    }
  };

  const renderSensorsIcon = () => {
    const areSensorsUsed =
      Array.isArray(lesson.sensors) && lesson.sensors.length ? true : false;

    return areSensorsUsed ? (
      <InformationHex sensors={sensors} style={styles.hex} />
    ) : null;
  };

  return (
    <TouchableOpacity
      onPress={handleLessonPress}
      style={styles.container}
      disabled={lesson.status === 'locked'}>
      {renderSensorsIcon()}
      <View style={styles.lessonCircle}>
        <Image source={ellipse} style={styles.ellipse} />
        <View style={styles.internalCirlce}>
          <Text style={styles.numberText}>{index + 1}</Text>
        </View>
        <Image
          source={displayLessonStatusIcon()}
          style={styles.lessonstatusIcon}
        />
      </View>

      <View style={styles.nameContainer}>
        <Text style={styles.name}>{lesson.name}</Text>
      </View>
    </TouchableOpacity>
  );
};

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 0,
    width: '100%',
    minHeight: 70,
    marginBottom: 49,
    paddingLeft: 24,
    paddingRight: 24,
  },
  nameContainer: {
    width: 184,
  },
  name: {
    fontFamily: typography.fonts.primary,
    fontSize: typography.sizes.sm,
    maxWidth: 173,
    color: COLORS.mainText,
    fontWeight: 'bold',
    lineHeight: 21,
    marginLeft: 31,
  },
  lessonCircle: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    width: 75,
    height: 75,
    marginLeft: 17,
  },
  internalCirlce: {
    position: 'absolute',
    width: 60,
    height: 60,
    backgroundColor: COLORS.lessonsInternalCircle,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
  },
  ellipse: {
    width: 76,
    height: 80,
  },
  finishedIcon: {
    position: 'absolute',
    width: 35,
    height: 30,
  },
  numberText: {
    ...typography.lessonsNumber,
    color: COLORS.mainText,
    marginBottom: 10,
  },
  lessonstatusIcon: {
    width: 35,
    height: 35,
    position: 'absolute',
    right: -8,
    bottom: -8,
  },
});

export default LessonBox;
