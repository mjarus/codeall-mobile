import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Image} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import typography from '../../../../theme/typography';
import TranslatedText from '../../../../components/TranslatedText';
import COLORS from 'CodeallMobile/src/theme/colors';

const sensorIcon = require('CodeallMobile/src/assets/img/sensorsIconActive.png');

const TopicsBox = ({topic, sensors}) => {
  const navigation = useNavigation();

  const goToLessonsScreen = () => {
    navigation.navigate('LessonsScreen', {topic, sensors});
  };

  const renderLessonsDots = () => {
    return (
      <View style={styles.dotsBox}>
        {topic.lessons.map((lesson, index) => {
          return lesson.status === 'done' ? (
            <View
              style={[
                styles.dot,
                styles.yellowDot,
                index === 0 ? styles.firstDot : null,
                index === topic.lessons.length - 1 ? styles.lastDot : null,
              ]}
              key={index}
            />
          ) : (
            <View
              style={[
                styles.dot,
                styles.goldenDot,
                index === 0 ? styles.firstDot : null,
                index === topic.lessons.length - 1 ? styles.lastDot : null,
              ]}
              key={index}
            />
          );
        })}
      </View>
    );
  };

  const renderSensorsIcon = () => {
    const areSensorsUsed = topic.lessons
      .map((lesson) =>
        Array.isArray(lesson.sensors) && lesson.sensors.length ? true : false,
      )
      .includes(true);

    return areSensorsUsed ? (
      <Image source={sensorIcon} style={styles.sensorIcon} />
    ) : null;
  };

  return (
    <View style={styles.externalBox}>
      <View style={styles.shadowBox} />
      <TouchableOpacity style={styles.topicBox} onPress={goToLessonsScreen}>
        <Text style={typography.topicName}>{topic.name}</Text>
        <Text style={styles.lessonsLengthText}>
          {topic.lessons.length} <TranslatedText id={'topic.lessons'} />
        </Text>
        {renderSensorsIcon()}
      </TouchableOpacity>
      <View style={styles.lessonsRow}>{renderLessonsDots()}</View>
    </View>
  );
};

const styles = StyleSheet.create({
  topicIcon: {
    width: 150,
    height: 150,
  },

  externalBox: {
    marginRight: 12,
    marginLeft: 12,
    zIndex: 1000,
    marginBottom: 20,
    width: 163,
    height: 112,
    backgroundColor: COLORS.buttonShadow,
    borderRadius: 4,
  },

  topicBox: {
    padding: 11,
    backgroundColor: COLORS.lessonTopic,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    height: 96,
  },

  lessonsLengthText: {
    fontFamily: typography.fonts.primary,
    fontSize: 12,
    color: COLORS.topicText,
    marginTop: 5,
  },
  dotsBox: {
    width: '100%',
    height: 12,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexGrow: 1,
  },
  dot: {
    width: '100%',
    height: 12,
    flexShrink: 1,
  },

  firstDot: {
    borderBottomLeftRadius: 4,
  },

  lastDot: {
    borderBottomRightRadius: 4,
  },

  yellowDot: {
    backgroundColor: COLORS.lessonsDoneDots,
  },

  goldenDot: {
    backgroundColor: COLORS.lessonsToDoDots,
  },
  lessonsRow: {
    flexGrow: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  sensorIcon: {
    width: 25,
    height: 22,
    position: 'absolute',
    bottom: 9,
    right: 10,
    resizeMode: 'contain',
  },
  noSensorIcon: {
    width: 23,
    height: 23,
    position: 'absolute',
    bottom: 9,
    right: 10,
  },
});

export default TopicsBox;
