import React from 'react';
import {View, StyleSheet} from 'react-native';
import IconButton from '../../../../components/IconButton/index.js';

const hexagon = require('CodeallMobile/src/assets/img/sensorsIconActive.png');

const InformationHex = ({sensors, style}) => {
  return (
    <View style={{...style, ...styles.progressBox}}>
      {sensors && (
        <IconButton
          style={styles.containerHexagon}
          imageStyle={styles.hexagon}
          source={hexagon}
        />
      )}
    </View>
  );
};
const styles = StyleSheet.create({
  progressBox: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    zIndex: 10000000,
    position: 'absolute',
  },
  hexagon: {
    width: 25,
    height: 25,
  },
  containerHexagon: {
    zIndex: 1000,
  },
});

export default InformationHex;
