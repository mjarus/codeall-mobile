import React from 'react';
import {View, Text, Image, TouchableOpacity, StyleSheet} from 'react-native';
import {theme} from '../../../../theme/index.js';
import {useTheme} from '@react-navigation/native';

const hexagon = require('../../../../assets/img/hexagon.png');
// TO DO ProgressBar in lessons tab ...
const ProgressBar = (props) => {
  const width = props.sensors ? 62 : 87;
  const {typography} = useTheme();
  return (
    <View style={styles.progressBox}>
      {props.sensors && (
        <TouchableOpacity onPress={props.setLesson}>
          <Image source={hexagon} style={styles.hexagon} />
        </TouchableOpacity>
      )}
      <View style={styles.bar(width)}>
        <View style={styles.progressBar(props.percent, width)} />
      </View>
      <Text style={typography.percent}>{props.percent}%</Text>
    </View>
  );
};
const progressBarHeight = 10;
const styles = StyleSheet.create({
  progressBox: {
    flexDirection: 'row',
    ...theme.centeredElement,
    marginTop: 12,
  },
  bar: (width) => ({
    width: width,
    height: progressBarHeight,
    borderRadius: 15,
    backgroundColor: theme.colors.progressBarBackground,
  }),
  progressBar: (value, width) => ({
    backgroundColor: theme.colors.progressBarProgress,
    width: (width * value) / 100,
    height: progressBarHeight,
    borderRadius: 15,
  }),
  hexagon: {
    width: 19,
    height: 17,
    marginRight: 6,
  },
});

export default ProgressBar;
