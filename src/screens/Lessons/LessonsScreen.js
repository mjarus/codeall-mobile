import React from 'react';
import {
  StyleSheet,
  Text,
  View,
  Image,
  FlatList,
  ScrollView,
} from 'react-native';
import {useNavigation, useRoute} from '@react-navigation/native';
import {useDispatch, useSelector} from 'react-redux';
import LessonBox from './components/LessonBox';
import {getLessonsDetails} from '../../store/chat/actions';
import PopupWithSensorsList from '../../components/PopupWithSensorsList';
import {useState} from 'react';
import typography from '../../theme/typography';
import COLORS from 'CodeallMobile/src/theme/colors';
import TranslatedText from '../../components/TranslatedText';
import HTML from 'react-native-render-html';
import Header from '../../components/Header';

const exitImage = require('CodeallMobile/src/assets/img/exit.png');
const owlImage = require('CodeallMobile/src/assets/img/owlImage.png');

const LessonsScreen = () => {
  const navigation = useNavigation();
  const route = useRoute();
  const dispatch = useDispatch();
  const currentUnitId = useSelector((state) => state.lessons.currentUnitId);
  const [currentLesson, setCurrentLesson] = useState(true);
  const [isModalVisible, setIsModalVisible] = useState(false);
  const {topic, sensors} = route.params;

  const goToChatbot = async (item) => {
    dispatch(
      getLessonsDetails({
        unitId: currentUnitId,
        lessonId: item.id,
      }),
    );
    navigation.navigate('LessonNavigator', {
      currentLesson: item,
      lesson: true,
    });
  };

  return (
    <>
      <PopupWithSensorsList
        onClose={() => setIsModalVisible(false)}
        onPress={() => goToChatbot(currentLesson)}
        visible={isModalVisible}
        lesson={currentLesson}
      />
      <ScrollView contentContainerStyle={styles.container}>
        <View style={styles.exitImageRow}>
          <Header leftIcon={exitImage} onLeftPress={navigation.goBack} />
        </View>
        <View style={styles.lessonsContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.name}>{topic.name}</Text>
            {/* <Text style={{...styles.description, ...styles.descriptionText}}> */}
            <View style={styles.descriptionContainer}>
              <HTML
                html={topic.description}
                baseFontStyle={styles.description}
              />
            </View>
            {/* </Text> */}
            <Text style={styles.description}>
              ({topic.lessons.length}{' '}
              <TranslatedText id="lessons.lessonsTotal" />)
            </Text>
            <Image source={owlImage} style={styles.owlImage} />
          </View>
          <FlatList
            contentContainerStyle={styles.list}
            data={topic.lessons}
            showsHorizontalScrollIndicator={false}
            keyExtractor={(item, index) => `${item.id || index}-units`}
            renderItem={({item, index}) => (
              <LessonBox
                lesson={item}
                sensors={sensors}
                index={index}
                setCurrentLesson={setCurrentLesson}
                setIsModalVisible={setIsModalVisible}
              />
            )}
          />
        </View>
      </ScrollView>
    </>
  );
};

const styles = StyleSheet.create({
  container: {
    paddingBottom: 30,
  },
  exitImageRow: {
    width: '100%',
    backgroundColor: COLORS.secondaryBackground,
    paddingBottom: 27,
  },
  lessonsContainer: {
    width: '100%',
    justifyContent: 'center',
  },
  headerContainer: {
    alignItems: 'center',
    backgroundColor: COLORS.secondaryBackground,
  },
  name: {
    width: '50%',
    textAlign: 'center',
    fontFamily: typography.fonts.secondary,
    fontSize: typography.sizes.sm,
    color: COLORS.textSecondary,
  },
  description: {
    textAlign: 'center',
    fontFamily: typography.fonts.secondaryRegular,
    fontSize: typography.sizes.xs,
    color: COLORS.textSecondary,
  },
  descriptionContainer: {
    width: '70%',
    marginTop: 24,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
  },
  descriptionText: {
    marginTop: 24,
  },
  list: {
    alignItems: 'center',
    width: '100%',
    paddingTop: 46,
  },
  owlImage: {
    width: 125,
    height: 116,
    transform: [{translateX: 90}, {translateY: -10}],
  },
});

export default LessonsScreen;
