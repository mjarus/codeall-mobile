import React, {useEffect, useState} from 'react';
import {
  ScrollView,
  Text,
  FlatList,
  StyleSheet,
  View,
  Image,
  LayoutAnimation,
  UIManager,
} from 'react-native';
import {connect} from 'react-redux';
import {getLessons, setCurrentUnitId} from '../../store/lessons/actions';
import TopicsBox from './components/TopicsBox';
import {theme} from '../../theme';
import {RoundedButton} from 'CodeallMobile/src/components/Buttons';
import COLORS from 'CodeallMobile/src/theme/colors';
import TranslatedText from '../../components/TranslatedText';
import NetworkAwareStatusBar from 'CodeallMobile/src/components/NetworkAwareStatusBar';

const owl = require('CodeallMobile/src/assets/img/lessonsOwl.png');
const HEADER_HEIGHT = 65;

if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

const Lessons = (props) => {
  const [imageSize, setImageSize] = useState([83, 76]);

  useEffect(() => {
    props.getLessons();
  }, []);

  const renderTopic = (topic, index) => <TopicsBox topic={topic} />;

  const handleScrollEvent = (event) => {
    const scrollPosition = event.nativeEvent.contentOffset.y;
    LayoutAnimation.configureNext(
      LayoutAnimation.create(
        100,
        LayoutAnimation.Types.linear,
        LayoutAnimation.Properties.scaleXY,
      ),
    );
    if (scrollPosition > HEADER_HEIGHT) {
      setImageSize([63, 56]);
    } else {
      setImageSize([83, 76]);
    }
  };

  return (
    <ScrollView
      onScroll={handleScrollEvent}
      contentContainerStyle={styles.mainBody}
      stickyHeaderIndices={[1, 3]}>
      <NetworkAwareStatusBar backgroundColor={COLORS.secondaryBackground} />
      <View style={styles.imageContainer}>
        <Image source={owl} style={styles.image(imageSize[0], imageSize[1])} />
      </View>
      <View style={styles.welcome}>
        <TranslatedText
          id={'lessons.welcome'}
          style={theme.typography.welcomeHeader}
        />
        <Text> </Text>
        <Text style={theme.typography.welcomeHeaderName}>
          {props.name !== '' ? props.name : props.nick}
        </Text>
        <Text style={theme.typography.welcomeHeaderName}>!</Text>
      </View>
      <View style={styles.buttonContainer}>
        <RoundedButton
          headerTextId={'lessons.learnNew'}
          size={'big'}
          onPress={() => props.navigation.navigate('LessonNavigator')}
          style={styles.roundedButton}
        />
      </View>
      <View style={styles.topicsList}>
        <TranslatedText
          id={'lessons.section'}
          style={theme.typography.lessonsHeader}
        />
        <TranslatedText
          id={'lessons.introduction'}
          style={theme.typography.lessonsHeader}
        />
        <FlatList
          horizontal={false}
          scrollEnabled={true}
          numColumns={2}
          contentContainerStyle={styles.list}
          data={props.units}
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item, index) => `${item.id || index}-units`}
          renderItem={({item, index}) => renderTopic(item, index)}
        />
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  buttonContainer: {
    paddingTop: 16,
    height: 60,
  },
  imageContainer: {
    position: 'absolute',
    top: 62,
    right: 0,
    backgroundColor: COLORS.secondaryBackground,
    width: '100%',
    alignItems: 'flex-end',
    paddingTop: 12,
    paddingBottom: 16,
  },
  welcome: {
    flexDirection: 'row',
    minHeight: 91,
    marginTop: 20,
    paddingHorizontal: 50,
    justifyContent: 'center',
    paddingTop: 30,
    flexWrap: 'wrap',
    marginBottom: 15,
    zIndex: 10,
  },
  list: {
    ...theme.centeredElement,
    marginTop: 50,
    backgroundColor: COLORS.background,
  },
  mainBody: {
    ...theme.centeredElement,
    backgroundColor: COLORS.secondaryBackground,
  },
  topicsList: {
    width: '100%',
    paddingBottom: 25,
    backgroundColor: COLORS.background,
    marginTop: 32,
    paddingTop: 40,
  },
  image: (width, height) => ({
    width: width,
    height: height,
  }),
});

const mapStateToProps = (state) => ({
  userId: state.auth.userId,
  units: state.lessons.units,
  name: state.profile.name,
  nick: state.profile.nick,
  currentLesson: state.chat.currentLesson,
  currentUnitId: state.lessons.currentUnitId,
});

const mapDispatchToProps = (dispatch) => ({
  getLessons: () => dispatch(getLessons()),
  setCurrentUnitId: (data) => dispatch(setCurrentUnitId(data)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Lessons);
