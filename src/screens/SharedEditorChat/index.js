import React, {Component} from 'react';
import {Text, ScrollView, TextInput} from 'react-native';
import {connect} from 'react-redux';
import {sendSharedEditorChatMessage} from '../../store/editor/actions';

// todo refactor - export styles
const containerStyle = {
  flexGrow: 1,
  padding: 10,
  backgroundColor: '#000000cc',
};

const teacherMessageStyle = {
  color: '#668bb3',
  fontSize: 16,
  lineHeight: 20,
  textAlign: 'left',
};

const userMessageStyle = {
  color: '#b36666',
  fontSize: 16,
  lineHeight: 21,
  textAlign: 'right',
};

const inputMessageStyle = {
  color: 'white',
  fontSize: 14,
  lineHeight: 21,
};

class SharedEditorChat extends Component {
  scrollView = null;
  messageInputField = null;

  constructor(props) {
    super(props);
    this.state = {
      message: '',
    };
  }

  onChangeMessageInput = message => {
    this.setState({message});
  };

  onSubmitMessageInput = () => {
    if (this.state.message.length > 0) {
      this.props.sendSharedEditorChatMessage(this.state.message);
      this.messageInputField.clear();
    }
  };

  render() {
    return (
      <ScrollView
        ref={r => (this.scrollView = r)}
        onContentSizeChange={() =>
          this.scrollView.scrollToEnd({animated: true})
        }
        contentContainerStyle={containerStyle}>
        {this.props.sharedEditorChatMessages.map(
          ({content, isUserMessage}, index) => {
            return (
              <Text
                key={index.toString()}
                style={isUserMessage ? userMessageStyle : teacherMessageStyle}>
                {content}
              </Text>
            );
          },
        )}
        <TextInput
          autoCapitalize="none"
          autoCompleteType="off"
          autoCorrect={false}
          autoFocus={true}
          onSubmitEditing={this.onSubmitMessageInput}
          placeholder="Napisz do nauczyciela..."
          placeholderTextColor="#bcbcbc"
          style={inputMessageStyle}
          onChangeText={text => this.onChangeMessageInput(text)}
          ref={r => {
            this.messageInputField = r;
          }}
        />
      </ScrollView>
    );
  }
}

const mapStateToProps = state => ({
  sharedEditorChatMessages: state.editor.sharedEditorChatMessages,
});

const mapDispatchToProps = dispatch => ({
  sendSharedEditorChatMessage: data =>
    dispatch(sendSharedEditorChatMessage(data)),
});

/**
 * TODO REFACTOR!!! INFO BELOW:
 * this connect function disables some optimization and should not be used!
 * this change was made according to this site:
 * https://stackoverflow.com/questions/37623628/react-componentdidupdate-method-wont-fire-on-inherited-props-change-if-connecte
 * without this, component was not updating after props change
 */

// export default connect(
//   mapStateToProps,
//   mapDispatchToProps,
// )(SharedEditorChat);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  {pure: false},
)(SharedEditorChat);
