import React, {useContext, useRef, useState} from 'react';
import {Text, ScrollView, TextInput, StyleSheet, Button} from 'react-native';
import {connect} from 'react-redux';
import {SmartBoxContext} from '../../utils/SmartBoxProvider';
import {bindActionCreators} from 'redux';
import {clearConsoleOutput} from '../../store/editor/actions';
import { useTranslation } from 'react-i18next';

// todo move styles
const styles = StyleSheet.create({
  container: {
    flexGrow: 1,
    padding: 10,
    backgroundColor: '#000000cc',
  },
  text: {
    color: 'white',
    fontSize: 14,
    fontFamily: 'monospace',
    lineHeight: 20,
  },
});

const Console = props => {
  const {t} = useTranslation();
  const SmartBox = useContext(SmartBoxContext);
  const scrollView = useRef();
  const [input, setInput] = useState('');

  const handleCommandInputSubmit = () => {
    SmartBox.sendMessage('input', input);
  };

  const scrollToEnd = () => {
    scrollView.current.scrollToEnd({animated: true});
  };

  return (
    <>
      <Button title={t("editor.clear")} onPress={props.clearConsoleOutput} />
      <ScrollView
        ref={scrollView}
        onContentSizeChange={scrollToEnd}
        contentContainerStyle={styles.container}>
        <Text style={styles.text}>{props.consoleOutput}</Text>
        <TextInput
          autoCapitalize="none"
          autoCompleteType="off"
          autoCorrect={false}
          onSubmitEditing={handleCommandInputSubmit}
          placeholder={t("editor.send")}
          placeholderTextColor="#bcbcbc" // todo color
          style={styles.text}
          onChangeText={setInput}
          value={input}
        />
      </ScrollView>
    </>
  );
};

const mapStateToProps = state => ({
  consoleOutput: state.editor.consoleOutput,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators({clearConsoleOutput}, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Console);
