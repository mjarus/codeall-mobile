import PopUp from '../../../../components/PopUp';
import React from 'react';
import {TextInput, View} from 'react-native';
import {styles} from './styles';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../components/TranslatedText';
import { RoundedButton } from '../../../../components/Buttons';

const ChangeFrequencyPopUp = ({onClose, visible}) => {
  const {typography} = useTheme();
  return (
    <PopUp onClose={onClose} visible={visible}>
      <TranslatedText
        id="sensors.sensor.changeFrequencyDescription"
        style={typography.popupText}
      />
      <TextInput style={styles.input} />
      <View style={styles.buttons}>
        <RoundedButton headerTextId="general.ok" onPress={onClose} size="medium"/>
      </View>
    </PopUp>
  );
};

export default ChangeFrequencyPopUp;
