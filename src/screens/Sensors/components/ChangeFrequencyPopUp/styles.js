import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const buttons = {
  flexDirection: 'row',
  justifyContent: 'space-evenly',
  marginTop: 20,
  alignSelf: 'stretch',
};

const input = {
  borderColor: COLORS.turquoiseHeader,
  borderWidth: 1,
  width: '85%',
};

export const styles = StyleSheet.create({
  buttons,
  input,
});
