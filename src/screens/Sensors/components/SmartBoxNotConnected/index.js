import React from 'react';
import {StyleSheet, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../components/TranslatedText';

const SmartBoxNotConnected = () => {
  const {typography} = useTheme();

  return (
    <View style={styles.textInformationContainer}>
      <TranslatedText
        id="sensors.smartbox.description.notConnected.line1"
        style={typography.smartBoxNotConnected}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  textInformationContainer: {
    alignItems: 'center',
    textAlign: 'center',
    lineHeight: 15,
    marginLeft: 5,
  },
});

export default SmartBoxNotConnected;
