import React from 'react';
import {View, Text, Image, StyleSheet, TouchableOpacity} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import Modal from '../../../../components/Modal';
import {useTranslation} from 'react-i18next';
import typography from '../../../../theme/typography';
import getSensorImage from 'CodeallMobile/src/utils/getSensorImage';
import {RoundedButton} from '../../../../components/Buttons';
import Header from '../../../../components/Header';

const exitIcon = require('CodeallMobile/src/assets/img/turquoiseExit.png');
const googlePayLogotype = require('CodeallMobile/src/assets/img/googlePayLogotype.png');

const PaymentModal = ({onClose, visibility, sensor}) => {
  console.log(sensor);
  const {t} = useTranslation();
  return (
    <Modal onRequestClose={onClose} onClose={onClose} visibility={visibility}>
      <TouchableOpacity style={styles.backdrop} onPress={onClose} />
      <View style={styles.box}>
        <Header
          title={'payment.payment'}
          leftIcon={exitIcon}
          onLeftPress={onClose}
          style={typography.turquoiseHeader}
        />
        <Text style={[typography.paymentText, styles.row]}>
          {t('payment.googlePlay')}
        </Text>
        <View style={styles.separator} />
        <View style={styles.informationBox}>
          <Image
            source={getSensorImage(sensor.type)}
            style={styles.sensorImage}
          />
          <Text style={typography.paymentTitle}>{sensor.name}</Text>
          <Text style={typography.paymentTitle}>{sensor.price.EUR}</Text>
        </View>
        <View style={styles.separator} />
        <View style={styles.cardRow}>
          <Image source={googlePayLogotype} style={styles.googlePayLogotype} />
          <Text style={typography.paymentText}>Visa-4257</Text>
        </View>
        <View style={styles.separator} />
        <View style={styles.descriptionBox}>
          <Text style={typography.paymentText}>
            {t('payment.subscriptionInformation')}
          </Text>
        </View>
        <RoundedButton
          size={t('payment.buy') === 'Complete Purchase' ? 'big' : 'medium'}
          headerTextId={'payment.buy'}
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  modal: {
    width: '100%',
    height: '100%',
    backgroundColor: COLORS.opacityBackground,
    opacity: 0.5,
    justifyContent: 'flex-end',
  },

  box: {
    width: '100%',
    height: '55%',
    backgroundColor: COLORS.background,
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    alignItems: 'center',
    paddingBottom: 25,
  },

  backdrop: {
    width: '100%',
    height: '45%',
  },

  exitIcon: {
    marginTop: 10,
    marginLeft: 10,
    width: 32.27,
    height: 31.21,
  },

  headerField: {
    marginTop: 10,
    marginLeft: 10,
    width: 32.27,
    height: 31.21,
  },

  headerRow: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  header: {
    textAlign: 'center',
    marginTop: 10,
  },
  separator: {
    width: '80%',
    height: 1,
    marginTop: 11,
    marginBottom: 17,
    backgroundColor: COLORS.profileSeparator,
  },
  informationBox: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 46,
    paddingRight: 62,
    alignItems: 'center',
    width: '100%',
  },
  sensorImage: {
    width: 60,
    height: 51,
  },
  cardRow: {
    flexDirection: 'row',
    paddingLeft: 18,
    alignItems: 'center',
    width: '100%',
  },
  googlePayLogotype: {
    width: 62,
    height: 25,
  },
  descriptionBox: {
    width: '100%',
    paddingHorizontal: 30,
    marginBottom: 20,
  },
  row: {
    width: '100%',
    textAlign: 'left',
    paddingLeft: 30,
    marginTop: 20,
  },
});

export default PaymentModal;
