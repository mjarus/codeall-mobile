import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const mainContainer = {
  flexDirection: 'row',
  justifyContent: 'flex-start',
  alignItems: 'center',
  width: '100%',
  marginBottom: 10,
  backgroundColor: COLORS.background,
  elevation: 15,
  borderRadius: 12,
  paddingBottom: 5,
  paddingTop: 5,
};

const image = {
  width: 45,
  height: 45,
  resizeMode: 'contain',
  marginLeft: 10,
  marginRight: 2,
};

const buttonsContainer = {
  flexDirection: 'row',
  marginTop: 5,
  marginBottom: 5,
  marginRight: 10,
  marginLeft: 5,
  justifyContent: 'space-between',
  flexWrap: 'wrap',
};
const optionsContainer = {
  flexDirection: 'row',
  justifyContent: 'center',
  marginTop: 5,
  marginBottom: 5,
  marginRight: 15,
  marginLeft: 5,
};
const contentContainer = {
  flexShrink: 1,
  width: '100%',
};
const nameContainer = {
  flexDirection: 'row',
  alignItems: 'flex-start',
};
const valuesName = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
  bottom: 6,
  marginTop: 5,
  marginLeft: 5,
  marginRight: 5,
};
const verticalLine = {
  height: 30,
  width: 1,
  backgroundColor: COLORS.progressBarBackground,
  marginRight: 8,
  marginLeft: 5,
};

const informationImage = {
  width: 22,
  height: 22,
  position: 'absolute',
  right: '5%',
  top: '5%',
  bottom: '5%',
  zIndex: 1,
};
const sizeImage = {
  width: 40,
  height: 40,
};

const dots = {
  flexDirection: 'row',
  marginTop: 6,
  marginBottom: 5,
  marginRight: 5,
  marginLeft: 5,
  justifyContent: 'space-between',
  flexWrap: 'wrap',
};

const dotsImage = {
  width: 26,
  height: 5,
};

export const styles = StyleSheet.create({
  mainContainer,
  buttonsContainer,
  optionsContainer,
  contentContainer,
  image,
  nameContainer,
  valuesName,
  verticalLine,
  informationImage,
  dots,
  sizeImage,
  dotsImage,
});
