import React from 'react';
import {useNavigation} from '@react-navigation/native';
import {Image, StyleSheet, View, Text} from 'react-native';
import getSensorImage from '../../../../utils/getSensorImage';
import typography from '../../../../theme/typography';
import {useTranslation} from 'react-i18next';
import {
  TurquoiseButton,
  LightBlueButton,
} from 'CodeallMobile/src/components/Buttons';

const SensorShop = ({
  sensor,
  openPayment,
  setCurrentSensorId,
  sensorsArr,
  isShop,
}) => {
  const {t} = useTranslation();
  const navigation = useNavigation();
  const imageSource = getSensorImage(sensor.type);

  const buyButton = () => {
    setCurrentSensorId(sensor.id);
    openPayment();
  };

  const sensorId = sensor.id;

  return (
    <View style={styles.box}>
      <Image source={imageSource} style={styles.sensorImage} />
      <View>
        <Text style={typography.sensorTitle}>{sensor.name}</Text>
        <Text style={typography.sensorPrice}>
          {t('sensors.sensor.attribute.price') + ': ' + sensor.price.PLN}
        </Text>
        <View style={styles.buttonsContainer}>
          <LightBlueButton
            text={t('sensors.sensor.more')}
            customStyles={styles.button}
            action={() =>
              navigation.navigate('SensorDetails', {
                sensorsArr,
                sensorId,
                isShop,
              })
            }
          />
          <TurquoiseButton
            text={t('sensors.sensor.buy')}
            customStyles={styles.button}
            action={buyButton}
          />
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  box: {
    marginTop: 24,
    width: '90%',
    flexDirection: 'row',
    marginLeft: '5%',
    marginRight: '5%',
  },
  sensorImage: {
    height: 97.5,
    width: 112,
    marginRight: 25,
    resizeMode: 'contain',
  },
  button: {
    marginTop: 8,
    marginRight: 16,
  },
  buttonsContainer: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export default SensorShop;
