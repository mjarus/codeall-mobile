import React, {useState, useContext} from 'react';
import {TouchableOpacity, View, Text, Image, StyleSheet} from 'react-native';
import getSensorImage from 'CodeallMobile/src/utils/getSensorImage';
import {useTranslation} from 'react-i18next';
import typography from '../../../../theme/typography';
import OptionsPopUp from '../OptionsPopUp';
import {SmallOvalButton} from '../../../../components/Buttons';

const bulbIcon = require('CodeallMobile/src/assets/img/bulbIcon.png');
const batteryIcon = require('CodeallMobile/src/assets/img/batteryIcon.png');
const threeDotsOptionIcon = require('CodeallMobile/src/assets/img/threeDotsOption.png');
import {SmartBoxContext} from '../../../../utils/SmartBoxProvider';

const SensorsListItem = ({sensor, type}) => {
  const {t} = useTranslation();
  const [isDotsModalVisible, setDotsModalVisible] = useState(false);
  const closeDotsModal = () => {
    setDotsModalVisible(false);
  };
  const SmartBox = useContext(SmartBoxContext);

  const connectSensor = (id) => {
    SmartBox.connectNewBoard(id);
  };

  return (
    <>
      <OptionsPopUp onClose={closeDotsModal} visible={isDotsModalVisible} />
      <View style={styles.row}>
        <Image
          source={getSensorImage(sensor.type)}
          style={styles.sensorImage}
        />
        <View style={styles.titleBox}>
          <Text style={typography.sensorsListName}>{sensor.name}</Text>
          <Text style={typography.sensorsListText}>
            {t('sensors.sensor.attribute.idShort')}
            {sensor.id}
          </Text>
        </View>
        {type === 'CONNECTED' && (
          <Text style={[styles.connectionStatus, typography.sensorsListText]}>
            disconnected
          </Text>
        )}
        <Image source={bulbIcon} style={styles.bulbIcon} />
        <Image source={batteryIcon} style={styles.batteryIcon} />
        {type === 'CONNECTED' && (
          <TouchableOpacity
            onPress={() => setDotsModalVisible(true)}
            style={styles.dotsButton}>
            <Image
              source={threeDotsOptionIcon}
              style={styles.threeDotsOptionIcon}
            />
          </TouchableOpacity>
        )}
        {type === 'CONNECT' && (
          <>
            {sensor.isConnected ? (
              <SmallOvalButton
                type={'PRIMARY'}
                headerTextId={'sensors.sensor.disconnect'}
              />
            ) : (
              <SmallOvalButton
                type={'SECONDARY'}
                headerTextId={'sensors.sensor.connect'}
                onPress={() => connectSensor(sensor.id)}
              />
            )}
          </>
        )}
      </View>
    </>
  );
};

const styles = StyleSheet.create({
  row: {
    width: '100%',
    flexDirection: 'row',
    paddingLeft: 26.5,
    paddingRight: 21.5,
    alignItems: 'center',
    marginTop: 4,
    height: 53,
  },
  sensorImage: {
    width: 48,
    height: 42,
  },
  titleBox: {
    flexDirection: 'column',
    marginLeft: 5,
    width: 95,
  },
  connectionStatus: {
    width: 54,
  },
  bulbIcon: {
    width: 25,
    height: 27,
    marginLeft: 10,
    marginRight: 10,
  },
  batteryIcon: {
    width: 24,
    height: 13,
    marginRight: 10,
  },
  threeDotsOptionIcon: {
    width: 22,
    height: 6,
  },
  dotsButton: {
    height: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default SensorsListItem;
