import React, {useState} from 'react';
import SensorAttribute, {SensorAttributeType} from '../SensorAttribute';
import Sensor from './Sensor';
import IconButton from '../../../../components/IconButton';
import {styles} from './styles';
import {View} from 'react-native';
import OptionsPopUp from '../OptionsPopUp';

const threeDotsOptions = require('../../../../assets/img/threeDotsOptions.png');

const SensorConnected = ({sensor, style}) => {
  const [isDotsModalVisible, setDotsModalVisible] = useState(false);

  const closeDotsModal = () => {
    setDotsModalVisible(false);
  };
  const buttons = (
    <>
      <View style={styles.verticalLine} />
      <IconButton
        onPress={() => setDotsModalVisible(true)}
        source={threeDotsOptions}
        style={styles.dots}
        imageStyle={styles.dotsImage}
      />
    </>
  );
  return (
    <>
      <OptionsPopUp onClose={closeDotsModal} visible={isDotsModalVisible} />
      <Sensor buttons={buttons} sensor={sensor} style={style}>
        <View style={styles.nameContainer}>
          <SensorAttribute
            nameId={'sensors.sensor.attribute.id'}
            value={sensor.id}
          />
          <View style={styles.valuesName}>
            <SensorAttribute
              value={sensor.currentReading}
              type={SensorAttributeType.TEMPERATURE}
            />
          </View>
        </View>
      </Sensor>
    </>
  );
};

export default SensorConnected;
