import React from 'react';
import {Text, View} from 'react-native';
import getSensorImage from '../../../../utils/getSensorImage';
import {styles} from './styles';
import {useNavigation} from '@react-navigation/native';
import BatteryStatus from '../../../../components/BatteryLevel';
import LightBulb from '../../../../components/LightBulb';
import {useTheme} from '@react-navigation/native';
import IconButton from '../../../../components/IconButton';

const Sensor = ({
  sensor,
  buttons,
  battery,
  lightBulb,
  style,
  percentage,
  disableImagePress,
  ...otherProps
}) => {
  const navigation = useNavigation();
  const imageSource = getSensorImage(sensor.type); //TODO get image from backend
  const handleImagePress = () => navigation.navigate('SensorDetails', {sensor});
  const {typography} = useTheme();

  return (
    <View style={[styles.mainContainer, style]}>
      <IconButton
        onPress={handleImagePress}
        disabled={disableImagePress}
        source={imageSource}
        imageStyle={styles.image}
      />

      <View style={styles.contentContainer}>
        {sensor.name && (
          <Text style={typography.sensorName}>{sensor.name}</Text>
        )}
        {otherProps.children}
      </View>
      <LightBulb batteryStatusPercentage={sensor.batteryLevel} />
      <BatteryStatus batteryStatusPercentage={sensor.batteryLevel} />
      {buttons && <View style={styles.buttonsContainer}>{buttons}</View>}
    </View>
  );
};

export default Sensor;
