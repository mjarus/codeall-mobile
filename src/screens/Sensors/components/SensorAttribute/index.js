import {Text, View} from 'react-native';
import {styles} from './styles';
import React, {useState} from 'react';
import TouchableText from '../../../../components/TouchableText';
import SensorUnavailablePopUp from '../SensorUnavailablePopUp';
import ChangeFrequencyPopUp from '../ChangeFrequencyPopUp';

import {theme} from '../../../../theme';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../components/TranslatedText';

export const SensorAttributeType = {
  BATTERY: 'battery',
  STATUS: 'status',
  TEMPERATURE: 'temperature',
  PRICE: 'price',
  FREQUENCY: 'frequency',
  CONNECT: 'connect',
};

const TextValue = ({value}) => (
  <Text style={theme.typography.sensorTitleValue}>{value} </Text>
);

const SensorAttribute = ({nameId, value, style, type}) => {
  const {typography} = useTheme();
  const [isSensorUnavailablePopUpVisible, setSensorUnavailablePopUpVisible] =
    useState(false);

  const [isChangeFrequencyPopUpVisible, setChangeFrequencyPopUpVisible] =
    useState(false);

  if (value === null || value === undefined) {
    return <TextValue value="-" />;
  }

  const closeSensorUnavailablePopUp = () =>
    setSensorUnavailablePopUpVisible(false);

  const closeChangeFrequencyPopUp = () => setChangeFrequencyPopUpVisible(false);

  let valueComponent;
  switch (type) {
    case SensorAttributeType.BATTERY:
      valueComponent = <TextValue value={`${value}%`} />;
      break;

    case SensorAttributeType.TEMPERATURE:
      valueComponent = <TextValue value={`${value}°C`} />;
      break;
    case SensorAttributeType.PRICE:
      valueComponent = <TextValue value={`${value} PLN`} />;
      break;

    case SensorAttributeType.STATUS:
      valueComponent = value ? (
        <TranslatedText
          id={'sensors.sensor.attribute.status.available'}
          style={typography.sensorTextAvailable}
        />
      ) : (
        <TouchableText
          style={typography.sensorTouchableText}
          textId={'sensors.sensor.attribute.status.unavailable'}
          onPress={() => setSensorUnavailablePopUpVisible(true)}
        />
      );
      break;
    case SensorAttributeType.FREQUENCY:
      valueComponent = (
        <>
          <TextValue value={`${value}s`} />
          <TouchableText
            textId={'sensors.sensor.attribute.frequency.change'}
            onPress={() => setChangeFrequencyPopUpVisible(true)}
          />
        </>
      );
      break;
    default:
      valueComponent = <TextValue value={value} />;
  }

  return (
    <>
      <SensorUnavailablePopUp
        onClose={closeSensorUnavailablePopUp}
        visible={isSensorUnavailablePopUpVisible}
      />
      <ChangeFrequencyPopUp
        onClose={closeChangeFrequencyPopUp}
        visible={isChangeFrequencyPopUpVisible}
      />
      <View style={[styles.container, style]}>
        <View style={styles.titleContainer}>
          <TranslatedText
            style={[typography.sensorTitleValue, styles.sensorProperty]}
            id={nameId}
          />
          <Text style={typography.sensorTitleValue}>{valueComponent}</Text>
        </View>
      </View>
    </>
  );
};

export default SensorAttribute;
