import {StyleSheet} from 'react-native';

const container = {
  flexDirection: 'row',
  alignItems: 'flex-start',
  width: '52%',
};

const titleContainer = {
  flexDirection: 'row',
  alignItems: 'flex-start',
  flexWrap: 'wrap',
  marginLeft: 6,
};

const textContainer = {
  alignItems: 'flex-start',
  lineHeight: 15,
};

const sensorProperty = {
  marginRight: 2,
};
export const styles = StyleSheet.create({
  container,
  titleContainer,
  textContainer,
  sensorProperty,
});
