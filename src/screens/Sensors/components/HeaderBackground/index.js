import React from 'react';
import {StyleSheet, View, Image} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import TanslatedText from 'CodeallMobile/src/components/TranslatedText';
import {useTheme} from '@react-navigation/native';

const warningIcon = require('CodeallMobile/src/assets/img/warningIcon.png');
const connectionImage = require('CodeallMobile/src/assets/img/connectionImage.png');
const searchForSensors = require('CodeallMobile/src/assets/img/searchForSensors.png');
const octagon = require('CodeallMobile/src/assets/img/octagon.png');

const HeaderBackground = ({children, type}) => {
  const {typography} = useTheme();
  return (
    <View style={styles.container}>
      <View style={styles.internalBox}>
        <View style={[styles.circle, styles.bigCircle]} opacity={0.13} />
        <View style={[styles.circle, styles.middleCircle]} opacity={0.13} />
        <View style={[styles.circle, styles.smallCircle]} opacity={0.13} />
        <View style={styles.imageBox}>
          <View style={styles.box(type)}>
            {type !== 'DEVICE' ? (
              <>
                <View style={[styles.informationBox, styles.shadowBox]} />
                <View style={styles.informationBox}>
                  {type === 'MISSING' && (
                    <>
                      <Image source={warningIcon} style={styles.warningIcon} />
                      <TanslatedText
                        id={'sensors.smartbox.description.notConnected.error'}
                        style={typography.smartboxNotConnectedError}
                      />
                    </>
                  )}
                  {children}
                </View>
              </>
            ) : (
              <View>{children}</View>
            )}
          </View>
          {type === 'MISSING' && (
            <Image source={connectionImage} style={styles.connectionImage} />
          )}
          {type === 'CONNECT' && (
            <>
              <Image
                source={searchForSensors}
                style={styles.searchForSensorsImage}
              />
              <Image
                source={octagon}
                style={[styles.octagon, styles.leftOctagon]}
              />
              <Image
                source={octagon}
                style={[styles.octagon, styles.middleOctagon]}
              />
              <Image
                source={octagon}
                style={[styles.octagon, styles.rightOctagon]}
              />
            </>
          )}
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 300,
    backgroundColor: COLORS.secondaryBackground,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 70,
  },
  circle: {
    borderRadius: 200,
    backgroundColor: COLORS.backgroundCircles,
    position: 'absolute',
  },
  bigCircle: {
    width: 363,
    height: 363,
  },
  middleCircle: {
    width: 265,
    height: 265,
  },
  smallCircle: {
    width: 181,
    height: 181,
  },
  internalBox: {
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
  },
  box: (type) => ({
    marginBottom: type === 'SHOP' || type === 'CONNECTED' ? 70 : 0,
  }),
  informationBox: {
    minWidth: 229,
    maxWidth: 300,
    backgroundColor: COLORS.background,
    borderRadius: 4,
    padding: 10,
    zIndex: 1000,
  },
  shadowBox: {
    backgroundColor: COLORS.buttonShadow,
    position: 'absolute',
    top: 4,
    borderRadius: 4,
    width: '100%',
    height: '100%',
  },
  warningIcon: {
    width: 22,
    height: 22,
    position: 'absolute',
    top: 10,
    right: 10,
  },
  connectionImage: {
    width: 275,
    height: 138,
    marginBottom: 50,
  },
  imageBox: {
    position: 'absolute',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchForSensorsImage: {
    width: 90,
    height: 94,
    marginTop: 20,
    marginBottom: 50,
  },
  octagon: {
    width: 86,
    height: 86,
    position: 'absolute',
  },
  leftOctagon: {
    top: 100,
    left: -30,
  },
  middleOctagon: {
    top: 150,
    right: 0,
  },
  rightOctagon: {
    top: 70,
    right: -50,
  },
});

export default HeaderBackground;
