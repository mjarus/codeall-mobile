import React, {useState} from 'react';
import {useNavigation} from '@react-navigation/native';
import TouchableText from '../../../../components/TouchableText';
import SensorInformationPopUp from '../SensorInformationPopUp';
import {View, Text, Image, StyleSheet} from 'react-native';
import IconButton from '../../../../components/IconButton';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../components/TranslatedText';
import getSensorImage from '../../../../utils/getSensorImage';
import {useTranslation} from 'react-i18next';

const informationIcon = require('../../../../assets/img/informationIcon.png');

const SensorSmartbox = ({sensor, style, isShop}) => {
  const {typography} = useTheme();

  const navigation = useNavigation();

  const [modalVisible, setModalVisible] = useState(false);

  const closeInformationModal = () => {
    setModalVisible(!modalVisible);
  };

  console.log(sensor);
  const {t} = useTranslation();
  return (
    <>
      <SensorInformationPopUp
        onClose={closeInformationModal}
        visible={modalVisible}
      />

      <>
        <IconButton
          onPress={() => setModalVisible(true)}
          source={informationIcon}
          style={styles.informationButton}
          imageStyle={styles.informationImage}
        />
        <View style={styles.textContainer}>
          <TranslatedText
            id={'sensors.smartbox.connected'}
            style={typography.smartboxNotConnectedError}
          />
          <View style={styles.detailsBox}>
            <Image
              source={getSensorImage(sensor.type)}
              style={styles.sensorImage}
            />
            <View style={styles.detailsTextBox}>
              <Text style={typography.smartBoxNotConnected}>
                {t('sensors.sensor.attribute.version')}
              </Text>
              <Text style={typography.smartBoxNotConnected}>
                {sensor.smartboxVersion}
              </Text>
              <Text style={typography.smartBoxNotConnected}>
                {t('sensors.sensor.attribute.micropythonVersion')}
              </Text>
              <Text style={typography.smartBoxNotConnected}>
                {sensor.micropythonVersion}
              </Text>
              <TouchableText
                textId={'sensors.sensor.more'}
                style={typography.sensorTouchableText}
                onPress={() => {
                  navigation.navigate('SensorDetails', {sensor, isShop});
                }}
              />
            </View>
          </View>

          <Text style={typography.smartBoxNotConnected}>
            {t('smartbox.configuration.information')}
          </Text>
        </View>
      </>
    </>
  );
};

const styles = StyleSheet.create({
  textContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    marginLeft: 5,
    marginRight: 5,
    lineHeight: 15,
  },

  informationImage: {
    width: 22,
    height: 22,
  },

  informationButton: {
    position: 'absolute',
    right: '5%',
    top: '5%',
    bottom: '5%',
    zIndex: 1,
  },

  sensorImage: {
    width: 80,
    height: 70,
  },

  detailsBox: {
    flexDirection: 'row',
  },
  detailsTextBox: {
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginLeft: 25,
  },
});

export default SensorSmartbox;
