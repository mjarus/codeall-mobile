import PopUp from '../../../../components/PopUp';
import React from 'react';
import {View, StyleSheet} from 'react-native';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../components/TranslatedText';
import { RoundedButton } from '../../../../components/Buttons';

const SensorInformationPopUp = ({onClose, visible}) => {
  const {typography} = useTheme();

  return (
    <PopUp visible={visible} onClose={onClose}>
      <View style={styles.contentBox}>
        <TranslatedText
          id="sensors.connectionInformation.line1"
          style={typography.popupText}
        />
        <TranslatedText
          id="sensors.connectionInformation.line2"
          style={typography.popupText}
        />
        <TranslatedText
          id="sensors.connectionInformation.line3"
          style={typography.popupText}
        />
        <View style={styles.buttons}>
          <RoundedButton
            headerTextId="general.ok"
            onPress={onClose}
            size="medium"
          />
        </View>
      </View>
    </PopUp>
  );
};

const styles = StyleSheet.create({
  buttons: {
    flexDirection: 'row',
    justifyContent: 'space-evenly',
    alignSelf: 'stretch',
    marginTop: 10,
  },
  contentBox: {
    padding: 30,
  },
});

export default SensorInformationPopUp;
