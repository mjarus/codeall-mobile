import React from 'react';
import {Text, View} from 'react-native';
import TranslatedText from '../../../../components/TranslatedText';
import {styles} from './styles';

const Table = ({children}) => <View style={styles.table}>{children}</View>;

const Row = ({children, last}) => (
  <View style={last ? [styles.row, styles.lastRow] : styles.row}>
    {children}
  </View>
);

const Cell = ({textId, text, right}) => (
  <View style={right ? [styles.cell, styles.rightCell] : styles.cell}>
    {textId ? (
      <TranslatedText id={textId} style={styles.text} />
    ) : (
      <Text style={styles.text}>{text}</Text>
    )}
  </View>
);

const TechnicalDataTable = ({sensor}) => (
  <Table>
    <Row>
      <Cell textId={'sensors.sensor.attribute.powerSupply'} />
      <Cell right text={sensor.technicalData?.powerSupply} />
    </Row>
    <Row>
      <Cell textId={'sensors.sensor.attribute.lifespan'} />
      <Cell right text={sensor.technicalData?.lifespan} />
    </Row>
    <Row>
      <Cell textId={'sensors.sensor.attribute.accuracy'} />
      <Cell right text={sensor.technicalData?.accuracy} />
    </Row>
    <Row>
      <Cell textId={'sensors.sensor.attribute.temperature'} />
      <Cell right text={sensor.technicalData?.temperature} />
    </Row>
    <Row>
      <Cell textId={'sensors.sensor.attribute.alarm'} />
      <Cell right text={sensor.technicalData?.alarm} />
    </Row>
    <Row last>
      <Cell textId={'sensors.sensor.attribute.dimensions'} />
      <Cell right text={sensor.technicalData?.dimensions} />
    </Row>
  </Table>
);

export default TechnicalDataTable;
