import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';
import {StyleSheet} from 'react-native';

const table = {
  width: '100%',
  borderWidth: 1,
  borderColor: COLORS.inputBorder,
  borderRadius: 4,
};

const row = {
  borderBottomWidth: 1,
  borderColor: COLORS.inputBorder,
  flexDirection: 'row',
  width: '100%',
};

const lastRow = {
  borderBottomWidth: 0,
};

const cell = {
  borderRightWidth: 1,
  borderColor: COLORS.inputBorder,
  width: '50%',
  alignItems: 'flex-start',
  justifyContent: 'center',
  height: 44,
  paddingLeft: 5,
};

const rightCell = {
  borderRightWidth: 0,
};

const text = {
  color: COLORS.header,
  lineHeight: 15,
  letterSpacing: 0.3,
  height: 'auto',
  textAlign: 'center',
  padding: 5,
  fontSize: typography.sizes.xxss,
};

export const styles = StyleSheet.create({
  table,
  row,
  cell,
  text,
  lastRow,
  rightCell,
});
