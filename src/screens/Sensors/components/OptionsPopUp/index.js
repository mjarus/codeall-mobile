import React from 'react';
import Modal from '../../../../components/Modal';
import {View, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import { RoundedButton } from '../../../../components/Buttons';

const OptionsPopUp = ({onClose, visible}) => {
  return (
    <Modal visible={visible} onClose={onClose}>
      <View style={styles.modalContent}>
        <View style={styles.divider} />
        <RoundedButton
          headerTextId={'sensors.sensor.disconnect'}
          size="medium"
        />
        <RoundedButton
          headerTextId={'sensors.sensor.back'}
          size="medium"
        />
      </View>
    </Modal>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: COLORS.popupSeperator,
    width: 50,
    height: 3,
    marginBottom: 22,
  },
  modalContent: {
    padding: 30,
    alignItems: 'center',
  },
});
export default OptionsPopUp;
