import PopUp from '../../../../components/PopUp';
import React from 'react';
import {View} from 'react-native';
import {styles} from './styles';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../../components/TranslatedText';
import { RoundedButton } from '../../../../components/Buttons';

const DisconnectPopUp = ({onClose}) => {
  const {typography} = useTheme();
  return (
    <PopUp onClose={onClose}>
      <TranslatedText
        id="sensors.sensor.disconnectConfirmation"
        style={typography.popupText}
      />
      <View style={styles.buttons}>
        <RoundedButton headerTextId="general.yes" size="medium"/>
        <RoundedButton headerTextId="general.no" onPress={onClose} size="medium"/>
      </View>
    </PopUp>
  );
};

export default DisconnectPopUp;
