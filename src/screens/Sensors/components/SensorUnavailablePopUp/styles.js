import {StyleSheet} from 'react-native';

const buttons = {
  flexDirection: 'row',
  justifyContent: 'space-evenly',
  marginTop: 20,
  alignSelf: 'stretch',
};

export const styles = StyleSheet.create({
  buttons,
});
