import {ScrollView, StyleSheet} from 'react-native';
import React, {useEffect, useState} from 'react';
import {SensorShop} from '../components/Sensor';
import {connect} from 'react-redux';
import FriendsInvitationBanner from '../../../components/FriendsInvitationBanner';
import {bindActionCreators} from 'redux';
import {getSensors} from '../../../store/shop/actions';
import COLORS from 'CodeallMobile/src/theme/colors';
import HeaderBackground from '../components/HeaderBackground';
import PaymentModal from '../components/PaymentModal';

const ShopScreen = ({sensors, ...otherProps}) => {
  useEffect(() => {
    otherProps.getSensors();
  }, []);
  const [isPaymentModalVisible, setPaymentModalVisibility] = useState(false);
  const [currentSensorId, setCurrentSensorId] = useState();
  const sensorsArr = sensors;

  return (
    <>
      {isPaymentModalVisible && (
        <PaymentModal
          onClose={() => setPaymentModalVisibility(false)}
          visibility={isPaymentModalVisible}
          sensor={sensors.find((sensor) => sensor.id === currentSensorId)}
        />
      )}
      <HeaderBackground type={'SHOP'}>
        <FriendsInvitationBanner />
      </HeaderBackground>
      <ScrollView contentContainerStyle={styles.body} style={{flex: 1}}>
        {sensors.map((sensor, index) => (
          <SensorShop
            sensor={sensor}
            key={index}
            openPayment={() => setPaymentModalVisibility(true)}
            setCurrentSensorId={setCurrentSensorId}
            sensorsArr={sensorsArr}
            isShop={true}
          />
        ))}
      </ScrollView>
    </>
  );
};

const mapStateToProps = (state) => ({
  sensors: state.shop.sensors,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({getSensors}, dispatch);

const styles = StyleSheet.create({
  body: {
    flexGrow: 1,
    alignItems: 'center',
    justifyContent: 'flex-start',
    backgroundColor: COLORS.background,
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(ShopScreen);
