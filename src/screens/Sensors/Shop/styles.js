import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const body = {
  flexGrow: 1,
  alignItems: 'center',
  justifyContent: 'flex-start',
  padding: 40,
  backgroundColor: COLORS.background,
};

export const styles = StyleSheet.create({
  body,
});
