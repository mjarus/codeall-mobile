import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const body = {
  flexGrow: 1,
  alignItems: 'center',
  justifyContent: 'flex-start',
  padding: 5,
};

const containerList = {
  marginTop: 5,
  marginBottom: 5,
};
const uploadingCode = {
  width: '100%',
  bottom: '3%',
};

const mainContainer = {
  position: 'absolute',
  backgroundColor: COLORS.background,
  elevation: 15,
  borderRadius: 12,
  width: '65%',
  alignItems: 'center',
  justifyContent: 'flex-start',
  marginTop: 20,
  marginBottom: 10,
};
const listContainer = {
  padding: 10,
  bottom: '20%',
};
const informationImage = {
  width: 22,
  height: 22,
};

const informationButton = {
  position: 'absolute',
  right: '4%',
  top: '5%',
};

export const styles = StyleSheet.create({
  body,
  mainContainer,
  uploadingCode,
  listContainer,
  informationImage,
  informationButton,
  containerList,
});
