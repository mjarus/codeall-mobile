import React, {useState} from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import SmartBoxNotConnected from '../components/SmartBoxNotConnected';
import SensorInformationPopUp from '../components/SensorInformationPopUp';
import {useTheme} from '@react-navigation/native';
import COLORS from 'CodeallMobile/src/theme/colors';
import HeaderBackground from '../components/HeaderBackground';
import TranslatedText from '../../../components/TranslatedText';
import SensorsListItem from '../components/Sensor/SensorsListItem';

const ConnectNewSensorScreen = (props) => {
  const {typography} = useTheme();
  const [modalVisible, setmodalVisible] = useState(false);
  const closeInformationModal = () => {
    setmodalVisible(false);
  };
  return (
    <>
      <SensorInformationPopUp
        onClose={closeInformationModal}
        visible={modalVisible}
      />

      <ScrollView contentContainerStyle={styles.body}>
        <HeaderBackground type="CONNECT">
          <TranslatedText
            id="sensors.connectionInstructions"
            style={typography.smartBoxNotConnected}
          />
        </HeaderBackground>
        <View style={styles.listContainer}>
          <View style={styles.containerList}>
            <TranslatedText
              id="sensors.sensor.found"
              style={typography.sensorTextList}
            />
          </View>
          {props.smartbox ? (
            Object.keys(props.sensors).map((sensorId, index) => (
              <SensorsListItem
                sensor={{
                  id: sensorId,
                  ...props.sensors[sensorId],
                }}
                type="CONNECT"
                key={index}
              />
            ))
          ) : (
            <SmartBoxNotConnected navigation={props.navigation} />
          )}
        </View>
      </ScrollView>
    </>
  );
};

const mapStateToProps = (state) => ({
  smartbox: state.smartbox.smartbox,
  sensors: state.smartbox.newSensors,
});

const styles = StyleSheet.create({
  body: {
    alignItems: 'center',
    justifyContent: 'flex-start',
  },

  containerList: {
    marginTop: 5,
    marginBottom: 5,
  },
  uploadingCode: {
    width: '100%',
    bottom: '3%',
  },

  mainContainer: {
    position: 'absolute',
    backgroundColor: COLORS.background,
    elevation: 15,
    borderRadius: 12,
    width: '65%',
    alignItems: 'center',
    justifyContent: 'flex-start',
    marginTop: 20,
    marginBottom: 10,
  },
  listContainer: {
    padding: 10,
    backgroundColor: COLORS.background,
  },
  informationImage: {
    width: 22,
    height: 22,
  },

  informationButton: {
    position: 'absolute',
    right: '4%',
    top: '5%',
  },
});
export default connect(mapStateToProps)(ConnectNewSensorScreen);
