import React from 'react';
import {ScrollView, View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {SensorSmartbox} from '../components/Sensor';
import SmartBoxNotConnected from '../components/SmartBoxNotConnected';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../components/TranslatedText';
import HeaderBackground from '../components/HeaderBackground';
import COLORS from 'CodeallMobile/src/theme/colors';
import SensorsListItem from '../components/Sensor/SensorsListItem';

const ConnectedSensorsScreen = (props) => {
  const {typography} = useTheme();
  return (
    <ScrollView contentContainerStyle={styles.body} style={{flex: 1}}>
      {props.smartbox ? (
        <HeaderBackground type="CONNECTED">
          <SensorSmartbox
            sensor={props.smartbox}
            style={styles.smartbox}
            isShop={false}
          />
        </HeaderBackground>
      ) : (
        <HeaderBackground type={'MISSING'}>
          <SmartBoxNotConnected />
        </HeaderBackground>
      )}

      <View style={styles.containerList}>
        <TranslatedText
          id={'smartbox.list.sensors'}
          style={typography.sensorTextList}
        />
        {props.smartbox &&
          Object.keys(props.sensors).map((sensorHandle, index) => (
            <SensorsListItem
              sensor={{sensorHandle, ...props.sensors[sensorHandle]}}
              key={index}
              type={'CONNECTED'}
            />
          ))}
      </View>
    </ScrollView>
  );
};
const styles = StyleSheet.create({
  body: {
    flexGrow: 1,
    backgroundColor: COLORS.background,
  },
  containerList: {
    zIndex: 100000,
    width: '100%',
    backgroundColor: COLORS.background,
    minHeight: 150,
  },
});
const mapStateToProps = (state) => ({
  sensors: state.smartbox.connectedSensors,
  smartbox: state.smartbox.smartbox,
});

export default connect(mapStateToProps)(ConnectedSensorsScreen);
