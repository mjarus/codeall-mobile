import React from 'react';
import {ScrollView, View} from 'react-native';
import {styles} from './styles';
import {connect} from 'react-redux';
import {SensorConnected} from '../components/Sensor';
import Header from '../../../components/Header';

const back = require('../../../assets/img/back.png');
const plus = require('../../../assets/img/plus.png');

const SensorAvailable = (props) => {
  return (
    <>
      <View style={styles.headerContainer}>
        <Header
          title="sensors.sensor.available"
          leftIcon={back}
          rightIcon={plus}
          onLeftPress={props.navigation.goBack}
          onRightPress={() => props.navigation.navigate('Sensors')}
        />
      </View>
      <ScrollView contentContainerStyle={styles.body} style={{flex: 1}}>
        {props.smartbox &&
          Object.keys(props.sensors).map((sensorHandle, index) => (
            <SensorConnected
              sensor={{...props.sensors[sensorHandle]}}
              key={index}
            />
          ))}
      </ScrollView>
    </>
  );
};

const mapStateToProps = (state) => ({
  sensors: state.smartbox.connectedSensors,
  smartbox: state.smartbox.smartbox,
});

export default connect(mapStateToProps)(SensorAvailable);
