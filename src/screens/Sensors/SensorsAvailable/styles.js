import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const headerContainer = {
  backgroundColor: COLORS.purpleishBlue,
  height: 80,
  alignItems: 'center',
  justifyContent: 'space-between',
  flexDirection: 'row',
  marginBottom: 10,
};
const headerText = {
  fontSize: 17,
  lineHeight: 18,
  color: COLORS.white,
  fontWeight: 'bold',
};

const button = {
  margin: 10,
};

const backImage = {
  width: 15,
  height: 25,
};

const plusImage = {
  width: 25,
  height: 25,
};

const body = {
  flexGrow: 1,
  padding: 15,
};
export const styles = StyleSheet.create({
  headerContainer,
  headerText,
  backImage,
  plusImage,
  button,
  body,
});
