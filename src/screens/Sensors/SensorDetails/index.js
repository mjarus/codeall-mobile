import React, {useState} from 'react';
import {
  Text,
  View,
  Image,
  ScrollView,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import Input from 'CodeallMobile/src/components/Input';
import SensorAttribute, {
  SensorAttributeType,
} from '../components/SensorAttribute';
// import SwitchButton from 'CodeallMobile/src/components/SwitchButton';

import TechnicalDataTable from '../components/TechnicalDataTable';
import getSensorImage from '../../../utils/getSensorImage';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../../../components/TranslatedText';
import COLORS from 'CodeallMobile/src/theme/colors';
import HeaderBackground from '../components/HeaderBackground';
import Header from '../../../components/Header';

const leftArrow = require('../../../assets/img/leftBlueArrow.png');
const rightArrow = require('../../../assets/img/rightBlueArrow.png');
const backImage = require('CodeallMobile/src/assets/img/exit.png');

const SensorDetailsScreen = ({
  navigation,
  route: {
    params: {sensorsArr, sensorId, isShop},
  },
}) => {
  const [sensor, setSensor] = useState(sensorsArr[sensorId - 1]);
  const [currentSensorId, setSensorId] = useState(sensorId - 1);
  const [inputValue, updateInputValue] = useState(sensor.name);
  const {typography} = useTheme();

  const switchSensor = (action) => {
    setSensorId(
      action === 'increase' ? currentSensorId + 1 : currentSensorId - 1,
    );
    setSensor(
      sensorsArr[
        action === 'increase' ? currentSensorId + 1 : currentSensorId - 1
      ],
    );
  };

  if (!sensor) {
    return null;
  }

  return (
    <ScrollView contentContainerStyle={styles.body}>
      <View style={styles.headerBox}>
        <Header
          title={'sensors.sensor.hardware'}
          style={typography.headerWhite}
          leftIcon={backImage}
          onLeftPress={() => navigation.goBack()}
        />
        <HeaderBackground type={'DEVICE'}>
          <View style={styles.container}>
            <View style={styles.arrowsWrapper}>
              {Boolean(currentSensorId) && (
                <TouchableOpacity
                  style={styles.arrowCircle}
                  onPress={() => switchSensor('decrease')}>
                  <Image style={styles.arrowImage} source={leftArrow} />
                </TouchableOpacity>
              )}
            </View>
            <View style={styles.sensorBox}>
              <View style={styles.shadowBox} />
              <View style={styles.shadowWhite} />
              <Text style={typography.sensorDetailsName}>{sensor.name}</Text>
              <View style={styles.deviceContainer}>
                <View style={styles.imageBox}>
                  <Image
                    source={getSensorImage(sensor.type)}
                    style={styles.sensorImage}
                  />
                </View>
                <View style={styles.textContainer}>
                  <SensorAttribute
                    nameId={'sensors.sensor.attribute.type'}
                    value={sensor.type}
                  />
                  <SensorAttribute
                    nameId={'sensors.sensor.attribute.status.name'}
                    value={true}
                    type={SensorAttributeType.STATUS}
                  />
                  <SensorAttribute
                    nameId={'sensors.sensor.attribute.battery'}
                    value={90}
                    type={SensorAttributeType.BATTERY}
                  />
                  <SensorAttribute
                    nameId={'sensors.sensor.attribute.measurement'}
                    value={25}
                    type={SensorAttributeType.TEMPERATURE}
                  />
                </View>
              </View>
            </View>
            <View style={styles.arrowsWrapper}>
              {currentSensorId !== sensorsArr.length - 1 && (
                <TouchableOpacity
                  style={styles.arrowCircle}
                  onPress={() => switchSensor('increase')}>
                  <Image style={styles.arrowImage} source={rightArrow} />
                </TouchableOpacity>
              )}
            </View>
          </View>
        </HeaderBackground>
      </View>
      <View style={styles.mainContainer}>
        <View style={styles.background} />
        {isShop && (
          <>
            <TranslatedText
              id={'sensors.sensor.attribute.name'}
              style={typography.sensorTextLabel}
            />
            <Input
              onChangeText={updateInputValue}
              defaultValue={inputValue}
              style={styles.nameInput}
            />
          </>
        )}
        {/* MEASUREMENTS VALUE SWITCH BUTTON */}
        {/* <TranslatedText
          id="sensors.sensor.attribute.measurement"
          style={typography.sensorTextLabel}
        />
        <SwitchButton /> */}

        <TranslatedText
          id={'sensors.sensor.attribute.originalName'}
          style={typography.sensorTextLabel}
        />
        <Text style={typography.sensorborderText}>{sensor.name}</Text>

        <TranslatedText
          id={'sensors.sensor.attribute.id'}
          style={typography.sensorTextLabel}
        />
        <Text style={typography.sensorborderText}>{sensor.id}</Text>

        <TranslatedText
          id={'sensors.sensor.attribute.productionData'}
          style={typography.sensorTextLabel}
        />

        <View style={styles.table}>
          <TechnicalDataTable sensor={sensor} />
        </View>
      </View>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  body: {
    alignItems: 'center',
    backgroundColor: COLORS.white,
  },

  sensorImage: {
    width: 93,
    height: 79,
    resizeMode: 'contain',
  },

  container: {
    alignItems: 'center',
    justifyContent: 'space-between',
    flexDirection: 'row',
    width: '100%',
    paddingHorizontal: 15,
    marginTop: -50,
  },
  arrowImage: {
    width: 15,
    height: 25,
  },

  imageBox: {
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 10,
  },

  shadowBox: {
    left: 0,
    right: 0,
    height: 8,
    backgroundColor: COLORS.buttonShadow,
    bottom: -2,
    position: 'absolute',
    borderRadius: 4,
  },

  shadowWhite: {
    left: 0,
    right: 0,
    height: 8,
    backgroundColor: COLORS.background,
    bottom: 2,
    position: 'absolute',
    borderRadius: 4,
  },

  textContainer: {
    alignSelf: 'center',
    width: '100%',
  },

  arrowsWrapper: {
    width: 45,
  },

  arrowButton: {
    right: '20%',
    top: '2%',
  },

  arrowCircle: {
    width: '100%',
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    backgroundColor: COLORS.background,
  },

  backImage: {
    width: 22,
    height: 22,
  },

  backButton: {
    position: 'absolute',
    top: 17,
    left: 17,
    zIndex: 1000,
  },

  nameInput: {
    justifyContent: 'center',
  },

  headerBox: {
    backgroundColor: COLORS.secondaryBackground,
    width: '100%',
    alignItems: 'center',
  },

  background: {
    position: 'absolute',
    height: 150,
    width: '200%',
    backgroundColor: COLORS.background,
  },

  mainContainer: {
    alignItems: 'flex-start',
    width: 283,
    paddingHorizontal: 20,
    paddingVertical: 51,
  },

  header: {
    marginTop: 13,
  },

  sensorBox: {
    maxWidth: 246,
    paddingTop: 20,
    paddingRight: 16,
    paddingBottom: 32,
    paddingLeft: 24,
    backgroundColor: COLORS.background,
    borderRadius: 4,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 12,
  },
  deviceContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    width: '100%',
    marginTop: 24,
    backgroundColor: COLORS.background,
  },
  table: {
    width: '100%',
  },
});

export default SensorDetailsScreen;
