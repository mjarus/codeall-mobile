import React from 'react';
import {Image, View, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const projectImage = require('../../../../assets/img/projectImage.png');

const ProjectImage = () => {
  return (
    <View style={styles.container}>
      <View style={styles.shadowContainer}/>
      <View style={styles.projectImageContainer}>
          <Image source={projectImage} style={styles.projectImage}/>
      </View>
    </View>
  );
};

export const styles = StyleSheet.create({
  projectImageContainer: {
    width: 100,
    height: 100,
    backgroundColor: COLORS.background,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 4,
    marginRight: 37,
  },
  projectImage: {
    width: 44.34,
    height: 38.8,
    backgroundColor: COLORS.silver,
    alignItems: 'center',
    justifyContent: 'center',
  },
  container: {
    position: "relative",
  },
  shadowContainer: {
    width: 100,
    height: 100,
    backgroundColor: COLORS.buttonShadow,    
    borderRadius: 4,
    position: "absolute",
    top: 4,
  }
});

export {ProjectImage};
