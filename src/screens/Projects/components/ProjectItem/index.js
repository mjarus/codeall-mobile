import React from 'react';
import {useTranslation} from 'react-i18next';
import {View, Text, StyleSheet} from 'react-native';
import {ProjectImage} from '../ProjectImage';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';
import { SmallOvalButton } from '../../../../components/Buttons';

const ProjectItem = ({projectTitle, status, lastModification}) => {
  const {t} = useTranslation();

  return (
    <View style={styles.container}>
      <ProjectImage />
      <View style={styles.projectDetails}>
        <Text style={styles.projectTitle}>{projectTitle}</Text>
        <Text style={styles.projectInfo}>{t('projects.status', {status})}</Text>
        <Text style={styles.projectInfo}>
          {t('projects.lastModification', {lastModification})}
        </Text>
        <View style={styles.buttonsContainer}>
          <SmallOvalButton type="PRIMARY" headerTextId='projects.button.delete' />
          <SmallOvalButton type="SECONDARY" headerTextId='projects.button.load' />
        </View>
      </View>
    </View>
  );
};

export const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    marginVertical: 10,
    marginHorizontal: 40,
  },
  projectDetails: {
    width: '60%',
  },
  projectTitle: {
    fontSize: 18,
    fontFamily: typography.fonts.primary,
    fontWeight: "bold",
    color: COLORS.textSecondary,
  },
  projectInfo: {
    fontSize: 13,
    fontFamily: typography.fonts.primary,
    color: COLORS.textSecondary,
  },
  buttonsContainer: {
    marginTop: 11,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
});

export {ProjectItem};
