import React from 'react';
import {TouchableOpacity, Text, StyleSheet, View} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';

const ProjectButton = ({type, title}) => {
  return (
    <View style={styles.outerContainer}>
      <View style={[styles.container(), styles.shadowBox]} />
      <TouchableOpacity style={styles.container(type)} activeOpacity={0.8}>
        <Text style={styles.text(type)}>{title}</Text>
      </TouchableOpacity>
    </View>
  );
};

export const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: type === 'DELETE' ? COLORS.background : COLORS.loadButton,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    width: 85,
    height: 35,
  }),
  text: (type) => ({
    fontSize: 11,
    fontFamily: typography.fonts.secondaryRegular,
    color: type === 'DELETE' ? COLORS.deleteButton : COLORS.buttonDefaultText,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  }),
  outerContainer: {
    position: 'relative',
  },
  shadowBox: {
    position: 'absolute',
    top: 4,
    backgroundColor: COLORS.buttonShadow,
  },
});

export {ProjectButton};
