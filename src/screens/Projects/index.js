import React from 'react';
import {
  FlatList,
  ScrollView,
  TouchableOpacity,
  Image,
  StyleSheet,
  View,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {projectsMock} from '../../mocks/projects';
import {ProjectItem} from './components/ProjectItem';
import COLORS from 'CodeallMobile/src/theme/colors';
import Header from '../../components/Header';

const plus = require('CodeallMobile/src/assets/img/bluePlus.png');
const divider = require('CodeallMobile/src/assets/img/projectsDivider.png');

const Projects = () => {
  const navigation = useNavigation();

  const handlePlusIconPress = () => {
    navigation.navigate('LessonNavigator', {
      isEditorInitial: true,
    });
  };

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <View style={styles.headerContainer}>
        <Header 
          title="projects.headerText" 
          onRightPress={handlePlusIconPress} 
          rightIcon={plus}
          style={styles.headerText}
        />
      </View>
      <View style={styles.mainContainer}>
        <Image source={divider} style={styles.divider} />
        <FlatList
          data={projectsMock}
          keyExtractor={(item) => item.id}
          style={styles.list}
          renderItem={({item: {title, status, updatedAt}}) => (
            <ProjectItem
              projectTitle={title}
              status={status}
              lastModification={updatedAt}
            />
          )}
        />
      </View>
    </ScrollView>
  );
};

export const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: COLORS.secondaryBackground,
    flex: 1,
  },
  headerContainer: {
    height: 55,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 25,
    backgroundColor: COLORS.background,
    width: '100%',
    marginTop: 0,
  },
  headerText: {
    color: COLORS.mainText,
  },
  plus: {
    width: 20,
    height: 20,
  },
  plusBox: {
    position: 'absolute',
    right: 20,
    top: 20,
  },
  mainContainer: {
    backgroundColor: COLORS.secondaryBackground,
    flexGrow: 1,
  },
  list: {
    marginTop: 38,
  },
  divider: {
    width: 400,
    height: 8,
  },
});

export default Projects;
