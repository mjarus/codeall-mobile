class ApiError extends Error {
  httpStatusCode: number;
  userMessage: string;
  code: string;

  constructor(
    message: string,
    httpStatusCode: number,
    userMessage: string,
    code: string,
  ) {
    super(message);
    this.httpStatusCode = httpStatusCode;
    this.userMessage = userMessage;
    this.code = code;
  }
}

export default ApiError;
