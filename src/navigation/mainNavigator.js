import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import HomeNavigator from './homeNavigator';
import LessonNavigator from './lessonNavigator';
import InviteScreen from '../screens/User/InviteScreen';
import SensorAvailable from '../screens/Sensors/SensorsAvailable';
import SensorDetails from '../screens/Sensors/SensorDetails';
import LessonsScreen from '../screens/Lessons/LessonsScreen';

const Stack = createStackNavigator();

const MainNavigator = () => (
  <Stack.Navigator
    initialRouteName="HomeNavigator"
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="HomeNavigator" component={HomeNavigator} />
    <Stack.Screen name="LessonNavigator" component={LessonNavigator} />
    <Stack.Screen name="InviteScreen" component={InviteScreen} />
    <Stack.Screen name="LessonsScreen" component={LessonsScreen} />
    <Stack.Screen name="SensorAvailable" component={SensorAvailable} />
    <Stack.Screen name="SensorDetails" component={SensorDetails} />
  </Stack.Navigator>
);

export default MainNavigator;
