import React from 'react';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {Image, View, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import LessonsScreen from '../../screens/Lessons';
import SensorsNavigator from '../sensorsNavigator';
import ProjectsScreen from '../../screens/Projects';
import ProfileNavigator from '../profileNavigator';
import {useTranslation} from 'react-i18next';

const IconLessons = require('CodeallMobile/src/assets/img/lessonsIcon.png');
const IconLessonsActive = require('CodeallMobile/src/assets/img/lessonsIconActive.png');
const IconSensors = require('CodeallMobile/src/assets/img/sensorsIcon.png');
const IconSensorsActive = require('CodeallMobile/src/assets/img/sensorsIconActive.png');
const IconProjects = require('CodeallMobile/src/assets/img/projectsIcon.png');
const IconProjectsActive = require('CodeallMobile/src/assets/img/projectsIconActive.png');
const IconProfile = require('CodeallMobile/src/assets/img/profileIcon.png');
const IconProfileActive = require('CodeallMobile/src/assets/img/profileIconActive.png');

const Tab = createBottomTabNavigator();

const HomeNavigator = (props) => {
  const {t} = useTranslation();

  return (
    <Tab.Navigator
      initialRouteName="Lessons"
      screenOptions={{
        headerShown: false,
      }}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        activeTintColor: COLORS.navigationActive,
        inactiveTintColor: COLORS.navigationInactive,
        activeBackgroundColor: COLORS.background,
        safeAreaInsets: {top: 0, bottom: 0},
        inactiveBackgroundColor: COLORS.background,
        style: {
          backgroundColor: COLORS.background,
          paddingTop: 5,
          paddingBottom: 5,
          height: 50,
        },
        labelStyle: {
          fontSize: 13,
          letterSpacing: 0.12,
          fontFamily: 'Lato',
          fontWeight: 'bold',
        },
        defaultNavigationOptions: {
          headerStyle: {
            height: 50,
          },
          headerForceInset: {top: 'never', bottom: 'never'},
        },
      }}>
      <Tab.Screen
        name="Lessons"
        component={LessonsScreen}
        options={{
          tabBarLabel: t('navigation.tab1'),
          tabBarIcon: ({focused, color}) => (
            <View>
              <Image
                style={[styles.lessonsIcon, {tintColor: color}]}
                source={focused ? IconLessonsActive : IconLessons}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Sensors"
        component={SensorsNavigator}
        options={{
          tabBarLabel: t('navigation.tab2'),
          tabBarIcon: ({focused, color}) => (
            <View>
              <Image
                style={[styles.sensorsIcon, {tintColor: color}]}
                source={focused ? IconSensorsActive : IconSensors}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="Projects"
        component={ProjectsScreen}
        options={{
          tabBarLabel: t('navigation.tab3'),
          tabBarIcon: ({focused, color}) => (
            <View>
              <Image
                style={[styles.projectsIcon, {tintColor: color}]}
                source={focused ? IconProjectsActive : IconProjects}
              />
            </View>
          ),
        }}
      />
      <Tab.Screen
        name="UserProfile"
        component={ProfileNavigator}
        options={{
          tabBarLabel: t('navigation.tab4'),
          tabBarIcon: ({focused, color}) => (
            <View>
              <Image
                style={[styles.profileIcon, {tintColor: color}]}
                source={focused ? IconProfileActive : IconProfile}
              />
            </View>
          ),
        }}
      />
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  lessonsIcon: {
    width: 25,
    height: 24,
  },
  sensorsIcon: {
    width: 25,
    height: 24,
  },
  projectsIcon: {
    width: 21,
    height: 18,
  },
  profileIcon: {
    width: 19,
    height: 22,
  },
});

export default HomeNavigator;
