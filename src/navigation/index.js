/* eslint-disable */
/* Disable ESList because of NavigationContainer formatting */

import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {connect} from 'react-redux';

import MainNavigator from './mainNavigator';

import {theme} from '../theme';
import SplashScreen from '../screens/SplashScreen';
import StartNavigator from "./startNavigator";

const RootNavigator = ({isSignedIn, isInitializing, requireDeviceRegistrationError}) => {
  return (
    <NavigationContainer theme={theme}>
      {
        isSignedIn
        ? isInitializing ? <SplashScreen /> : <MainNavigator />
        : requireDeviceRegistrationError ? <StartNavigator /> : <SplashScreen />
      }
    </NavigationContainer>
  );
};

const mapStateToProps = state => ({
  isInitializing: state.profile.isInitializing || state.lessons.isInitializing,
  isSignedIn: !!state.auth.token && !!state.auth.userId,
  requireDeviceRegistrationError: state.auth.requireDeviceRegistrationError,
});

export default connect(mapStateToProps)(RootNavigator);
