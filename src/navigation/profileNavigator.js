import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import UserProfileScreen from '../screens/User/UserProfile';
import UserSettingsScreen from '../screens/User/UserSettings';
import UserLockScreen from '../screens/User/UserLock';

const Stack = createStackNavigator();

const ProfileNavigator = () => (
  <Stack.Navigator
    initialRouteName="UserProfile"
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="UserProfile" component={UserProfileScreen} />
    <Stack.Screen name="UserSettings" component={UserSettingsScreen} />
    <Stack.Screen name="UserLock" component={UserLockScreen} />
  </Stack.Navigator>
);

export default ProfileNavigator;
