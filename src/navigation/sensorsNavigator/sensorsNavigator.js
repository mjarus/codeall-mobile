import React from 'react';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import COLORS from 'CodeallMobile/src/theme/colors';
import ConnectedSensorsScreen from '../../screens/Sensors/ConnectedSensors';
import ConnectNewSensorScreen from '../../screens/Sensors/ConnectNewSensor';
import ShopScreen from '../../screens/Sensors/Shop';
import typography from '../../theme/typography';
import {useTranslation} from 'react-i18next';
import {StyleSheet, View, Text} from 'react-native';

const Tab = createMaterialTopTabNavigator();

const SensorsNavigator = (props) => {
  const {t} = useTranslation();

  return (
    <Tab.Navigator
      initialRouteName="ConnectedSensorsNavigator"
      tabBarOptions={{
        style: styles.tabBar,
        labelStyle: typography.navSensorTabLabel,
        renderIndicator: () => null,
      }}>
      <Tab.Screen
        name={'ConnectedSensors'}
        options={{
          tabBarLabel: ({focused}) => (
            <View style={styles.bar}>
              {focused && <View style={styles.activeBar} />}
              <Text style={typography.navSensorTabLabel(focused)}>
                {t('sensors.nav.connectedSensors')}
              </Text>
            </View>
          ),
        }}>
        {() => <ConnectedSensorsScreen {...props} />}
      </Tab.Screen>
      <Tab.Screen
        name={'ConnectNewSensor'}
        options={{
          tabBarLabel: ({focused}) => (
            <View style={styles.bar}>
              {focused && <View style={styles.activeBar} />}
              <Text style={typography.navSensorTabLabel(focused)}>
                {t('sensors.nav.connectNewSensors')}
              </Text>
            </View>
          ),
        }}>
        {() => <ConnectNewSensorScreen {...props} />}
      </Tab.Screen>
      <Tab.Screen
        name={'Shop'}
        options={{
          tabBarLabel: ({focused}) => (
            <View style={styles.bar}>
              {focused && <View style={styles.activeBar} />}
              <Text style={typography.navSensorTabLabel(focused)}>
                {t('sensors.nav.shop')}
              </Text>
            </View>
          ),
        }}>
        {() => <ShopScreen {...props} />}
      </Tab.Screen>
    </Tab.Navigator>
  );
};

const styles = StyleSheet.create({
  tabBar: {
    backgroundColor: COLORS.secondaryBackground,
    margin: 0,
    elevation: 0,
  },
  bar: {
    height: 45,
    alignItems: 'center',
    justifyContent: 'center',
    width: 124,
    margin: 0,
    paddingTop: 10,
    paddingBottom: 20,
    marginTop: -10,
  },
  activeBar: {
    width: 124,
    height: 4,
    borderRadius: 10,
    backgroundColor: COLORS.activeTint,
    position: 'absolute',
    bottom: 0,
  },
});

export default SensorsNavigator;
