import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import SensorsNavigator from './sensorsNavigator';

const Stack = createStackNavigator();

const MainSensorsNavigator = () => (
  <Stack.Navigator
    initialRouteName="SensorsNavigator"
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="SensorsNavigator" component={SensorsNavigator} />
  </Stack.Navigator>
);

export default MainSensorsNavigator;
