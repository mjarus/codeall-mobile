import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const headerContainer = {
  backgroundColor: COLORS.barBackround,
  height: 52,
  alignItems: 'center',
  justifyContent: 'space-between',
  flexDirection: 'row',
};

const tabBar = {
  backgroundColor: COLORS.barBackround,
};

const HEADER_HEIGHT = 40;

const exitButton = {
  justifyContent: 'center',
  alignItems: 'center',
  position: 'absolute',
  height: HEADER_HEIGHT,
  width: HEADER_HEIGHT,
  left: 0,
  top: 0,
};

const exitButtonIcon = {
  width: 18,
  height: 18,
};

const titleText = {
  fontSize: 14,
  color: COLORS.background,
  textAlign: 'center',
  fontFamily: 'Quicksand-Bold',
};

const tabBarStyle = {
  backgroundColor: COLORS.barBackround,
};

const tabStyle = {
  padding: 0,
  borderTopWidth: 1,
  borderColor: COLORS.barBackround,
  height: HEADER_HEIGHT,
  justifyContent: 'center',
  flexDirection: 'row',
  alignItems: 'center',
};

const tabLabelStyle = {
  marginTop: (HEADER_HEIGHT - 22) / 2,
  fontFamily: 'Quicksand',
  fontStyle: 'normal',
  fontWeight: '500',
  fontSize: 11,
  lineHeight: 14,
  textAlign: 'center',
  letterSpacing: 0.9,
  textTransform: 'none',
  marginBottom: 20,
};

const indicatorStyle = {
  backgroundColor: COLORS.supernova,
  height: 4,
};

const button = {
  margin: 10,
};

const exitIcon = {
  width: 34.27,
  height: 33.07,
};

const buttonImage = {
  width: 22,
  height: 22,
};

const chatIcon = {
  width: 12.5,
  height: 12.5,
};

const editorIcon = {
  width: 19,
  height: 14.5,
};

export const styles = StyleSheet.create({
  headerContainer,
  tabBar,
  button,
  buttonImage,
  exitButton,
  exitButtonIcon,
  titleText,
  tabBarStyle,
  tabStyle,
  tabLabelStyle,
  indicatorStyle,
  chatIcon,
  editorIcon,
  exitIcon,
});
