import React from 'react';
import {styles} from './styles';
import Chatbot from '../../screens/Chatbot';
import Editor from '../../screens/Editors/ObjectEditor';
import Console from '../../screens/Console';
import COLORS from 'CodeallMobile/src/theme/colors';
import {View, Image} from 'react-native';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createStackNavigator} from '@react-navigation/stack';
import NetworkAwareStatusBar from 'CodeallMobile/src/components/NetworkAwareStatusBar';
import {useTranslation} from 'react-i18next';
import Header from '../../components/Header';

const iconExit = require('CodeallMobile/src/assets/img/exit.png');
const iconSensorsAvailable = require('CodeallMobile/src/assets/img/sensorsAvailable.png');
const editorIcon = require('CodeallMobile/src/assets/img/editorIcon.png');
const chatIcon = require('CodeallMobile/src/assets/img/chatIcon.png');

const Tab = createMaterialTopTabNavigator();
const WrappedTabNavigator = (props) => {
  const {params} = props.route;
  const {t} = useTranslation();
  const initialRouteName = params
    ? params.isEditorInitial && 'Edytor'
    : 'Chatbot';

  return (
    <Tab.Navigator
      swipeEnabled={false}
      initialRouteName={initialRouteName}
      tabBarOptions={{
        keyboardHidesTabBar: true,
        style: styles.tabBarStyle,
        tabStyle: styles.tabStyle,
        labelStyle: styles.tabLabelStyle,
        indicatorStyle: styles.indicatorStyle,
        activeTintColor: COLORS.background,
        inactiveTintColor: COLORS.background,
        safeAreaInsets: {top: 0, bottom: 0},
        showIcon: true,
        showLabel: true,
      }}>
      <Tab.Screen
        name={'Chatbot'}
        options={{
          tabBarLabel: t('navigation.chatbot'),
          tabBarIcon: () => (
            <View>
              <Image style={styles.chatIcon} source={chatIcon} />
            </View>
          ),
        }}>
        {() => <Chatbot {...props} />}
      </Tab.Screen>
      <Tab.Screen
        name={'Edytor'}
        options={{
          tabBarLabel: t('navigation.editor'),
          tabBarIcon: () => (
            <View>
              <Image style={styles.editorIcon} source={editorIcon} />
            </View>
          ),
        }}>
        {() => <Editor {...props} />}
      </Tab.Screen>
      {/* TODO temporary hidden tab */}
      {/* <Tab.Screen name={'Rozmowa'}>
      {() => <SharedEditorChat {...props} />}
    </Tab.Screen> */}
      <Tab.Screen
        name={'Konsola'}
        options={{
          tabBarLabel: t('navigation.console'),
        }}>
        {() => <Console {...props} />}
      </Tab.Screen>
    </Tab.Navigator>
  );
};

const NavHeader = (props) => {
  return (
    <View style={styles.headerContainer}>
      <NetworkAwareStatusBar
        backgroundColor={COLORS.stTropaz}
        barStyle="light-content"
      />
      <Header
        title={props.title}
        leftIcon={iconExit}
        rightIcon={iconSensorsAvailable}
        onLeftPress={() => props.navigation.navigate('HomeNavigator')}
        onRightPress={() => props.navigation.navigate('SensorAvailable')}
        style={styles.titleText}
      />
    </View>
  );
};

// todo refactor {() => <WrappedTabNavigator {...props} />} this is not optimal. See BACK-1252
// TabNavigator must be wrapped inside StackNavigator to display a header
const WrapperStack = createStackNavigator();
const LessonNavigator = (props) => {
  const params = props.route.params;
  const lessonName =
    params && params.currentLesson && params.currentLesson.name;
  return (
    <WrapperStack.Navigator initialRouteName="LessonNavigator">
      <WrapperStack.Screen
        name="LessonNavigator"
        options={{
          header: (headerProps) => (
            <NavHeader {...headerProps} title={lessonName} />
          ),
        }}>
        {() => <WrappedTabNavigator {...props} />}
      </WrapperStack.Screen>
    </WrapperStack.Navigator>
  );
};

export default LessonNavigator;
