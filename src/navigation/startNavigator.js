import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import StartScreen from '../screens/Start';
import RegisterDevice from '../screens/Register/RegisterDevice';

const Stack = createStackNavigator();

const StartNavigator = () => (
  <Stack.Navigator
    initialRouteName="StartScreen"
    screenOptions={{
      headerShown: false,
    }}>
    <Stack.Screen name="StartScreen" component={StartScreen} />
    <Stack.Screen name="RegisterDevice" component={RegisterDevice} />
  </Stack.Navigator>
);

export default StartNavigator;
