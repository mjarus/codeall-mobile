import {StyleSheet} from 'react-native';

export const mainContainer = {
  flex: 1,
};

export const styles = StyleSheet.create({
  mainContainer,
});
