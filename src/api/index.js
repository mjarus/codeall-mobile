import {store} from '../store';
import ApiError from '../errors/apiError';
import {API_URL} from '@env';
import getLocale from '../utils/getLocale';
import {Platform} from 'react-native';
import DeviceInfo from 'react-native-device-info';
import {endpoints} from './urls';
import { getLanguageDB } from '../db';
import {sensorsList} from '../mocks/sensors'

const getParsedEndpoint = endpoint => {
  let [path, templateQueryString] = endpoint.path.split('?');

  const templateQueryParams = templateQueryString
    ? templateQueryString.split('&')
    : [];

  if (endpoint.pathParams) {
    for (const [name, value] of Object.entries(endpoint.pathParams)) {
      const nameRegex = `{${name}}`;
      path = path.replace(nameRegex, value);
    }
  }

  let queryString = '';
  let queryStringElements = [];
  if (templateQueryParams.length && endpoint.queryParams) {
    for (const nameRegex of templateQueryParams) {
      const name = nameRegex.substring(1, nameRegex.length - 1);
      const value = endpoint.queryParams[name];
      if (value) {
        queryStringElements.push(`${name}=${value}`);
      }
    }

    queryString = queryStringElements.join('&');
    queryString = queryString ? `?${queryString}` : '';
  }

  return `${path}${queryString}`;
};

const getHeaders = (language) => {
  const token = store.getState().auth.token || '';
  const requestId =
    Math.random()
      .toString(36)
      .substring(2, 15) +
    Math.random()
      .toString(36)
      .substring(2, 15);

  const generalInfo = [
    `SYSTEM: ${Platform.OS}`,
    `SYSTEM-VERSION: ${DeviceInfo.getSystemVersion()}`,
    `SYSTEM-API-VERSION: ${Platform.Version}`,
    `DEVICE-MANUFACTURE: ${DeviceInfo.getManufacturer()}`,
    `DEVICE-MODEL: ${DeviceInfo.getModel()}`,
    `DEVICE-ID: ${DeviceInfo.getDeviceId()}`,
    `APP-VERSION: ${DeviceInfo.getVersion()}`,
  ];

  return {
    'Content-Type': 'application/json',
    Authorization: 'Basic Y29kZWFsbDoyMDIw', // Basic Auth for RC server
    'X-AUTH-TOKEN': token,
    'REQUEST-ID': requestId,
    locale: language,
    'GENERAL-INFO': generalInfo.join(','),
  };
};

/**
 * @param uriData: object {path, pathParams, queryParams}, where path can contain vars like so `/users/{id}/?{foo}&{bar}`
 * @param method
 * @param body
 * @throws ApiError
 * @throws RepositoryError
 * @return {httpStatusCode, data}
 */
const apiRequest = async (uriData, method = 'GET', body) => {
  const parsedEndpoint = getParsedEndpoint(uriData);

  const languageDB = await getLanguageDB();
  const language = languageDB ? languageDB : getLocale();
  const options = {
    headers: getHeaders(language),
    body: body ? JSON.stringify(body) : null,
    method: method,
  };

  let response;
  try {
    response = await fetch(API_URL + parsedEndpoint, options);
  } catch (e) {
    throw new Error(e.message);
  }

  const httpStatusCode = response.status;
  let data = null;
  try {
    data = await response.json();
  } catch (e) {
    // empty response data
  }
  if (!response.ok) {
    throw new ApiError(
      data?.error?.message ?? 'ApiError',
      httpStatusCode,
      data?.error?.userMessage,
      data?.error?.code,
    );
  }

  return {httpStatusCode, data};
};

const getUserProfile = async userId => {
  const uriData = {
    path: endpoints.USER_PROFILE,
    pathParams: {userId},
  };
  const response = await apiRequest(uriData);
  return response.data;
};

const updateUserProfile = async (userId, name, surname, nick) => {
  const uriData = {
    path: endpoints.USER_PROFILE,
    pathParams: {userId},
  };
  const body = {name, surname, nick};
  const response = await apiRequest(uriData, 'PUT', body);
  return response.data;
};

const getChatbotHistory = async page => {
  const uriData = {
    path: endpoints.CHATBOT_HISTORY,
    queryParams: {page},
  };
  const response = await apiRequest(uriData);
  return response.data;
};

const getStep = async stepId => {
  const uriData = {
    path: endpoints.STEP,
    pathParams: {stepId},
  };
  const response = await apiRequest(uriData);
  return response.data;
};

const getLesson = async (unitId, lessonId) => {
  const uriData = {
    path: endpoints.LESSON,
    pathParams: {unitId, lessonId},
  };
  const response = await apiRequest(uriData);
  return response.data;
};

const saveSourceCodeForStep = async (stepId, sourceCode) => {
  const uriData = {
    path: endpoints.SOURCE_CODE,
    pathParams: {stepId},
  };
  const body = {sourceCode};
  const response = await apiRequest(uriData, 'PUT', body);
  return response.data;
};

const verifySourceCode = async (stepId, sourceCode) => {
  const uriData = {
    path: endpoints.VERIFY_SOURCE_CODE,
    pathParams: {stepId},
  };
  const body = {sourceCode};
  const response = await apiRequest(uriData, 'PUT', body);
  return response.data;
};

const login = async (email, password) => {
  const uriData = {
    path: endpoints.AUTH,
  };
  const body = {email, password};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const register = async (
  name,
  nick,
  email,
  password,
  rePassword,
  type,
  scenarioToken,
) => {
  const uriData = {
    path: endpoints.REGISTER,
  };
  const body = {name, nick, email, password, rePassword, type, scenarioToken};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const facebookLogin = async token => {
  const serviceName = 'facebook';
  const scenarioToken = '92pCP5CdPZK'; // TODO move to config

  const uriData = {
    path: endpoints.SOCIAL_AUTH,
  };
  const body = {token, scenarioToken, serviceName};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const deviceLogin = async (token, nick, referralCode) => {
  const serviceName = 'device';
  const scenarioToken = '92pCP5CdPZK'; // TODO move to config

  const uriData = {
    path: endpoints.SOCIAL_AUTH,
  };
  const body = {token, scenarioToken, serviceName, nick, referralCode};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const getRanking = async () => {
  const uriData = {
    path: endpoints.RANKING,
  };
  const response = await apiRequest(uriData);
  return response.data;
};

const sendChatbotMessage = async (question, learningSetName) => {
  const uriData = {
    path: endpoints.CHATBOT_CONVERSATION,
  };
  const body = {question, learningSetName};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const sendIntentProposal = async (question, answer, context) => {
  const uriData = {
    path: endpoints.INTENTS,
  };
  const body = {question, answer, context};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const markChatBubbleCompleted = async (
  chatBubbleId,
  answerId,
  openQuestionAnswer,
) => {
  const uriData = {
    path: endpoints.CHAT_BUBBLES,
  };
  const body = {chatBubbleId, answerId, openQuestionAnswer};
  const response = await apiRequest(uriData, 'POST', body);
  return response.data;
};

const getUnits = async () => {
  const uriData = {
    path: endpoints.UNITS,
  };
  const response = await apiRequest(uriData);
  return response.data;
};

const getProducts = async () => {
  //COMMENTED SENSORS FETCHING / TEMPORARILY MOCKED
  // const uriData = {
  //   path: endpoints.PRODUCTS,
  // };
  // const response = await apiRequest(uriData);
  // return response.data;
  return sensorsList
};

const api = {
  getUserProfile,
  updateUserProfile,
  getChatbotHistory,
  getStep,
  getLesson,
  saveSourceCodeForStep,
  verifySourceCode,
  login,
  register,
  facebookLogin,
  deviceLogin,
  getRanking,
  sendChatbotMessage,
  sendIntentProposal,
  markChatBubbleCompleted,
  getUnits,
  getProducts,
};

export default api;
