const API_V1 = '/api/v1';

export const endpoints = {
  AUTH: `${API_V1}/auth/authenticate`,
  SOCIAL_AUTH: `${API_V1}/auth/authenticate/social-media`,
  REGISTER: `${API_V1}/register`,
  USER_PROFILE: `${API_V1}/users/{userId}`,
  RANKING: `${API_V1}/users/ranking`,
  CHATBOT_CONVERSATION: `${API_V1}/chatbot/conversation`,
  CHATBOT_HISTORY: `${API_V1}/chatbot/conversation/history?{page}`,
  INTENTS: `${API_V1}/machine-learning/intents`,
  STEP: `${API_V1}/learning/step/{stepId}`,
  CHAT_BUBBLES: `${API_V1}/learning/chat-bubbles`,
  LESSON: `${API_V1}/learning/units/{unitId}/lessons/{lessonId}`,
  UNITS: `${API_V1}/learning/units`,
  SOURCE_CODE: `${API_V1}/learning/step/{stepId}/source-code`,
  VERIFY_SOURCE_CODE: `${API_V1}/learning/step/{stepId}/source-code/verify`,
  PRODUCTS: `${API_V1}/products`,
};
