import {store} from '../store';
import DeviceInfo from 'react-native-device-info';
import {Platform} from 'react-native';
import getLocale from '../utils/getLocale';

export const defaultHeaders = (headers = {}) => {
  return {
    ...headers,
    Authorization: 'Basic Y29kZWFsbDoyMDIw', // for RC server
    locale: getLocale(),
    'Content-Type': 'application/json',
    'GENERAL-INFO': `SYSTEM: ${
      Platform.OS
    }, SYSTEM-VERSION: ${DeviceInfo.getSystemVersion()}, SYSTEM-API-VERSION: ${
      Platform.Version
    }, DEVICE-MANUFACTURE: ${DeviceInfo.getManufacturer()}, DEVICE-MODEL: ${DeviceInfo.getModel()}, DEVICE-ID: ${DeviceInfo.getDeviceId()}, APP-VERSION: ${DeviceInfo.getVersion()}`,
  };
};

export const setRequestId = (headers = {}) => ({
  ...defaultHeaders(headers),
  'REQUEST-ID': store.getState().auth.requestID,
});

export const setXAuthToken = (headers = {}) => ({
  ...setRequestId(headers),
  'X-AUTH-TOKEN': store.getState().auth.token,
});
