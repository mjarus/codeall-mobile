import i18n from 'i18next';
import {initReactI18next} from 'react-i18next';
import getLocale from '../utils/getLocale';
import {en} from './en';
import {pl} from './pl';
import {ar} from './ar';

export const SUPPORTED_LANGUAGES = ['en', 'pl', 'ar'];

let lng = getLocale();

if (!SUPPORTED_LANGUAGES.includes(lng)) {
  lng = 'en';
}

i18n.use(initReactI18next).init({
  resources: {
    en: {translation: en},
    pl: {translation: pl},
    ar: {translation: ar},
  },
  lng: lng,
  fallbackLng: 'en',
  interpolation: {escapeValue: false},
});

export default i18n;
