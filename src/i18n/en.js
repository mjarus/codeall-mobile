const en = {
  general: {
    unknownError: 'Unknown error',
    networkError:
      'You need the Internet connection to start CodeAll application',
    yes: 'Yes',
    no: 'No',
    ok: 'OK',
  },

  connection: {
    internet: {
      information: 'You are offline! Check your internet connection.',
    },
  },

  login: {
    header: 'Welcome back!',
    subHeader: 'Login to continue your studies.',
    password: 'Password:',
    logInButton: 'Sign in',
    socialLogin: 'or sign in using:',
    noAccount: "Don't have an account?",
    register: 'Register',
  },

  start: {
    hi: 'Hi',

    header: {
      line1: 'Are you ready to start your',
      line2: 'programming adeventure?',
    },
  },

  chatbot: {
    message: {
      suggestion: 'Your suggestion',
      thanks: 'Thank you for your answer!',
    },

    inputPlaceholder: 'Send a message',
    naughtWords: "You're using very naughty words...",
    noAnswear: "I'm sorry, but I don't know how to reply...",
    somethingsWrong: 'Somethings wrong... I received this information: ',
    talkWithChatbot: 'Talk with chatbot',
    noMessages: 'No messages',
  },

  leaderboard: {
    achievments: 'Achievements',
    ranking: 'Ranking',
  },

  register: {
    mail: {
      greeting: 'Hi!',
      readyForAdventure:
        'Are you ready to begin your adventure with programming?',
      email: 'E-mail:',
      next: 'Next',
      alternativeLogin: 'or log in using:',
      account: 'Do you have an account? ',
      login: 'Login',
    },

    password: {
      createPassword: 'Create a password',
      lastStep:
        'This is the last step of formalities. Your adventure will begin shortly.',
      password: 'Password:',
      createAccount: 'Create an account',
      accountQuestion: 'Do you have an account',
      login: 'Sign in',
    },

    refferalDescription: {
      line1: 'If you have received an app invitation',
      line2: 'from a friend, enter the code here',
      line3: 'reference. Your friend and you',
      line4: 'will get extra points.',
    },

    code: {
      error: 'Incorrect code',
    },

    placeholder: {
      referral: 'E.g. ABCDEFGH',
    },

    setNick: 'Enter your name',
    optional: 'optional',
    setRefferal: 'enter the code from your friend',
  },

  userProfile: {
    account: 'Profile',
    achievements: 'Achievemnets',
    settings: 'Settings',
    inviteButton: 'Invite users',
  },

  userSettings: {
    yourAccount: 'Settings',
    name: 'Name',
    surname: 'Surname',
    email: 'Email',
    password: 'Password',
    currentPassword: 'Current password',
    newPassword: 'New password',
    saveChanges: 'Save',
    saveChangesError: 'Unable to save changes',
    logout: 'Logout',
    changePhoto: 'Change Photo',
    language: 'Language',
    setLock: 'Set Lock',
  },

  userLock: {
    back: 'Back',
    header: 'Set Lock',

    errors: {
      short: 'PIN needs to be at least 4 characters long',
      notMatch: 'PINS are not the same',
    },

    labels: {
      currentPin: 'Current PIN',
      newPin: 'New PIN',
      confirmPin: 'Confirm PIN',
    },
  },

  inviteFriendsBanner: {
    button: 'Invite Users',

    description: {
      line1: 'Invite Users',
      line2: 'and get free sensors',
    },
  },

  inviteScreen: {
    header: 'Invite Users and receive sensors',

    description: {
      line1:
        'If other people start using CodeAll thanks to your invitation, you will have chance to get CodeAll sensors for free.',
      line2:
        'For every 10 accepted invitations you will get 1 sensor of your choice for free!',
    },

    shareButton: 'Share the invitation link',
    yourCode: 'Your code',

    statistics: {
      accountsCreated: 'Number of accounts created from invitations',
      freeSensors: 'Number of free sensors',
      usedFreeSensors: 'Number of sensors used',
    },

    copy: {
      notification: 'Copied!',
    },
  },

  navigation: {
    tab1: 'Lessons',
    tab2: 'Sensors',
    tab3: 'Projects',
    tab4: 'Profile',
    chatbot: 'Chatbot',
    editor: 'Editor',
    console: 'Console',
  },

  lessons: {
    welcome: 'Hi',
    learnNew: 'Talk with chatbot',
    chooseLesson: 'or choose a lesson',
    topics: 'Topics',
    lessons: 'Lessons',
    lessonsPlural: 'lessons',
    lessonsTotal: 'lessons total',
    section: 'Section 1,',
    introduction: 'introduction',
  },

  flashMessage: {
    attention: 'Alert',
  },

  sensors: {
    nav: {
      connectedSensors: 'Connected',
      connectNewSensors: 'Connect',
      shop: 'Shop',
    },

    sensor: {
      found: 'Found:',
      name: 'Sensor',
      more: 'More...',
      buy: 'Buy now',
      disconnect: 'Disconnect',
      back: 'back',
      disconnectConfirmation:
        'Are you sure you want to disconnect this sensor? You will be able to pair it again in the Connect tab',
      connect: 'Connect',
      unavailableDescription:
        'This sensor has been connected (paired) to your SmartBox but is currently not visible in its range. Maybe it is too far away or the battery has run out?',
      changeFrequencyDescription: 'Enter the frequency of data reading: ',
      available: 'Sensors available',
      hardware: 'Hardware details',

      attribute: {
        battery: 'Battery: ',

        status: {
          name: 'Status:',
          available: 'available',
          unavailable: 'unavailable',
        },

        name: 'Name:',
        originalName: 'Nazwa fabryczna:',
        type: 'Type:',
        id: 'ID:',
        idShort: 'ID:',
        measurement: 'Measure: ',
        productionData: 'Production data',
        powerSupply: 'Power supply',
        lifespan: 'Lifespan under typical conditions',
        accuracy: 'Measuring accuracy',
        temperature: 'Operating temperature',
        alarm: 'Alarm siren sound level',
        dimensions: 'Dimensions',
        price: 'Price',
        version: 'Version: ',
        micropythonVersion: 'Micropython version: ',
        frequency: 'Frequency',
        'frequency.change': 'Change',
      },
    },

    smartbox: {
      connected: 'Connected SmartBox!',

      description: {
        notConnected: {
          error: 'SmartBox missing!',
          line1:
            'Connect SmartBox to you smartphone to get access to CodeAll sensors!!',
        },
      },
    },

    connectionInformation: {
      line1:
        'On this screen you will see sensors that you can connect (pair) with your SmartBox.',
      line2:
        'Using the sensor is possible only after connecting to the SmartBox.',
      line3:
        "To see the sensor in the list, mount it on the base, approach it within 30 cm, and wait up to 10 seconds. After clicking 'Connect', the LED on the sensor will flash 3 times. This will be a signal that pairing is successful.",
    },

    noSensorsConnected: 'No sensors connected',
    connectionInstructions:
      'Place the sensors you want to pair with the app within 30 cm of the SmartBox',
    specification: 'Technical Specification',
  },

  textButton: {
    edit: 'Edit',
    save: 'Save'
  },

  payment: {
    payment: 'Payment',
    googlePlay: 'Google Play',
    subscriptionInformation:
      'You can delete subscription in any time in Google Play Subscriptions',
    buy: 'Complete Purchase',
  },

  smartbox: {
    configuration: {
      information:
        'SmartBox has been configured with the application. Connect the right sensor, upload the project and see the possibilities offered by the Internet of Things!',
    },

    list: {
      sensors: 'List of sensors:',
    },

    errors: {
      1: 'Device not found!',
      2: 'Device name cannot be invalid or empty!',
      3: 'BaudRate cannot be invalid!',
      4: 'Connection Failed!',
      5: 'Could not open Serial Port!',
      6: 'Disconnect Failed!',
      7: 'Serial Port is already connected',
      8: 'Serial Port is already disconnected',
      9: 'Usb service not started. Please first start Usb service!',
      10: 'No device with name {Device name}',
      11: 'User did not allow to connect',
      12: 'Service could not stopped off. Please close connection first',
      13: 'There is no connection',
      14: 'Error reading from port',
      15: 'Driver type is not defined',
      16: 'Device not supported',
    },
  },

  smartboxConnectionManager: {
    connected: 'SmartBox connected',
    disconnected: 'SmartBox disconnected',
    codeSentToSmartbox: 'Source code has been sent to SmartBox',
  },

  popup: {
    button: 'OK',
    information: 'The lesson uses:',
    listEmptyMessage: 'This lesson does not use any sensors',
  },

  projects: {
    headerText: 'My Projects',
    status: 'Status: {{status}}',
    lastModification: 'Last modification: {{lastModification}}',

    button: {
      load: 'Load',
      delete: 'Delete',
    },
  },

  topic: {
    lessons: 'lessons',
  },

  languages: {
    en: 'English',
    pl: 'Polish',
    ar: 'Arabic',
  },

  editor: {
    projectSave: {
      title: 'Name the project before saving.',
      placeholder: 'Project_001',
      save: 'SAVE',
      exists:
        'Project with given name already exists. Do you want to overwrite it?',
      yes: 'YES',
      no: 'NO',
    },

    refresh: {
      title: 'Are you sure, that you want to clear all your progress?',
    },

    functionInfo: {
      parameters: 'Parameters: ',
      noParams: 'Takes no parameters',
      returns: 'Returns: ',
      example: 'Example:',
    },

    playCode: {
      error: 'Something is not okay...',
      success: 'Very well!',
      next: 'NEXT',
    },
    clear: 'CLEAR',
    send: 'Send...',
    error1: 'Select code that you want to remove',
    error2: 'Select place where you want to enter code',
    error3: 'You can not put another code here',
  },
};

export {en};
