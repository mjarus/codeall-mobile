const pl = {
  general: {
    unknownError: 'Nieznany błąd',
    networkError:
      'Do uruchomienia aplikacji CodeAll potrzebne jest połączenie internetowe',
    yes: 'Tak',
    no: 'Nie',
    ok: 'OK',
  },

  connection: {
    internet: {
      information: 'Jesteś offline! Sprawdź połączenie internetowe.',
    },
  },

  login: {
    header: 'Witaj ponownie!',
    subHeader: 'Zaloguj się, by kontynuować naukę.',
    password: 'Hasło:',
    logInButton: 'Zaloguj się',
    socialLogin: 'lub zaloguj się przez:',
    noAccount: 'Nie masz jeszcze konta? ',
    register: 'Zarejestruj się',
  },

  start: {
    hi: 'Cześć',

    header: {
      line1: 'Jesteś gotowy rozpocząć przygodę',
      line2: 'z programowaniem?',
    },
  },

  chatbot: {
    message: {
      suggestion: 'Twoja propozycja',
      thanks: 'Dziękuję za odpowiedź!',
    },

    inputPlaceholder: 'Wyślij wiadomość',
    naughtWords: 'Używasz bardzo brzydkich słów',
    noAnswear: 'Niestety nie wiem co odpowiedzieć...',
    somethingsWrong: 'Coś poszło nie tak... Otrzymałem taką informację: ',
    talkWithChatbot: 'Rozmowa z chatbotem',
    noMessages: 'Brak wiadomości',
  },

  leaderboard: {
    achievments: 'Osiągnięcia',
    ranking: 'Ranking',
  },

  register: {
    mail: {
      greeting: 'Cześć!',
      readyForAdventure:
        'Jesteś gotowy by rozpocząć przygodę z programowaniem?',
      email: 'E-mail:',
      next: 'Dalej',
      alternativeLogin: 'lub zaloguj się przez:',
      account: 'Masz już konto? ',
      login: 'Zaloguj się',
    },

    password: {
      createPassword: 'Utwórz hasło',
      lastStep:
        'To już ostatni krok formalności. Twoja przygoda zaraz się rozpocznie.',
      password: 'Hasło:',
      createAccount: 'Załóż konto',
      accountQuestion: 'Masz już konto? ',
      login: 'Zaloguj się',
    },

    refferalDescription: {
      line1: 'Jeśli otrzymałeś zaproszenie do aplikacji',
      line2: 'od znajomego, wprowadź tutaj kod',
      line3: 'referencyjny. Twój znajomy oraz Ty',
      line4: 'dostaniecie dodatkowe punkty.',
    },

    code: {
      error: 'nieprawidłowy kod',
    },

    placeholder: {
      referral: 'Np. ABCDEFGH',
    },

    setNick: 'Podaj swój nick',
    optional: 'opcjonalnie',
    setRefferal: 'Wpisz kod od znajomego',
  },

  userProfile: {
    account: 'Profil',
    achievements: 'Osiągnięcia',
    settings: 'Ustawienia',
    inviteButton: 'Zaproś użytkowników',
  },

  userSettings: {
    yourAccount: 'Ustawienia',
    name: 'Imię',
    surname: 'Nazwisko',
    email: 'Adres e-mail',
    password: 'Hasło',
    currentPassword: 'Aktualne hasło',
    newPassword: 'Nowe hasło',
    saveChanges: 'Zapisz',
    saveChangesError: 'Nie można zapisać zmian',
    logout: 'Wyloguj się',
    changePhoto: 'Zmień Zdjęcie',
    language: 'Język',
    setLock: 'Ustaw Blokadę',
  },

  userLock: {
    back: 'Wróć',
    header: 'Ustaw Blokadę',

    errors: {
      short: 'PIN musi mieć conajmniej 4 cyfry',
      notMatch: 'PINY nie są takie same',
    },

    labels: {
      currentPin: 'Obecny PIN',
      newPin: 'Nowy PIN',
      confirmPin: 'Potwierdź PIN',
    },
  },

  inviteFriendsBanner: {
    button: 'Zaproś użytkowników',

    description: {
      line1: 'Zaproś użytkowników',
      line2: 'i zdobądź darmowe czujniki!',
    },
  },

  inviteScreen: {
    header: 'Zaproś użytkowników i odbierz czujniki',

    description: {
      line1:
        'Jeśli inne osoby rozpoczną korzystanie z CodeAll dzięki Twojemu zaproszeniu, będziesz miał szansę otrzymać za darmo czujniki CodeAll.',
      line2:
        'Za każde 10 zaakceptowanych zaproszeń otrzymasz 1 czujnik do wyboru za darmo!',
    },

    shareButton: 'Udostępnij link z zaproszeniem',
    yourCode: 'Twój kod',

    statistics: {
      accountsCreated: 'Liczba utworzonych kont z zaproszeń',
      freeSensors: 'Liczba darmowych czujników',
      usedFreeSensors: 'Liczba wykorzystanych czujników',
    },

    copy: {
      notification: 'Skopiowano!',
    },
  },

  navigation: {
    tab1: 'Lekcje',
    tab2: 'Czujniki',
    tab3: 'Projekty',
    tab4: 'Profil',
    chatbot: 'Chatbot',
    editor: 'Edytor',
    console: 'Konsola',
  },

  lessons: {
    welcome: 'Cześć',
    learnNew: 'Porozmawiaj z chatbotem',
    chooseLesson: 'lub sam wybierz lekcję',
    topics: 'Tematy',
    lessons: 'Lekcje',
    lessonsPlural: 'lekcji',
    lessonsTotal: 'lekcji łącznie',
    section: 'Sekcja 1,',
    introduction: 'wprowadzenie',
  },

  flashMessage: {
    attention: 'Uwaga',
  },

  sensors: {
    nav: {
      connectedSensors: 'Czujniki',
      connectNewSensors: 'Podłącz',
      shop: 'Sklep',
    },

    sensor: {
      found: 'Czujniki sparowane z aplikacją:',
      name: 'Sensor',
      more: 'Więcej...',
      buy: 'Kup teraz',
      disconnect: 'Rozłącz',
      back: 'Cofnij',
      disconnectConfirmation:
        'Czy na pewno chcesz rozłączyć ten czujnik? Będziesz mógł go ponownie sparować w zakładce Podłącz',
      connect: 'Podłącz',
      unavailableDescription:
        'Ten czujnik został podłączony (sparowany) do Twojego SmartBoxa, ale obecnie nie jest widoczny w jego zasięgu. Być może jest za daleko albo rozładowała się bateria?',
      changeFrequencyDescription: 'Wprowadź częstotliwość odczytu danych: ',
      available: 'Dostępne czujniki',
      hardware: 'Specyfikacja urządzenia',

      attribute: {
        battery: 'Bateria: ',

        status: {
          name: 'Dostępność:',
          available: 'aktywny',
          unavailable: 'niedostępny',
        },

        name: 'Nazwa:',
        originalName: 'Nazwa fabryczna:',
        type: 'Typ:',
        id: 'Numer identyfikacyjny:',
        idShort: 'ID:',
        measurement: 'Wartość pomiaru: ',
        productionData: 'Dane produkcyjne:',
        powerSupply: 'Zasilacz',
        lifespan: 'Żywotność',
        accuracy: 'Dokładność',
        temperature: 'Temperatura',
        alarm: 'Dźwięk alarmu',
        dimensions: 'Wymiary',
        price: 'Cena',
        version: 'Numer SmartBox: ',
        micropythonVersion: 'Wersja Micropython: ',
        frequency: 'Częstotliwość',
        'frequency.change': 'Zmień',
      },
    },

    smartbox: {
      connected: 'Podłączono SmartBox!',

      description: {
        notConnected: {
          error: 'Brak SmartBoxa!',
          line1:
            'Podłącz SmartBox do telefonu, aby uzyskać dostęp do czujników CodeAll!',
        },
      },
    },

    connectionInformation: {
      line1:
        'Na tym ekranie zobaczysz czujniki, które będziesz mógł podłączyć (sparować) ze swoim SmartBoxem.',
      line2:
        'Korzystanie z czujnika jest możliwe dopiero po podłączeniu do SmartBoxa.',
      line3:
        "Aby zobaczyć czujnik na liście, zamontuj go na bazie, zbliż na odległość nie większą niż 30 cm i poczekaj do 10 sekund. Po kliknięciu 'Połącz' dioda na czujniku zamruga 3 razy. Będzie to sygnał, że łączenie przebiegło pomyślnie.",
    },

    noSensorsConnected:
      'Obecnie żadne czujniki nie są podłączone do Twojego SmartBoxa.',
    connectionInstructions:
      'Umieść czujniki, które chcesz sparować z aplikacją, w odległości do 30 cm od SmartBoxa',
    specification: 'Specyfikacja techniczna',
  },

  textButton: {
    edit: 'Edytuj',
    save: 'Zapisz'
  },

  payment: {
    payment: 'Płatność',
    googlePlay: 'Google Play',
    subscriptionInformation:
      'Mozesz w dowolnym momencie anulować subskrypcję obszarze Subskrypcje w Google Play',
    buy: 'Zapłać',
  },

  smartbox: {
    configuration: {
      information:
        'SmartBox został skonfigurowany z aplikacją. Podłącz odpowiedni czujnik, wgraj projekt i zobacz, jakie możliwości daje Internet Rzeczy!',
    },

    list: {
      sensors: 'Czujniki widoczne dla SmartBoxa:',
    },

    errors: {
      1: 'Urządzenie nie zostało znalezione!',
      2: 'Nazwa urządzenia nie może być nieprawidłowa lub pusta!',
      3: 'BaudRate nie może być nieprawidłowy!',
      4: 'Połączenie nieudane!!',
      5: 'Nie można otworzyć portu!',
      6: 'Nieudane rozłączenie!',
      7: 'Port jest już podłączony',
      8: 'Port jest już odłączony',
      9: 'Usługa USB nie została uruchomiona. Proszę najpierw uruchomić usługę USB!',
      10: 'Brak urządzenia o nazwie {Nazwa urządzenia}',
      11: 'Użytkownik nie zezwolił na połączenie',
      12: 'Obsługa nie mogła się zatrzymać. Najpierw zamknij połączenie',
      13: 'Brak połączenia',
      14: 'Błąd odczytu z portu',
      15: 'Typ sterownika nie jest zdefiniowany',
      16: 'Urządzenie nie jest wspierane',
    },
  },

  smartboxConnectionManager: {
    connected: 'Nawiązano połączenie ze SmartBoxem',
    disconnected: 'Utracono połączenie ze SmartBoxem',
    codeSentToSmartbox: 'Kod został przesłany na SmartBoxa',
  },

  popup: {
    button: 'OK',
    information: 'W lekcji wykorzystywane są:',
    listEmptyMessage: 'W tej lekcji nie są wykorzystywane czujniki.',
  },

  projects: {
    headerText: 'Moje Projekty',
    status: 'Status: {{status}}',
    lastModification: 'Ostatnia zmiana: {{lastModification}}',

    button: {
      load: 'Wczytaj',
      delete: 'Usuń',
    },
  },

  topic: {
    lessons: 'lekcji',
  },

  languages: {
    en: 'Angielski',
    pl: 'Polski',
    ar: 'Arabski',
  },

  editor: {
    projectSave: {
      title: 'Nazwij projekt przed zapisaniem.',
      placeholder: 'Projekt_001',
      save: 'ZAPISZ',
      exists: 'Projekt o tej nazwie już istnieje. Czy chcesz nadpisać projekt?',
      yes: 'TAK',
      no: 'NIE',
    },

    refresh: {
      title: 'Czy jesteś pewny, że chcesz wyczyścić cały swój postęp?',
    },

    functionInfo: {
      parameters: 'Parametry: ',
      noParams: 'Nie przyjmuje parametrów',
      returns: 'Zwraca: ',
      example: 'Przykład:',
    },

    playCode: {
      error: 'Coś nie zagrało...',
      success: 'Bardzo dobrze!',
      next: 'DALEJ',
    },
    clear: 'WYCZYŚĆ',
    send: 'Wyślij...',
    error1: 'Wybierz kod który chcesz usunąć',
    error2: 'Wybierz miejsce, w którym chcesz wstawić kod',
    error3: 'Tutaj nie można wstawić innego kodu',
  },
};

export {pl};
