import {RNCamera} from 'react-native-camera';

export const CameraType = {
  FRONT: RNCamera.Constants.Type.front,
  BACK: RNCamera.Constants.Type.back,
};
