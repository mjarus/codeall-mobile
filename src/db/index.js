import AsyncStorage from '@react-native-async-storage/async-storage';

export const LANGUAGE_KEY = '@language';

export const storeLanguageDB = async lang => {
  try {
    await AsyncStorage.setItem(LANGUAGE_KEY, lang);
  } catch (e) {
    console.log(e);
  }
};

export const getLanguageDB = async () => {
  try {
    const value = await AsyncStorage.getItem(LANGUAGE_KEY);
    return value;
  } catch (e) {
    console.log(e);
  }
};
