import React, {useState} from 'react';
import {Switch} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const SwitchButton = ({onPress}) => {
  const [switchValue, setSwitchValue] = useState(false);

  return (
    <Switch
      style={{transform: [{scaleX: 1.3}, {scaleY: 1.3}]}}
      trackColor={{true: COLORS.switch, false: COLORS.switchSecondary}}
      thumbColor={COLORS.background}
      onValueChange={setSwitchValue}
      value={switchValue}
      onPress={onPress}
    />
  );
};

export default SwitchButton;
