import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const container = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
};
const screen = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  right: 0,
  left: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.25)',
  elevation: 1,
  zIndex: 10000,
  alignItems: 'center',
  justifyContent: 'flex-end',
};

const box = {
  width: '100%',
  minHeight: 200,
  alignItems: 'center',
  justifyContent: 'center',
  borderTopLeftRadius: 4,
  borderTopRightRadius: 4,
  backgroundColor: COLORS.background,
  paddingTop: 35,
  paddingBottom: 31,
  height: 290,
};

export const styles = StyleSheet.create({
  container,
  box,
  screen,
});
