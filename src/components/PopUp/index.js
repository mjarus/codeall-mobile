import React from 'react';
import {styles} from './styles';
import {Modal, View, Pressable} from 'react-native';

const PopUp = ({onClose, visible, ...otherProps}) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      closeOnClick={true}
      visible={visible}
      onRequestClose={onClose}>
      <Pressable onPress={onClose} style={styles.container}>
        <View style={styles.screen}>
          <View style={styles.box}>{otherProps.children}</View>
        </View>
      </Pressable>
    </Modal>
  );
};

export default PopUp;
