import React, {useState, useRef} from 'react';
import {ScrollView, View} from 'react-native';

import styles from './styles';

const ScrollWithCustomIndicator = (props) => {
  const [contentOffset, setContentOffset] = useState({x: 0, y: 0});
  const scrollRef = useRef(null);
  const [contentSize, setContentSize] = useState(0);
  const [scrollViewHeight, setScrollViewHeight] = useState(0);

  const handleLayout = (e) => {
    setScrollViewHeight(e.nativeEvent.layout.height);
  };

  const handleContentSizeChange = (_, height) => {
    setContentSize(height);
  };

  const handleScroll = (e: any) => {
    setContentOffset(e.nativeEvent.contentOffset);
  };

  const barHeight = scrollViewHeight * (scrollViewHeight / contentSize);
  const scrollPosPercent = (contentOffset.y / contentSize) * 100;

  const shouldShowIndicator = contentSize > scrollViewHeight;

  return (
    <>
      <View style={styles.container(props.style)}>
        <ScrollView
          {...props}
          ref={scrollRef}
          scrollEventThrottle={16}
          showsHorizontalScrollIndicator
          disableScrollViewPanResponder
          showsVerticalScrollIndicator={false}
          bounces={false}
          automaticallyAdjustContentInsets={false}
          onLayout={handleLayout}
          onContentSizeChange={handleContentSizeChange}
          onScroll={handleScroll}>
          {props.children}
        </ScrollView>

        {shouldShowIndicator && (
          <View style={styles.indicatorContainer(scrollViewHeight)}>
            <View style={styles.indicatorBar(scrollPosPercent, barHeight)} />
          </View>
        )}
      </View>
    </>
  );
};

export default ScrollWithCustomIndicator;
