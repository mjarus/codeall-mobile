import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const container = (givenStyles) => ({
  ...givenStyles,
  flexDirection: 'row',
});

const indicatorContainer = (height) => ({
  height,
  width: 10,
  backgroundColor: '#E1E1E180',
});

const indicatorBar = (positionPercent, height) => ({
  position: 'absolute',
  top: `${positionPercent}%`,
  width: '100%',
  height: height,
  backgroundColor: COLORS.editorKeyboardSecondary,
  borderRadius: 15,
});

const styles = StyleSheet.create({container, indicatorContainer, indicatorBar});

export default styles;
