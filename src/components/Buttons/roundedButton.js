import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import TranslatedText from '../TranslatedText';
import typography from '../../theme/typography';

const RoundedButton = ({size, headerTextId, onPress, disabled}) => {
  return (
    <View style={styles.box(size)}>
      <View style={styles.boxShadow} />
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        disabled={disabled}>
        <TranslatedText id={headerTextId} style={{...typography.buttonsText}} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  boxShadow: {
    width: '100%',
    backgroundColor: COLORS.buttonShadow,
    position: 'absolute',
    top: 4,
    borderRadius: 30,
    minHeight: 57,
  },
  box: (size) => ({
    position: 'relative',
    width: size === 'big' ? 234 : 122,
  }),
  button: {
    width: '100%',
    backgroundColor: COLORS.goldButton,
    zIndex: 10,
    borderRadius: 30,
    minHeight: 57,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default RoundedButton;
