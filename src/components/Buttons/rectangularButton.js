import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';

import COLORS from 'CodeallMobile/src/theme/colors';
import TranslatedText from '../TranslatedText';
import typography from '../../theme/typography';

const RectangularButton = ({headerTextId, onPress, disabled}) => {
  return (
    <View style={styles.box}>
      <View style={styles.boxShadow} />
      <TouchableOpacity
        style={styles.button}
        onPress={onPress}
        disabled={disabled}>
        <TranslatedText id={headerTextId} style={{...typography.buttonsText}} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  boxShadow: {
    width: '100%',
    backgroundColor: COLORS.buttonShadow,
    position: 'absolute',
    top: 4,
    borderRadius: 4,
    minHeight: 57,
  },
  box: {
    position: 'relative',
    width: 139,
  },
  button: {
    width: '100%',
    backgroundColor: COLORS.goldButton,
    zIndex: 10,
    borderRadius: 4,
    minHeight: 57,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default RectangularButton;
