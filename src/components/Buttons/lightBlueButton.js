import React from 'react';
import {TouchableOpacity, View, Text, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';

const LightBlueButton = ({text, action, customStyles}) => {
  return (
    <View style={[styles.outerContainer, customStyles]}>
      <View style={[styles.container, styles.shadowBox]} />
      <TouchableOpacity
        style={styles.container}
        activeOpacity={0.8}
        onPress={action}>
        <Text style={styles.text}>{text}</Text>
      </TouchableOpacity>
    </View>
  );
};

export const styles = StyleSheet.create({
  container: {
    backgroundColor: COLORS.moreButton,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    width: 85,
    height: 35,
  },
  text: {
    fontSize: 10,
    fontFamily: typography.fonts.secondary,
    color: COLORS.moreButtonLabel,
    marginBottom: 3,
  },
  outerContainer: {
    position: 'relative',
  },
  shadowBox: {
    position: 'absolute',
    top: 4,
    backgroundColor: COLORS.buttonShadow,
  },
});

export default LightBlueButton;
