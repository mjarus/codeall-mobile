import React from 'react';
import {View, TouchableOpacity, Image, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const arrow = require('../../assets/img/arrowDown.png');

const CircleButton = ({onPress}) => {
  return (
    <View style={styles.button}>
      <View style={styles.shadowBox} />
      <TouchableOpacity style={styles.circle} onPress={onPress}>
        <Image style={styles.arrow} source={arrow} />
      </TouchableOpacity>
    </View>
  );
};

const styles = StyleSheet.create({
  arrow: {
    width: 30.21,
    height: 17.8,
  },
  button: {
    width: 50,
    height: 50,
    borderRadius: 100,
    position: 'relative',
    marginTop: 52,
  },
  circle: {
    width: '100%',
    height: '100%',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 100,
    backgroundColor: COLORS.goldButton,
    position: 'absolute',
  },
  shadowBox: {
    width: '100%',
    height: '100%',
    position: 'absolute',
    top: 4,
    borderRadius: 100,
    backgroundColor: COLORS.buttonShadow,
  },
});

export default CircleButton;
