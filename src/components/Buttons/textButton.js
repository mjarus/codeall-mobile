import React from 'react';
import {TouchableOpacity, Text, StyleSheet} from 'react-native';
import typography from '../../theme/typography';

const textButton = ({text, action, color}) => {
  return (
    <TouchableOpacity onPress={action}>
      <Text style={styles({color}).textStyle}>{text}</Text>
    </TouchableOpacity>
  );
};

const styles = ({color}) =>
  StyleSheet.create({
    textStyle: {
      fontFamily: typography.fonts.primary,
      fontSize: typography.sizes.xxs,
      lineHeight: 15,
      marginTop: 3,
      marginBottom: 3,
      textDecorationLine: 'underline',
      color: color,
    },
  });

export default textButton;
