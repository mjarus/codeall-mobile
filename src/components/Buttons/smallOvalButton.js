import React from 'react';
import {View, TouchableOpacity, StyleSheet} from 'react-native';
import TranslatedText from '../TranslatedText';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';

const SmallOvalButton = ({type, headerTextId, onPress}) => {
  return (
    <View style={styles.outerContainer}>
      <View style={[styles.container(), styles.shadowBox]} />
      <TouchableOpacity
        style={styles.container(type)}
        activeOpacity={0.8}
        onPress={onPress}>
        <TranslatedText id={headerTextId} style={styles.text(type)} />
      </TouchableOpacity>
    </View>
  );
};

export const styles = StyleSheet.create({
  container: (type) => ({
    backgroundColor: type === 'PRIMARY' ? COLORS.background : COLORS.loadButton,
    borderRadius: 18,
    alignItems: 'center',
    justifyContent: 'center',
    width: 85,
    height: 35,
  }),
  text: (type) => ({
    fontSize: 11,
    fontFamily: typography.fonts.secondaryRegular,
    color: type === 'PRIMARY' ? COLORS.deleteButton : COLORS.buttonDefaultText,
    textTransform: 'uppercase',
    fontWeight: 'bold',
  }),
  outerContainer: {
    position: 'relative',
  },
  shadowBox: {
    position: 'absolute',
    top: 4,
    backgroundColor: COLORS.buttonShadow,
  },
});

export default SmallOvalButton;
