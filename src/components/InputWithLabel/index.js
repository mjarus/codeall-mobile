import React, {useState, useEffect} from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  Image,
  TouchableOpacity,
  I18nManager,
} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';
import i18n, {SUPPORTED_LANGUAGES} from 'CodeallMobile/src/i18n';
import TranslatedText from 'CodeallMobile/src/components/TranslatedText';
import {useTranslation} from 'react-i18next';
import {storeLanguageDB} from 'CodeallMobile/src/db';
import RNRestart from 'react-native-restart';

const inputRectangle = require('CodeallMobile/src/assets/img/inputRectangle.png');

const LTR_LANGUAGES = SUPPORTED_LANGUAGES.filter((lang) => lang !== 'ar');

const InputWithLabel = ({
  labelId,
  onChange,
  defaultValue,
  isSelect,
  isSave,
  placeholder,
}) => {
  const [currLanguage, setCurrLanguage] = useState(i18n.language);
  const [isDropdown, setIsDropdown] = useState(false);
  const {t} = useTranslation();

  const language = SUPPORTED_LANGUAGES.find((lang) => lang === currLanguage);

  useEffect(() => {
    if (currLanguage !== i18n.language && isSave) {
      if (
        LTR_LANGUAGES.includes(currLanguage) &&
        LTR_LANGUAGES.includes(i18n.language)
      ) {
        i18n.changeLanguage(currLanguage);
        storeLanguageDB(currLanguage);
      } else {
        i18n.changeLanguage(currLanguage).then(() => {
          I18nManager.forceRTL(i18n.language === 'ar');
          storeLanguageDB(currLanguage);
          RNRestart.Restart();
        });
      }
    }
  }, [isSave]);

  const handleLanguageChange = (lang) => {
    setIsDropdown(false);
    setCurrLanguage(lang);
  };

  const LanguageList = () => (
    <View style={styles.languageList}>
      {SUPPORTED_LANGUAGES.map((lang) => (
        <TouchableOpacity onPress={() => handleLanguageChange(lang)} key={lang}>
          <TextInput
            style={{...styles.input, ...styles.languageInput}}
            editable={false}
            defaultValue={t(`languages.${lang}`)}
          />
        </TouchableOpacity>
      ))}
    </View>
  );

  return (
    <View style={styles.container}>
      <TranslatedText style={typography.inputLabel} id={labelId} />
      {isSelect ? (
        <>
          {isDropdown ? (
            LanguageList()
          ) : (
            <TouchableOpacity onPress={() => setIsDropdown(true)}>
              <TextInput
                style={styles.input}
                defaultValue={t(`languages.${language}`)}
                editable={false}
              />
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={styles.inputRectangle(isDropdown)}
            onPress={() => setIsDropdown((prev) => !prev)}>
            <Image source={inputRectangle} style={styles.rectangleIcon} />
          </TouchableOpacity>
        </>
      ) : (
        <TextInput
          style={styles.input}
          onChangeText={onChange}
          defaultValue={defaultValue}
          placeholder={t(placeholder)}
          placeholderTextColor={COLORS.inputContent}
        />
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    width: 256,
    marginVertical: 5,
  },

  input: {
    width: 243,
    fontSize: typography.sizes.s,
    padding: 0,
    fontFamily: typography.primary,
    color: COLORS.inputContent,
    backgroundColor: COLORS.background,
    borderRadius: 4,
    height: 32,
    paddingLeft: 12.15,
  },

  selectText: {
    fontSize: typography.sizes.s,
    color: COLORS.inputContent,
    paddingBottom: 2,
  },

  inputRectangle: (isDropdown) => ({
    position: 'absolute',
    right: 22,
    top: 46,
    transform: isDropdown ? [{rotate: '180deg'}] : [{rotate: '0deg'}],
  }),

  languageList: {
    flexDirection: 'column',
  },

  languageInput: {
    marginBottom: 10,
  },

  rectangleIcon: {
    width: 14,
    height: 9,
  },
});

export default InputWithLabel;
