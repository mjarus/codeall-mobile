import React from 'react';
import {Image, View, StyleSheet} from 'react-native';
import {useNavigation, useTheme} from '@react-navigation/native';
import {RoundedButton} from '../Buttons';
import colors from '../../theme/colors';
import TranslatedText from '../TranslatedText';

const giftImage = require('../../assets/img/gift2.png');

const FriendsInvitationBanner = () => {
  const navigation = useNavigation();
  const {typography} = useTheme();

  return (
    <View style={styles.mainContainer}>
      <Image source={giftImage} style={styles.image} />
      <View style={styles.contentContainer}>
        <View style={styles.textContainer}>
          <TranslatedText
            id={'inviteFriendsBanner.description.line1'}
            style={typography.invitationText}
          />
          <TranslatedText
            id={'inviteFriendsBanner.description.line2'}
            style={typography.invitationText}
          />
        </View>
        <RoundedButton
          headerTextId={'inviteFriendsBanner.button'}
          size={'big'}
          onPress={() => navigation.navigate('InviteScreen')}
        />
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  mainContainer: {
    alignItems: 'center',
    justifyContent: 'space-evenly',
    width: '100%',
    backgroundColor: colors.background,
    paddingTop: 10,
    paddingBottom: 20,
  },

  textContainer: {
    alignItems: 'center',
    marginBottom: 10,
  },

  image: {
    width: 67,
    height: 67,
    resizeMode: 'contain',
    marginBottom: 12,
  },
});
export default FriendsInvitationBanner;
