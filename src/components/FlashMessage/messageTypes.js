import {Dimensions} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';

const SCREEN_HEIGHT = Dimensions.get('window').height;

export const FLASH_MESSAGE_TYPES = {
  CUSTOM: 'CUSTOM',
  EDITOR_INFO: 'EDITOR_INFO',
  EDITOR_ERROR: 'EDITOR_ERROR',
  SMART_BOX_INFO: 'SMART_BOX_INFO',
  SMART_BOX_ERROR: 'SMART_BOX_ERROR',
};

export const FLASH_MESSAGE_PARAMS = {
  [FLASH_MESSAGE_TYPES.CUSTOM]: {
    top: 100,
    color: COLORS.popupText,
    backgroundColor: COLORS.background,
  },
  [FLASH_MESSAGE_TYPES.EDITOR_INFO]: {
    top: 100,
    color: 'black',
    backgroundColor: '#ffffffcc',
  },
  [FLASH_MESSAGE_TYPES.EDITOR_ERROR]: {
    top: 100,
    color: 'white',
    backgroundColor: '#ff0000cc',
  },
  [FLASH_MESSAGE_TYPES.SMART_BOX_INFO]: {
    top: 100,
    color: COLORS.popupText,
    backgroundColor: COLORS.background,
  },
  [FLASH_MESSAGE_TYPES.SMART_BOX_ERROR]: {
    top: 100,
    color: COLORS.popupText,
    backgroundColor: COLORS.background,
  },
};
