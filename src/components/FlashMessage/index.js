import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {connect} from 'react-redux';
import FadeView from '../FadeView';
import {FLASH_MESSAGE_TYPES, FLASH_MESSAGE_PARAMS} from './messageTypes';
import {styles} from './styles';
import TranslatedText from 'CodeallMobile/src/components/TranslatedText';

class FlashMessage extends Component {
  constructor(props) {
    super(props);
    this.flashMessageTimer = null;
    this.state = {
      flashMessageVisible: false,
    };
  }

  componentDidUpdate = (prevProps) => {
    if (prevProps.lastFlashMessageTime !== this.props.lastFlashMessageTime) {
      this.showFlashMessage();
    }
  };

  showFlashMessage = () => {
    clearTimeout(this.flashMessageTimer);
    this.setState({flashMessageVisible: true});
    this.flashMessageTimer = setTimeout(
      () => this.setState({flashMessageVisible: false}),
      3000,
    );
  };

  componentWillUnmount = () => {
    clearTimeout(this.flashMessageTimer);
  };

  render() {
    const type = FLASH_MESSAGE_TYPES.hasOwnProperty(this.props.flashMessageType)
      ? this.props.flashMessageType
      : FLASH_MESSAGE_TYPES.CUSTOM;

    if (this.props.flashMessageText) {
      return (
        <FadeView
          active={!this.state.flashMessageVisible}
          style={styles.fadeContainer(FLASH_MESSAGE_PARAMS[type].top)}
          pointerEvents={'none'}>
          <View style={styles.box}>
            <View style={styles.boxShadow} />
            <View
              style={styles.alertContainer(
                FLASH_MESSAGE_PARAMS[type].backgroundColor,
              )}>
              <TranslatedText
                style={styles.title}
                id={'flashMessage.attention'}
              />
              <View style={styles.separator} />
              <Text style={styles.alertText(FLASH_MESSAGE_PARAMS[type].color)}>
                {this.props.flashMessageText}
              </Text>
            </View>
          </View>
        </FadeView>
      );
    }

    return null;
  }
}

const mapStateToProps = (state) => ({
  flashMessageText: state.editor.flashMessageText,
  flashMessageType: state.editor.flashMessageType,
  lastFlashMessageTime: state.editor.lastFlashMessageTime,
});

export default connect(mapStateToProps)(FlashMessage);
