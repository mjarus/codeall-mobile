import {StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';

const fadeContainer = (top) => ({
  position: 'absolute',
  top,
  left: 20,
  right: 20,
  zIndex: 1000,
  alignItems: 'center',
  justifyContent: 'center',
});

const alertContainer = (backgroundColor) => ({
  paddingHorizontal: 20,
  paddingVertical: 14,
  borderRadius: 4,
  backgroundColor,
  width: 338,
  height: 125,
  alignItems: 'center',
  justifyContent: 'center',
});

const box = {
  position: 'relative',
  alignItems: 'center',
  justifyContent: 'center',
};

const boxShadow = {
  width: 338,
  height: 125,
  position: 'absolute',
  top: 4,
  borderRadius: 4,
  backgroundColor: COLORS.buttonShadow,
};

const alertText = (color) => ({
  textAlign: 'center',
  fontSize: 14,
  color,
});

const separator = {
  width: 320,
  height: 1,
  backgroundColor: COLORS.separator,
  marginTop: 10,
  marginBottom: 10,
};

const title = {
  fontSize: 14,
  fontFamily: typography.primary,
};

export const styles = StyleSheet.create({
  fadeContainer,
  alertContainer,
  alertText,
  box,
  boxShadow,
  separator,
  title,
});
