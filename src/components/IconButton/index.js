import React from 'react';
import {Image, Pressable, StyleSheet} from 'react-native';

const IconButton = ({style, imageStyle, onPress, source, disabled}) => {
  return (
    <Pressable
      onPress={onPress}
      disabled={disabled}
      style={[styles.container, style]}>
      <Image style={imageStyle} source={source} />
    </Pressable>
  );
};

const styles = StyleSheet.create({
  container: {
    alignContent: 'center',
  },
});

export default IconButton;
