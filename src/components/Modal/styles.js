import {StyleSheet} from 'react-native';

const container = {
  flex: 1,
  justifyContent: 'center',
  alignItems: 'center',
};
const screen = {
  position: 'absolute',
  top: 0,
  bottom: 0,
  right: 0,
  left: 0,
  backgroundColor: 'rgba(0, 0, 0, 0.25)',
  elevation: 1,
  zIndex: 10000,
  alignItems: 'center',
  justifyContent: 'flex-end',
};

const box = {
  width: '100%',
  minHeight: 120,
  alignItems: 'center',
  justifyContent: 'center',
};

export const styles = StyleSheet.create({
  container,
  box,
  screen,
});
