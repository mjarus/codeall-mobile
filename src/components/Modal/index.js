import React from 'react';
import {styles} from './styles';
import {Modal, View} from 'react-native';

const PopUp = ({onClose, visible, ...otherProps}) => {
  return (
    <Modal
      animationType={'slide'}
      transparent={true}
      closeOnClick={true}
      visible={visible}
      onRequestClose={onClose}>
      <View style={styles.container}>
        <View style={styles.screen}>
          <View style={styles.box}>{otherProps.children}</View>
        </View>
      </View>
    </Modal>
  );
};

export default PopUp;
