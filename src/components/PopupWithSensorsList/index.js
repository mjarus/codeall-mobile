import React from 'react';
import PopUp from '../PopUp';
import {Text, StyleSheet, FlatList, View} from 'react-native';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../TranslatedText';
import COLORS from 'CodeallMobile/src/theme/colors';
import {RectangularButton} from '../Buttons';

const PopupWithSensorsList = ({onClose, onPress, visible, lesson}) => {
  const {typography} = useTheme();

  const _renderItem = ({item}) => (
    <Text style={typography.popupText}>{item.name}</Text>
  );

  return (
    <PopUp onClose={onClose} visible={visible}>
      <View style={styles.divider} />
      {lesson.sensors?.length ? (
        <TranslatedText id={'popup.information'} style={typography.popupText} />
      ) : (
        <TranslatedText
          id={'popup.listEmptyMessage'}
          style={typography.popupText}
        />
      )}
      <FlatList
        data={lesson.sensors}
        keyExtractor={(item) => item.name}
        renderItem={_renderItem}
        style={styles.list}
      />
      <RectangularButton
        headerTextId={'popup.button'}
        onPress={() => {
          onPress();
          onClose();
        }}
      />
    </PopUp>
  );
};

const styles = StyleSheet.create({
  divider: {
    backgroundColor: COLORS.popupSeperator,
    width: 50,
    height: 3,
    marginBottom: 52,
  },
  list: {
    flexGrow: 0,
    marginBottom: 38,
  },
});

export default PopupWithSensorsList;
