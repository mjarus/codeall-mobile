import React from 'react';
import {Image, StyleSheet, Text, View} from 'react-native';

import TranslatedText from 'CodeallMobile/src/components/TranslatedText';

const logoCodeAll = require('../../assets/img/logo_codeall.png');

import {useTheme} from '@react-navigation/native';

const NETWORK_ERROR_MESSAGE = 'Network request failed';

const SplashLogo = ({error}) => {
  const theme = useTheme();
  const style = styles(theme);

  const errorMessage =
    error?.userMessage || error?.message.includes(NETWORK_ERROR_MESSAGE) ? (
      <TranslatedText id="general.networkError" style={style.errorMessage} />
    ) : (
      error?.message
    );

  return (
    <View style={theme.base}>
      <Image style={theme.images.logotype.x} source={logoCodeAll} />
      <Text style={style.errorMessage}>{errorMessage}</Text>
    </View>
  );
};

const styles = (theme) =>
  StyleSheet.create({
    errorMessage: {
      color: theme.colors.error,
      textAlign: 'center',
    },
  });

export default SplashLogo;
