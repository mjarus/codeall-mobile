import React from 'react';
import {TouchableOpacity} from 'react-native';
import {useTheme} from '@react-navigation/native';
import TranslatedText from '../TranslatedText';

const TouchableText = ({onPress, textId, style}) => {
  const {typography} = useTheme();

  onPress = onPress ?? (() => {});
  return (
    <TouchableOpacity onPress={onPress}>
      <TranslatedText
        style={[typography.sensorTouchableText, style]}
        id={textId}
      />
    </TouchableOpacity>
  );
};

export default TouchableText;
