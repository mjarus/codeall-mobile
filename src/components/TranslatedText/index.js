import React from 'react';
import {useTranslation} from 'react-i18next';
import {Text} from 'react-native';

const TranslatedText = ({style, id}) => {
  const {t} = useTranslation();

  return <Text style={style}>{t(id)}</Text>;
};

export default TranslatedText;
