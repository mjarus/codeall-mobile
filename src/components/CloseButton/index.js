import React from 'react';
import {TouchableOpacity, Image, View, StyleSheet} from 'react-native';

const closeIcon = require('../../assets/img/turquiseCloseIcon.png');

const CloseButton = ({onPress}) => {
  return (
    <View style={styles.closeBar}>
      <TouchableOpacity style={styles.button} onPress={onPress}>
        <Image source={closeIcon} style={styles.icon} />
      </TouchableOpacity>
    </View>
  );
};
const styles = StyleSheet.create({
  button: {
    marginRight: 17,
  },
  icon: {
    width: 16.6,
    height: 15.6,
  },

  closeBar: {
    width: '100%',
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
});
export default CloseButton;
