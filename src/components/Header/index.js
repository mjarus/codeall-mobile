import React from 'react';
import {useTranslation} from 'react-i18next';
import {View, Pressable, Text, StyleSheet, Image} from 'react-native';
import typography from 'CodeallMobile/src/theme/typography';

const Header = ({
  title,
  leftIcon,
  rightIcon,
  onLeftPress,
  onRightPress,
  style,
}) => {
  const {t} = useTranslation();

  return (
    <View style={styles.headerRow}>
      <Pressable onPress={onLeftPress} style={styles.headerIcons}>
        {leftIcon && <Image source={leftIcon} style={styles.icon} />}
      </Pressable>
      <Text style={[styles.header, style]}>{t(title)}</Text>
      <Pressable onPress={onRightPress} style={styles.headerIcons}>
        {rightIcon && <Image source={rightIcon} style={styles.icon} />}
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  headerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: '100%',
    height: 56,
    paddingHorizontal: 8,
    marginBottom: 8,
  },

  header: {
    flexShrink: 1,
    textAlign: 'center',
    fontSize: typography.sizes.m,
    fontFamily: typography.fonts.secondary,
  },

  headerIcons: {
    width: 32,
    height: 32,
    padding: 5,
  },

  icon: {
    width: '100%',
    height: '100%',
  },
});

export default Header;
