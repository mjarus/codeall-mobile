import React from 'react';
import {View, Image, StyleSheet} from 'react-native';
const lightBulbOff = require('../../assets/img/lightBulbOff.png');
const lightBulbOn = require('../../assets/img/lightBulbOn.png');

const BulbStatus = ({batteryStatusPercentage}) => {
  if (!batteryStatusPercentage) {
    return null;
  }
  let image = lightBulbOn;
  if (batteryStatusPercentage < 90) {
    image = lightBulbOff;
  }

  return (
    <View style={styles.container}>
      <Image style={styles.image} source={image} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 25,
    marginRight: 10,
    marginLeft: 5,
    resizeMode: 'contain',
  },
});

export default BulbStatus;
