import React from 'react';
import {TouchableOpacity, Text} from 'react-native';
import typography from '../../theme/typography';

const TurquoiseHyperlink = ({text, action}) => {
  return (
    <TouchableOpacity onPress={action}>
      <Text style={typography.turquoiseLink}>{text}</Text>
    </TouchableOpacity>
  );
};

export default TurquoiseHyperlink;
