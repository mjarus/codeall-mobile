import React, {useRef, useState} from 'react';
import {TextInput, View, StyleSheet, Platform} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from 'CodeallMobile/src/theme/typography';
// import TextButton from '../Buttons/textButton';
import {useTranslation} from 'react-i18next';

const Input = ({onChangeText, defaultValue, style, width}) => {
  const [focus, setFocus] = useState(false);
  const [isEditable, setIsEditable] = useState(true);
  // const editableInput = useRef()
  const {t} = useTranslation();

  const onFocus = () => {
    setFocus(!focus);
  };

  // COMMENTED LINES INCLUDE INPUT VERSION WITH EDIT/SAVE BUTTON

  // const toggleEditing = () => {
  //   setIsEditable(!isEditable)
  //   editableInput.current.focus()
  // }

  // const inputButtonText = isEditable ? t('textButton.save') : t('textButton.edit')

  return (
    <View
      style={{
        ...styles.inputBox(focus, width),
        ...style,
      }}>
      <TextInput
        editable={isEditable}
        style={[styles.inputText, typography.inputText]}
        onFocus={onFocus}
        onBlur={onFocus}
        onChangeText={onChangeText}
        defaultValue={defaultValue}
        // ref={editableInput}
      />
      {/* <View style={styles.hideBox}>
          <TextButton
            color={COLORS.textButton}
            style={styles.hideButton}
            text={inputButtonText}
            action={toggleEditing}/>
        </View> */}
    </View>
  );
};
const styles = StyleSheet.create({
  inputBox: (isFocus, isWidth) => ({
    flexDirection: 'column',
    borderWidth: isFocus ? 2 : 1,
    borderColor: COLORS.inputBorder,
    borderRadius: 4,
    width: isWidth ? isWidth : 243,
    height: 36,
    marginBottom: 14,
    fontSize: 20,
    ...Platform.select({
      ios: {
        paddingLeft: 8,
      },
      android: {
        paddingHorizontal: 5,
        paddingTop: 3,
      },
    }),
    lineHeight: 20,
    letterSpacing: 0.3,
    color: COLORS.spaceGrey,
    textAlign: 'left',
  }),

  inputText: {
    height: 38,
    width: '100%',
  },

  hideButton: {
    width: 22,
    height: 22,
    tintColor: COLORS.darkGrey,
  },

  hideBox: {
    position: 'absolute',
    right: 8,
    top: 6,
  },
});
export default Input;
