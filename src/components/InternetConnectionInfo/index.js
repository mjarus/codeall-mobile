import NetInfo from '@react-native-community/netinfo';
import React, {useEffect, useState} from 'react';
import {View, StyleSheet, SafeAreaView} from 'react-native';
import colors from '../../theme/colors';
import TranslatedText from '../TranslatedText';

const InternetConnectionInfo = () => {
  const [isOffline, setOfflineStatus] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
    return () => removeNetInfoSubscription();
  }, []);

  const NoInternetModal = ({show}) => {
    return (
      show && (
        <View style={styles.content}>
          <TranslatedText
            id={'connection.internet.information'}
            style={styles.contentText}
          />
        </View>
      )
    );
  };

  return (
    <SafeAreaView>
      <NoInternetModal show={isOffline} />
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  content: {
    alignItems: 'center',
    backgroundColor: colors.error,
    padding: 10,
    width: '100%',
  },
  contentText: {
    color: colors.connectionInternet,
    textAlign: 'center',
    fontSize: 15,
  },
});

export default InternetConnectionInfo;
