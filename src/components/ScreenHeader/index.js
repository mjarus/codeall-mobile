import React from 'react';
import {View, Text, StyleSheet} from 'react-native';
import COLORS from 'CodeallMobile/src/theme/colors';
import typography from '../../theme/typography';
import TranslatedText from '../TranslatedText';

const ScreenHeader = ({id, containerStyles, textStyles, children}) => {
  return (
    <View style={{...styles.headerContainer, ...containerStyles}}>
      <Text style={{...styles.headerText, ...textStyles}}>
        <TranslatedText id={id} />
      </Text>
      {children}
    </View>
  );
};

const styles = StyleSheet.create({
  headerContainer: {
    width: '70%',
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    alignSelf: 'center',
  },

  headerText: {
    fontSize: 22,
    fontFamily: typography.fonts.secondary,
    color: COLORS.mainText,
  },
});

export default ScreenHeader;
