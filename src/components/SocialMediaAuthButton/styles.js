import {StyleSheet} from 'react-native';

const icon = {
  width: 40,
  height: 40,
  marginLeft: 12,
  marginRight: 12,
};

export const styles = StyleSheet.create({
  icon,
});
