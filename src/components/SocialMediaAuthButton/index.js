import {Image, TouchableOpacity} from 'react-native';
import React from 'react';
import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {userFacebookLogin} from '../../store/auth/actions';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

const IconGoogle = require('../../../src/assets/img/google.png');
const IconFacebook = require('../../../src/assets/img/facebook.png');

const SocialMediaAuthButton = (props) => {
  const fbAuth = () => {
    LoginManager.logInWithPermissions([
      'public_profile',
      'email',
      'user_friends',
    ])
      .then((result) => {
        if (result.isCancelled) {
          props.onAuthCancelled();
        } else {
          AccessToken.getCurrentAccessToken().then((user) => {
            props
              .userFacebookLogin({
                token: user.accessToken,
              })
              .then(props.onAuthSuccess)
              .catch(props.onAuthError);
          });
        }
      })
      .catch(props.onAuthError);
  };

  // TODO google auth implementation
  const googleAuth = () => {
    props.onAuthError(new Error('Not implemented'));
  };

  let iconSource = null;
  let handleButtonPress = () => {};
  switch (props.type) {
    case 'facebook':
      iconSource = IconFacebook;
      handleButtonPress = fbAuth;
      break;
    case 'google':
      iconSource = IconGoogle;
      handleButtonPress = googleAuth;
      break;
  }

  return (
    <TouchableOpacity onPress={handleButtonPress}>
      <Image source={iconSource} />
    </TouchableOpacity>
  );
};

const mapDispatchToProps = (dispatch) =>
  bindActionCreators({userFacebookLogin}, dispatch);

export default connect(null, mapDispatchToProps)(SocialMediaAuthButton);
