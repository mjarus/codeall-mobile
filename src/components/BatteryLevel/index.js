import React from 'react';
import {View, Image, StyleSheet} from 'react-native';

const batteryNotCharged = require('../../assets/img/battery/batteryNotCharged.png');
const batteryOneLine = require('../../assets/img/battery/batteryOneLine.png');
const batteryTwoLines = require('../../assets/img/battery/batteryTwoLines.png');
const batteryThreeLines = require('../../assets/img/battery/batteryThreeLines.png');
const batteryCharged = require('../../assets/img/battery/batteryCharged.png');

const BatteryStatus = ({batteryStatusPercentage, style}) => {
  if (!batteryStatusPercentage) {
    return null;
  }

  let image = batteryCharged;
  if (batteryStatusPercentage < 100 && batteryStatusPercentage >= 75) {
    image = batteryCharged;
  } else if (batteryStatusPercentage < 75 && batteryStatusPercentage >= 50) {
    image = batteryThreeLines;
  } else if (batteryStatusPercentage < 50 && batteryStatusPercentage >= 25) {
    image = batteryTwoLines;
  } else if (batteryStatusPercentage < 25 && batteryStatusPercentage >= 5) {
    image = batteryOneLine;
  } else if (batteryStatusPercentage < 5) {
    image = batteryNotCharged;
  }

  return (
    <View style={[styles.container, style]}>
      <Image style={styles.image} source={image} />
    </View>
  );
};
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 25,
    marginRight: 10,
    marginLeft: 5,
    resizeMode: 'contain',
  },
});

export default BatteryStatus;
