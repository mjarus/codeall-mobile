import React, {useState, useEffect} from 'react';
import {StatusBar} from 'react-native';

import NetInfo from '@react-native-community/netinfo';

import colors from 'CodeallMobile/src/theme/colors';

const NetworkAwareStatusBar = (props) => {
  const [isDeviceOffline, setIsDeviceOffline] = useState(false);

  useEffect(() => {
    const removeNetInfoSubscription = NetInfo.addEventListener((state) => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setIsDeviceOffline(offline);
    });
    return () => removeNetInfoSubscription();
  }, []);

  const backgroundColor = isDeviceOffline
    ? colors.error
    : props.backgroundColor;
  const barStyle = isDeviceOffline ? 'light-content' : props.barStyle;

  return (
    <StatusBar
      {...props}
      backgroundColor={backgroundColor}
      barStyle={barStyle}
    />
  );
};

export default NetworkAwareStatusBar;
