export const temperatureSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik temperatury',
  price: {EUR: 10, PLN: 40},
  points: 10,
  type: 'temperature',
  isConnected: true,
};

export const distanceSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik dystansu',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'distance',
  isConnected: false,
};

export const accelerometerSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik przyspieszenia',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'accelerometer',
  isConnected: false,
};

export const baseSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik podstawowy',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'base',
  isConnected: false,
};

export const buttonsSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik przycisku',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'buttons',
  isConnected: false,
};

export const gyroscopeSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik orientacji',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'gyroscope',
  isConnected: false,
};

export const lightSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik światła',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'light',
  isConnected: false,
};

export const microphoneSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik głosu',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'microphone',
  isConnected: false,
};

export const sliderSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik slajdera',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'slider',
  isConnected: false,
};

export const vibrationSensor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik wibracji',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'vibration',
  isConnected: false,
};

export const smartbox = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'SmartBox',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'smartbox',
  isConnected: false,
};

export const humidity = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik wilgoci',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'humidity',
  isConnected: false,
};

export const stepperMotor = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Silnik krokowy',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'stepperMotor',
  isConnected: false,
};

export const rgbwLight = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik światła RGBW',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'rgbwLight',
  isConnected: false,
};

export const speaker = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik głośnika',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'speaker',
  isConnected: false,
};

export const door = {
  description: 'Description',
  id: 1,
  images: [],
  isPointsExchange: false,
  mainImage: {},
  name: 'Czujnik drzwi',
  price: {EUR: 20, PLN: 80},
  points: 10,
  type: 'door',
  isConnected: false,
};

export const sensorsList = [
  temperatureSensor,
  distanceSensor,
  accelerometerSensor,
  baseSensor,
  buttonsSensor,
  gyroscopeSensor,
  lightSensor,
  microphoneSensor,
  sliderSensor,
  vibrationSensor,
  smartbox,
  humidity,
  stepperMotor,
  rgbwLight,
  speaker,
  door,
];
