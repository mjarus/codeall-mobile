export const projectsMock = [
  {
    id: '1',
    title: 'File Name #1',
    status: 'last loaded',
    updatedAt: '16.01.2020',
  },
  {
    id: '2',
    title: 'File Name #2',
    status: 'last loaded',
    updatedAt: '16.01.2020',
  },
  {
    id: '3',
    title: 'File Name #3',
    status: 'last loaded',
    updatedAt: '16.01.2020',
  },
];
