import {useEffect} from 'react';
import {getLanguageDB} from '../db';
import i18n from '../i18n';

const usePreviouslySelectedLanguage = () => {
  useEffect(() => {
    const getLanguage = async () => {
      const language = await getLanguageDB();
      if (!language) {
        return;
      }

      i18n.changeLanguage(language);
    };

    getLanguage();
  }, []);
};

export default usePreviouslySelectedLanguage;
