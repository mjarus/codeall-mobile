import {useRef} from 'react';
import {useContext, useEffect} from 'react';
import {
  gyroscope,
  accelerometer,
  magnetometer,
  setUpdateIntervalForType,
  SensorTypes,
} from 'react-native-sensors';
import {SmartBoxContext} from './SmartBoxProvider';

const STD_GRAVITY_THRESHOLD = 9;

const PhoneSensors = () => {
  const isFaceDown = useRef(false);
  const smartBox = useContext(SmartBoxContext);

  const send = (type, data) => {
    smartBox.sendMessage(type, data);
  };

  const handleScreenPositionChange = z => {
    if (z < -STD_GRAVITY_THRESHOLD && !isFaceDown.current) {
      isFaceDown.current = true;
      smartBox.sendMessage('screenPositionChange', {
        isFaceDown: isFaceDown.current,
      });
    } else if (z > STD_GRAVITY_THRESHOLD && isFaceDown.current) {
      isFaceDown.current = false;
      smartBox.sendMessage('screenPositionChange', {
        isFaceDown: isFaceDown.current,
      });
    }
  };

  setUpdateIntervalForType(SensorTypes.accelerometer, 1000);
  setUpdateIntervalForType(SensorTypes.gyroscope, 1000);
  setUpdateIntervalForType(SensorTypes.magnetometer, 1000);

  useEffect(() => {
    const subscriptionAccelerometer = accelerometer.subscribe(
      data => {
        handleScreenPositionChange(data.z);
        // send('accelerometer', data);
      },
      error => {
        console.warn(error);
      },
    );

    // const subscriptionGyroscope = gyroscope.subscribe(
    //   data => send('gyroscope', data),
    //   error => {
    //     console.warn(error);
    //   },
    // );
    //
    // const subscriptionMagnetometer = magnetometer.subscribe(
    //   data => send('magnetometer', data),
    //   error => {
    //     console.warn(error);
    //   },
    // );

    return () => {
      subscriptionAccelerometer.unsubscribe();
      // subscriptionGyroscope.unsubscribe();
      // subscriptionMagnetometer.unsubscribe();
    };
  }, []);

  return null;
};

export default PhoneSensors;
