import RNShake from 'react-native-shake';
import {useContext, useEffect, useRef} from 'react';
import {SmartBoxContext} from './SmartBoxProvider';
import {AppState} from 'react-native';

const ShakeEventListener = () => {
  const smartBox = useContext(SmartBoxContext);
  const shakeTimeoutSubscriber = useRef(null);
  const isShaking = useRef(false);

  const setStopShakeTimeout = () =>
    setTimeout(() => {
      smartBox.sendMessage('stopShake');
      isShaking.current = false;
    }, 500);

  const addShakeListener = () => {
    RNShake.addEventListener('ShakeEvent', () => {
      if (isShaking.current) {
        clearTimeout(shakeTimeoutSubscriber.current);
        shakeTimeoutSubscriber.current = setStopShakeTimeout();
      } else {
        smartBox.sendMessage('shake');
        shakeTimeoutSubscriber.current = setStopShakeTimeout();
        isShaking.current = true;
      }
    });
  };

  const removeShakeListener = () => {
    RNShake.removeEventListener('ShakeEvent');
  };

  const handleAppStateChange = nextAppState => {
    if (nextAppState === 'active') {
      addShakeListener();
    } else {
      removeShakeListener();
    }
  };

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);
    addShakeListener();

    return () => {
      removeShakeListener();
    };
  }, []);

  return null;
};

export default ShakeEventListener;
