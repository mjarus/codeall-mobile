import {syncUniqueId} from 'react-native-device-info';

export default () => {
  return syncUniqueId();
};
