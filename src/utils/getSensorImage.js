const DEFAULT = 'base';

const images = {
  base: require('../assets/img/sensors/base.png'),
  smartbox: require('../assets/img/sensors/smartbox.png'),
  accelerometer: require('../assets/img/sensors/accelerometer.png'),
  buttons: require('../assets/img/sensors/buttons.png'),
  distance: require('../assets/img/sensors/distance.png'),
  gyroscope: require('../assets/img/sensors/gyroscope.png'),
  light: require('../assets/img/sensors/light.png'),
  microphone: require('../assets/img/sensors/microphone.png'),
  slider: require('../assets/img/sensors/slider.png'),
  temperature: require('../assets/img/sensors/temperature.png'),
  vibration: require('../assets/img/sensors/vibration.png'),
  humidity: require('../assets/img/sensors/humidity.png'),
  stepperMotor: require('../assets/img/sensors/stepperMotor.png'),
  rgbwLight: require('../assets/img/sensors/rgbwLight.png'),
  speaker: require('../assets/img/sensors/speaker.png'),
  door: require('../assets/img/sensors/door.png'),
};

export default function getSensorImage(name) {
  if (!images[name]) {
    return images[DEFAULT];
  }

  return images[name];
}
