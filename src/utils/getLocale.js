import * as RNLocalize from 'react-native-localize';

const DEFAULT_LOCALE = 'en';

const getLocale = () => {
  const locales = RNLocalize.getLocales();
  return locales[0] ? locales[0].languageCode : DEFAULT_LOCALE;
};

export default getLocale;
