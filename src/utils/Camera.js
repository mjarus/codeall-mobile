import React, {useContext, useEffect, useRef} from 'react';
import {RNCamera} from 'react-native-camera';
import {Modal, View} from 'react-native';
import {PermissionsAndroid, Platform} from 'react-native';
import CameraRoll from '@react-native-community/cameraroll';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {cameraDeactivated} from '../store/phone/actions';
import {SmartBoxContext} from './SmartBoxProvider';
import {CameraType} from '../constants/cameraType';

const Camera = props => {
  const smartBox = useContext(SmartBoxContext);
  const camera = useRef(null);
  const cameraType =
    props.cameraType === 'front' ? CameraType.FRONT : CameraType.BACK;
  const flashMode =
    props.flashMode === 'on'
      ? RNCamera.Constants.FlashMode.on
      : RNCamera.Constants.FlashMode.off;

  useEffect(() => {
    smartBox.subscribe('cameraPicture', () => {
      takePicture()
        .then(uri => console.log(uri))
        .catch(err => console.warn(err));
    });

    return () => {
      smartBox.unsubscribe('cameraPicture');
    };
  }, []);

  const hasAndroidPermission = async () => {
    const permission = PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE;
    const hasPermission = await PermissionsAndroid.check(permission);
    if (hasPermission) {
      return true;
    }
    const status = await PermissionsAndroid.request(permission);
    return status === 'granted';
  };

  const takePicture = async () => {
    if (camera.current) {
      const options = {quality: 0.5, base64: false};
      const data = await camera.current.takePictureAsync(options);
      return savePicture(data.uri);
    }
  };

  const savePicture = async uri => {
    if (Platform.OS === 'android' && !(await hasAndroidPermission())) {
      return;
    }
    CameraRoll.save(uri);
  };

  return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={props.isCameraActive}
      onRequestClose={props.cameraDeactivated}>
      <View style={{flex: 1}}>
        <RNCamera
          ref={camera}
          style={{flex: 1}}
          type={cameraType}
          flashMode={flashMode}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel',
          }}
        />
      </View>
    </Modal>
  );
};

const mapStateToProps = state => ({
  cameraType: state.phone.cameraType,
  flashMode: state.phone.flashMode,
  isCameraActive: state.phone.isCameraActive,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(cameraDeactivated, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Camera);
