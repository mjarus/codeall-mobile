import React from 'react';
import {Component} from 'react';
import {DeviceEventEmitter} from 'react-native';
import {connect} from 'react-redux';
import {RNSerialport, definitions, actions} from 'react-native-serialport';
import {FLASH_MESSAGE_TYPES} from '../components/FlashMessage/messageTypes';
import {
  updateConsoleOutput,
  setFlashMessage,
  codeSentToSmartBoxSuccessfully,
} from '../store/editor/actions';
import {bindActionCreators} from 'redux';
import {
  connectableSensorsUpdated,
  connectedSensorsUpdated,
  smartboxConnected,
  smartboxDisconnected,
} from '../store/smartbox/actions';
import {cameraActivated, cameraDeactivated} from '../store/phone/actions';
import {withTranslation} from 'react-i18next';

const messProp = text => ({
  bottom: 65,
  background: '#2244ffaa',
  color: 'white',
  text,
});

const INTERFACE = '-1';
const RETURNED_DATA_TYPE = definitions.RETURNED_DATA_TYPES.HEXSTRING;
const BAUD_RATE = 115200;
const FILENAME = 'user_code.py';

// eslint-disable-next-line no-unused-vars
const PROMPT_HEX = '3E3E3E20'; // string HEX of (>>> )
const SOH_HEX = '01'; // string HEX of ASCII SOH 0x01
// eslint-disable-next-line no-unused-vars
const STX_HEX = '02'; // string HEX of ASCII STX 0x02
// eslint-disable-next-line no-unused-vars
const ETX_HEX = '03'; // string HEX of ASCII ETX 0x03
const EOT_HEX = '04'; // string HEX of ASCII EOH 0x04
const SOH = RNSerialport.hexToUtf16(SOH_HEX);
const EOT = RNSerialport.hexToUtf16(EOT_HEX);

export const SmartBoxContext = React.createContext();

class SmartBoxProvider extends Component {
  constructor(props) {
    super(props);
    this.packet = '';
    this.handlers = {};
    this.sendingMessagesEnabled = false;
    this.serviceStarted = false;
    this.usbAttached = false;
  }

  componentDidMount = () => {
    this.startUsbListener();
    this.subscribe('enableMessages', () => {
      setTimeout(() => (this.sendingMessagesEnabled = true), 1000);
    });
    this.subscribe('smartbox', payload => {
      this.props.smartboxConnected(payload);
    });
    this.subscribe('popup', payload => {
      this.props.setFlashMessage(messProp(`${payload}`));
    });
    this.subscribe('connectableBoards', payload => {
      this.props.connectableSensorsUpdated(payload);
    });
    this.subscribe('connectedBoards', payload => {
      this.props.connectedSensorsUpdated(payload);
    });
    this.subscribe('cameraState', payload => {
      if (payload.state) {
        this.props.cameraActivated(payload.type);
      } else {
        this.props.cameraDeactivated();
      }
    });
  };

  componentWillUnmount = () => {
    this.stopUsbListener();
  };

  startUsbListener = () => {
    DeviceEventEmitter.addListener(
      actions.ON_SERVICE_STARTED,
      this.onServiceStarted,
      this,
    );
    DeviceEventEmitter.addListener(
      actions.ON_SERVICE_STOPPED,
      this.onServiceStopped,
      this,
    );
    DeviceEventEmitter.addListener(
      actions.ON_DEVICE_ATTACHED,
      this.onDeviceAttached,
      this,
    );
    DeviceEventEmitter.addListener(
      actions.ON_DEVICE_DETACHED,
      this.onDeviceDetached,
      this,
    );
    DeviceEventEmitter.addListener(
      actions.ON_CONNECTED,
      this.onConnected,
      this,
    );
    DeviceEventEmitter.addListener(
      actions.ON_DISCONNECTED,
      this.onDisconnected,
      this,
    );
    DeviceEventEmitter.addListener(actions.ON_READ_DATA, this.onReadData, this);
    DeviceEventEmitter.addListener(actions.ON_ERROR, this.onError, this);
    RNSerialport.setReturnedDataType(RETURNED_DATA_TYPE);
    RNSerialport.setAutoConnectBaudRate(BAUD_RATE);
    RNSerialport.setInterface(parseInt(INTERFACE, 10));
    RNSerialport.setAutoConnect(true);
    RNSerialport.startUsbService();
  };

  stopUsbListener = () => {
    DeviceEventEmitter.removeAllListeners();
    RNSerialport.isOpen().then(isOpen => {
      if (isOpen) {
        RNSerialport.disconnect();
      }
      RNSerialport.stopUsbService();
    });
  };

  onServiceStarted = response => {
    this.serviceStarted = true;
    if (response.deviceAttached) {
      this.onDeviceAttached();
    }
  };

  onServiceStopped = () => {
    this.serviceStarted = false;
  };

  onDeviceAttached = () => {
    this.serviceStarted = true;
  };

  onDeviceDetached = () => {
    this.serviceStarted = false;
  };

  onConnected = () => {
    this.props.smartboxConnected(true);
    this.props.setFlashMessage(
      messProp(this.props.t('smartboxConnectionManager.connected')),
    );
    this.packet = '';
    // RNSerialport.softReset();
  };

  onDisconnected = () => {
    this.props.setFlashMessage(
      messProp(this.props.t('smartboxConnectionManager.disconnected')),
    );
    this.props.smartboxDisconnected();
  };

  onError = error => {
    this.props.setFlashMessage({
      text: `SmartBox error: ${t(`smartbox.errors.${error.code}`)}`,
      type: FLASH_MESSAGE_TYPES.SMART_BOX_ERROR,
    });
  };

  onReadData = data => {
    let chunk = RNSerialport.hexToUtf16(data.payload);

    while (chunk.length > 0) {
      const SOH_pos = chunk.indexOf(SOH);
      const EOT_pos = chunk.indexOf(EOT);
      // const PROMPT_pos = chunk.indexOf(PROMPT_STR);

      const isPacketStarted = this.packet.length;

      if (isPacketStarted) {
        if (EOT_pos > -1) {
          this.packet += chunk.substring(0, EOT_pos + 1);
          chunk = chunk.substring(EOT_pos + 1);
        } else {
          this.packet += chunk;
          chunk = '';
        }
      } else {
        if (SOH_pos > -1) {
          this.props.updateConsoleOutput(chunk.substring(0, SOH_pos));

          if (EOT_pos > -1) {
            this.packet = chunk.substring(SOH_pos, EOT_pos + 1);
            chunk = chunk.substring(EOT_pos + 1);
          } else {
            this.packet = chunk.substring(SOH_pos);
            chunk = '';
          }
        } else {
          this.props.updateConsoleOutput(chunk);
          chunk = '';
        }
      }

      if (this.packet.endsWith(EOT)) {
        const payload = this.packet.substring(1, this.packet.length - 1);
        this.packet = '';
        if (payload.length) {
          try {
            const message = JSON.parse(payload);
            this.callSubscribedCallback(message.type, message.payload);
          } catch (e) {
            console.warn('Error parsing json message', e);
          }
        }
        // if (PROMPT_pos > -1) {
        //   this.props.updateConsoleOutput(PROMPT_STR);
        // }
      }
    }
  };

  subscribe = (name, callback) => {
    this.handlers[name] = callback;
  };

  unsubscribe = name => {
    if (this.handlers.hasOwnProperty(name)) {
      delete this.handlers.name;
    }
  };

  callSubscribedCallback = (name, payload) => {
    const handler = this.handlers[name];
    if (typeof handler === 'function') {
      handler(payload);
    } else {
      console.log(`No handler for ${name}`);
    }
  };

  write = text => {
    text = text + '\r\n';
    RNSerialport.writeString(text);
  };

  sendMessage = (type, payload) => {
    // First we need to disable sending messages and then send 'stop' message.
    // That's why there is this exclusion of "stop"
    if (!this.sendingMessagesEnabled && type !== 'stop') {
      return;
    }

    const message = {type};
    if (payload !== undefined && payload !== null) {
      message.payload = payload;
    }
    const json = JSON.stringify(message);
    this.write(json);
  };

  handleData = data => {
    try {
      const payloadJSON = JSON.parse(data);
      console.log('JSON: ', payloadJSON);
      const body = payloadJSON.body;
      switch (payloadJSON.type) {
        case 'temperature':
          this.props.setFlashMessage({
            text: `Temperatura: ${body.value}`,
            type: FLASH_MESSAGE_TYPES.SMART_BOX_INFO,
          });
          break;

        case 'smartbox':
          this.props.smartboxConnected(body);
          break;

        case 'connectableBoards':
          this.props.connectableSensorsUpdated(body);
          break;

        case 'connectedBoards':
          this.props.connectedSensorsUpdated(body);
          break;

        default:
          this.props.setFlashMessage({
            text: `${payloadJSON.type}: ${body.value}`,
            type: FLASH_MESSAGE_TYPES.SMART_BOX_INFO,
          });
          break;
      }
    } catch (e) {
      console.warn(e);
    }
  };

  sendFile = content => {
    // TODO 'content' should be utf-8 bytes. If there is a problem with upload check this first.
    const length = content.length;
    const command = `_save_file("${FILENAME}",${length})`;

    this.sendingMessagesEnabled = false;
    this.sendMessage('stop');
    RNSerialport.enterRawRepl();
    RNSerialport.sendRawData(command);
    RNSerialport.sendRawData(content, false);
    RNSerialport.exitRawRepl();
    RNSerialport.softReset();

    this.props.codeSentToSmartBoxSuccessfully();
    setFlashMessage(
      messProp(this.props.t('smartboxConnectionManager.codeSentToSmartbox')),
    );
  };

  connectNewBoard = id => {
    // TODO this will no longer work
    console.log('Connect to ', id);
    this.write(`connect_new_board('${id}')`);
  };

  runUserCode = () => {
    this.write('run_user_code()');
  };

  render() {
    return (
      <SmartBoxContext.Provider
        value={{
          sendMessage: this.sendMessage,
          sendFile: this.sendFile,
          connectNewBoard: this.connectNewBoard,
          runUserCode: this.runUserCode,
          subscribe: this.subscribe,
          unsubscribe: this.unsubscribe,
        }}>
        {this.props.children}
      </SmartBoxContext.Provider>
    );
  }
}

const mapStateToProps = state => ({
  sendCodeToSmartboxPending: state.editor.sendCodeToSmartboxPending,
  smartboxCode: state.editor.smartboxCode,
  smartboxCommand: state.editor.smartboxCommand,
  consoleOutput: state.editor.consoleOutput,
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(
    {
      setFlashMessage,
      updateConsoleOutput,
      codeSentToSmartBoxSuccessfully,
      smartboxConnected,
      smartboxDisconnected,
      connectableSensorsUpdated,
      connectedSensorsUpdated,
      cameraActivated,
      cameraDeactivated,
    },
    dispatch,
  );

export default withTranslation()(
  connect(
    mapStateToProps,
    mapDispatchToProps,
  )(SmartBoxProvider),
);
