import {useContext, useEffect, useRef} from 'react';
import {SmartBoxContext} from './SmartBoxProvider';
import {AppState} from 'react-native';
const RNSound = require('react-native-sound');
RNSound.setCategory('Playback');

const SoundPlayer = () => {
  const smartBox = useContext(SmartBoxContext);
  const sound = useRef(new RNSound('sample_sound.mp3', RNSound.MAIN_BUNDLE));

  const playInLoop = () => {
    try {
      if (!sound.current.isPlaying()) {
        sound.current.setNumberOfLoops(-1);
        sound.current.play();
      }
    } catch (e) {
      console.log('cannot play the sound file', e);
    }
  };

  const stop = () => {
    sound.current.stop();
  };

  const pause = () => {
    sound.current.pause();
  };

  const handleAppStateChange = nextAppState => {
    if (nextAppState !== 'active') {
      stop();
    }
  };

  useEffect(() => {
    AppState.addEventListener('change', handleAppStateChange);
    smartBox.subscribe('playSound', playInLoop);
    smartBox.subscribe('stopSound', stop);
    smartBox.subscribe('pauseSound', pause);

    return () => {
      smartBox.unsubscribe('playSound');
      smartBox.unsubscribe('stopSound');
      smartBox.unsubscribe('pauseSound');
      stop();
      sound.current.release();
    };
  }, []);

  return null;
};

export default SoundPlayer;
