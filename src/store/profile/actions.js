import {SET_REQUEST_ID} from '../auth/actionTypes';
import {
  GET_PROFILE_PENDING,
  GET_PROFILE_REJECTED,
  GET_PROFILE_FULFILLED,
  PUT_PROFILE_INFO_PENDING,
  PUT_PROFILE_INFO_REJECTED,
  PUT_PROFILE_INFO_FULFILLED,
  PUT_PROFILE_AVATAR,
} from './actionTypes';
import api from '../../api';

export function getProfile({userId}) {
  return dispatch => {
    dispatch({type: GET_PROFILE_PENDING});
    return api
      .getUserProfile(userId)
      .then(data =>
        dispatch({
          type: GET_PROFILE_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_PROFILE_REJECTED,
          payload: {error: error.message},
        }),
      );
  };
}

export function putProfileInfo({userId, name, surname, nick}) {
  return (dispatch, getState) => {
    dispatch({type: SET_REQUEST_ID});
    dispatch({type: PUT_PROFILE_INFO_PENDING});
    return api
      .updateUserProfile(userId, name, surname, nick)
      .then(data =>
        dispatch({
          type: PUT_PROFILE_INFO_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: PUT_PROFILE_INFO_REJECTED,
          payload: {error: error.message},
        }),
      );
  };
}

export const putProfileAvatar = avatarUri => {
  return dispatch => {
    dispatch({
      type: PUT_PROFILE_AVATAR,
      payload: {avatarUri},
    });
  };
};
