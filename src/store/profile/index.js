import {
  GET_PROFILE_PENDING,
  GET_PROFILE_REJECTED,
  GET_PROFILE_FULFILLED,
  PUT_PROFILE_AVATAR,
} from './actionTypes';

const initialState = {
  isInitializing: true,
  isPending: false,
  error: null,
  name: null,
  surname: null,
  nick: null,
  email: null,
  intentPoints: null,
  newIntentsCount: null,
  rankingPosition: null,
  isCurrentUser: false,
  status: 1,
  user: null,
  avatarUri: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_PROFILE_PENDING:
      return {
        ...state,
        isPending: true,
      };

    case GET_PROFILE_REJECTED:
      return {
        ...state,
        isPending: false,
        isInitializing: false,
        error: action.payload.error,
      };

    case GET_PROFILE_FULFILLED:
      return {
        ...state,
        isPending: false,
        isInitializing: false,
        user: action.payload.data,
        name: action.payload.data.name,
        surname: action.payload.data.surname,
        nick: action.payload.data.nick,
        email: action.payload.data.email,
        intentPoints: action.payload.data.intentPoints,
        newIntentsCount: action.payload.data.newIntentsCount,
        rankingPosition: action.payload.data.rankingPosition,
        isCurrentUser: action.payload.data.isCurrentUser,
        status: action.payload.data.status,
      };

    case PUT_PROFILE_AVATAR:
      return {
        ...state,
        avatarUri: action.payload.avatarUri,
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
