import {
  USERS_RANKING_PENDING,
  USERS_RANKING_REJECTED,
  USERS_RANKING_FULFILLED,
} from './actionTypes';
import api from '../../api';

export function getUsersRanking() {
  return dispatch => {
    dispatch({type: USERS_RANKING_PENDING});
    return api
      .getRanking()
      .then(data =>
        dispatch({
          type: USERS_RANKING_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: USERS_RANKING_REJECTED,
          payload: {error},
        }),
      );
  };
}
