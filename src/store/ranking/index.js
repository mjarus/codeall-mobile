import {
  USERS_RANKING_PENDING,
  USERS_RANKING_REJECTED,
  USERS_RANKING_FULFILLED,
} from './actionTypes';

const initialState = {
  isPending: false,
  error: null,
  ranking: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case USERS_RANKING_PENDING:
      return {
        ...state,
        isPending: true,
      };

    case USERS_RANKING_REJECTED:
      return {
        ...state,
        isPending: false,
        error: action.payload.error,
      };

    case USERS_RANKING_FULFILLED:
      return {
        ...state,
        isPending: false,
        ranking: action.payload.data,
      };

    default:
      return state;
  }
};
