import {
  GET_CONVERSATION_HISTORY_PENDING,
  GET_CONVERSATION_HISTORY_REJECTED,
  GET_CONVERSATION_HISTORY_FULFILLED,
  POST_CONVERSATION_PENDING,
  POST_CONVERSATION_REJECTED,
  POST_CONVERSATION_FULFILLED,
  POST_INTENTS_PENDING,
  POST_INTENTS_REJECTED,
  POST_INTENTS_FULFILLED,
  ADD_MESSAGE_TO_CONVERSATION,
  ADD_INTENT_FIELD,
  POST_CHAT_BUBBLE_PENDING,
  POST_CHAT_BUBBLE_REJECTED,
  POST_CHAT_BUBBLE_FULFILLED,
  GET_LESSONS_DETAILS_PENDING,
  GET_LESSONS_DETAILS_REJECTED,
  GET_LESSONS_DETAILS_FULFILLED,
  GET_STEP_FULFILLED,
  GET_STEP_REJECTED,
} from './actionTypes';
import api from '../../api';

export function getConversationHistory({page, MESSAGES_PER_PAGE}) {
  return dispatch => {
    dispatch({type: GET_CONVERSATION_HISTORY_PENDING});
    return api
      .getChatbotHistory(page)
      .then(data => {
        return dispatch({
          type: GET_CONVERSATION_HISTORY_FULFILLED,
          payload: {data, MESSAGES_PER_PAGE},
        });
      })
      .catch(error => {
        return dispatch({
          type: GET_CONVERSATION_HISTORY_REJECTED,
          payload: {error},
        });
      });
  };
}

export function postConversation({question, learningSetName}) {
  return dispatch => {
    dispatch({type: POST_CONVERSATION_PENDING});
    return api
      .sendChatbotMessage(question, learningSetName)
      .then(data =>
        dispatch({
          type: POST_CONVERSATION_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: POST_CONVERSATION_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function postIntents({question, answer, context, id, newValue}) {
  return dispatch => {
    dispatch({type: POST_INTENTS_PENDING});
    return api
      .sendIntentProposal(question, answer, context)
      .then(() =>
        dispatch({
          type: POST_INTENTS_FULFILLED,
          payload: {id, newValue},
        }),
      )
      .catch(error =>
        dispatch({
          type: POST_INTENTS_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function addMessageToConversation(args) {
  return dispatch => {
    dispatch({
      type: ADD_MESSAGE_TO_CONVERSATION,
      payload: {args},
    });
  };
}

export function addIntentField({id}) {
  return dispatch => {
    dispatch({
      type: ADD_INTENT_FIELD,
      payload: {id},
    });
  };
}

export function getStep({stepId}) {
  return dispatch => {
    dispatch({type: GET_LESSONS_DETAILS_PENDING});
    return api
      .getStep(stepId)
      .then(step =>
        dispatch({
          type: GET_STEP_FULFILLED,
          payload: {step},
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_STEP_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function postChatBubbles({chatBubbleId, answerId, openQuestionAnswer}) {
  return dispatch => {
    dispatch({type: POST_CHAT_BUBBLE_PENDING});
    return api
      .markChatBubbleCompleted(chatBubbleId, answerId, openQuestionAnswer)
      .then(data =>
        dispatch({
          type: POST_CHAT_BUBBLE_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: POST_CHAT_BUBBLE_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function getLessonsDetails({unitId, lessonId}) {
  return dispatch => {
    dispatch({type: GET_LESSONS_DETAILS_PENDING});
    return api
      .getLesson(unitId, lessonId)
      .then(data =>
        dispatch({
          type: GET_LESSONS_DETAILS_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_LESSONS_DETAILS_REJECTED,
          payload: {error},
        }),
      );
  };
}
