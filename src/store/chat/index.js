import {uniqBy, sortBy} from 'lodash';
import {
  GET_CONVERSATION_HISTORY_PENDING,
  GET_CONVERSATION_HISTORY_REJECTED,
  GET_CONVERSATION_HISTORY_FULFILLED,
  POST_CONVERSATION_PENDING,
  POST_CONVERSATION_REJECTED,
  POST_CONVERSATION_FULFILLED,
  POST_INTENTS_PENDING,
  POST_INTENTS_REJECTED,
  POST_INTENTS_FULFILLED,
  ADD_MESSAGE_TO_CONVERSATION,
  ADD_INTENT_FIELD,
  POST_CHAT_BUBBLE_PENDING,
  POST_CHAT_BUBBLE_REJECTED,
  POST_CHAT_BUBBLE_FULFILLED,
  GET_LESSONS_DETAILS_PENDING,
  GET_LESSONS_DETAILS_REJECTED,
  GET_LESSONS_DETAILS_FULFILLED,
  GET_STEP_FULFILLED,
  GET_STEP_REJECTED,
} from './actionTypes';

const initialState = {
  error: null,
  historyPending: false,
  intentsPending: false,
  lessonsPending: false,
  chatBubbelsPending: false,
  conversationPending: false,
  endOfChat: false,
  conversation: [],
  currentLesson: [],
  currentStep: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_CONVERSATION_HISTORY_PENDING:
      return {
        ...state,
        historyPending: true,
      };

    case POST_CONVERSATION_PENDING:
      return {
        ...state,
        conversationPending: true,
      };

    case GET_LESSONS_DETAILS_PENDING:
      return {
        ...state,
        lessonsPending: true,
      };

    case POST_CHAT_BUBBLE_PENDING:
      return {
        ...state,
        chatBubbelsPending: true,
      };

    case POST_INTENTS_PENDING:
      return {
        ...state,
        intentsPending: true,
      };

    case GET_CONVERSATION_HISTORY_REJECTED:
      return {
        ...state,
        historyPending: false,
        error: action.payload.error,
      };

    case POST_CONVERSATION_REJECTED:
      return {
        ...state,
        conversationPending: false,
        error: action.payload.error,
      };

    case GET_LESSONS_DETAILS_REJECTED:
      return {
        ...state,
        lessonsPending: false,
        error: action.payload.error,
      };

    case POST_CHAT_BUBBLE_REJECTED:
      return {
        ...state,
        chatBubbelsPending: false,
        error: action.payload.error,
      };

    case POST_INTENTS_REJECTED:
      return {
        ...state,
        intentsPending: false,
        error: action.payload.error,
      };

    case GET_CONVERSATION_HISTORY_FULFILLED:
      return {
        ...state,
        historyPending: false,
        endOfChat:
          action.payload.data.length < action.payload.MESSAGES_PER_PAGE,
        conversation: sortBy(
          uniqBy([...state.conversation, ...action.payload.data], 'id'),
          'id',
        ).reverse(),
      };

    case POST_CONVERSATION_FULFILLED:
      return {
        ...state,
        conversationPending: false,
      };

    case POST_INTENTS_FULFILLED:
      return {
        ...state,
        intentsPending: false,
        conversation: state.conversation.map(item => {
          if (item.id === action.payload.id) {
            return {
              ...item,
              content: action.payload.newValue,
              fieldState: 'state-2',
            };
          } else {
            return item;
          }
        }),
      };

    case ADD_MESSAGE_TO_CONVERSATION:
      return {
        ...state,
        conversation: [
          {
            id:
              Math.max(...state.conversation.map(item => item.id || 0), 0) + 10,
            createdAt: Math.floor(new Date().getTime() / 1000),
            createdAtText: '',
            ...action.payload.args,
          },
          ...state.conversation,
        ],
      };

    case ADD_INTENT_FIELD:
      return {
        ...state,
        conversation: state.conversation.map(item => {
          if (item.id === action.payload.id) {
            return {
              ...item,
              fieldState: 'state-1',
            };
          }
          return item;
        }),
      };

    case GET_LESSONS_DETAILS_FULFILLED:
      const length =
        action.payload.data.chatBubbles &&
        action.payload.data.chatBubbles.length;
      return {
        ...state,
        lessonsPending: false,
        currentLesson: length ? action.payload.data.chatBubbles.reverse() : [],
      };

    case POST_CHAT_BUBBLE_FULFILLED:
      const nextChatBubble = action.payload.data.nextChatBubble;
      const finishedChatBubble = action.payload.data.finishedChatBubble;
      const additionalBubbles = nextChatBubble
        ? [nextChatBubble, finishedChatBubble]
        : [finishedChatBubble];
      const currentLesson = state.currentLesson.slice(
        1,
        state.currentLesson.length,
      );
      const currentLessonFinished = [...additionalBubbles, ...currentLesson];
      return {
        ...state,
        chatBubbelsPending: false,
        currentLesson: currentLessonFinished,
      };

    case GET_STEP_FULFILLED:
      return {
        ...state,
        currentStep: action.payload.step,
      };

    // todo refactor...
    case GET_STEP_REJECTED:
      return {
        ...state,
        currentStep: null,
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
