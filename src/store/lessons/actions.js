import {
  GET_LESSONS_PENDING,
  GET_LESSONS_REJECTED,
  GET_LESSONS_FULFILLED,
  SET_CURRENT_UNIT_ID,
} from './actionTypes';
import api from '../../api';

export function getLessons() {
  return dispatch => {
    dispatch({type: GET_LESSONS_PENDING});
    return api
      .getUnits()
      .then(data =>
        dispatch({
          type: GET_LESSONS_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_LESSONS_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function setCurrentUnitId({el}) {
  return dispatch => {
    dispatch({type: SET_CURRENT_UNIT_ID, payload: {el}});
  };
}
