import {
  GET_LESSONS_PENDING,
  GET_LESSONS_REJECTED,
  GET_LESSONS_FULFILLED,
  SET_CURRENT_UNIT_ID,
  GET_CURRENT_STEP_FULLFIELD,
  GET_CURRENT_STEP_PENDING,
  GET_CURRENT_STEP_REJECTED,
} from './actionTypes';

const initialState = {
  error: null,
  isPending: false,
  isCurrentStepPending: false,
  units: [],
  currentStep: null,
  currentUnitId: null,
  isInitializing: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_LESSONS_PENDING:
      return {
        ...state,
        isPending: true,
      };

    case GET_LESSONS_REJECTED:
      return {
        ...state,
        isPending: false,
        error: action.payload.error,
        isInitializing: false,
      };

    case GET_LESSONS_FULFILLED:
      return {
        ...state,
        isPending: false,
        units: action.payload.data,
        currentUnitId: action.payload.data[0].id,
        isInitializing: false,
      };

    case SET_CURRENT_UNIT_ID:
      return {
        ...state,
        currentUnitId: action.payload.el,
      };

    case GET_CURRENT_STEP_REJECTED:
      return {
        ...state,
        isCurrentStepPending: false,
        error: action.payload.error,
      };

    case GET_CURRENT_STEP_FULLFIELD:
      return {
        ...state,
        isCurrentStepPending: false,
        currentStep: action.payload.data,
      };

    case GET_CURRENT_STEP_PENDING:
      return {
        ...state,
        isCurrentStepPending: true,
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
