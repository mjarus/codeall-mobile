import {
  GET_SENSORS_PENDING,
  GET_SENSORS_REJECTED,
  GET_SENSORS_FULFILLED,
} from './actionTypes';

const initialState = {
  sensors: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case GET_SENSORS_PENDING:
      return {
        ...state,
        isPending: true,
      };

    case GET_SENSORS_REJECTED:
      return {
        ...state,
        isPending: false,
        error: action.payload.error,
      };

    case GET_SENSORS_FULFILLED:
      return {
        ...state,
        isPending: false,
        sensors: action.payload.data,
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
