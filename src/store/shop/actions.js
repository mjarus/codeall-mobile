import {
  GET_SENSORS_PENDING,
  GET_SENSORS_REJECTED,
  GET_SENSORS_FULFILLED,
} from './actionTypes';
import api from '../../api';

export function getSensors() {
  return dispatch => {
    dispatch({type: GET_SENSORS_PENDING});
    return api
      .getProducts()
      .then(data =>
        dispatch({
          type: GET_SENSORS_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: GET_SENSORS_REJECTED,
          payload: {error: error.message},
        }),
      );
  };
}
