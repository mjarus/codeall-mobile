import {createStore, combineReducers, applyMiddleware} from 'redux';
import {createLogger} from 'redux-logger';
import {persistStore, persistReducer} from 'redux-persist';
import {composeWithDevTools} from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import AsyncStorage from '@react-native-async-storage/async-storage';
import {default as authReducer} from './auth';
import {default as profileReducer} from './profile';
import {default as chatReducer} from './chat';
import {default as rankingReducer} from './ranking';
import {default as lessonsReducer} from './lessons';
import {default as shopReducer} from './shop';
import {default as editorReducer} from './editor';
import {default as smartboxReducer} from './smartbox';
import {default as phoneReducer} from './phone';

const logger = createLogger({
  collapsed: true,
  duration: true,
});

const authPersistConfig = {
  key: 'auth',
  storage: AsyncStorage,
  whitelist: ['token', 'userId', 'login'],
};

const rootReducer = combineReducers({
  auth: persistReducer(authPersistConfig, authReducer),
  profile: profileReducer,
  ranking: rankingReducer,
  chat: chatReducer,
  lessons: lessonsReducer,
  shop: shopReducer,
  editor: editorReducer,
  smartbox: smartboxReducer,
  phone: phoneReducer,
});

const initialState = {};

const store = createStore(
  rootReducer,
  initialState,
  composeWithDevTools(applyMiddleware(thunk, logger)),
);

const persistor = persistStore(store);

export {store, persistor};
