import {CAMERA_ACTIVATED, CAMERA_DEACTIVATED} from './actionTypes';

export const cameraActivated = (cameraType, flashMode) => ({
  type: CAMERA_ACTIVATED,
  payload: {cameraType, flashMode},
});

export const cameraDeactivated = () => ({
  type: CAMERA_DEACTIVATED,
});
