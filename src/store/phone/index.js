import {CAMERA_ACTIVATED, CAMERA_DEACTIVATED} from './actionTypes';
import {CameraType} from '../../constants/cameraType';

const initialState = {
  isCameraActive: false,
  cameraType: CameraType.BACK,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case CAMERA_ACTIVATED:
      return {
        ...state,
        isCameraActive: true,
        cameraType: action.payload.cameraType,
        flashMode: action.payload.flashMode,
      };

    case CAMERA_DEACTIVATED:
      return {
        ...state,
        isCameraActive: false,
      };

    case 'CLEAR_STATE': // todo refactor
      return initialState;

    default:
      return state;
  }
};
