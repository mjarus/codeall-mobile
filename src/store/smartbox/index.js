import {
  CONNECTABLE_SENSORS_UPDATED,
  CONNECTED_SENSORS_UPDATED,
  SENSOR_CONNECTED,
  SMARTBOX_CONNECTED,
  SMARTBOX_DISCONNECTED,
} from './actionTypes';

const connectedSensorsMock = {
  XABC6271: {
    id: 'XABC6271',
    type: 'temperature',
    name: 'Temperatura 1',
    batteryLevel: 5,
    currentReading: 31.2,
    frequency: 30,
    isAvailable: true,
    technicalData: {
      powerSupply: 'CR123A battery',
      lifespan: '8 years',
      accuracy: '+/- 1 C',
      temperature: '5-40 C',
      alarm: '30dB',
      dimensions: '25x25mm',
    },
  },
  KSMC8291: {
    id: 'KSMC8291',
    type: 'base',
    name: 'Baza 1',
    batteryLevel: 100,
    currentReading: null,
    isAvailable: false,
    technicalData: {
      powerSupply: 'CR123A battery',
      lifespan: '8 years',
      accuracy: '',
      temperature: '5-40 C',
      alarm: '30dB',
      dimensions: '25x25mm',
    },
  },
};

const newSensorsMock = {
  KWAO284: {
    id: 'KWAO284',
    type: 'base',
    name: 'Baza',
    batteryLevel: 76,
    isConnected: true,
    technicalData: {
      powerSupply: 'CR123A battery',
      lifespan: '8 years',
      accuracy: '+/- 1 C',
      temperature: '5-40 C',
      alarm: '30dB',
      dimensions: '25x25mm',
    },
  },
  ZKVC3102: {
    id: 'ZKVC3102',
    type: 'base',
    name: 'Baza',
    batteryLevel: 98,
    isConnected: true,
    technicalData: {
      powerSupply: 'CR123A battery',
      lifespan: '8 years',
      accuracy: '',
      temperature: '5-40 C',
      alarm: '30dB',
      dimensions: '25x25mm',
    },
  },
  HHCX3922: {
    id: 'HHCX3922',
    type: 'base',
    name: 'Baza',
    batteryLevel: 98,
    isConnected: false,
    technicalData: {
      powerSupply: 'CR123A battery',
      lifespan: '8 years',
      accuracy: '',
      temperature: '5-40 C',
      alarm: '30dB',
      dimensions: '25x25mm',
    },
  },
};

const smartboxMock = {
  // type: 'smartbox',
  // name: 'SmartBox',
  id: 'ECG2521',
  smartboxVersion: '1.0',
  micropythonVersion: '2.7.4',
};

const initialState = {
  smartbox: null,
  connectedSensors: {},
  newSensors: {},
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SMARTBOX_CONNECTED:
      return {
        ...state,
        smartbox: action.payload.smartbox,
      };

    case SMARTBOX_DISCONNECTED:
      return {
        ...state,
        smartbox: initialState.smartbox,
        connectedSensors: initialState.connectedSensors,
        newSensors: initialState.newSensors,
      };

    case CONNECTABLE_SENSORS_UPDATED:
      return {
        ...state,
        newSensors: action.payload.sensors,
      };

    case CONNECTED_SENSORS_UPDATED:
      return {
        ...state,
        connectedSensors: action.payload.sensors,
      };

    case SENSOR_CONNECTED:
      const id = action.payload.sensor.id;
      return {
        ...state,
        connectedSensors: {
          ...state.connectedSensors,
          [id]: action.payload.sensor,
        },
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
