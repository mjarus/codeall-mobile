import {
  CONNECTABLE_SENSORS_UPDATED,
  CONNECTED_SENSORS_UPDATED,
  SENSOR_CONNECTED,
  SMARTBOX_CONNECTED,
  SMARTBOX_DISCONNECTED,
} from './actionTypes';

export const smartboxConnected = smartbox => ({
  type: SMARTBOX_CONNECTED,
  payload: {
    smartbox,
  },
});

export const smartboxDisconnected = () => ({
  type: SMARTBOX_DISCONNECTED,
});

export const connectableSensorsUpdated = sensors => ({
  type: CONNECTABLE_SENSORS_UPDATED,
  payload: {
    sensors,
  },
});

export const connectedSensorsUpdated = sensors => ({
  type: CONNECTED_SENSORS_UPDATED,
  payload: {
    sensors,
  },
});

export const sensorConnected = sensor => ({
  type: SENSOR_CONNECTED,
  payload: {
    sensor,
  },
});
