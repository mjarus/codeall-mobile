const USER_LOGIN_PENDING = 'USER_LOGIN_PENDING';
const USER_LOGIN_REJECTED = 'USER_LOGIN_REJECTED';
const USER_LOGIN_FULFILLED = 'USER_LOGIN_FULFILLED';
const USER_REGISTER_PENDING = 'USER_REGISTER_PENDING';
const USER_REGISTER_REJECTED = 'USER_REGISTER_REJECTED';
const USER_REGISTER_FULFILLED = 'USER_REGISTER_FULFILLED';
const USER_SET_LOGIN = 'USER_SET_LOGIN';
const SET_REQUEST_ID = 'SET_REQUEST_ID';

export {
  USER_LOGIN_PENDING,
  USER_LOGIN_REJECTED,
  USER_LOGIN_FULFILLED,
  USER_REGISTER_PENDING,
  USER_REGISTER_REJECTED,
  USER_REGISTER_FULFILLED,
  USER_SET_LOGIN,
  SET_REQUEST_ID,
};
