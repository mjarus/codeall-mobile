import {
  USER_LOGIN_PENDING,
  USER_LOGIN_REJECTED,
  USER_LOGIN_FULFILLED,
  USER_REGISTER_PENDING,
  USER_REGISTER_REJECTED,
  USER_REGISTER_FULFILLED,
  USER_SET_LOGIN,
  SET_REQUEST_ID,
} from './actionTypes';

const initialState = {
  isPending: false,
  error: null,
  requireDeviceRegistrationError: false,
  requestID: null,
  token: null,
  userId: null,
  login: null,
  password: null,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_REQUEST_ID:
      return {
        ...state,
        requestID:
          Math.random().toString(36).substring(2, 15) +
          Math.random().toString(36).substring(2, 15),
      };

    case USER_LOGIN_PENDING:
    case USER_REGISTER_PENDING:
      return {
        ...state,
        isPending: true,
      };

    case USER_LOGIN_REJECTED:
    case USER_REGISTER_REJECTED:
      const requireDeviceRegistrationError =
        action.payload.error.code === '1023';
      return {
        ...state,
        isPending: false,
        error: requireDeviceRegistrationError ? null : action.payload.error, // don't show this error on the screen
        userId: initialState.userId,
        token: initialState.token,
        requireDeviceRegistrationError,
      };

    case USER_LOGIN_FULFILLED:
      return {
        ...state,
        isPending: false,
        token: action.payload.data.token,
        userId: action.payload.data.userId,
      };

    case USER_REGISTER_FULFILLED:
      return {
        ...state,
        isPending: false,
      };

    case USER_SET_LOGIN:
      return {
        ...state,
        password: action.payload.mail,
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
