import {
  USER_LOGIN_PENDING,
  USER_LOGIN_REJECTED,
  USER_LOGIN_FULFILLED,
  USER_REGISTER_PENDING,
  USER_REGISTER_REJECTED,
  USER_REGISTER_FULFILLED,
} from './actionTypes';
import getDeviceId from '../../utils/getDeviceId';
import api from '../../api';

export function userLogin({email, password}) {
  return dispatch => {
    dispatch({type: USER_LOGIN_PENDING});
    return api
      .login(email, password)
      .then(data =>
        dispatch({
          type: USER_LOGIN_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: USER_LOGIN_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function userRegister({
  name,
  nick,
  email,
  password,
  rePassword,
  type,
  scenarioToken,
}) {
  return dispatch => {
    dispatch({type: USER_REGISTER_PENDING});
    return api
      .register(name, nick, email, password, rePassword, type, scenarioToken)
      .then(data =>
        dispatch({
          type: USER_REGISTER_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: USER_REGISTER_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function userFacebookLogin({token}) {
  return dispatch => {
    dispatch({type: USER_LOGIN_PENDING});
    return api
      .facebookLogin(token)
      .then(data =>
        dispatch({
          type: USER_LOGIN_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error => {
        dispatch({
          type: USER_LOGIN_REJECTED,
          payload: {error},
        });
        throw error;
      });
  };
}

export function userDeviceLogin({nick, referralCode}) {
  return dispatch => {
    dispatch({type: USER_LOGIN_PENDING});
    nick = nick ?? '';
    referralCode = referralCode ?? '';
    return getDeviceId().then(deviceId => {
      return api
        .deviceLogin(deviceId, nick, referralCode)
        .then(data =>
          dispatch({
            type: USER_LOGIN_FULFILLED,
            payload: {data},
          }),
        )
        .catch(error => {
          dispatch({
            type: USER_LOGIN_REJECTED,
            payload: {error},
          });
          throw error;
        });
    });
  };
}
