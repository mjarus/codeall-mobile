import {endpoints} from '../../api/urls';
import {setXAuthToken} from '../../api/headers';
import {SET_REQUEST_ID} from '../auth/actionTypes';
import {
  VERIFY_SOURCE_CODE_FULFILLED,
  VERIFY_SOURCE_CODE_PENDING,
  VERIFY_SOURCE_CODE_REJECTED,
  SAVE_SOURCE_CODE_FOR_STEP_FULFILLED,
  SAVE_SOURCE_CODE_FOR_STEP_PENDING,
  SAVE_SOURCE_CODE_FOR_STEP_REJECTED,
  SET_FLASH_MESSAGE,
  SEND_CODE_TO_SMARTBOX_SUCCESS,
  UPDATE_CONSOLE_OUTPUT,
  ADD_SHARED_EDITOR_CHAT_MESSAGE,
  CLEAR_CONSOLE_OUTPUT,
} from './actionTypes';
import {
  SHARED_EDITOR_SEND_CHAT_MESSAGE_DONE,
  SHARED_EDITOR_SEND_CHAT_MESSAGE_PENDING,
} from '../chat/actionTypes';
import api from '../../api';

export function verifySourceCode({stepId, sourceCode}) {
  return dispatch => {
    dispatch({type: SET_REQUEST_ID});
    dispatch({type: VERIFY_SOURCE_CODE_PENDING});
    return api
      .verifySourceCode(stepId, sourceCode)
      .then(data =>
        dispatch({
          type: VERIFY_SOURCE_CODE_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: VERIFY_SOURCE_CODE_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function saveSourceCodeForStep({stepId, sourceCode}) {
  return dispatch => {
    dispatch({type: SET_REQUEST_ID});
    dispatch({type: SAVE_SOURCE_CODE_FOR_STEP_PENDING});
    return api
      .saveSourceCodeForStep(stepId, sourceCode)
      .then(data =>
        dispatch({
          type: SAVE_SOURCE_CODE_FOR_STEP_FULFILLED,
          payload: {data},
        }),
      )
      .catch(error =>
        dispatch({
          type: SAVE_SOURCE_CODE_FOR_STEP_REJECTED,
          payload: {error},
        }),
      );
  };
}

export function setFlashMessage({text, type}) {
  return (dispatch, getState) => {
    dispatch({
      type: SET_FLASH_MESSAGE,
      payload: {text, type},
    });
  };
}

export function updateConsoleOutput(text) {
  return {
    type: UPDATE_CONSOLE_OUTPUT,
    payload: {text},
  };
}

export function clearConsoleOutput() {
  return {
    type: CLEAR_CONSOLE_OUTPUT,
  };
}

export function codeSentToSmartBoxSuccessfully() {
  return {
    type: SEND_CODE_TO_SMARTBOX_SUCCESS,
  };
}

export function addSharedEditorChatMessage({message, isUserMessage}) {
  return (dispatch, getState) => {
    dispatch({
      type: ADD_SHARED_EDITOR_CHAT_MESSAGE,
      payload: {message, isUserMessage},
    });
  };
}

export function sendSharedEditorChatMessage(message) {
  return (dispatch, getState) => {
    dispatch({
      type: SHARED_EDITOR_SEND_CHAT_MESSAGE_PENDING,
      payload: {message},
    });
  };
}

export function sendSharedEditorChatMessageDone() {
  return (dispatch, getState) => {
    dispatch({
      type: SHARED_EDITOR_SEND_CHAT_MESSAGE_DONE,
    });
  };
}
