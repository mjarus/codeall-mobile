import {
  VERIFY_SOURCE_CODE_FULFILLED,
  VERIFY_SOURCE_CODE_REJECTED,
  VERIFY_SOURCE_CODE_PENDING,
  SAVE_SOURCE_CODE_FOR_STEP_FULFILLED,
  SAVE_SOURCE_CODE_FOR_STEP_PENDING,
  SAVE_SOURCE_CODE_FOR_STEP_REJECTED,
  SET_FLASH_MESSAGE,
  UPDATE_CONSOLE_OUTPUT,
  ADD_SHARED_EDITOR_CHAT_MESSAGE,
  CLEAR_CONSOLE_OUTPUT,
} from './actionTypes';
import {
  SHARED_EDITOR_SEND_CHAT_MESSAGE_DONE,
  SHARED_EDITOR_SEND_CHAT_MESSAGE_PENDING,
} from '../chat/actionTypes';

const initialState = {
  isVerfificationPending: false,
  saveCodeForStepPending: false,
  error: null,
  sourceCodeError: null,
  cosourceCodeCorrect: null,
  lastSavedSourceCode: null,
  lastFlashMessageTime: null,
  flashMessageText: null,
  flashMessageType: null,
  consoleOutput: '',
  sharedEditorChatMessages: [],
  sharedEditorSendChatMessagePending: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case VERIFY_SOURCE_CODE_PENDING:
      return {
        ...state,
        isVerfificationPending: true,
      };

    case SAVE_SOURCE_CODE_FOR_STEP_PENDING:
      return {
        ...state,
        saveCodeForStepPending: true,
      };

    case VERIFY_SOURCE_CODE_FULFILLED:
      return {
        ...state,
        isVerfificationPending: false,
        sourceCodeError: action.payload.data.error,
        cosourceCodeCorrect: action.payload.data.correct,
      };

    case SAVE_SOURCE_CODE_FOR_STEP_FULFILLED:
      return {
        ...state,
        saveCodeForStepPending: false,
        lastSavedSourceCode: action.payload.data.sourceCode,
      };

    case VERIFY_SOURCE_CODE_REJECTED:
      return {
        ...state,
        isVerfificationPending: false,
        error: action.payload.error,
      };

    case SAVE_SOURCE_CODE_FOR_STEP_REJECTED:
      return {
        ...state,
        saveCodeForStepPending: false,
        error: action.payload.error,
      };

    case SET_FLASH_MESSAGE:
      return {
        ...state,
        lastFlashMessageTime: new Date().getTime(),
        flashMessageText: action.payload.text,
        flashMessageType: action.payload.type,
      };

    case UPDATE_CONSOLE_OUTPUT:
      return {
        ...state,
        consoleOutput: (state.consoleOutput += action.payload.text),
      };

    case CLEAR_CONSOLE_OUTPUT:
      return {
        ...state,
        consoleOutput: initialState.consoleOutput,
      };

    case ADD_SHARED_EDITOR_CHAT_MESSAGE:
      return {
        ...state,
        sharedEditorChatMessages: [
          ...state.sharedEditorChatMessages,
          {
            content: action.payload.message,
            isUserMessage: action.payload.isUserMessage,
          },
        ],
      };

    case SHARED_EDITOR_SEND_CHAT_MESSAGE_PENDING:
      return {
        ...state,
        sharedEditorSendChatMessagePending: true,
        sharedEditorChatMessages: [
          ...state.sharedEditorChatMessages,
          {
            content: action.payload.message,
            isUserMessage: true,
          },
        ],
      };

    case SHARED_EDITOR_SEND_CHAT_MESSAGE_DONE:
      return {
        ...state,
        sharedEditorSendChatMessagePending: false,
      };

    case 'CLEAR_STATE':
      return initialState;

    default:
      return state;
  }
};
