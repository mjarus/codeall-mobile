import './i18n';
import React from 'react';
import 'react-native-gesture-handler';
import {LogBox, View} from 'react-native';
import {Provider} from 'react-redux';
import {store, persistor} from './store';
import {styles} from './styles';
import Navigation from './navigation';
import {PersistGate} from 'redux-persist/integration/react';
import SmartBoxProvider from './utils/SmartBoxProvider';
import FlashMessage from './components/FlashMessage';
import PhoneSensors from './utils/PhoneSensors';
import Camera from './utils/Camera';
import SoundPlayer from './utils/SoundPlayer';
import ShakeEventListener from './utils/ShakeEventListener';
import InternetConnectionInfo from '../src/components/InternetConnectionInfo';

import colors from './theme/colors';
import usePreviouslySelectedLanguage from './hooks/usePreviouslySelectedLanguage';
import NetworkAwareStatusBar from 'CodeallMobile/src/components/NetworkAwareStatusBar';

LogBox.ignoreLogs(['Remote debugger is in a background']);
LogBox.ignoreLogs(['VirtualizedLists']);

const App = () => {
  usePreviouslySelectedLanguage();

  return (
    <Provider store={store}>
      <PersistGate persistor={persistor}>
        <SmartBoxProvider>
          <Camera />
          <SoundPlayer />
          <PhoneSensors />
          <ShakeEventListener />
          <View style={styles.mainContainer}>
            <NetworkAwareStatusBar
              backgroundColor={colors.background}
              barStyle="dark-content"
            />
            <InternetConnectionInfo />
            <Navigation />
            <FlashMessage />
          </View>
        </SmartBoxProvider>
      </PersistGate>
    </Provider>
  );
};

export default App;
